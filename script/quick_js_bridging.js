!(function (t) {
  var e = {};
  function r(n) {
    if (e[n]) return e[n].exports;
    var i = (e[n] = { i: n, l: !1, exports: {} });
    return t[n].call(i.exports, i, i.exports, r), (i.l = !0), i.exports;
  }
  (r.m = t),
    (r.c = e),
    (r.d = function (t, e, n) {
      r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
    }),
    (r.r = function (t) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(t, "__esModule", { value: !0 });
    }),
    (r.t = function (t, e) {
      if ((1 & e && (t = r(t)), 8 & e)) return t;
      if (4 & e && "object" == typeof t && t && t.__esModule) return t;
      var n = Object.create(null);
      if (
        (r.r(n),
        Object.defineProperty(n, "default", { enumerable: !0, value: t }),
        2 & e && "string" != typeof t)
      )
        for (var i in t)
          r.d(
            n,
            i,
            function (e) {
              return t[e];
            }.bind(null, i)
          );
      return n;
    }),
    (r.n = function (t) {
      var e =
        t && t.__esModule
          ? function () {
              return t.default;
            }
          : function () {
              return t;
            };
      return r.d(e, "a", e), e;
    }),
    (r.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }),
    (r.p = ""),
    r((r.s = 335));
})([
  function (t, e, r) {
    var n = r(1),
      i = r(7),
      o = r(14),
      a = r(11),
      s = r(17),
      u = function (t, e, r) {
        var c,
          f,
          l,
          h,
          p = t & u.F,
          d = t & u.G,
          v = t & u.S,
          y = t & u.P,
          g = t & u.B,
          m = d ? n : v ? n[e] || (n[e] = {}) : (n[e] || {}).prototype,
          b = d ? i : i[e] || (i[e] = {}),
          w = b.prototype || (b.prototype = {});
        for (c in (d && (r = e), r))
          (l = ((f = !p && m && void 0 !== m[c]) ? m : r)[c]),
            (h =
              g && f
                ? s(l, n)
                : y && "function" == typeof l
                ? s(Function.call, l)
                : l),
            m && a(m, c, l, t & u.U),
            b[c] != l && o(b, c, h),
            y && w[c] != l && (w[c] = l);
      };
    (n.core = i),
      (u.F = 1),
      (u.G = 2),
      (u.S = 4),
      (u.P = 8),
      (u.B = 16),
      (u.W = 32),
      (u.U = 64),
      (u.R = 128),
      (t.exports = u);
  },
  function (t, e) {
    var r = (t.exports =
      "undefined" != typeof window && window.Math == Math
        ? window
        : "undefined" != typeof self && self.Math == Math
        ? self
        : Function("return this")());
    "number" == typeof __g && (__g = r);
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return !!t();
      } catch (t) {
        return !0;
      }
    };
  },
  function (t, e, r) {
    var n = r(4);
    t.exports = function (t) {
      if (!n(t)) throw TypeError(t + " is not an object!");
      return t;
    };
  },
  function (t, e) {
    t.exports = function (t) {
      return "object" == typeof t ? null !== t : "function" == typeof t;
    };
  },
  function (t, e, r) {
    var n = r(48)("wks"),
      i = r(29),
      o = r(1).Symbol,
      a = "function" == typeof o;
    (t.exports = function (t) {
      return n[t] || (n[t] = (a && o[t]) || (a ? o : i)("Symbol." + t));
    }).store = n;
  },
  function (t, e, r) {
    var n = r(19),
      i = Math.min;
    t.exports = function (t) {
      return t > 0 ? i(n(t), 9007199254740991) : 0;
    };
  },
  function (t, e) {
    var r = (t.exports = { version: "2.6.12" });
    "number" == typeof __e && (__e = r);
  },
  function (t, e, r) {
    t.exports = !r(2)(function () {
      return (
        7 !=
        Object.defineProperty({}, "a", {
          get: function () {
            return 7;
          },
        }).a
      );
    });
  },
  function (t, e, r) {
    var n = r(3),
      i = r(93),
      o = r(26),
      a = Object.defineProperty;
    e.f = r(8)
      ? Object.defineProperty
      : function (t, e, r) {
          if ((n(t), (e = o(e, !0)), n(r), i))
            try {
              return a(t, e, r);
            } catch (t) {}
          if ("get" in r || "set" in r)
            throw TypeError("Accessors not supported!");
          return "value" in r && (t[e] = r.value), t;
        };
  },
  function (t, e, r) {
    var n = r(24);
    t.exports = function (t) {
      return Object(n(t));
    };
  },
  function (t, e, r) {
    var n = r(1),
      i = r(14),
      o = r(13),
      a = r(29)("src"),
      s = r(136),
      u = ("" + s).split("toString");
    (r(7).inspectSource = function (t) {
      return s.call(t);
    }),
      (t.exports = function (t, e, r, s) {
        var c = "function" == typeof r;
        c && (o(r, "name") || i(r, "name", e)),
          t[e] !== r &&
            (c && (o(r, a) || i(r, a, t[e] ? "" + t[e] : u.join(String(e)))),
            t === n
              ? (t[e] = r)
              : s
              ? t[e]
                ? (t[e] = r)
                : i(t, e, r)
              : (delete t[e], i(t, e, r)));
      })(Function.prototype, "toString", function () {
        return ("function" == typeof this && this[a]) || s.call(this);
      });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(2),
      o = r(24),
      a = /"/g,
      s = function (t, e, r, n) {
        var i = String(o(t)),
          s = "<" + e;
        return (
          "" !== r &&
            (s += " " + r + '="' + String(n).replace(a, "&quot;") + '"'),
          s + ">" + i + "</" + e + ">"
        );
      };
    t.exports = function (t, e) {
      var r = {};
      (r[t] = e(s)),
        n(
          n.P +
            n.F *
              i(function () {
                var e = ""[t]('"');
                return e !== e.toLowerCase() || e.split('"').length > 3;
              }),
          "String",
          r
        );
    };
  },
  function (t, e) {
    var r = {}.hasOwnProperty;
    t.exports = function (t, e) {
      return r.call(t, e);
    };
  },
  function (t, e, r) {
    var n = r(9),
      i = r(28);
    t.exports = r(8)
      ? function (t, e, r) {
          return n.f(t, e, i(1, r));
        }
      : function (t, e, r) {
          return (t[e] = r), t;
        };
  },
  function (t, e, r) {
    var n = r(44),
      i = r(24);
    t.exports = function (t) {
      return n(i(t));
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(2);
    t.exports = function (t, e) {
      return (
        !!t &&
        n(function () {
          e ? t.call(null, function () {}, 1) : t.call(null);
        })
      );
    };
  },
  function (t, e, r) {
    var n = r(18);
    t.exports = function (t, e, r) {
      if ((n(t), void 0 === e)) return t;
      switch (r) {
        case 1:
          return function (r) {
            return t.call(e, r);
          };
        case 2:
          return function (r, n) {
            return t.call(e, r, n);
          };
        case 3:
          return function (r, n, i) {
            return t.call(e, r, n, i);
          };
      }
      return function () {
        return t.apply(e, arguments);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if ("function" != typeof t) throw TypeError(t + " is not a function!");
      return t;
    };
  },
  function (t, e) {
    var r = Math.ceil,
      n = Math.floor;
    t.exports = function (t) {
      return isNaN((t = +t)) ? 0 : (t > 0 ? n : r)(t);
    };
  },
  function (t, e, r) {
    var n = r(45),
      i = r(28),
      o = r(15),
      a = r(26),
      s = r(13),
      u = r(93),
      c = Object.getOwnPropertyDescriptor;
    e.f = r(8)
      ? c
      : function (t, e) {
          if (((t = o(t)), (e = a(e, !0)), u))
            try {
              return c(t, e);
            } catch (t) {}
          if (s(t, e)) return i(!n.f.call(t, e), t[e]);
        };
  },
  function (t, e, r) {
    var n = r(0),
      i = r(7),
      o = r(2);
    t.exports = function (t, e) {
      var r = (i.Object || {})[t] || Object[t],
        a = {};
      (a[t] = e(r)),
        n(
          n.S +
            n.F *
              o(function () {
                r(1);
              }),
          "Object",
          a
        );
    };
  },
  function (t, e, r) {
    var n = r(17),
      i = r(44),
      o = r(10),
      a = r(6),
      s = r(109);
    t.exports = function (t, e) {
      var r = 1 == t,
        u = 2 == t,
        c = 3 == t,
        f = 4 == t,
        l = 6 == t,
        h = 5 == t || l,
        p = e || s;
      return function (e, s, d) {
        for (
          var v,
            y,
            g = o(e),
            m = i(g),
            b = n(s, d, 3),
            w = a(m.length),
            _ = 0,
            E = r ? p(e, w) : u ? p(e, 0) : void 0;
          w > _;
          _++
        )
          if ((h || _ in m) && ((y = b((v = m[_]), _, g)), t))
            if (r) E[_] = y;
            else if (y)
              switch (t) {
                case 3:
                  return !0;
                case 5:
                  return v;
                case 6:
                  return _;
                case 2:
                  E.push(v);
              }
            else if (f) return !1;
        return l ? -1 : c || f ? f : E;
      };
    };
  },
  function (t, e) {
    var r = {}.toString;
    t.exports = function (t) {
      return r.call(t).slice(8, -1);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if (null == t) throw TypeError("Can't call method on  " + t);
      return t;
    };
  },
  function (t, e, r) {
    "use strict";
    if (r(8)) {
      var n = r(30),
        i = r(1),
        o = r(2),
        a = r(0),
        s = r(59),
        u = r(88),
        c = r(17),
        f = r(42),
        l = r(28),
        h = r(14),
        p = r(43),
        d = r(19),
        v = r(6),
        y = r(120),
        g = r(32),
        m = r(26),
        b = r(13),
        w = r(46),
        _ = r(4),
        E = r(10),
        T = r(80),
        x = r(33),
        S = r(35),
        A = r(34).f,
        I = r(82),
        O = r(29),
        R = r(5),
        P = r(22),
        N = r(49),
        L = r(47),
        D = r(84),
        F = r(40),
        C = r(52),
        M = r(41),
        B = r(83),
        U = r(111),
        j = r(9),
        k = r(20),
        V = j.f,
        q = k.f,
        Y = i.RangeError,
        H = i.TypeError,
        G = i.Uint8Array,
        $ = Array.prototype,
        W = u.ArrayBuffer,
        z = u.DataView,
        X = P(0),
        Q = P(2),
        K = P(3),
        J = P(4),
        Z = P(5),
        tt = P(6),
        et = N(!0),
        rt = N(!1),
        nt = D.values,
        it = D.keys,
        ot = D.entries,
        at = $.lastIndexOf,
        st = $.reduce,
        ut = $.reduceRight,
        ct = $.join,
        ft = $.sort,
        lt = $.slice,
        ht = $.toString,
        pt = $.toLocaleString,
        dt = R("iterator"),
        vt = R("toStringTag"),
        yt = O("typed_constructor"),
        gt = O("def_constructor"),
        mt = s.CONSTR,
        bt = s.TYPED,
        wt = s.VIEW,
        _t = P(1, function (t, e) {
          return At(L(t, t[gt]), e);
        }),
        Et = o(function () {
          return 1 === new G(new Uint16Array([1]).buffer)[0];
        }),
        Tt =
          !!G &&
          !!G.prototype.set &&
          o(function () {
            new G(1).set({});
          }),
        xt = function (t, e) {
          var r = d(t);
          if (r < 0 || r % e) throw Y("Wrong offset!");
          return r;
        },
        St = function (t) {
          if (_(t) && bt in t) return t;
          throw H(t + " is not a typed array!");
        },
        At = function (t, e) {
          if (!_(t) || !(yt in t))
            throw H("It is not a typed array constructor!");
          return new t(e);
        },
        It = function (t, e) {
          return Ot(L(t, t[gt]), e);
        },
        Ot = function (t, e) {
          for (var r = 0, n = e.length, i = At(t, n); n > r; ) i[r] = e[r++];
          return i;
        },
        Rt = function (t, e, r) {
          V(t, e, {
            get: function () {
              return this._d[r];
            },
          });
        },
        Pt = function (t) {
          var e,
            r,
            n,
            i,
            o,
            a,
            s = E(t),
            u = arguments.length,
            f = u > 1 ? arguments[1] : void 0,
            l = void 0 !== f,
            h = I(s);
          if (null != h && !T(h)) {
            for (a = h.call(s), n = [], e = 0; !(o = a.next()).done; e++)
              n.push(o.value);
            s = n;
          }
          for (
            l && u > 2 && (f = c(f, arguments[2], 2)),
              e = 0,
              r = v(s.length),
              i = At(this, r);
            r > e;
            e++
          )
            i[e] = l ? f(s[e], e) : s[e];
          return i;
        },
        Nt = function () {
          for (var t = 0, e = arguments.length, r = At(this, e); e > t; )
            r[t] = arguments[t++];
          return r;
        },
        Lt =
          !!G &&
          o(function () {
            pt.call(new G(1));
          }),
        Dt = function () {
          return pt.apply(Lt ? lt.call(St(this)) : St(this), arguments);
        },
        Ft = {
          copyWithin: function (t, e) {
            return U.call(
              St(this),
              t,
              e,
              arguments.length > 2 ? arguments[2] : void 0
            );
          },
          every: function (t) {
            return J(St(this), t, arguments.length > 1 ? arguments[1] : void 0);
          },
          fill: function (t) {
            return B.apply(St(this), arguments);
          },
          filter: function (t) {
            return It(
              this,
              Q(St(this), t, arguments.length > 1 ? arguments[1] : void 0)
            );
          },
          find: function (t) {
            return Z(St(this), t, arguments.length > 1 ? arguments[1] : void 0);
          },
          findIndex: function (t) {
            return tt(
              St(this),
              t,
              arguments.length > 1 ? arguments[1] : void 0
            );
          },
          forEach: function (t) {
            X(St(this), t, arguments.length > 1 ? arguments[1] : void 0);
          },
          indexOf: function (t) {
            return rt(
              St(this),
              t,
              arguments.length > 1 ? arguments[1] : void 0
            );
          },
          includes: function (t) {
            return et(
              St(this),
              t,
              arguments.length > 1 ? arguments[1] : void 0
            );
          },
          join: function (t) {
            return ct.apply(St(this), arguments);
          },
          lastIndexOf: function (t) {
            return at.apply(St(this), arguments);
          },
          map: function (t) {
            return _t(
              St(this),
              t,
              arguments.length > 1 ? arguments[1] : void 0
            );
          },
          reduce: function (t) {
            return st.apply(St(this), arguments);
          },
          reduceRight: function (t) {
            return ut.apply(St(this), arguments);
          },
          reverse: function () {
            for (
              var t, e = St(this).length, r = Math.floor(e / 2), n = 0;
              n < r;

            )
              (t = this[n]), (this[n++] = this[--e]), (this[e] = t);
            return this;
          },
          some: function (t) {
            return K(St(this), t, arguments.length > 1 ? arguments[1] : void 0);
          },
          sort: function (t) {
            return ft.call(St(this), t);
          },
          subarray: function (t, e) {
            var r = St(this),
              n = r.length,
              i = g(t, n);
            return new (L(r, r[gt]))(
              r.buffer,
              r.byteOffset + i * r.BYTES_PER_ELEMENT,
              v((void 0 === e ? n : g(e, n)) - i)
            );
          },
        },
        Ct = function (t, e) {
          return It(this, lt.call(St(this), t, e));
        },
        Mt = function (t) {
          St(this);
          var e = xt(arguments[1], 1),
            r = this.length,
            n = E(t),
            i = v(n.length),
            o = 0;
          if (i + e > r) throw Y("Wrong length!");
          for (; o < i; ) this[e + o] = n[o++];
        },
        Bt = {
          entries: function () {
            return ot.call(St(this));
          },
          keys: function () {
            return it.call(St(this));
          },
          values: function () {
            return nt.call(St(this));
          },
        },
        Ut = function (t, e) {
          return (
            _(t) &&
            t[bt] &&
            "symbol" != typeof e &&
            e in t &&
            String(+e) == String(e)
          );
        },
        jt = function (t, e) {
          return Ut(t, (e = m(e, !0))) ? l(2, t[e]) : q(t, e);
        },
        kt = function (t, e, r) {
          return !(Ut(t, (e = m(e, !0))) && _(r) && b(r, "value")) ||
            b(r, "get") ||
            b(r, "set") ||
            r.configurable ||
            (b(r, "writable") && !r.writable) ||
            (b(r, "enumerable") && !r.enumerable)
            ? V(t, e, r)
            : ((t[e] = r.value), t);
        };
      mt || ((k.f = jt), (j.f = kt)),
        a(a.S + a.F * !mt, "Object", {
          getOwnPropertyDescriptor: jt,
          defineProperty: kt,
        }),
        o(function () {
          ht.call({});
        }) &&
          (ht = pt =
            function () {
              return ct.call(this);
            });
      var Vt = p({}, Ft);
      p(Vt, Bt),
        h(Vt, dt, Bt.values),
        p(Vt, {
          slice: Ct,
          set: Mt,
          constructor: function () {},
          toString: ht,
          toLocaleString: Dt,
        }),
        Rt(Vt, "buffer", "b"),
        Rt(Vt, "byteOffset", "o"),
        Rt(Vt, "byteLength", "l"),
        Rt(Vt, "length", "e"),
        V(Vt, vt, {
          get: function () {
            return this[bt];
          },
        }),
        (t.exports = function (t, e, r, u) {
          var c = t + ((u = !!u) ? "Clamped" : "") + "Array",
            l = "get" + t,
            p = "set" + t,
            d = i[c],
            g = d || {},
            m = d && S(d),
            b = !d || !s.ABV,
            E = {},
            T = d && d.prototype,
            I = function (t, r) {
              V(t, r, {
                get: function () {
                  return (function (t, r) {
                    var n = t._d;
                    return n.v[l](r * e + n.o, Et);
                  })(this, r);
                },
                set: function (t) {
                  return (function (t, r, n) {
                    var i = t._d;
                    u &&
                      (n =
                        (n = Math.round(n)) < 0 ? 0 : n > 255 ? 255 : 255 & n),
                      i.v[p](r * e + i.o, n, Et);
                  })(this, r, t);
                },
                enumerable: !0,
              });
            };
          b
            ? ((d = r(function (t, r, n, i) {
                f(t, d, c, "_d");
                var o,
                  a,
                  s,
                  u,
                  l = 0,
                  p = 0;
                if (_(r)) {
                  if (
                    !(
                      r instanceof W ||
                      "ArrayBuffer" == (u = w(r)) ||
                      "SharedArrayBuffer" == u
                    )
                  )
                    return bt in r ? Ot(d, r) : Pt.call(d, r);
                  (o = r), (p = xt(n, e));
                  var g = r.byteLength;
                  if (void 0 === i) {
                    if (g % e) throw Y("Wrong length!");
                    if ((a = g - p) < 0) throw Y("Wrong length!");
                  } else if ((a = v(i) * e) + p > g) throw Y("Wrong length!");
                  s = a / e;
                } else (s = y(r)), (o = new W((a = s * e)));
                for (
                  h(t, "_d", { b: o, o: p, l: a, e: s, v: new z(o) });
                  l < s;

                )
                  I(t, l++);
              })),
              (T = d.prototype = x(Vt)),
              h(T, "constructor", d))
            : (o(function () {
                d(1);
              }) &&
                o(function () {
                  new d(-1);
                }) &&
                C(function (t) {
                  new d(), new d(null), new d(1.5), new d(t);
                }, !0)) ||
              ((d = r(function (t, r, n, i) {
                var o;
                return (
                  f(t, d, c),
                  _(r)
                    ? r instanceof W ||
                      "ArrayBuffer" == (o = w(r)) ||
                      "SharedArrayBuffer" == o
                      ? void 0 !== i
                        ? new g(r, xt(n, e), i)
                        : void 0 !== n
                        ? new g(r, xt(n, e))
                        : new g(r)
                      : bt in r
                      ? Ot(d, r)
                      : Pt.call(d, r)
                    : new g(y(r))
                );
              })),
              X(
                m !== Function.prototype ? A(g).concat(A(m)) : A(g),
                function (t) {
                  t in d || h(d, t, g[t]);
                }
              ),
              (d.prototype = T),
              n || (T.constructor = d));
          var O = T[dt],
            R = !!O && ("values" == O.name || null == O.name),
            P = Bt.values;
          h(d, yt, !0),
            h(T, bt, c),
            h(T, wt, !0),
            h(T, gt, d),
            (u ? new d(1)[vt] == c : vt in T) ||
              V(T, vt, {
                get: function () {
                  return c;
                },
              }),
            (E[c] = d),
            a(a.G + a.W + a.F * (d != g), E),
            a(a.S, c, { BYTES_PER_ELEMENT: e }),
            a(
              a.S +
                a.F *
                  o(function () {
                    g.of.call(d, 1);
                  }),
              c,
              { from: Pt, of: Nt }
            ),
            "BYTES_PER_ELEMENT" in T || h(T, "BYTES_PER_ELEMENT", e),
            a(a.P, c, Ft),
            M(c),
            a(a.P + a.F * Tt, c, { set: Mt }),
            a(a.P + a.F * !R, c, Bt),
            n || T.toString == ht || (T.toString = ht),
            a(
              a.P +
                a.F *
                  o(function () {
                    new d(1).slice();
                  }),
              c,
              { slice: Ct }
            ),
            a(
              a.P +
                a.F *
                  (o(function () {
                    return (
                      [1, 2].toLocaleString() != new d([1, 2]).toLocaleString()
                    );
                  }) ||
                    !o(function () {
                      T.toLocaleString.call([1, 2]);
                    })),
              c,
              { toLocaleString: Dt }
            ),
            (F[c] = R ? O : P),
            n || R || h(T, dt, P);
        });
    } else t.exports = function () {};
  },
  function (t, e, r) {
    var n = r(4);
    t.exports = function (t, e) {
      if (!n(t)) return t;
      var r, i;
      if (e && "function" == typeof (r = t.toString) && !n((i = r.call(t))))
        return i;
      if ("function" == typeof (r = t.valueOf) && !n((i = r.call(t)))) return i;
      if (!e && "function" == typeof (r = t.toString) && !n((i = r.call(t))))
        return i;
      throw TypeError("Can't convert object to primitive value");
    };
  },
  function (t, e, r) {
    var n = r(29)("meta"),
      i = r(4),
      o = r(13),
      a = r(9).f,
      s = 0,
      u =
        Object.isExtensible ||
        function () {
          return !0;
        },
      c = !r(2)(function () {
        return u(Object.preventExtensions({}));
      }),
      f = function (t) {
        a(t, n, { value: { i: "O" + ++s, w: {} } });
      },
      l = (t.exports = {
        KEY: n,
        NEED: !1,
        fastKey: function (t, e) {
          if (!i(t))
            return "symbol" == typeof t
              ? t
              : ("string" == typeof t ? "S" : "P") + t;
          if (!o(t, n)) {
            if (!u(t)) return "F";
            if (!e) return "E";
            f(t);
          }
          return t[n].i;
        },
        getWeak: function (t, e) {
          if (!o(t, n)) {
            if (!u(t)) return !0;
            if (!e) return !1;
            f(t);
          }
          return t[n].w;
        },
        onFreeze: function (t) {
          return c && l.NEED && u(t) && !o(t, n) && f(t), t;
        },
      });
  },
  function (t, e) {
    t.exports = function (t, e) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: e,
      };
    };
  },
  function (t, e) {
    var r = 0,
      n = Math.random();
    t.exports = function (t) {
      return "Symbol(".concat(
        void 0 === t ? "" : t,
        ")_",
        (++r + n).toString(36)
      );
    };
  },
  function (t, e) {
    t.exports = !1;
  },
  function (t, e, r) {
    var n = r(95),
      i = r(67);
    t.exports =
      Object.keys ||
      function (t) {
        return n(t, i);
      };
  },
  function (t, e, r) {
    var n = r(19),
      i = Math.max,
      o = Math.min;
    t.exports = function (t, e) {
      return (t = n(t)) < 0 ? i(t + e, 0) : o(t, e);
    };
  },
  function (t, e, r) {
    var n = r(3),
      i = r(96),
      o = r(67),
      a = r(66)("IE_PROTO"),
      s = function () {},
      u = function () {
        var t,
          e = r(64)("iframe"),
          n = o.length;
        for (
          e.style.display = "none",
            r(68).appendChild(e),
            e.src = "javascript:",
            (t = e.contentWindow.document).open(),
            t.write("<script>document.F=Object</script>"),
            t.close(),
            u = t.F;
          n--;

        )
          delete u.prototype[o[n]];
        return u();
      };
    t.exports =
      Object.create ||
      function (t, e) {
        var r;
        return (
          null !== t
            ? ((s.prototype = n(t)),
              (r = new s()),
              (s.prototype = null),
              (r[a] = t))
            : (r = u()),
          void 0 === e ? r : i(r, e)
        );
      };
  },
  function (t, e, r) {
    var n = r(95),
      i = r(67).concat("length", "prototype");
    e.f =
      Object.getOwnPropertyNames ||
      function (t) {
        return n(t, i);
      };
  },
  function (t, e, r) {
    var n = r(13),
      i = r(10),
      o = r(66)("IE_PROTO"),
      a = Object.prototype;
    t.exports =
      Object.getPrototypeOf ||
      function (t) {
        return (
          (t = i(t)),
          n(t, o)
            ? t[o]
            : "function" == typeof t.constructor && t instanceof t.constructor
            ? t.constructor.prototype
            : t instanceof Object
            ? a
            : null
        );
      };
  },
  function (t, e, r) {
    var n = r(5)("unscopables"),
      i = Array.prototype;
    null == i[n] && r(14)(i, n, {}),
      (t.exports = function (t) {
        i[n][t] = !0;
      });
  },
  function (t, e, r) {
    var n = r(4);
    t.exports = function (t, e) {
      if (!n(t) || t._t !== e)
        throw TypeError("Incompatible receiver, " + e + " required!");
      return t;
    };
  },
  function (t, e, r) {
    var n = r(9).f,
      i = r(13),
      o = r(5)("toStringTag");
    t.exports = function (t, e, r) {
      t &&
        !i((t = r ? t : t.prototype), o) &&
        n(t, o, { configurable: !0, value: e });
    };
  },
  function (t, e, r) {
    var n = r(0),
      i = r(24),
      o = r(2),
      a = r(70),
      s = "[" + a + "]",
      u = RegExp("^" + s + s + "*"),
      c = RegExp(s + s + "*$"),
      f = function (t, e, r) {
        var i = {},
          s = o(function () {
            return !!a[t]() || "​" != "​"[t]();
          }),
          u = (i[t] = s ? e(l) : a[t]);
        r && (i[r] = u), n(n.P + n.F * s, "String", i);
      },
      l = (f.trim = function (t, e) {
        return (
          (t = String(i(t))),
          1 & e && (t = t.replace(u, "")),
          2 & e && (t = t.replace(c, "")),
          t
        );
      });
    t.exports = f;
  },
  function (t, e) {
    t.exports = {};
  },
  function (t, e, r) {
    "use strict";
    var n = r(1),
      i = r(9),
      o = r(8),
      a = r(5)("species");
    t.exports = function (t) {
      var e = n[t];
      o &&
        e &&
        !e[a] &&
        i.f(e, a, {
          configurable: !0,
          get: function () {
            return this;
          },
        });
    };
  },
  function (t, e) {
    t.exports = function (t, e, r, n) {
      if (!(t instanceof e) || (void 0 !== n && n in t))
        throw TypeError(r + ": incorrect invocation!");
      return t;
    };
  },
  function (t, e, r) {
    var n = r(11);
    t.exports = function (t, e, r) {
      for (var i in e) n(t, i, e[i], r);
      return t;
    };
  },
  function (t, e, r) {
    var n = r(23);
    t.exports = Object("z").propertyIsEnumerable(0)
      ? Object
      : function (t) {
          return "String" == n(t) ? t.split("") : Object(t);
        };
  },
  function (t, e) {
    e.f = {}.propertyIsEnumerable;
  },
  function (t, e, r) {
    var n = r(23),
      i = r(5)("toStringTag"),
      o =
        "Arguments" ==
        n(
          (function () {
            return arguments;
          })()
        );
    t.exports = function (t) {
      var e, r, a;
      return void 0 === t
        ? "Undefined"
        : null === t
        ? "Null"
        : "string" ==
          typeof (r = (function (t, e) {
            try {
              return t[e];
            } catch (t) {}
          })((e = Object(t)), i))
        ? r
        : o
        ? n(e)
        : "Object" == (a = n(e)) && "function" == typeof e.callee
        ? "Arguments"
        : a;
    };
  },
  function (t, e, r) {
    var n = r(3),
      i = r(18),
      o = r(5)("species");
    t.exports = function (t, e) {
      var r,
        a = n(t).constructor;
      return void 0 === a || null == (r = n(a)[o]) ? e : i(r);
    };
  },
  function (t, e, r) {
    var n = r(7),
      i = r(1),
      o = i["__core-js_shared__"] || (i["__core-js_shared__"] = {});
    (t.exports = function (t, e) {
      return o[t] || (o[t] = void 0 !== e ? e : {});
    })("versions", []).push({
      version: n.version,
      mode: r(30) ? "pure" : "global",
      copyright: "© 2020 Denis Pushkarev (zloirock.ru)",
    });
  },
  function (t, e, r) {
    var n = r(15),
      i = r(6),
      o = r(32);
    t.exports = function (t) {
      return function (e, r, a) {
        var s,
          u = n(e),
          c = i(u.length),
          f = o(a, c);
        if (t && r != r) {
          for (; c > f; ) if ((s = u[f++]) != s) return !0;
        } else
          for (; c > f; f++)
            if ((t || f in u) && u[f] === r) return t || f || 0;
        return !t && -1;
      };
    };
  },
  function (t, e) {
    e.f = Object.getOwnPropertySymbols;
  },
  function (t, e, r) {
    var n = r(23);
    t.exports =
      Array.isArray ||
      function (t) {
        return "Array" == n(t);
      };
  },
  function (t, e, r) {
    var n = r(5)("iterator"),
      i = !1;
    try {
      var o = [7][n]();
      (o.return = function () {
        i = !0;
      }),
        Array.from(o, function () {
          throw 2;
        });
    } catch (t) {}
    t.exports = function (t, e) {
      if (!e && !i) return !1;
      var r = !1;
      try {
        var o = [7],
          a = o[n]();
        (a.next = function () {
          return { done: (r = !0) };
        }),
          (o[n] = function () {
            return a;
          }),
          t(o);
      } catch (t) {}
      return r;
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(3);
    t.exports = function () {
      var t = n(this),
        e = "";
      return (
        t.global && (e += "g"),
        t.ignoreCase && (e += "i"),
        t.multiline && (e += "m"),
        t.unicode && (e += "u"),
        t.sticky && (e += "y"),
        e
      );
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(46),
      i = RegExp.prototype.exec;
    t.exports = function (t, e) {
      var r = t.exec;
      if ("function" == typeof r) {
        var o = r.call(t, e);
        if ("object" != typeof o)
          throw new TypeError(
            "RegExp exec method returned something other than an Object or null"
          );
        return o;
      }
      if ("RegExp" !== n(t))
        throw new TypeError("RegExp#exec called on incompatible receiver");
      return i.call(t, e);
    };
  },
  function (t, e, r) {
    "use strict";
    r(113);
    var n = r(11),
      i = r(14),
      o = r(2),
      a = r(24),
      s = r(5),
      u = r(85),
      c = s("species"),
      f = !o(function () {
        var t = /./;
        return (
          (t.exec = function () {
            var t = [];
            return (t.groups = { a: "7" }), t;
          }),
          "7" !== "".replace(t, "$<a>")
        );
      }),
      l = (function () {
        var t = /(?:)/,
          e = t.exec;
        t.exec = function () {
          return e.apply(this, arguments);
        };
        var r = "ab".split(t);
        return 2 === r.length && "a" === r[0] && "b" === r[1];
      })();
    t.exports = function (t, e, r) {
      var h = s(t),
        p = !o(function () {
          var e = {};
          return (
            (e[h] = function () {
              return 7;
            }),
            7 != ""[t](e)
          );
        }),
        d = p
          ? !o(function () {
              var e = !1,
                r = /a/;
              return (
                (r.exec = function () {
                  return (e = !0), null;
                }),
                "split" === t &&
                  ((r.constructor = {}),
                  (r.constructor[c] = function () {
                    return r;
                  })),
                r[h](""),
                !e
              );
            })
          : void 0;
      if (!p || !d || ("replace" === t && !f) || ("split" === t && !l)) {
        var v = /./[h],
          y = r(a, h, ""[t], function (t, e, r, n, i) {
            return e.exec === u
              ? p && !i
                ? { done: !0, value: v.call(e, r, n) }
                : { done: !0, value: t.call(r, e, n) }
              : { done: !1 };
          }),
          g = y[0],
          m = y[1];
        n(String.prototype, t, g),
          i(
            RegExp.prototype,
            h,
            2 == e
              ? function (t, e) {
                  return m.call(t, this, e);
                }
              : function (t) {
                  return m.call(t, this);
                }
          );
      }
    };
  },
  function (t, e, r) {
    var n = r(17),
      i = r(108),
      o = r(80),
      a = r(3),
      s = r(6),
      u = r(82),
      c = {},
      f = {};
    ((e = t.exports =
      function (t, e, r, l, h) {
        var p,
          d,
          v,
          y,
          g = h
            ? function () {
                return t;
              }
            : u(t),
          m = n(r, l, e ? 2 : 1),
          b = 0;
        if ("function" != typeof g) throw TypeError(t + " is not iterable!");
        if (o(g)) {
          for (p = s(t.length); p > b; b++)
            if ((y = e ? m(a((d = t[b]))[0], d[1]) : m(t[b])) === c || y === f)
              return y;
        } else
          for (v = g.call(t); !(d = v.next()).done; )
            if ((y = i(v, m, d.value, e)) === c || y === f) return y;
      }).BREAK = c),
      (e.RETURN = f);
  },
  function (t, e, r) {
    var n = r(1).navigator;
    t.exports = (n && n.userAgent) || "";
  },
  function (t, e, r) {
    "use strict";
    var n = r(1),
      i = r(0),
      o = r(11),
      a = r(43),
      s = r(27),
      u = r(56),
      c = r(42),
      f = r(4),
      l = r(2),
      h = r(52),
      p = r(38),
      d = r(71);
    t.exports = function (t, e, r, v, y, g) {
      var m = n[t],
        b = m,
        w = y ? "set" : "add",
        _ = b && b.prototype,
        E = {},
        T = function (t) {
          var e = _[t];
          o(
            _,
            t,
            "delete" == t || "has" == t
              ? function (t) {
                  return !(g && !f(t)) && e.call(this, 0 === t ? 0 : t);
                }
              : "get" == t
              ? function (t) {
                  return g && !f(t) ? void 0 : e.call(this, 0 === t ? 0 : t);
                }
              : "add" == t
              ? function (t) {
                  return e.call(this, 0 === t ? 0 : t), this;
                }
              : function (t, r) {
                  return e.call(this, 0 === t ? 0 : t, r), this;
                }
          );
        };
      if (
        "function" == typeof b &&
        (g ||
          (_.forEach &&
            !l(function () {
              new b().entries().next();
            })))
      ) {
        var x = new b(),
          S = x[w](g ? {} : -0, 1) != x,
          A = l(function () {
            x.has(1);
          }),
          I = h(function (t) {
            new b(t);
          }),
          O =
            !g &&
            l(function () {
              for (var t = new b(), e = 5; e--; ) t[w](e, e);
              return !t.has(-0);
            });
        I ||
          (((b = e(function (e, r) {
            c(e, b, t);
            var n = d(new m(), e, b);
            return null != r && u(r, y, n[w], n), n;
          })).prototype = _),
          (_.constructor = b)),
          (A || O) && (T("delete"), T("has"), y && T("get")),
          (O || S) && T(w),
          g && _.clear && delete _.clear;
      } else
        (b = v.getConstructor(e, t, y, w)), a(b.prototype, r), (s.NEED = !0);
      return (
        p(b, t),
        (E[t] = b),
        i(i.G + i.W + i.F * (b != m), E),
        g || v.setStrong(b, t, y),
        b
      );
    };
  },
  function (t, e, r) {
    for (
      var n,
        i = r(1),
        o = r(14),
        a = r(29),
        s = a("typed_array"),
        u = a("view"),
        c = !(!i.ArrayBuffer || !i.DataView),
        f = c,
        l = 0,
        h =
          "Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array".split(
            ","
          );
      l < 9;

    )
      (n = i[h[l++]])
        ? (o(n.prototype, s, !0), o(n.prototype, u, !0))
        : (f = !1);
    t.exports = { ABV: c, CONSTR: f, TYPED: s, VIEW: u };
  },
  function (t, e) {
    var r;
    r = (function () {
      return this;
    })();
    try {
      r = r || new Function("return this")();
    } catch (t) {
      "object" == typeof window && (r = window);
    }
    t.exports = r;
  },
  function (t, e, r) {
    "use strict";
    var n = r(320),
      i = r(322);
    function o() {
      (this.protocol = null),
        (this.slashes = null),
        (this.auth = null),
        (this.host = null),
        (this.port = null),
        (this.hostname = null),
        (this.hash = null),
        (this.search = null),
        (this.query = null),
        (this.pathname = null),
        (this.path = null),
        (this.href = null);
    }
    (e.parse = b),
      (e.resolve = function (t, e) {
        return b(t, !1, !0).resolve(e);
      }),
      (e.resolveObject = function (t, e) {
        return t ? b(t, !1, !0).resolveObject(e) : e;
      }),
      (e.format = function (t) {
        i.isString(t) && (t = b(t));
        return t instanceof o ? t.format() : o.prototype.format.call(t);
      }),
      (e.Url = o);
    var a = /^([a-z0-9.+-]+:)/i,
      s = /:[0-9]*$/,
      u = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,
      c = ["{", "}", "|", "\\", "^", "`"].concat([
        "<",
        ">",
        '"',
        "`",
        " ",
        "\r",
        "\n",
        "\t",
      ]),
      f = ["'"].concat(c),
      l = ["%", "/", "?", ";", "#"].concat(f),
      h = ["/", "?", "#"],
      p = /^[+a-z0-9A-Z_-]{0,63}$/,
      d = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
      v = { javascript: !0, "javascript:": !0 },
      y = { javascript: !0, "javascript:": !0 },
      g = {
        http: !0,
        https: !0,
        ftp: !0,
        gopher: !0,
        file: !0,
        "http:": !0,
        "https:": !0,
        "ftp:": !0,
        "gopher:": !0,
        "file:": !0,
      },
      m = r(62);
    function b(t, e, r) {
      if (t && i.isObject(t) && t instanceof o) return t;
      var n = new o();
      return n.parse(t, e, r), n;
    }
    (o.prototype.parse = function (t, e, r) {
      if (!i.isString(t))
        throw new TypeError(
          "Parameter 'url' must be a string, not " + typeof t
        );
      var o = t.indexOf("?"),
        s = -1 !== o && o < t.indexOf("#") ? "?" : "#",
        c = t.split(s);
      c[0] = c[0].replace(/\\/g, "/");
      var b = (t = c.join(s));
      if (((b = b.trim()), !r && 1 === t.split("#").length)) {
        var w = u.exec(b);
        if (w)
          return (
            (this.path = b),
            (this.href = b),
            (this.pathname = w[1]),
            w[2]
              ? ((this.search = w[2]),
                (this.query = e
                  ? m.parse(this.search.substr(1))
                  : this.search.substr(1)))
              : e && ((this.search = ""), (this.query = {})),
            this
          );
      }
      var _ = a.exec(b);
      if (_) {
        var E = (_ = _[0]).toLowerCase();
        (this.protocol = E), (b = b.substr(_.length));
      }
      if (r || _ || b.match(/^\/\/[^@\/]+@[^@\/]+/)) {
        var T = "//" === b.substr(0, 2);
        !T || (_ && y[_]) || ((b = b.substr(2)), (this.slashes = !0));
      }
      if (!y[_] && (T || (_ && !g[_]))) {
        for (var x, S, A = -1, I = 0; I < h.length; I++) {
          -1 !== (O = b.indexOf(h[I])) && (-1 === A || O < A) && (A = O);
        }
        -1 !== (S = -1 === A ? b.lastIndexOf("@") : b.lastIndexOf("@", A)) &&
          ((x = b.slice(0, S)),
          (b = b.slice(S + 1)),
          (this.auth = decodeURIComponent(x))),
          (A = -1);
        for (I = 0; I < l.length; I++) {
          var O;
          -1 !== (O = b.indexOf(l[I])) && (-1 === A || O < A) && (A = O);
        }
        -1 === A && (A = b.length),
          (this.host = b.slice(0, A)),
          (b = b.slice(A)),
          this.parseHost(),
          (this.hostname = this.hostname || "");
        var R =
          "[" === this.hostname[0] &&
          "]" === this.hostname[this.hostname.length - 1];
        if (!R)
          for (
            var P = this.hostname.split(/\./), N = ((I = 0), P.length);
            I < N;
            I++
          ) {
            var L = P[I];
            if (L && !L.match(p)) {
              for (var D = "", F = 0, C = L.length; F < C; F++)
                L.charCodeAt(F) > 127 ? (D += "x") : (D += L[F]);
              if (!D.match(p)) {
                var M = P.slice(0, I),
                  B = P.slice(I + 1),
                  U = L.match(d);
                U && (M.push(U[1]), B.unshift(U[2])),
                  B.length && (b = "/" + B.join(".") + b),
                  (this.hostname = M.join("."));
                break;
              }
            }
          }
        this.hostname.length > 255
          ? (this.hostname = "")
          : (this.hostname = this.hostname.toLowerCase()),
          R || (this.hostname = n.toASCII(this.hostname));
        var j = this.port ? ":" + this.port : "",
          k = this.hostname || "";
        (this.host = k + j),
          (this.href += this.host),
          R &&
            ((this.hostname = this.hostname.substr(
              1,
              this.hostname.length - 2
            )),
            "/" !== b[0] && (b = "/" + b));
      }
      if (!v[E])
        for (I = 0, N = f.length; I < N; I++) {
          var V = f[I];
          if (-1 !== b.indexOf(V)) {
            var q = encodeURIComponent(V);
            q === V && (q = escape(V)), (b = b.split(V).join(q));
          }
        }
      var Y = b.indexOf("#");
      -1 !== Y && ((this.hash = b.substr(Y)), (b = b.slice(0, Y)));
      var H = b.indexOf("?");
      if (
        (-1 !== H
          ? ((this.search = b.substr(H)),
            (this.query = b.substr(H + 1)),
            e && (this.query = m.parse(this.query)),
            (b = b.slice(0, H)))
          : e && ((this.search = ""), (this.query = {})),
        b && (this.pathname = b),
        g[E] && this.hostname && !this.pathname && (this.pathname = "/"),
        this.pathname || this.search)
      ) {
        j = this.pathname || "";
        var G = this.search || "";
        this.path = j + G;
      }
      return (this.href = this.format()), this;
    }),
      (o.prototype.format = function () {
        var t = this.auth || "";
        t &&
          ((t = (t = encodeURIComponent(t)).replace(/%3A/i, ":")), (t += "@"));
        var e = this.protocol || "",
          r = this.pathname || "",
          n = this.hash || "",
          o = !1,
          a = "";
        this.host
          ? (o = t + this.host)
          : this.hostname &&
            ((o =
              t +
              (-1 === this.hostname.indexOf(":")
                ? this.hostname
                : "[" + this.hostname + "]")),
            this.port && (o += ":" + this.port)),
          this.query &&
            i.isObject(this.query) &&
            Object.keys(this.query).length &&
            (a = m.stringify(this.query));
        var s = this.search || (a && "?" + a) || "";
        return (
          e && ":" !== e.substr(-1) && (e += ":"),
          this.slashes || ((!e || g[e]) && !1 !== o)
            ? ((o = "//" + (o || "")),
              r && "/" !== r.charAt(0) && (r = "/" + r))
            : o || (o = ""),
          n && "#" !== n.charAt(0) && (n = "#" + n),
          s && "?" !== s.charAt(0) && (s = "?" + s),
          e +
            o +
            (r = r.replace(/[?#]/g, function (t) {
              return encodeURIComponent(t);
            })) +
            (s = s.replace("#", "%23")) +
            n
        );
      }),
      (o.prototype.resolve = function (t) {
        return this.resolveObject(b(t, !1, !0)).format();
      }),
      (o.prototype.resolveObject = function (t) {
        if (i.isString(t)) {
          var e = new o();
          e.parse(t, !1, !0), (t = e);
        }
        for (var r = new o(), n = Object.keys(this), a = 0; a < n.length; a++) {
          var s = n[a];
          r[s] = this[s];
        }
        if (((r.hash = t.hash), "" === t.href)) return (r.href = r.format()), r;
        if (t.slashes && !t.protocol) {
          for (var u = Object.keys(t), c = 0; c < u.length; c++) {
            var f = u[c];
            "protocol" !== f && (r[f] = t[f]);
          }
          return (
            g[r.protocol] &&
              r.hostname &&
              !r.pathname &&
              (r.path = r.pathname = "/"),
            (r.href = r.format()),
            r
          );
        }
        if (t.protocol && t.protocol !== r.protocol) {
          if (!g[t.protocol]) {
            for (var l = Object.keys(t), h = 0; h < l.length; h++) {
              var p = l[h];
              r[p] = t[p];
            }
            return (r.href = r.format()), r;
          }
          if (((r.protocol = t.protocol), t.host || y[t.protocol]))
            r.pathname = t.pathname;
          else {
            for (
              var d = (t.pathname || "").split("/");
              d.length && !(t.host = d.shift());

            );
            t.host || (t.host = ""),
              t.hostname || (t.hostname = ""),
              "" !== d[0] && d.unshift(""),
              d.length < 2 && d.unshift(""),
              (r.pathname = d.join("/"));
          }
          if (
            ((r.search = t.search),
            (r.query = t.query),
            (r.host = t.host || ""),
            (r.auth = t.auth),
            (r.hostname = t.hostname || t.host),
            (r.port = t.port),
            r.pathname || r.search)
          ) {
            var v = r.pathname || "",
              m = r.search || "";
            r.path = v + m;
          }
          return (r.slashes = r.slashes || t.slashes), (r.href = r.format()), r;
        }
        var b = r.pathname && "/" === r.pathname.charAt(0),
          w = t.host || (t.pathname && "/" === t.pathname.charAt(0)),
          _ = w || b || (r.host && t.pathname),
          E = _,
          T = (r.pathname && r.pathname.split("/")) || [],
          x =
            ((d = (t.pathname && t.pathname.split("/")) || []),
            r.protocol && !g[r.protocol]);
        if (
          (x &&
            ((r.hostname = ""),
            (r.port = null),
            r.host && ("" === T[0] ? (T[0] = r.host) : T.unshift(r.host)),
            (r.host = ""),
            t.protocol &&
              ((t.hostname = null),
              (t.port = null),
              t.host && ("" === d[0] ? (d[0] = t.host) : d.unshift(t.host)),
              (t.host = null)),
            (_ = _ && ("" === d[0] || "" === T[0]))),
          w)
        )
          (r.host = t.host || "" === t.host ? t.host : r.host),
            (r.hostname =
              t.hostname || "" === t.hostname ? t.hostname : r.hostname),
            (r.search = t.search),
            (r.query = t.query),
            (T = d);
        else if (d.length)
          T || (T = []),
            T.pop(),
            (T = T.concat(d)),
            (r.search = t.search),
            (r.query = t.query);
        else if (!i.isNullOrUndefined(t.search)) {
          if (x)
            (r.hostname = r.host = T.shift()),
              (R =
                !!(r.host && r.host.indexOf("@") > 0) && r.host.split("@")) &&
                ((r.auth = R.shift()), (r.host = r.hostname = R.shift()));
          return (
            (r.search = t.search),
            (r.query = t.query),
            (i.isNull(r.pathname) && i.isNull(r.search)) ||
              (r.path =
                (r.pathname ? r.pathname : "") + (r.search ? r.search : "")),
            (r.href = r.format()),
            r
          );
        }
        if (!T.length)
          return (
            (r.pathname = null),
            r.search ? (r.path = "/" + r.search) : (r.path = null),
            (r.href = r.format()),
            r
          );
        for (
          var S = T.slice(-1)[0],
            A =
              ((r.host || t.host || T.length > 1) &&
                ("." === S || ".." === S)) ||
              "" === S,
            I = 0,
            O = T.length;
          O >= 0;
          O--
        )
          "." === (S = T[O])
            ? T.splice(O, 1)
            : ".." === S
            ? (T.splice(O, 1), I++)
            : I && (T.splice(O, 1), I--);
        if (!_ && !E) for (; I--; I) T.unshift("..");
        !_ || "" === T[0] || (T[0] && "/" === T[0].charAt(0)) || T.unshift(""),
          A && "/" !== T.join("/").substr(-1) && T.push("");
        var R,
          P = "" === T[0] || (T[0] && "/" === T[0].charAt(0));
        x &&
          ((r.hostname = r.host = P ? "" : T.length ? T.shift() : ""),
          (R = !!(r.host && r.host.indexOf("@") > 0) && r.host.split("@")) &&
            ((r.auth = R.shift()), (r.host = r.hostname = R.shift())));
        return (
          (_ = _ || (r.host && T.length)) && !P && T.unshift(""),
          T.length
            ? (r.pathname = T.join("/"))
            : ((r.pathname = null), (r.path = null)),
          (i.isNull(r.pathname) && i.isNull(r.search)) ||
            (r.path =
              (r.pathname ? r.pathname : "") + (r.search ? r.search : "")),
          (r.auth = t.auth || r.auth),
          (r.slashes = r.slashes || t.slashes),
          (r.href = r.format()),
          r
        );
      }),
      (o.prototype.parseHost = function () {
        var t = this.host,
          e = s.exec(t);
        e &&
          (":" !== (e = e[0]) && (this.port = e.substr(1)),
          (t = t.substr(0, t.length - e.length))),
          t && (this.hostname = t);
      });
  },
  function (t, e, r) {
    "use strict";
    (e.decode = e.parse = r(323)), (e.encode = e.stringify = r(324));
  },
  function (t, e, r) {
    (function (t) {
      const n = r(92);
      (e.between = (t, e, r) => {
        let n;
        if (e instanceof RegExp) {
          const r = t.match(e);
          if (!r) return "";
          n = r.index + r[0].length;
        } else {
          if (((n = t.indexOf(e)), -1 === n)) return "";
          n += e.length;
        }
        return (
          (n = (t = t.slice(n)).indexOf(r)), -1 === n ? "" : (t = t.slice(0, n))
        );
      }),
        (e.parseAbbreviatedNumber = (t) => {
          const e = t
            .replace(",", ".")
            .replace(" ", "")
            .match(/([\d,.]+)([MK]?)/);
          if (e) {
            let [, t, r] = e;
            return (
              (t = parseFloat(t)),
              Math.round("M" === r ? 1e6 * t : "K" === r ? 1e3 * t : t)
            );
          }
          return null;
        }),
        (e.cutAfterJSON = (t) => {
          let e, r;
          if (
            ("[" === t[0]
              ? ((e = "["), (r = "]"))
              : "{" === t[0] && ((e = "{"), (r = "}")),
            !e)
          )
            throw new Error(
              "Can't cut unsupported JSON (need to begin with [ or { ) but got: " +
                t[0]
            );
          let n,
            i = !1,
            o = 0;
          for (n = 0; n < t.length; n++)
            if ('"' !== t[n] || "\\" === t[n - 1]) {
              if (!i && (t[n] === e ? o++ : t[n] === r && o--, 0 === o))
                return t.substr(0, n + 1);
            } else i = !i;
          throw Error(
            "Can't cut unsupported JSON (no matching closing bracket found)"
          );
        }),
        (e.playError = (t, e, r = Error) => {
          let n = t && t.playabilityStatus;
          return n && e.includes(n.status)
            ? new r(n.reason || (n.messages && n.messages[0]))
            : null;
        }),
        (e.deprecate = (t, e, r, n, i) => {
          Object.defineProperty(t, e, {
            get: () => (
              console.warn(
                `\`${n}\` will be removed in a near future release, use \`${i}\` instead.`
              ),
              r
            ),
          });
        });
      const i = r(332);
      (e.lastUpdateCheck = 0),
        (e.checkForUpdates = () =>
          !t.env.YTDL_NO_UPDATE &&
          !i.version.startsWith("0.0.0-") &&
          Date.now() - e.lastUpdateCheck >= 432e5
            ? ((e.lastUpdateCheck = Date.now()),
              n(
                "https://api.github.com/repos/fent/node-ytdl-core/releases/latest",
                { headers: { "User-Agent": "ytdl-core" } }
              )
                .text()
                .then((t) => {
                  JSON.parse(t).tag_name !== "v" + i.version &&
                    console.warn(
                      '[33mWARNING:[0m react-native-ytdl is out of date! If the latest port is available, update with "npm install react-native-ytdl@latest".'
                    );
                }))
            : null);
    }).call(this, r(331));
  },
  function (t, e, r) {
    var n = r(4),
      i = r(1).document,
      o = n(i) && n(i.createElement);
    t.exports = function (t) {
      return o ? i.createElement(t) : {};
    };
  },
  function (t, e, r) {
    e.f = r(5);
  },
  function (t, e, r) {
    var n = r(48)("keys"),
      i = r(29);
    t.exports = function (t) {
      return n[t] || (n[t] = i(t));
    };
  },
  function (t, e) {
    t.exports =
      "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(
        ","
      );
  },
  function (t, e, r) {
    var n = r(1).document;
    t.exports = n && n.documentElement;
  },
  function (t, e, r) {
    var n = r(4),
      i = r(3),
      o = function (t, e) {
        if ((i(t), !n(e) && null !== e))
          throw TypeError(e + ": can't set as prototype!");
      };
    t.exports = {
      set:
        Object.setPrototypeOf ||
        ("__proto__" in {}
          ? (function (t, e, n) {
              try {
                (n = r(17)(
                  Function.call,
                  r(20).f(Object.prototype, "__proto__").set,
                  2
                ))(t, []),
                  (e = !(t instanceof Array));
              } catch (t) {
                e = !0;
              }
              return function (t, r) {
                return o(t, r), e ? (t.__proto__ = r) : n(t, r), t;
              };
            })({}, !1)
          : void 0),
      check: o,
    };
  },
  function (t, e) {
    t.exports = "\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff";
  },
  function (t, e, r) {
    var n = r(4),
      i = r(69).set;
    t.exports = function (t, e, r) {
      var o,
        a = e.constructor;
      return (
        a !== r &&
          "function" == typeof a &&
          (o = a.prototype) !== r.prototype &&
          n(o) &&
          i &&
          i(t, o),
        t
      );
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(19),
      i = r(24);
    t.exports = function (t) {
      var e = String(i(this)),
        r = "",
        o = n(t);
      if (o < 0 || o == 1 / 0) throw RangeError("Count can't be negative");
      for (; o > 0; (o >>>= 1) && (e += e)) 1 & o && (r += e);
      return r;
    };
  },
  function (t, e) {
    t.exports =
      Math.sign ||
      function (t) {
        return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
      };
  },
  function (t, e) {
    var r = Math.expm1;
    t.exports =
      !r ||
      r(10) > 22025.465794806718 ||
      r(10) < 22025.465794806718 ||
      -2e-17 != r(-2e-17)
        ? function (t) {
            return 0 == (t = +t)
              ? t
              : t > -1e-6 && t < 1e-6
              ? t + (t * t) / 2
              : Math.exp(t) - 1;
          }
        : r;
  },
  function (t, e, r) {
    var n = r(19),
      i = r(24);
    t.exports = function (t) {
      return function (e, r) {
        var o,
          a,
          s = String(i(e)),
          u = n(r),
          c = s.length;
        return u < 0 || u >= c
          ? t
            ? ""
            : void 0
          : (o = s.charCodeAt(u)) < 55296 ||
            o > 56319 ||
            u + 1 === c ||
            (a = s.charCodeAt(u + 1)) < 56320 ||
            a > 57343
          ? t
            ? s.charAt(u)
            : o
          : t
          ? s.slice(u, u + 2)
          : a - 56320 + ((o - 55296) << 10) + 65536;
      };
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(30),
      i = r(0),
      o = r(11),
      a = r(14),
      s = r(40),
      u = r(107),
      c = r(38),
      f = r(35),
      l = r(5)("iterator"),
      h = !([].keys && "next" in [].keys()),
      p = function () {
        return this;
      };
    t.exports = function (t, e, r, d, v, y, g) {
      u(r, e, d);
      var m,
        b,
        w,
        _ = function (t) {
          if (!h && t in S) return S[t];
          switch (t) {
            case "keys":
            case "values":
              return function () {
                return new r(this, t);
              };
          }
          return function () {
            return new r(this, t);
          };
        },
        E = e + " Iterator",
        T = "values" == v,
        x = !1,
        S = t.prototype,
        A = S[l] || S["@@iterator"] || (v && S[v]),
        I = A || _(v),
        O = v ? (T ? _("entries") : I) : void 0,
        R = ("Array" == e && S.entries) || A;
      if (
        (R &&
          (w = f(R.call(new t()))) !== Object.prototype &&
          w.next &&
          (c(w, E, !0), n || "function" == typeof w[l] || a(w, l, p)),
        T &&
          A &&
          "values" !== A.name &&
          ((x = !0),
          (I = function () {
            return A.call(this);
          })),
        (n && !g) || (!h && !x && S[l]) || a(S, l, I),
        (s[e] = I),
        (s[E] = p),
        v)
      )
        if (
          ((m = {
            values: T ? I : _("values"),
            keys: y ? I : _("keys"),
            entries: O,
          }),
          g)
        )
          for (b in m) b in S || o(S, b, m[b]);
        else i(i.P + i.F * (h || x), e, m);
      return m;
    };
  },
  function (t, e, r) {
    var n = r(78),
      i = r(24);
    t.exports = function (t, e, r) {
      if (n(e)) throw TypeError("String#" + r + " doesn't accept regex!");
      return String(i(t));
    };
  },
  function (t, e, r) {
    var n = r(4),
      i = r(23),
      o = r(5)("match");
    t.exports = function (t) {
      var e;
      return n(t) && (void 0 !== (e = t[o]) ? !!e : "RegExp" == i(t));
    };
  },
  function (t, e, r) {
    var n = r(5)("match");
    t.exports = function (t) {
      var e = /./;
      try {
        "/./"[t](e);
      } catch (r) {
        try {
          return (e[n] = !1), !"/./"[t](e);
        } catch (t) {}
      }
      return !0;
    };
  },
  function (t, e, r) {
    var n = r(40),
      i = r(5)("iterator"),
      o = Array.prototype;
    t.exports = function (t) {
      return void 0 !== t && (n.Array === t || o[i] === t);
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(9),
      i = r(28);
    t.exports = function (t, e, r) {
      e in t ? n.f(t, e, i(0, r)) : (t[e] = r);
    };
  },
  function (t, e, r) {
    var n = r(46),
      i = r(5)("iterator"),
      o = r(40);
    t.exports = r(7).getIteratorMethod = function (t) {
      if (null != t) return t[i] || t["@@iterator"] || o[n(t)];
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(10),
      i = r(32),
      o = r(6);
    t.exports = function (t) {
      for (
        var e = n(this),
          r = o(e.length),
          a = arguments.length,
          s = i(a > 1 ? arguments[1] : void 0, r),
          u = a > 2 ? arguments[2] : void 0,
          c = void 0 === u ? r : i(u, r);
        c > s;

      )
        e[s++] = t;
      return e;
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(36),
      i = r(112),
      o = r(40),
      a = r(15);
    (t.exports = r(76)(
      Array,
      "Array",
      function (t, e) {
        (this._t = a(t)), (this._i = 0), (this._k = e);
      },
      function () {
        var t = this._t,
          e = this._k,
          r = this._i++;
        return !t || r >= t.length
          ? ((this._t = void 0), i(1))
          : i(0, "keys" == e ? r : "values" == e ? t[r] : [r, t[r]]);
      },
      "values"
    )),
      (o.Arguments = o.Array),
      n("keys"),
      n("values"),
      n("entries");
  },
  function (t, e, r) {
    "use strict";
    var n,
      i,
      o = r(53),
      a = RegExp.prototype.exec,
      s = String.prototype.replace,
      u = a,
      c =
        ((n = /a/),
        (i = /b*/g),
        a.call(n, "a"),
        a.call(i, "a"),
        0 !== n.lastIndex || 0 !== i.lastIndex),
      f = void 0 !== /()??/.exec("")[1];
    (c || f) &&
      (u = function (t) {
        var e,
          r,
          n,
          i,
          u = this;
        return (
          f && (r = new RegExp("^" + u.source + "$(?!\\s)", o.call(u))),
          c && (e = u.lastIndex),
          (n = a.call(u, t)),
          c && n && (u.lastIndex = u.global ? n.index + n[0].length : e),
          f &&
            n &&
            n.length > 1 &&
            s.call(n[0], r, function () {
              for (i = 1; i < arguments.length - 2; i++)
                void 0 === arguments[i] && (n[i] = void 0);
            }),
          n
        );
      }),
      (t.exports = u);
  },
  function (t, e, r) {
    "use strict";
    var n = r(75)(!0);
    t.exports = function (t, e, r) {
      return e + (r ? n(t, e).length : 1);
    };
  },
  function (t, e, r) {
    var n,
      i,
      o,
      a = r(17),
      s = r(101),
      u = r(68),
      c = r(64),
      f = r(1),
      l = f.process,
      h = f.setImmediate,
      p = f.clearImmediate,
      d = f.MessageChannel,
      v = f.Dispatch,
      y = 0,
      g = {},
      m = function () {
        var t = +this;
        if (g.hasOwnProperty(t)) {
          var e = g[t];
          delete g[t], e();
        }
      },
      b = function (t) {
        m.call(t.data);
      };
    (h && p) ||
      ((h = function (t) {
        for (var e = [], r = 1; arguments.length > r; ) e.push(arguments[r++]);
        return (
          (g[++y] = function () {
            s("function" == typeof t ? t : Function(t), e);
          }),
          n(y),
          y
        );
      }),
      (p = function (t) {
        delete g[t];
      }),
      "process" == r(23)(l)
        ? (n = function (t) {
            l.nextTick(a(m, t, 1));
          })
        : v && v.now
        ? (n = function (t) {
            v.now(a(m, t, 1));
          })
        : d
        ? ((o = (i = new d()).port2),
          (i.port1.onmessage = b),
          (n = a(o.postMessage, o, 1)))
        : f.addEventListener &&
          "function" == typeof postMessage &&
          !f.importScripts
        ? ((n = function (t) {
            f.postMessage(t + "", "*");
          }),
          f.addEventListener("message", b, !1))
        : (n =
            "onreadystatechange" in c("script")
              ? function (t) {
                  u.appendChild(c("script")).onreadystatechange = function () {
                    u.removeChild(this), m.call(t);
                  };
                }
              : function (t) {
                  setTimeout(a(m, t, 1), 0);
                })),
      (t.exports = { set: h, clear: p });
  },
  function (t, e, r) {
    "use strict";
    var n = r(1),
      i = r(8),
      o = r(30),
      a = r(59),
      s = r(14),
      u = r(43),
      c = r(2),
      f = r(42),
      l = r(19),
      h = r(6),
      p = r(120),
      d = r(34).f,
      v = r(9).f,
      y = r(83),
      g = r(38),
      m = n.ArrayBuffer,
      b = n.DataView,
      w = n.Math,
      _ = n.RangeError,
      E = n.Infinity,
      T = m,
      x = w.abs,
      S = w.pow,
      A = w.floor,
      I = w.log,
      O = w.LN2,
      R = i ? "_b" : "buffer",
      P = i ? "_l" : "byteLength",
      N = i ? "_o" : "byteOffset";
    function L(t, e, r) {
      var n,
        i,
        o,
        a = new Array(r),
        s = 8 * r - e - 1,
        u = (1 << s) - 1,
        c = u >> 1,
        f = 23 === e ? S(2, -24) - S(2, -77) : 0,
        l = 0,
        h = t < 0 || (0 === t && 1 / t < 0) ? 1 : 0;
      for (
        (t = x(t)) != t || t === E
          ? ((i = t != t ? 1 : 0), (n = u))
          : ((n = A(I(t) / O)),
            t * (o = S(2, -n)) < 1 && (n--, (o *= 2)),
            (t += n + c >= 1 ? f / o : f * S(2, 1 - c)) * o >= 2 &&
              (n++, (o /= 2)),
            n + c >= u
              ? ((i = 0), (n = u))
              : n + c >= 1
              ? ((i = (t * o - 1) * S(2, e)), (n += c))
              : ((i = t * S(2, c - 1) * S(2, e)), (n = 0)));
        e >= 8;
        a[l++] = 255 & i, i /= 256, e -= 8
      );
      for (n = (n << e) | i, s += e; s > 0; a[l++] = 255 & n, n /= 256, s -= 8);
      return (a[--l] |= 128 * h), a;
    }
    function D(t, e, r) {
      var n,
        i = 8 * r - e - 1,
        o = (1 << i) - 1,
        a = o >> 1,
        s = i - 7,
        u = r - 1,
        c = t[u--],
        f = 127 & c;
      for (c >>= 7; s > 0; f = 256 * f + t[u], u--, s -= 8);
      for (
        n = f & ((1 << -s) - 1), f >>= -s, s += e;
        s > 0;
        n = 256 * n + t[u], u--, s -= 8
      );
      if (0 === f) f = 1 - a;
      else {
        if (f === o) return n ? NaN : c ? -E : E;
        (n += S(2, e)), (f -= a);
      }
      return (c ? -1 : 1) * n * S(2, f - e);
    }
    function F(t) {
      return (t[3] << 24) | (t[2] << 16) | (t[1] << 8) | t[0];
    }
    function C(t) {
      return [255 & t];
    }
    function M(t) {
      return [255 & t, (t >> 8) & 255];
    }
    function B(t) {
      return [255 & t, (t >> 8) & 255, (t >> 16) & 255, (t >> 24) & 255];
    }
    function U(t) {
      return L(t, 52, 8);
    }
    function j(t) {
      return L(t, 23, 4);
    }
    function k(t, e, r) {
      v(t.prototype, e, {
        get: function () {
          return this[r];
        },
      });
    }
    function V(t, e, r, n) {
      var i = p(+r);
      if (i + e > t[P]) throw _("Wrong index!");
      var o = t[R]._b,
        a = i + t[N],
        s = o.slice(a, a + e);
      return n ? s : s.reverse();
    }
    function q(t, e, r, n, i, o) {
      var a = p(+r);
      if (a + e > t[P]) throw _("Wrong index!");
      for (var s = t[R]._b, u = a + t[N], c = n(+i), f = 0; f < e; f++)
        s[u + f] = c[o ? f : e - f - 1];
    }
    if (a.ABV) {
      if (
        !c(function () {
          m(1);
        }) ||
        !c(function () {
          new m(-1);
        }) ||
        c(function () {
          return new m(), new m(1.5), new m(NaN), "ArrayBuffer" != m.name;
        })
      ) {
        for (
          var Y,
            H = ((m = function (t) {
              return f(this, m), new T(p(t));
            }).prototype = T.prototype),
            G = d(T),
            $ = 0;
          G.length > $;

        )
          (Y = G[$++]) in m || s(m, Y, T[Y]);
        o || (H.constructor = m);
      }
      var W = new b(new m(2)),
        z = b.prototype.setInt8;
      W.setInt8(0, 2147483648),
        W.setInt8(1, 2147483649),
        (!W.getInt8(0) && W.getInt8(1)) ||
          u(
            b.prototype,
            {
              setInt8: function (t, e) {
                z.call(this, t, (e << 24) >> 24);
              },
              setUint8: function (t, e) {
                z.call(this, t, (e << 24) >> 24);
              },
            },
            !0
          );
    } else
      (m = function (t) {
        f(this, m, "ArrayBuffer");
        var e = p(t);
        (this._b = y.call(new Array(e), 0)), (this[P] = e);
      }),
        (b = function (t, e, r) {
          f(this, b, "DataView"), f(t, m, "DataView");
          var n = t[P],
            i = l(e);
          if (i < 0 || i > n) throw _("Wrong offset!");
          if (i + (r = void 0 === r ? n - i : h(r)) > n)
            throw _("Wrong length!");
          (this[R] = t), (this[N] = i), (this[P] = r);
        }),
        i &&
          (k(m, "byteLength", "_l"),
          k(b, "buffer", "_b"),
          k(b, "byteLength", "_l"),
          k(b, "byteOffset", "_o")),
        u(b.prototype, {
          getInt8: function (t) {
            return (V(this, 1, t)[0] << 24) >> 24;
          },
          getUint8: function (t) {
            return V(this, 1, t)[0];
          },
          getInt16: function (t) {
            var e = V(this, 2, t, arguments[1]);
            return (((e[1] << 8) | e[0]) << 16) >> 16;
          },
          getUint16: function (t) {
            var e = V(this, 2, t, arguments[1]);
            return (e[1] << 8) | e[0];
          },
          getInt32: function (t) {
            return F(V(this, 4, t, arguments[1]));
          },
          getUint32: function (t) {
            return F(V(this, 4, t, arguments[1])) >>> 0;
          },
          getFloat32: function (t) {
            return D(V(this, 4, t, arguments[1]), 23, 4);
          },
          getFloat64: function (t) {
            return D(V(this, 8, t, arguments[1]), 52, 8);
          },
          setInt8: function (t, e) {
            q(this, 1, t, C, e);
          },
          setUint8: function (t, e) {
            q(this, 1, t, C, e);
          },
          setInt16: function (t, e) {
            q(this, 2, t, M, e, arguments[2]);
          },
          setUint16: function (t, e) {
            q(this, 2, t, M, e, arguments[2]);
          },
          setInt32: function (t, e) {
            q(this, 4, t, B, e, arguments[2]);
          },
          setUint32: function (t, e) {
            q(this, 4, t, B, e, arguments[2]);
          },
          setFloat32: function (t, e) {
            q(this, 4, t, j, e, arguments[2]);
          },
          setFloat64: function (t, e) {
            q(this, 8, t, U, e, arguments[2]);
          },
        });
    g(m, "ArrayBuffer"),
      g(b, "DataView"),
      s(b.prototype, a.VIEW, !0),
      (e.ArrayBuffer = m),
      (e.DataView = b);
  },
  function (t, e) {
    var r = (t.exports =
      "undefined" != typeof window && window.Math == Math
        ? window
        : "undefined" != typeof self && self.Math == Math
        ? self
        : Function("return this")());
    "number" == typeof __g && (__g = r);
  },
  function (t, e) {
    t.exports = function (t) {
      return "object" == typeof t ? null !== t : "function" == typeof t;
    };
  },
  function (t, e, r) {
    t.exports = !r(125)(function () {
      return (
        7 !=
        Object.defineProperty({}, "a", {
          get: function () {
            return 7;
          },
        }).a
      );
    });
  },
  function (t, e) {
    new Set([429, 503]);
    const r = (t, e = {}) => {
      const r = { ...e };
      r.headers = { "Content-Type": "text/plain;charset=UTF-8", ...r.headers };
      const n = window.backWorker.fetch(t, r);
      return {
        on: (t, e) => {
          switch (t) {
            case "data":
              n.then(e);
              break;
            case "error":
              n.catch(e);
              break;
            case "end":
              n.finally(e);
              break;
            default:
              console.warn(
                "react-native-ytdl: miniget: unknown event listener received: " +
                  t
              );
          }
        },
        setEncoding: () => {
          console.warn(
            "react-native-ytdl: miniget: will not use specified encoding since request has already been made. Currently using utf8 encoding."
          );
        },
        text: () => n,
      };
    };
    (r.MinigetError = class extends Error {
      constructor(t) {
        super(t);
      }
    }),
      (r.defaultOptions = {
        maxRedirects: 10,
        maxRetries: 5,
        maxReconnects: 0,
        backoff: { inc: 100, max: 1e4 },
      }),
      (t.exports = r);
  },
  function (t, e, r) {
    t.exports =
      !r(8) &&
      !r(2)(function () {
        return (
          7 !=
          Object.defineProperty(r(64)("div"), "a", {
            get: function () {
              return 7;
            },
          }).a
        );
      });
  },
  function (t, e, r) {
    var n = r(1),
      i = r(7),
      o = r(30),
      a = r(65),
      s = r(9).f;
    t.exports = function (t) {
      var e = i.Symbol || (i.Symbol = o ? {} : n.Symbol || {});
      "_" == t.charAt(0) || t in e || s(e, t, { value: a.f(t) });
    };
  },
  function (t, e, r) {
    var n = r(13),
      i = r(15),
      o = r(49)(!1),
      a = r(66)("IE_PROTO");
    t.exports = function (t, e) {
      var r,
        s = i(t),
        u = 0,
        c = [];
      for (r in s) r != a && n(s, r) && c.push(r);
      for (; e.length > u; ) n(s, (r = e[u++])) && (~o(c, r) || c.push(r));
      return c;
    };
  },
  function (t, e, r) {
    var n = r(9),
      i = r(3),
      o = r(31);
    t.exports = r(8)
      ? Object.defineProperties
      : function (t, e) {
          i(t);
          for (var r, a = o(e), s = a.length, u = 0; s > u; )
            n.f(t, (r = a[u++]), e[r]);
          return t;
        };
  },
  function (t, e, r) {
    var n = r(15),
      i = r(34).f,
      o = {}.toString,
      a =
        "object" == typeof window && window && Object.getOwnPropertyNames
          ? Object.getOwnPropertyNames(window)
          : [];
    t.exports.f = function (t) {
      return a && "[object Window]" == o.call(t)
        ? (function (t) {
            try {
              return i(t);
            } catch (t) {
              return a.slice();
            }
          })(t)
        : i(n(t));
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(8),
      i = r(31),
      o = r(50),
      a = r(45),
      s = r(10),
      u = r(44),
      c = Object.assign;
    t.exports =
      !c ||
      r(2)(function () {
        var t = {},
          e = {},
          r = Symbol(),
          n = "abcdefghijklmnopqrst";
        return (
          (t[r] = 7),
          n.split("").forEach(function (t) {
            e[t] = t;
          }),
          7 != c({}, t)[r] || Object.keys(c({}, e)).join("") != n
        );
      })
        ? function (t, e) {
            for (
              var r = s(t), c = arguments.length, f = 1, l = o.f, h = a.f;
              c > f;

            )
              for (
                var p,
                  d = u(arguments[f++]),
                  v = l ? i(d).concat(l(d)) : i(d),
                  y = v.length,
                  g = 0;
                y > g;

              )
                (p = v[g++]), (n && !h.call(d, p)) || (r[p] = d[p]);
            return r;
          }
        : c;
  },
  function (t, e) {
    t.exports =
      Object.is ||
      function (t, e) {
        return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e;
      };
  },
  function (t, e, r) {
    "use strict";
    var n = r(18),
      i = r(4),
      o = r(101),
      a = [].slice,
      s = {},
      u = function (t, e, r) {
        if (!(e in s)) {
          for (var n = [], i = 0; i < e; i++) n[i] = "a[" + i + "]";
          s[e] = Function("F,a", "return new F(" + n.join(",") + ")");
        }
        return s[e](t, r);
      };
    t.exports =
      Function.bind ||
      function (t) {
        var e = n(this),
          r = a.call(arguments, 1),
          s = function () {
            var n = r.concat(a.call(arguments));
            return this instanceof s ? u(e, n.length, n) : o(e, n, t);
          };
        return i(e.prototype) && (s.prototype = e.prototype), s;
      };
  },
  function (t, e) {
    t.exports = function (t, e, r) {
      var n = void 0 === r;
      switch (e.length) {
        case 0:
          return n ? t() : t.call(r);
        case 1:
          return n ? t(e[0]) : t.call(r, e[0]);
        case 2:
          return n ? t(e[0], e[1]) : t.call(r, e[0], e[1]);
        case 3:
          return n ? t(e[0], e[1], e[2]) : t.call(r, e[0], e[1], e[2]);
        case 4:
          return n
            ? t(e[0], e[1], e[2], e[3])
            : t.call(r, e[0], e[1], e[2], e[3]);
      }
      return t.apply(r, e);
    };
  },
  function (t, e, r) {
    var n = r(1).parseInt,
      i = r(39).trim,
      o = r(70),
      a = /^[-+]?0[xX]/;
    t.exports =
      8 !== n(o + "08") || 22 !== n(o + "0x16")
        ? function (t, e) {
            var r = i(String(t), 3);
            return n(r, e >>> 0 || (a.test(r) ? 16 : 10));
          }
        : n;
  },
  function (t, e, r) {
    var n = r(1).parseFloat,
      i = r(39).trim;
    t.exports =
      1 / n(r(70) + "-0") != -1 / 0
        ? function (t) {
            var e = i(String(t), 3),
              r = n(e);
            return 0 === r && "-" == e.charAt(0) ? -0 : r;
          }
        : n;
  },
  function (t, e, r) {
    var n = r(23);
    t.exports = function (t, e) {
      if ("number" != typeof t && "Number" != n(t)) throw TypeError(e);
      return +t;
    };
  },
  function (t, e, r) {
    var n = r(4),
      i = Math.floor;
    t.exports = function (t) {
      return !n(t) && isFinite(t) && i(t) === t;
    };
  },
  function (t, e) {
    t.exports =
      Math.log1p ||
      function (t) {
        return (t = +t) > -1e-8 && t < 1e-8 ? t - (t * t) / 2 : Math.log(1 + t);
      };
  },
  function (t, e, r) {
    "use strict";
    var n = r(33),
      i = r(28),
      o = r(38),
      a = {};
    r(14)(a, r(5)("iterator"), function () {
      return this;
    }),
      (t.exports = function (t, e, r) {
        (t.prototype = n(a, { next: i(1, r) })), o(t, e + " Iterator");
      });
  },
  function (t, e, r) {
    var n = r(3);
    t.exports = function (t, e, r, i) {
      try {
        return i ? e(n(r)[0], r[1]) : e(r);
      } catch (e) {
        var o = t.return;
        throw (void 0 !== o && n(o.call(t)), e);
      }
    };
  },
  function (t, e, r) {
    var n = r(226);
    t.exports = function (t, e) {
      return new (n(t))(e);
    };
  },
  function (t, e, r) {
    var n = r(18),
      i = r(10),
      o = r(44),
      a = r(6);
    t.exports = function (t, e, r, s, u) {
      n(e);
      var c = i(t),
        f = o(c),
        l = a(c.length),
        h = u ? l - 1 : 0,
        p = u ? -1 : 1;
      if (r < 2)
        for (;;) {
          if (h in f) {
            (s = f[h]), (h += p);
            break;
          }
          if (((h += p), u ? h < 0 : l <= h))
            throw TypeError("Reduce of empty array with no initial value");
        }
      for (; u ? h >= 0 : l > h; h += p) h in f && (s = e(s, f[h], h, c));
      return s;
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(10),
      i = r(32),
      o = r(6);
    t.exports =
      [].copyWithin ||
      function (t, e) {
        var r = n(this),
          a = o(r.length),
          s = i(t, a),
          u = i(e, a),
          c = arguments.length > 2 ? arguments[2] : void 0,
          f = Math.min((void 0 === c ? a : i(c, a)) - u, a - s),
          l = 1;
        for (
          u < s && s < u + f && ((l = -1), (u += f - 1), (s += f - 1));
          f-- > 0;

        )
          u in r ? (r[s] = r[u]) : delete r[s], (s += l), (u += l);
        return r;
      };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return { value: e, done: !!t };
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(85);
    r(0)({ target: "RegExp", proto: !0, forced: n !== /./.exec }, { exec: n });
  },
  function (t, e, r) {
    r(8) &&
      "g" != /./g.flags &&
      r(9).f(RegExp.prototype, "flags", { configurable: !0, get: r(53) });
  },
  function (t, e, r) {
    "use strict";
    var n,
      i,
      o,
      a,
      s = r(30),
      u = r(1),
      c = r(17),
      f = r(46),
      l = r(0),
      h = r(4),
      p = r(18),
      d = r(42),
      v = r(56),
      y = r(47),
      g = r(87).set,
      m = r(246)(),
      b = r(116),
      w = r(247),
      _ = r(57),
      E = r(117),
      T = u.TypeError,
      x = u.process,
      S = x && x.versions,
      A = (S && S.v8) || "",
      I = u.Promise,
      O = "process" == f(x),
      R = function () {},
      P = (i = b.f),
      N = !!(function () {
        try {
          var t = I.resolve(1),
            e = ((t.constructor = {})[r(5)("species")] = function (t) {
              t(R, R);
            });
          return (
            (O || "function" == typeof PromiseRejectionEvent) &&
            t.then(R) instanceof e &&
            0 !== A.indexOf("6.6") &&
            -1 === _.indexOf("Chrome/66")
          );
        } catch (t) {}
      })(),
      L = function (t) {
        var e;
        return !(!h(t) || "function" != typeof (e = t.then)) && e;
      },
      D = function (t, e) {
        if (!t._n) {
          t._n = !0;
          var r = t._c;
          m(function () {
            for (
              var n = t._v,
                i = 1 == t._s,
                o = 0,
                a = function (e) {
                  var r,
                    o,
                    a,
                    s = i ? e.ok : e.fail,
                    u = e.resolve,
                    c = e.reject,
                    f = e.domain;
                  try {
                    s
                      ? (i || (2 == t._h && M(t), (t._h = 1)),
                        !0 === s
                          ? (r = n)
                          : (f && f.enter(),
                            (r = s(n)),
                            f && (f.exit(), (a = !0))),
                        r === e.promise
                          ? c(T("Promise-chain cycle"))
                          : (o = L(r))
                          ? o.call(r, u, c)
                          : u(r))
                      : c(n);
                  } catch (t) {
                    f && !a && f.exit(), c(t);
                  }
                };
              r.length > o;

            )
              a(r[o++]);
            (t._c = []), (t._n = !1), e && !t._h && F(t);
          });
        }
      },
      F = function (t) {
        g.call(u, function () {
          var e,
            r,
            n,
            i = t._v,
            o = C(t);
          if (
            (o &&
              ((e = w(function () {
                O
                  ? x.emit("unhandledRejection", i, t)
                  : (r = u.onunhandledrejection)
                  ? r({ promise: t, reason: i })
                  : (n = u.console) &&
                    n.error &&
                    n.error("Unhandled promise rejection", i);
              })),
              (t._h = O || C(t) ? 2 : 1)),
            (t._a = void 0),
            o && e.e)
          )
            throw e.v;
        });
      },
      C = function (t) {
        return 1 !== t._h && 0 === (t._a || t._c).length;
      },
      M = function (t) {
        g.call(u, function () {
          var e;
          O
            ? x.emit("rejectionHandled", t)
            : (e = u.onrejectionhandled) && e({ promise: t, reason: t._v });
        });
      },
      B = function (t) {
        var e = this;
        e._d ||
          ((e._d = !0),
          ((e = e._w || e)._v = t),
          (e._s = 2),
          e._a || (e._a = e._c.slice()),
          D(e, !0));
      },
      U = function (t) {
        var e,
          r = this;
        if (!r._d) {
          (r._d = !0), (r = r._w || r);
          try {
            if (r === t) throw T("Promise can't be resolved itself");
            (e = L(t))
              ? m(function () {
                  var n = { _w: r, _d: !1 };
                  try {
                    e.call(t, c(U, n, 1), c(B, n, 1));
                  } catch (t) {
                    B.call(n, t);
                  }
                })
              : ((r._v = t), (r._s = 1), D(r, !1));
          } catch (t) {
            B.call({ _w: r, _d: !1 }, t);
          }
        }
      };
    N ||
      ((I = function (t) {
        d(this, I, "Promise", "_h"), p(t), n.call(this);
        try {
          t(c(U, this, 1), c(B, this, 1));
        } catch (t) {
          B.call(this, t);
        }
      }),
      ((n = function (t) {
        (this._c = []),
          (this._a = void 0),
          (this._s = 0),
          (this._d = !1),
          (this._v = void 0),
          (this._h = 0),
          (this._n = !1);
      }).prototype = r(43)(I.prototype, {
        then: function (t, e) {
          var r = P(y(this, I));
          return (
            (r.ok = "function" != typeof t || t),
            (r.fail = "function" == typeof e && e),
            (r.domain = O ? x.domain : void 0),
            this._c.push(r),
            this._a && this._a.push(r),
            this._s && D(this, !1),
            r.promise
          );
        },
        catch: function (t) {
          return this.then(void 0, t);
        },
      })),
      (o = function () {
        var t = new n();
        (this.promise = t),
          (this.resolve = c(U, t, 1)),
          (this.reject = c(B, t, 1));
      }),
      (b.f = P =
        function (t) {
          return t === I || t === a ? new o(t) : i(t);
        })),
      l(l.G + l.W + l.F * !N, { Promise: I }),
      r(38)(I, "Promise"),
      r(41)("Promise"),
      (a = r(7).Promise),
      l(l.S + l.F * !N, "Promise", {
        reject: function (t) {
          var e = P(this);
          return (0, e.reject)(t), e.promise;
        },
      }),
      l(l.S + l.F * (s || !N), "Promise", {
        resolve: function (t) {
          return E(s && this === a ? I : this, t);
        },
      }),
      l(
        l.S +
          l.F *
            !(
              N &&
              r(52)(function (t) {
                I.all(t).catch(R);
              })
            ),
        "Promise",
        {
          all: function (t) {
            var e = this,
              r = P(e),
              n = r.resolve,
              i = r.reject,
              o = w(function () {
                var r = [],
                  o = 0,
                  a = 1;
                v(t, !1, function (t) {
                  var s = o++,
                    u = !1;
                  r.push(void 0),
                    a++,
                    e.resolve(t).then(function (t) {
                      u || ((u = !0), (r[s] = t), --a || n(r));
                    }, i);
                }),
                  --a || n(r);
              });
            return o.e && i(o.v), r.promise;
          },
          race: function (t) {
            var e = this,
              r = P(e),
              n = r.reject,
              i = w(function () {
                v(t, !1, function (t) {
                  e.resolve(t).then(r.resolve, n);
                });
              });
            return i.e && n(i.v), r.promise;
          },
        }
      );
  },
  function (t, e, r) {
    "use strict";
    var n = r(18);
    function i(t) {
      var e, r;
      (this.promise = new t(function (t, n) {
        if (void 0 !== e || void 0 !== r)
          throw TypeError("Bad Promise constructor");
        (e = t), (r = n);
      })),
        (this.resolve = n(e)),
        (this.reject = n(r));
    }
    t.exports.f = function (t) {
      return new i(t);
    };
  },
  function (t, e, r) {
    var n = r(3),
      i = r(4),
      o = r(116);
    t.exports = function (t, e) {
      if ((n(t), i(e) && e.constructor === t)) return e;
      var r = o.f(t);
      return (0, r.resolve)(e), r.promise;
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(9).f,
      i = r(33),
      o = r(43),
      a = r(17),
      s = r(42),
      u = r(56),
      c = r(76),
      f = r(112),
      l = r(41),
      h = r(8),
      p = r(27).fastKey,
      d = r(37),
      v = h ? "_s" : "size",
      y = function (t, e) {
        var r,
          n = p(e);
        if ("F" !== n) return t._i[n];
        for (r = t._f; r; r = r.n) if (r.k == e) return r;
      };
    t.exports = {
      getConstructor: function (t, e, r, c) {
        var f = t(function (t, n) {
          s(t, f, e, "_i"),
            (t._t = e),
            (t._i = i(null)),
            (t._f = void 0),
            (t._l = void 0),
            (t[v] = 0),
            null != n && u(n, r, t[c], t);
        });
        return (
          o(f.prototype, {
            clear: function () {
              for (var t = d(this, e), r = t._i, n = t._f; n; n = n.n)
                (n.r = !0), n.p && (n.p = n.p.n = void 0), delete r[n.i];
              (t._f = t._l = void 0), (t[v] = 0);
            },
            delete: function (t) {
              var r = d(this, e),
                n = y(r, t);
              if (n) {
                var i = n.n,
                  o = n.p;
                delete r._i[n.i],
                  (n.r = !0),
                  o && (o.n = i),
                  i && (i.p = o),
                  r._f == n && (r._f = i),
                  r._l == n && (r._l = o),
                  r[v]--;
              }
              return !!n;
            },
            forEach: function (t) {
              d(this, e);
              for (
                var r,
                  n = a(t, arguments.length > 1 ? arguments[1] : void 0, 3);
                (r = r ? r.n : this._f);

              )
                for (n(r.v, r.k, this); r && r.r; ) r = r.p;
            },
            has: function (t) {
              return !!y(d(this, e), t);
            },
          }),
          h &&
            n(f.prototype, "size", {
              get: function () {
                return d(this, e)[v];
              },
            }),
          f
        );
      },
      def: function (t, e, r) {
        var n,
          i,
          o = y(t, e);
        return (
          o
            ? (o.v = r)
            : ((t._l = o =
                {
                  i: (i = p(e, !0)),
                  k: e,
                  v: r,
                  p: (n = t._l),
                  n: void 0,
                  r: !1,
                }),
              t._f || (t._f = o),
              n && (n.n = o),
              t[v]++,
              "F" !== i && (t._i[i] = o)),
          t
        );
      },
      getEntry: y,
      setStrong: function (t, e, r) {
        c(
          t,
          e,
          function (t, r) {
            (this._t = d(t, e)), (this._k = r), (this._l = void 0);
          },
          function () {
            for (var t = this._k, e = this._l; e && e.r; ) e = e.p;
            return this._t && (this._l = e = e ? e.n : this._t._f)
              ? f(0, "keys" == t ? e.k : "values" == t ? e.v : [e.k, e.v])
              : ((this._t = void 0), f(1));
          },
          r ? "entries" : "values",
          !r,
          !0
        ),
          l(e);
      },
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(43),
      i = r(27).getWeak,
      o = r(3),
      a = r(4),
      s = r(42),
      u = r(56),
      c = r(22),
      f = r(13),
      l = r(37),
      h = c(5),
      p = c(6),
      d = 0,
      v = function (t) {
        return t._l || (t._l = new y());
      },
      y = function () {
        this.a = [];
      },
      g = function (t, e) {
        return h(t.a, function (t) {
          return t[0] === e;
        });
      };
    (y.prototype = {
      get: function (t) {
        var e = g(this, t);
        if (e) return e[1];
      },
      has: function (t) {
        return !!g(this, t);
      },
      set: function (t, e) {
        var r = g(this, t);
        r ? (r[1] = e) : this.a.push([t, e]);
      },
      delete: function (t) {
        var e = p(this.a, function (e) {
          return e[0] === t;
        });
        return ~e && this.a.splice(e, 1), !!~e;
      },
    }),
      (t.exports = {
        getConstructor: function (t, e, r, o) {
          var c = t(function (t, n) {
            s(t, c, e, "_i"),
              (t._t = e),
              (t._i = d++),
              (t._l = void 0),
              null != n && u(n, r, t[o], t);
          });
          return (
            n(c.prototype, {
              delete: function (t) {
                if (!a(t)) return !1;
                var r = i(t);
                return !0 === r
                  ? v(l(this, e)).delete(t)
                  : r && f(r, this._i) && delete r[this._i];
              },
              has: function (t) {
                if (!a(t)) return !1;
                var r = i(t);
                return !0 === r ? v(l(this, e)).has(t) : r && f(r, this._i);
              },
            }),
            c
          );
        },
        def: function (t, e, r) {
          var n = i(o(e), !0);
          return !0 === n ? v(t).set(e, r) : (n[t._i] = r), t;
        },
        ufstore: v,
      });
  },
  function (t, e, r) {
    var n = r(19),
      i = r(6);
    t.exports = function (t) {
      if (void 0 === t) return 0;
      var e = n(t),
        r = i(e);
      if (e !== r) throw RangeError("Wrong length!");
      return r;
    };
  },
  function (t, e, r) {
    var n = r(34),
      i = r(50),
      o = r(3),
      a = r(1).Reflect;
    t.exports =
      (a && a.ownKeys) ||
      function (t) {
        var e = n.f(o(t)),
          r = i.f;
        return r ? e.concat(r(t)) : e;
      };
  },
  function (t, e, r) {
    var n = r(6),
      i = r(72),
      o = r(24);
    t.exports = function (t, e, r, a) {
      var s = String(o(t)),
        u = s.length,
        c = void 0 === r ? " " : String(r),
        f = n(e);
      if (f <= u || "" == c) return s;
      var l = f - u,
        h = i.call(c, Math.ceil(l / c.length));
      return h.length > l && (h = h.slice(0, l)), a ? h + s : s + h;
    };
  },
  function (t, e, r) {
    var n = r(8),
      i = r(31),
      o = r(15),
      a = r(45).f;
    t.exports = function (t) {
      return function (e) {
        for (var r, s = o(e), u = i(s), c = u.length, f = 0, l = []; c > f; )
          (r = u[f++]), (n && !a.call(s, r)) || l.push(t ? [r, s[r]] : s[r]);
        return l;
      };
    };
  },
  function (t, e) {
    var r = (t.exports = { version: "2.6.12" });
    "number" == typeof __e && (__e = r);
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return !!t();
      } catch (t) {
        return !0;
      }
    };
  },
  function (t, e, r) {
    (function (e) {
      const r = {
        setTimeout: function () {
          const t = e.setTimeout(...arguments),
            r = { unref: () => t };
          return r;
        },
      };
      t.exports = r;
    }).call(this, r(60));
  },
  function (t, e, r) {
    const n = r(63),
      i = r(333),
      o = ["mp4a", "mp3", "vorbis", "aac", "opus", "flac"],
      a = [
        "mp4v",
        "avc1",
        "Sorenson H.283",
        "MPEG-4 Visual",
        "VP8",
        "VP9",
        "H.264",
      ],
      s = (t) => t.bitrate || 0,
      u = (t) => a.findIndex((e) => t.codecs && t.codecs.includes(e)),
      c = (t) => t.audioBitrate || 0,
      f = (t) => o.findIndex((e) => t.codecs && t.codecs.includes(e)),
      l = (t, e, r) => {
        let n = 0;
        for (let i of r) if (((n = i(e) - i(t)), 0 !== n)) break;
        return n;
      },
      h = (t, e) => l(t, e, [(t) => parseInt(t.qualityLabel), s, u]),
      p = (t, e) => l(t, e, [c, f]);
    (e.sortFormats = (t, e) =>
      l(t, e, [
        (t) => +!!t.isHLS,
        (t) => +!!t.isDashMPD,
        (t) => +(t.contentLength > 0),
        (t) => +(t.hasVideo && t.hasAudio),
        (t) => +t.hasVideo,
        (t) => parseInt(t.qualityLabel) || 0,
        s,
        c,
        u,
        f,
      ])),
      (e.chooseFormat = (t, r) => {
        if ("object" == typeof r.format) {
          if (!r.format.url)
            throw Error("Invalid format given, did you use `ytdl.getInfo()`?");
          return r.format;
        }
        let n;
        r.filter && (t = e.filterFormats(t, r.filter));
        const i = r.quality || "highest";
        switch (i) {
          case "highest":
            n = t[0];
            break;
          case "lowest":
            n = t[t.length - 1];
            break;
          case "highestaudio":
            (t = e.filterFormats(t, "audio")).sort(p), (n = t[0]);
            break;
          case "lowestaudio":
            (t = e.filterFormats(t, "audio")).sort(p), (n = t[t.length - 1]);
            break;
          case "highestvideo":
            (t = e.filterFormats(t, "video")).sort(h), (n = t[0]);
            break;
          case "lowestvideo":
            (t = e.filterFormats(t, "video")).sort(h), (n = t[t.length - 1]);
            break;
          default:
            n = d(i, t);
        }
        if (!n) throw Error("No such format found: " + i);
        return n;
      });
    const d = (t, e) => {
      let r = (t) => e.find((e) => "" + e.itag == "" + t);
      return Array.isArray(t) ? r(t.find((t) => r(t))) : r(t);
    };
    (e.filterFormats = (t, e) => {
      let r;
      switch (e) {
        case "videoandaudio":
        case "audioandvideo":
          r = (t) => t.hasVideo && t.hasAudio;
          break;
        case "video":
          r = (t) => t.hasVideo;
          break;
        case "videoonly":
          r = (t) => t.hasVideo && !t.hasAudio;
          break;
        case "audio":
          r = (t) => t.hasAudio;
          break;
        case "audioonly":
          r = (t) => !t.hasVideo && t.hasAudio;
          break;
        default:
          if ("function" != typeof e)
            throw TypeError(`Given filter (${e}) is not supported`);
          r = e;
      }
      return t.filter((t) => !!t.url && r(t));
    }),
      (e.addFormatMeta = (t) => (
        ((t = Object.assign({}, i[t.itag], t)).hasVideo = !!t.qualityLabel),
        (t.hasAudio = !!t.audioBitrate),
        (t.container = t.mimeType
          ? t.mimeType.split(";")[0].split("/")[1]
          : null),
        (t.codecs = t.mimeType ? n.between(t.mimeType, 'codecs="', '"') : null),
        (t.videoCodec =
          t.hasVideo && t.codecs ? t.codecs.split(", ")[0] : null),
        (t.audioCodec =
          t.hasAudio && t.codecs ? t.codecs.split(", ").slice(-1)[0] : null),
        (t.isLive = /\bsource[/=]yt_live_broadcast\b/.test(t.url)),
        (t.isHLS = /\/manifest\/hls_(variant|playlist)\//.test(t.url)),
        (t.isDashMPD = /\/manifest\/dash\//.test(t.url)),
        t
      ));
  },
  function (t, e, r) {
    const n = r(61),
      i = new Set([
        "youtube.com",
        "www.youtube.com",
        "m.youtube.com",
        "music.youtube.com",
        "gaming.youtube.com",
      ]),
      o = /^https?:\/\/(youtu\.be\/|(www\.)?youtube.com\/(embed|v)\/)/;
    (e.getURLVideoID = (t) => {
      const r = n.parse(t, !0);
      let s = r.query.v;
      if (o.test(t) && !s) {
        const t = r.pathname.split("/");
        s = t[t.length - 1];
      } else if (r.hostname && !i.has(r.hostname))
        throw Error("Not a YouTube domain");
      if (!s) throw Error("No video id found: " + t);
      if (((s = s.substring(0, 11)), !e.validateID(s)))
        throw TypeError(
          `Video id (${s}) does not match expected format (${a.toString()})`
        );
      return s;
    }),
      (e.getVideoID = (t) => (e.validateID(t) ? t : e.getURLVideoID(t)));
    const a = /^[a-zA-Z0-9-_]{11}$/;
    (e.validateID = (t) => a.test(t)),
      (e.validateURL = (t) => {
        try {
          return e.getURLVideoID(t), !0;
        } catch (t) {
          return console.log("-------validateURL--------err", t), !1;
        }
      });
  },
  function (t, e) {
    e.parseTimestamp = (t) => {
      const e = { ms: 1, s: 1e3, m: 6e4, h: 36e5 };
      if ("number" == typeof t) return t;
      if (/^\d+$/.test(t)) return +t;
      const r = /^(?:(?:(\d+):)?(\d{1,2}):)?(\d{1,2})(?:\.(\d{3}))?$/.exec(t);
      if (r)
        return (
          +(r[1] || 0) * e.h + +(r[2] || 0) * e.m + +r[3] * e.s + +(r[4] || 0)
        );
      {
        let r = 0;
        const n = /(-?\d+)(ms|s|m|h)/g;
        let i;
        for (; null != (i = n.exec(t)); ) r += +i[1] * e[i[2]];
        return r;
      }
    };
  },
  function (t, e, r) {
    const n = r(61),
      i = r(92),
      o = r(62),
      a = r(131);
    (e.cache = new a()),
      (e.getTokens = (t, r) =>
        e.cache.getOrSet(t, async () => {
          let n = await i(t, r.requestOptions).text();
          const o = e.extractActions(n);
          if (!o || !o.length)
            throw Error("Could not extract signature deciphering actions");
          return e.cache.set(t, o), o;
        })),
      (e.decipher = (t, e) => {
        e = e.split("");
        for (let r = 0, n = t.length; r < n; r++) {
          let n,
            i = t[r];
          switch (i[0]) {
            case "r":
              e = e.reverse();
              break;
            case "w":
              (n = ~~i.slice(1)), (e = s(e, n));
              break;
            case "s":
              (n = ~~i.slice(1)), (e = e.slice(n));
              break;
            case "p":
              (n = ~~i.slice(1)), e.splice(0, n);
          }
        }
        return e.join("");
      });
    const s = (t, e) => {
        const r = t[0];
        return (t[0] = t[e % t.length]), (t[e] = r), t;
      },
      u = "[a-zA-Z_\\$][a-zA-Z_0-9]*",
      c =
        "(?:'[^'\\\\]*(:?\\\\[\\s\\S][^'\\\\]*)*'|\"[^\"\\\\]*(:?\\\\[\\s\\S][^\"\\\\]*)*\")",
      f = `(?:${u}|${c})`,
      l = `(?:\\.${u}|\\[${c}\\])`,
      h = ":function\\(a\\)\\{(?:return )?a\\.reverse\\(\\)\\}",
      p = ":function\\(a,b\\)\\{return a\\.slice\\(b\\)\\}",
      d = ":function\\(a,b\\)\\{a\\.splice\\(0,b\\)\\}",
      v =
        ":function\\(a,b\\)\\{var c=a\\[0\\];a\\[0\\]=a\\[b(?:%a\\.length)?\\];a\\[b(?:%a\\.length)?\\]=c(?:;return a)?\\}",
      y = new RegExp(
        `var (${u})=\\{((?:(?:${f}${h}|${f}${p}|${f}${d}|${f}${v}),?\\r?\\n?)+)\\};`
      ),
      g = new RegExp(
        `function(?: ${u})?\\(a\\)\\{a=a\\.split\\((?:''|"")\\);\\s*((?:(?:a=)?[a-zA-Z_\\$][a-zA-Z_0-9]*${l}\\(a,\\d+\\);)+)return a\\.join\\((?:''|"")\\)\\}`
      ),
      m = new RegExp(`(?:^|,)(${f})${h}`, "m"),
      b = new RegExp(`(?:^|,)(${f})${p}`, "m"),
      w = new RegExp(`(?:^|,)(${f})${d}`, "m"),
      _ = new RegExp(`(?:^|,)(${f})${v}`, "m");
    (e.extractActions = (t) => {
      const e = y.exec(t),
        r = g.exec(t);
      if (!e || !r) return null;
      const n = e[1].replace(/\$/g, "\\$"),
        i = e[2].replace(/\$/g, "\\$"),
        o = r[1].replace(/\$/g, "\\$");
      let a = m.exec(i);
      const s = a && a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, "");
      a = b.exec(i);
      const u = a && a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, "");
      a = w.exec(i);
      const c = a && a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, "");
      a = _.exec(i);
      const f = a && a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, ""),
        l = `(${[s, u, c, f].join("|")})`,
        h = new RegExp(
          `(?:a=)?${n}(?:\\.${l}|\\['${l}'\\]|\\["${l}"\\])\\(a,(\\d+)\\)`,
          "g"
        ),
        p = [];
      for (; null !== (a = h.exec(o)); ) {
        switch (a[1] || a[2] || a[3]) {
          case f:
            p.push("w" + a[4]);
            break;
          case s:
            p.push("r");
            break;
          case u:
            p.push("s" + a[4]);
            break;
          case c:
            p.push("p" + a[4]);
        }
      }
      return p;
    }),
      (e.setDownloadURL = (t, e) => {
        let r;
        if (!t.url) return;
        r = t.url;
        try {
          r = decodeURIComponent(r);
        } catch (t) {
          return void console.log("-------setDownloadURL--------err", t);
        }
        const i = n.parse(r, !0);
        delete i.search;
        let o = i.query;
        (o.ratebypass = "yes"),
          e && (o[t.sp || "signature"] = e),
          (t.url = n.format(i));
      }),
      (e.decipherFormats = async (t, r, n) => {
        let i = {},
          a = await e.getTokens(r, n);
        return (
          t.forEach((t) => {
            let r = t.signatureCipher || t.cipher;
            r &&
              (Object.assign(t, o.parse(r)),
              delete t.signatureCipher,
              delete t.cipher);
            const n = a && t.s ? e.decipher(a, t.s) : null;
            e.setDownloadURL(t, n), (i[t.url] = t);
          }),
          i
        );
      });
  },
  function (t, e, r) {
    const { setTimeout: n } = r(126);
    t.exports = class extends Map {
      constructor(t = 1e3) {
        super(), (this.timeout = t);
      }
      set(t, e) {
        this.has(t) && clearTimeout(super.get(t).tid),
          super.set(t, {
            tid: n(this.delete.bind(this, t), this.timeout).unref(),
            value: e,
          });
      }
      get(t) {
        let e = super.get(t);
        return e ? e.value : null;
      }
      getOrSet(t, e) {
        if (this.has(t)) return this.get(t);
        {
          let r = e();
          return (
            this.set(t, r),
            (async () => {
              try {
                await r;
              } catch (e) {
                this.delete(t);
              }
            })(),
            r
          );
        }
      }
      delete(t) {
        let e = super.get(t);
        e && (clearTimeout(e.tid), super.delete(t));
      }
      clear() {
        for (let t of this.values()) clearTimeout(t.tid);
        super.clear();
      }
    };
  },
  function (t, e, r) {
    "use strict";
    r(133);
    var n,
      i = (n = r(305)) && n.__esModule ? n : { default: n };
    i.default._babelPolyfill &&
      "undefined" != typeof console &&
      console.warn &&
      console.warn(
        "@babel/polyfill is loaded more than once on this page. This is probably not desirable/intended and may have consequences if different versions of the polyfills are applied sequentially. If you do need to load the polyfill more than once, use @babel/polyfill/noConflict instead to bypass the warning."
      ),
      (i.default._babelPolyfill = !0);
  },
  function (t, e, r) {
    "use strict";
    r(134),
      r(277),
      r(279),
      r(282),
      r(284),
      r(286),
      r(288),
      r(290),
      r(292),
      r(294),
      r(296),
      r(298),
      r(300),
      r(304);
  },
  function (t, e, r) {
    r(135),
      r(138),
      r(139),
      r(140),
      r(141),
      r(142),
      r(143),
      r(144),
      r(145),
      r(146),
      r(147),
      r(148),
      r(149),
      r(150),
      r(151),
      r(152),
      r(153),
      r(154),
      r(155),
      r(156),
      r(157),
      r(158),
      r(159),
      r(160),
      r(161),
      r(162),
      r(163),
      r(164),
      r(165),
      r(166),
      r(167),
      r(168),
      r(169),
      r(170),
      r(171),
      r(172),
      r(173),
      r(174),
      r(175),
      r(176),
      r(177),
      r(178),
      r(179),
      r(181),
      r(182),
      r(183),
      r(184),
      r(185),
      r(186),
      r(187),
      r(188),
      r(189),
      r(190),
      r(191),
      r(192),
      r(193),
      r(194),
      r(195),
      r(196),
      r(197),
      r(198),
      r(199),
      r(200),
      r(201),
      r(202),
      r(203),
      r(204),
      r(205),
      r(206),
      r(207),
      r(208),
      r(209),
      r(210),
      r(211),
      r(212),
      r(213),
      r(214),
      r(216),
      r(217),
      r(219),
      r(220),
      r(221),
      r(222),
      r(223),
      r(224),
      r(225),
      r(227),
      r(228),
      r(229),
      r(230),
      r(231),
      r(232),
      r(233),
      r(234),
      r(235),
      r(236),
      r(237),
      r(238),
      r(239),
      r(84),
      r(240),
      r(113),
      r(241),
      r(114),
      r(242),
      r(243),
      r(244),
      r(245),
      r(115),
      r(248),
      r(249),
      r(250),
      r(251),
      r(252),
      r(253),
      r(254),
      r(255),
      r(256),
      r(257),
      r(258),
      r(259),
      r(260),
      r(261),
      r(262),
      r(263),
      r(264),
      r(265),
      r(266),
      r(267),
      r(268),
      r(269),
      r(270),
      r(271),
      r(272),
      r(273),
      r(274),
      r(275),
      r(276),
      (t.exports = r(7));
  },
  function (t, e, r) {
    "use strict";
    var n = r(1),
      i = r(13),
      o = r(8),
      a = r(0),
      s = r(11),
      u = r(27).KEY,
      c = r(2),
      f = r(48),
      l = r(38),
      h = r(29),
      p = r(5),
      d = r(65),
      v = r(94),
      y = r(137),
      g = r(51),
      m = r(3),
      b = r(4),
      w = r(10),
      _ = r(15),
      E = r(26),
      T = r(28),
      x = r(33),
      S = r(97),
      A = r(20),
      I = r(50),
      O = r(9),
      R = r(31),
      P = A.f,
      N = O.f,
      L = S.f,
      D = n.Symbol,
      F = n.JSON,
      C = F && F.stringify,
      M = p("_hidden"),
      B = p("toPrimitive"),
      U = {}.propertyIsEnumerable,
      j = f("symbol-registry"),
      k = f("symbols"),
      V = f("op-symbols"),
      q = Object.prototype,
      Y = "function" == typeof D && !!I.f,
      H = n.QObject,
      G = !H || !H.prototype || !H.prototype.findChild,
      $ =
        o &&
        c(function () {
          return (
            7 !=
            x(
              N({}, "a", {
                get: function () {
                  return N(this, "a", { value: 7 }).a;
                },
              })
            ).a
          );
        })
          ? function (t, e, r) {
              var n = P(q, e);
              n && delete q[e], N(t, e, r), n && t !== q && N(q, e, n);
            }
          : N,
      W = function (t) {
        var e = (k[t] = x(D.prototype));
        return (e._k = t), e;
      },
      z =
        Y && "symbol" == typeof D.iterator
          ? function (t) {
              return "symbol" == typeof t;
            }
          : function (t) {
              return t instanceof D;
            },
      X = function (t, e, r) {
        return (
          t === q && X(V, e, r),
          m(t),
          (e = E(e, !0)),
          m(r),
          i(k, e)
            ? (r.enumerable
                ? (i(t, M) && t[M][e] && (t[M][e] = !1),
                  (r = x(r, { enumerable: T(0, !1) })))
                : (i(t, M) || N(t, M, T(1, {})), (t[M][e] = !0)),
              $(t, e, r))
            : N(t, e, r)
        );
      },
      Q = function (t, e) {
        m(t);
        for (var r, n = y((e = _(e))), i = 0, o = n.length; o > i; )
          X(t, (r = n[i++]), e[r]);
        return t;
      },
      K = function (t) {
        var e = U.call(this, (t = E(t, !0)));
        return (
          !(this === q && i(k, t) && !i(V, t)) &&
          (!(e || !i(this, t) || !i(k, t) || (i(this, M) && this[M][t])) || e)
        );
      },
      J = function (t, e) {
        if (((t = _(t)), (e = E(e, !0)), t !== q || !i(k, e) || i(V, e))) {
          var r = P(t, e);
          return (
            !r || !i(k, e) || (i(t, M) && t[M][e]) || (r.enumerable = !0), r
          );
        }
      },
      Z = function (t) {
        for (var e, r = L(_(t)), n = [], o = 0; r.length > o; )
          i(k, (e = r[o++])) || e == M || e == u || n.push(e);
        return n;
      },
      tt = function (t) {
        for (
          var e, r = t === q, n = L(r ? V : _(t)), o = [], a = 0;
          n.length > a;

        )
          !i(k, (e = n[a++])) || (r && !i(q, e)) || o.push(k[e]);
        return o;
      };
    Y ||
      (s(
        (D = function () {
          if (this instanceof D)
            throw TypeError("Symbol is not a constructor!");
          var t = h(arguments.length > 0 ? arguments[0] : void 0),
            e = function (r) {
              this === q && e.call(V, r),
                i(this, M) && i(this[M], t) && (this[M][t] = !1),
                $(this, t, T(1, r));
            };
          return o && G && $(q, t, { configurable: !0, set: e }), W(t);
        }).prototype,
        "toString",
        function () {
          return this._k;
        }
      ),
      (A.f = J),
      (O.f = X),
      (r(34).f = S.f = Z),
      (r(45).f = K),
      (I.f = tt),
      o && !r(30) && s(q, "propertyIsEnumerable", K, !0),
      (d.f = function (t) {
        return W(p(t));
      })),
      a(a.G + a.W + a.F * !Y, { Symbol: D });
    for (
      var et =
          "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(
            ","
          ),
        rt = 0;
      et.length > rt;

    )
      p(et[rt++]);
    for (var nt = R(p.store), it = 0; nt.length > it; ) v(nt[it++]);
    a(a.S + a.F * !Y, "Symbol", {
      for: function (t) {
        return i(j, (t += "")) ? j[t] : (j[t] = D(t));
      },
      keyFor: function (t) {
        if (!z(t)) throw TypeError(t + " is not a symbol!");
        for (var e in j) if (j[e] === t) return e;
      },
      useSetter: function () {
        G = !0;
      },
      useSimple: function () {
        G = !1;
      },
    }),
      a(a.S + a.F * !Y, "Object", {
        create: function (t, e) {
          return void 0 === e ? x(t) : Q(x(t), e);
        },
        defineProperty: X,
        defineProperties: Q,
        getOwnPropertyDescriptor: J,
        getOwnPropertyNames: Z,
        getOwnPropertySymbols: tt,
      });
    var ot = c(function () {
      I.f(1);
    });
    a(a.S + a.F * ot, "Object", {
      getOwnPropertySymbols: function (t) {
        return I.f(w(t));
      },
    }),
      F &&
        a(
          a.S +
            a.F *
              (!Y ||
                c(function () {
                  var t = D();
                  return (
                    "[null]" != C([t]) ||
                    "{}" != C({ a: t }) ||
                    "{}" != C(Object(t))
                  );
                })),
          "JSON",
          {
            stringify: function (t) {
              for (var e, r, n = [t], i = 1; arguments.length > i; )
                n.push(arguments[i++]);
              if (((r = e = n[1]), (b(e) || void 0 !== t) && !z(t)))
                return (
                  g(e) ||
                    (e = function (t, e) {
                      if (
                        ("function" == typeof r && (e = r.call(this, t, e)),
                        !z(e))
                      )
                        return e;
                    }),
                  (n[1] = e),
                  C.apply(F, n)
                );
            },
          }
        ),
      D.prototype[B] || r(14)(D.prototype, B, D.prototype.valueOf),
      l(D, "Symbol"),
      l(Math, "Math", !0),
      l(n.JSON, "JSON", !0);
  },
  function (t, e, r) {
    t.exports = r(48)("native-function-to-string", Function.toString);
  },
  function (t, e, r) {
    var n = r(31),
      i = r(50),
      o = r(45);
    t.exports = function (t) {
      var e = n(t),
        r = i.f;
      if (r)
        for (var a, s = r(t), u = o.f, c = 0; s.length > c; )
          u.call(t, (a = s[c++])) && e.push(a);
      return e;
    };
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Object", { create: r(33) });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S + n.F * !r(8), "Object", { defineProperty: r(9).f });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S + n.F * !r(8), "Object", { defineProperties: r(96) });
  },
  function (t, e, r) {
    var n = r(15),
      i = r(20).f;
    r(21)("getOwnPropertyDescriptor", function () {
      return function (t, e) {
        return i(n(t), e);
      };
    });
  },
  function (t, e, r) {
    var n = r(10),
      i = r(35);
    r(21)("getPrototypeOf", function () {
      return function (t) {
        return i(n(t));
      };
    });
  },
  function (t, e, r) {
    var n = r(10),
      i = r(31);
    r(21)("keys", function () {
      return function (t) {
        return i(n(t));
      };
    });
  },
  function (t, e, r) {
    r(21)("getOwnPropertyNames", function () {
      return r(97).f;
    });
  },
  function (t, e, r) {
    var n = r(4),
      i = r(27).onFreeze;
    r(21)("freeze", function (t) {
      return function (e) {
        return t && n(e) ? t(i(e)) : e;
      };
    });
  },
  function (t, e, r) {
    var n = r(4),
      i = r(27).onFreeze;
    r(21)("seal", function (t) {
      return function (e) {
        return t && n(e) ? t(i(e)) : e;
      };
    });
  },
  function (t, e, r) {
    var n = r(4),
      i = r(27).onFreeze;
    r(21)("preventExtensions", function (t) {
      return function (e) {
        return t && n(e) ? t(i(e)) : e;
      };
    });
  },
  function (t, e, r) {
    var n = r(4);
    r(21)("isFrozen", function (t) {
      return function (e) {
        return !n(e) || (!!t && t(e));
      };
    });
  },
  function (t, e, r) {
    var n = r(4);
    r(21)("isSealed", function (t) {
      return function (e) {
        return !n(e) || (!!t && t(e));
      };
    });
  },
  function (t, e, r) {
    var n = r(4);
    r(21)("isExtensible", function (t) {
      return function (e) {
        return !!n(e) && (!t || t(e));
      };
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S + n.F, "Object", { assign: r(98) });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Object", { is: r(99) });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Object", { setPrototypeOf: r(69).set });
  },
  function (t, e, r) {
    "use strict";
    var n = r(46),
      i = {};
    (i[r(5)("toStringTag")] = "z"),
      i + "" != "[object z]" &&
        r(11)(
          Object.prototype,
          "toString",
          function () {
            return "[object " + n(this) + "]";
          },
          !0
        );
  },
  function (t, e, r) {
    var n = r(0);
    n(n.P, "Function", { bind: r(100) });
  },
  function (t, e, r) {
    var n = r(9).f,
      i = Function.prototype,
      o = /^\s*function ([^ (]*)/;
    "name" in i ||
      (r(8) &&
        n(i, "name", {
          configurable: !0,
          get: function () {
            try {
              return ("" + this).match(o)[1];
            } catch (t) {
              return "";
            }
          },
        }));
  },
  function (t, e, r) {
    "use strict";
    var n = r(4),
      i = r(35),
      o = r(5)("hasInstance"),
      a = Function.prototype;
    o in a ||
      r(9).f(a, o, {
        value: function (t) {
          if ("function" != typeof this || !n(t)) return !1;
          if (!n(this.prototype)) return t instanceof this;
          for (; (t = i(t)); ) if (this.prototype === t) return !0;
          return !1;
        },
      });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(102);
    n(n.G + n.F * (parseInt != i), { parseInt: i });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(103);
    n(n.G + n.F * (parseFloat != i), { parseFloat: i });
  },
  function (t, e, r) {
    "use strict";
    var n = r(1),
      i = r(13),
      o = r(23),
      a = r(71),
      s = r(26),
      u = r(2),
      c = r(34).f,
      f = r(20).f,
      l = r(9).f,
      h = r(39).trim,
      p = n.Number,
      d = p,
      v = p.prototype,
      y = "Number" == o(r(33)(v)),
      g = "trim" in String.prototype,
      m = function (t) {
        var e = s(t, !1);
        if ("string" == typeof e && e.length > 2) {
          var r,
            n,
            i,
            o = (e = g ? e.trim() : h(e, 3)).charCodeAt(0);
          if (43 === o || 45 === o) {
            if (88 === (r = e.charCodeAt(2)) || 120 === r) return NaN;
          } else if (48 === o) {
            switch (e.charCodeAt(1)) {
              case 66:
              case 98:
                (n = 2), (i = 49);
                break;
              case 79:
              case 111:
                (n = 8), (i = 55);
                break;
              default:
                return +e;
            }
            for (var a, u = e.slice(2), c = 0, f = u.length; c < f; c++)
              if ((a = u.charCodeAt(c)) < 48 || a > i) return NaN;
            return parseInt(u, n);
          }
        }
        return +e;
      };
    if (!p(" 0o1") || !p("0b1") || p("+0x1")) {
      p = function (t) {
        var e = arguments.length < 1 ? 0 : t,
          r = this;
        return r instanceof p &&
          (y
            ? u(function () {
                v.valueOf.call(r);
              })
            : "Number" != o(r))
          ? a(new d(m(e)), r, p)
          : m(e);
      };
      for (
        var b,
          w = r(8)
            ? c(d)
            : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(
                ","
              ),
          _ = 0;
        w.length > _;
        _++
      )
        i(d, (b = w[_])) && !i(p, b) && l(p, b, f(d, b));
      (p.prototype = v), (v.constructor = p), r(11)(n, "Number", p);
    }
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(19),
      o = r(104),
      a = r(72),
      s = (1).toFixed,
      u = Math.floor,
      c = [0, 0, 0, 0, 0, 0],
      f = "Number.toFixed: incorrect invocation!",
      l = function (t, e) {
        for (var r = -1, n = e; ++r < 6; )
          (n += t * c[r]), (c[r] = n % 1e7), (n = u(n / 1e7));
      },
      h = function (t) {
        for (var e = 6, r = 0; --e >= 0; )
          (r += c[e]), (c[e] = u(r / t)), (r = (r % t) * 1e7);
      },
      p = function () {
        for (var t = 6, e = ""; --t >= 0; )
          if ("" !== e || 0 === t || 0 !== c[t]) {
            var r = String(c[t]);
            e = "" === e ? r : e + a.call("0", 7 - r.length) + r;
          }
        return e;
      },
      d = function (t, e, r) {
        return 0 === e
          ? r
          : e % 2 == 1
          ? d(t, e - 1, r * t)
          : d(t * t, e / 2, r);
      };
    n(
      n.P +
        n.F *
          ((!!s &&
            ("0.000" !== (8e-5).toFixed(3) ||
              "1" !== (0.9).toFixed(0) ||
              "1.25" !== (1.255).toFixed(2) ||
              "1000000000000000128" !== (0xde0b6b3a7640080).toFixed(0))) ||
            !r(2)(function () {
              s.call({});
            })),
      "Number",
      {
        toFixed: function (t) {
          var e,
            r,
            n,
            s,
            u = o(this, f),
            c = i(t),
            v = "",
            y = "0";
          if (c < 0 || c > 20) throw RangeError(f);
          if (u != u) return "NaN";
          if (u <= -1e21 || u >= 1e21) return String(u);
          if ((u < 0 && ((v = "-"), (u = -u)), u > 1e-21))
            if (
              ((r =
                (e =
                  (function (t) {
                    for (var e = 0, r = t; r >= 4096; ) (e += 12), (r /= 4096);
                    for (; r >= 2; ) (e += 1), (r /= 2);
                    return e;
                  })(u * d(2, 69, 1)) - 69) < 0
                  ? u * d(2, -e, 1)
                  : u / d(2, e, 1)),
              (r *= 4503599627370496),
              (e = 52 - e) > 0)
            ) {
              for (l(0, r), n = c; n >= 7; ) l(1e7, 0), (n -= 7);
              for (l(d(10, n, 1), 0), n = e - 1; n >= 23; )
                h(1 << 23), (n -= 23);
              h(1 << n), l(1, 1), h(2), (y = p());
            } else l(0, r), l(1 << -e, 0), (y = p() + a.call("0", c));
          return (y =
            c > 0
              ? v +
                ((s = y.length) <= c
                  ? "0." + a.call("0", c - s) + y
                  : y.slice(0, s - c) + "." + y.slice(s - c))
              : v + y);
        },
      }
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(2),
      o = r(104),
      a = (1).toPrecision;
    n(
      n.P +
        n.F *
          (i(function () {
            return "1" !== a.call(1, void 0);
          }) ||
            !i(function () {
              a.call({});
            })),
      "Number",
      {
        toPrecision: function (t) {
          var e = o(this, "Number#toPrecision: incorrect invocation!");
          return void 0 === t ? a.call(e) : a.call(e, t);
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Number", { EPSILON: Math.pow(2, -52) });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(1).isFinite;
    n(n.S, "Number", {
      isFinite: function (t) {
        return "number" == typeof t && i(t);
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Number", { isInteger: r(105) });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Number", {
      isNaN: function (t) {
        return t != t;
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(105),
      o = Math.abs;
    n(n.S, "Number", {
      isSafeInteger: function (t) {
        return i(t) && o(t) <= 9007199254740991;
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Number", { MAX_SAFE_INTEGER: 9007199254740991 });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Number", { MIN_SAFE_INTEGER: -9007199254740991 });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(103);
    n(n.S + n.F * (Number.parseFloat != i), "Number", { parseFloat: i });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(102);
    n(n.S + n.F * (Number.parseInt != i), "Number", { parseInt: i });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(106),
      o = Math.sqrt,
      a = Math.acosh;
    n(
      n.S +
        n.F *
          !(a && 710 == Math.floor(a(Number.MAX_VALUE)) && a(1 / 0) == 1 / 0),
      "Math",
      {
        acosh: function (t) {
          return (t = +t) < 1
            ? NaN
            : t > 94906265.62425156
            ? Math.log(t) + Math.LN2
            : i(t - 1 + o(t - 1) * o(t + 1));
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0),
      i = Math.asinh;
    n(n.S + n.F * !(i && 1 / i(0) > 0), "Math", {
      asinh: function t(e) {
        return isFinite((e = +e)) && 0 != e
          ? e < 0
            ? -t(-e)
            : Math.log(e + Math.sqrt(e * e + 1))
          : e;
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = Math.atanh;
    n(n.S + n.F * !(i && 1 / i(-0) < 0), "Math", {
      atanh: function (t) {
        return 0 == (t = +t) ? t : Math.log((1 + t) / (1 - t)) / 2;
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(73);
    n(n.S, "Math", {
      cbrt: function (t) {
        return i((t = +t)) * Math.pow(Math.abs(t), 1 / 3);
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", {
      clz32: function (t) {
        return (t >>>= 0)
          ? 31 - Math.floor(Math.log(t + 0.5) * Math.LOG2E)
          : 32;
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = Math.exp;
    n(n.S, "Math", {
      cosh: function (t) {
        return (i((t = +t)) + i(-t)) / 2;
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(74);
    n(n.S + n.F * (i != Math.expm1), "Math", { expm1: i });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", { fround: r(180) });
  },
  function (t, e, r) {
    var n = r(73),
      i = Math.pow,
      o = i(2, -52),
      a = i(2, -23),
      s = i(2, 127) * (2 - a),
      u = i(2, -126);
    t.exports =
      Math.fround ||
      function (t) {
        var e,
          r,
          i = Math.abs(t),
          c = n(t);
        return i < u
          ? c * (i / u / a + 1 / o - 1 / o) * u * a
          : (r = (e = (1 + a / o) * i) - (e - i)) > s || r != r
          ? c * (1 / 0)
          : c * r;
      };
  },
  function (t, e, r) {
    var n = r(0),
      i = Math.abs;
    n(n.S, "Math", {
      hypot: function (t, e) {
        for (var r, n, o = 0, a = 0, s = arguments.length, u = 0; a < s; )
          u < (r = i(arguments[a++]))
            ? ((o = o * (n = u / r) * n + 1), (u = r))
            : (o += r > 0 ? (n = r / u) * n : r);
        return u === 1 / 0 ? 1 / 0 : u * Math.sqrt(o);
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = Math.imul;
    n(
      n.S +
        n.F *
          r(2)(function () {
            return -5 != i(4294967295, 5) || 2 != i.length;
          }),
      "Math",
      {
        imul: function (t, e) {
          var r = +t,
            n = +e,
            i = 65535 & r,
            o = 65535 & n;
          return (
            0 |
            (i * o +
              ((((65535 & (r >>> 16)) * o + i * (65535 & (n >>> 16))) << 16) >>>
                0))
          );
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", {
      log10: function (t) {
        return Math.log(t) * Math.LOG10E;
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", { log1p: r(106) });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", {
      log2: function (t) {
        return Math.log(t) / Math.LN2;
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", { sign: r(73) });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(74),
      o = Math.exp;
    n(
      n.S +
        n.F *
          r(2)(function () {
            return -2e-17 != !Math.sinh(-2e-17);
          }),
      "Math",
      {
        sinh: function (t) {
          return Math.abs((t = +t)) < 1
            ? (i(t) - i(-t)) / 2
            : (o(t - 1) - o(-t - 1)) * (Math.E / 2);
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0),
      i = r(74),
      o = Math.exp;
    n(n.S, "Math", {
      tanh: function (t) {
        var e = i((t = +t)),
          r = i(-t);
        return e == 1 / 0 ? 1 : r == 1 / 0 ? -1 : (e - r) / (o(t) + o(-t));
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Math", {
      trunc: function (t) {
        return (t > 0 ? Math.floor : Math.ceil)(t);
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(32),
      o = String.fromCharCode,
      a = String.fromCodePoint;
    n(n.S + n.F * (!!a && 1 != a.length), "String", {
      fromCodePoint: function (t) {
        for (var e, r = [], n = arguments.length, a = 0; n > a; ) {
          if (((e = +arguments[a++]), i(e, 1114111) !== e))
            throw RangeError(e + " is not a valid code point");
          r.push(
            e < 65536
              ? o(e)
              : o(55296 + ((e -= 65536) >> 10), (e % 1024) + 56320)
          );
        }
        return r.join("");
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(15),
      o = r(6);
    n(n.S, "String", {
      raw: function (t) {
        for (
          var e = i(t.raw),
            r = o(e.length),
            n = arguments.length,
            a = [],
            s = 0;
          r > s;

        )
          a.push(String(e[s++])), s < n && a.push(String(arguments[s]));
        return a.join("");
      },
    });
  },
  function (t, e, r) {
    "use strict";
    r(39)("trim", function (t) {
      return function () {
        return t(this, 3);
      };
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(75)(!0);
    r(76)(
      String,
      "String",
      function (t) {
        (this._t = String(t)), (this._i = 0);
      },
      function () {
        var t,
          e = this._t,
          r = this._i;
        return r >= e.length
          ? { value: void 0, done: !0 }
          : ((t = n(e, r)), (this._i += t.length), { value: t, done: !1 });
      }
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(75)(!1);
    n(n.P, "String", {
      codePointAt: function (t) {
        return i(this, t);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(6),
      o = r(77),
      a = "".endsWith;
    n(n.P + n.F * r(79)("endsWith"), "String", {
      endsWith: function (t) {
        var e = o(this, t, "endsWith"),
          r = arguments.length > 1 ? arguments[1] : void 0,
          n = i(e.length),
          s = void 0 === r ? n : Math.min(i(r), n),
          u = String(t);
        return a ? a.call(e, u, s) : e.slice(s - u.length, s) === u;
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(77);
    n(n.P + n.F * r(79)("includes"), "String", {
      includes: function (t) {
        return !!~i(this, t, "includes").indexOf(
          t,
          arguments.length > 1 ? arguments[1] : void 0
        );
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.P, "String", { repeat: r(72) });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(6),
      o = r(77),
      a = "".startsWith;
    n(n.P + n.F * r(79)("startsWith"), "String", {
      startsWith: function (t) {
        var e = o(this, t, "startsWith"),
          r = i(
            Math.min(arguments.length > 1 ? arguments[1] : void 0, e.length)
          ),
          n = String(t);
        return a ? a.call(e, n, r) : e.slice(r, r + n.length) === n;
      },
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("anchor", function (t) {
      return function (e) {
        return t(this, "a", "name", e);
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("big", function (t) {
      return function () {
        return t(this, "big", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("blink", function (t) {
      return function () {
        return t(this, "blink", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("bold", function (t) {
      return function () {
        return t(this, "b", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("fixed", function (t) {
      return function () {
        return t(this, "tt", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("fontcolor", function (t) {
      return function (e) {
        return t(this, "font", "color", e);
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("fontsize", function (t) {
      return function (e) {
        return t(this, "font", "size", e);
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("italics", function (t) {
      return function () {
        return t(this, "i", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("link", function (t) {
      return function (e) {
        return t(this, "a", "href", e);
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("small", function (t) {
      return function () {
        return t(this, "small", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("strike", function (t) {
      return function () {
        return t(this, "strike", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("sub", function (t) {
      return function () {
        return t(this, "sub", "", "");
      };
    });
  },
  function (t, e, r) {
    "use strict";
    r(12)("sup", function (t) {
      return function () {
        return t(this, "sup", "", "");
      };
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Date", {
      now: function () {
        return new Date().getTime();
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(10),
      o = r(26);
    n(
      n.P +
        n.F *
          r(2)(function () {
            return (
              null !== new Date(NaN).toJSON() ||
              1 !==
                Date.prototype.toJSON.call({
                  toISOString: function () {
                    return 1;
                  },
                })
            );
          }),
      "Date",
      {
        toJSON: function (t) {
          var e = i(this),
            r = o(e);
          return "number" != typeof r || isFinite(r) ? e.toISOString() : null;
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0),
      i = r(215);
    n(n.P + n.F * (Date.prototype.toISOString !== i), "Date", {
      toISOString: i,
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(2),
      i = Date.prototype.getTime,
      o = Date.prototype.toISOString,
      a = function (t) {
        return t > 9 ? t : "0" + t;
      };
    t.exports =
      n(function () {
        return "0385-07-25T07:06:39.999Z" != o.call(new Date(-50000000000001));
      }) ||
      !n(function () {
        o.call(new Date(NaN));
      })
        ? function () {
            if (!isFinite(i.call(this))) throw RangeError("Invalid time value");
            var t = this,
              e = t.getUTCFullYear(),
              r = t.getUTCMilliseconds(),
              n = e < 0 ? "-" : e > 9999 ? "+" : "";
            return (
              n +
              ("00000" + Math.abs(e)).slice(n ? -6 : -4) +
              "-" +
              a(t.getUTCMonth() + 1) +
              "-" +
              a(t.getUTCDate()) +
              "T" +
              a(t.getUTCHours()) +
              ":" +
              a(t.getUTCMinutes()) +
              ":" +
              a(t.getUTCSeconds()) +
              "." +
              (r > 99 ? r : "0" + a(r)) +
              "Z"
            );
          }
        : o;
  },
  function (t, e, r) {
    var n = Date.prototype,
      i = n.toString,
      o = n.getTime;
    new Date(NaN) + "" != "Invalid Date" &&
      r(11)(n, "toString", function () {
        var t = o.call(this);
        return t == t ? i.call(this) : "Invalid Date";
      });
  },
  function (t, e, r) {
    var n = r(5)("toPrimitive"),
      i = Date.prototype;
    n in i || r(14)(i, n, r(218));
  },
  function (t, e, r) {
    "use strict";
    var n = r(3),
      i = r(26);
    t.exports = function (t) {
      if ("string" !== t && "number" !== t && "default" !== t)
        throw TypeError("Incorrect hint");
      return i(n(this), "number" != t);
    };
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Array", { isArray: r(51) });
  },
  function (t, e, r) {
    "use strict";
    var n = r(17),
      i = r(0),
      o = r(10),
      a = r(108),
      s = r(80),
      u = r(6),
      c = r(81),
      f = r(82);
    i(
      i.S +
        i.F *
          !r(52)(function (t) {
            Array.from(t);
          }),
      "Array",
      {
        from: function (t) {
          var e,
            r,
            i,
            l,
            h = o(t),
            p = "function" == typeof this ? this : Array,
            d = arguments.length,
            v = d > 1 ? arguments[1] : void 0,
            y = void 0 !== v,
            g = 0,
            m = f(h);
          if (
            (y && (v = n(v, d > 2 ? arguments[2] : void 0, 2)),
            null == m || (p == Array && s(m)))
          )
            for (r = new p((e = u(h.length))); e > g; g++)
              c(r, g, y ? v(h[g], g) : h[g]);
          else
            for (l = m.call(h), r = new p(); !(i = l.next()).done; g++)
              c(r, g, y ? a(l, v, [i.value, g], !0) : i.value);
          return (r.length = g), r;
        },
      }
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(81);
    n(
      n.S +
        n.F *
          r(2)(function () {
            function t() {}
            return !(Array.of.call(t) instanceof t);
          }),
      "Array",
      {
        of: function () {
          for (
            var t = 0,
              e = arguments.length,
              r = new ("function" == typeof this ? this : Array)(e);
            e > t;

          )
            i(r, t, arguments[t++]);
          return (r.length = e), r;
        },
      }
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(15),
      o = [].join;
    n(n.P + n.F * (r(44) != Object || !r(16)(o)), "Array", {
      join: function (t) {
        return o.call(i(this), void 0 === t ? "," : t);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(68),
      o = r(23),
      a = r(32),
      s = r(6),
      u = [].slice;
    n(
      n.P +
        n.F *
          r(2)(function () {
            i && u.call(i);
          }),
      "Array",
      {
        slice: function (t, e) {
          var r = s(this.length),
            n = o(this);
          if (((e = void 0 === e ? r : e), "Array" == n))
            return u.call(this, t, e);
          for (
            var i = a(t, r), c = a(e, r), f = s(c - i), l = new Array(f), h = 0;
            h < f;
            h++
          )
            l[h] = "String" == n ? this.charAt(i + h) : this[i + h];
          return l;
        },
      }
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(18),
      o = r(10),
      a = r(2),
      s = [].sort,
      u = [1, 2, 3];
    n(
      n.P +
        n.F *
          (a(function () {
            u.sort(void 0);
          }) ||
            !a(function () {
              u.sort(null);
            }) ||
            !r(16)(s)),
      "Array",
      {
        sort: function (t) {
          return void 0 === t ? s.call(o(this)) : s.call(o(this), i(t));
        },
      }
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(0),
      o = r(16)([].forEach, !0);
    n(n.P + n.F * !o, "Array", {
      forEach: function (t) {
        return i(this, t, arguments[1]);
      },
    });
  },
  function (t, e, r) {
    var n = r(4),
      i = r(51),
      o = r(5)("species");
    t.exports = function (t) {
      var e;
      return (
        i(t) &&
          ("function" != typeof (e = t.constructor) ||
            (e !== Array && !i(e.prototype)) ||
            (e = void 0),
          n(e) && null === (e = e[o]) && (e = void 0)),
        void 0 === e ? Array : e
      );
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(1);
    n(n.P + n.F * !r(16)([].map, !0), "Array", {
      map: function (t) {
        return i(this, t, arguments[1]);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(2);
    n(n.P + n.F * !r(16)([].filter, !0), "Array", {
      filter: function (t) {
        return i(this, t, arguments[1]);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(3);
    n(n.P + n.F * !r(16)([].some, !0), "Array", {
      some: function (t) {
        return i(this, t, arguments[1]);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(4);
    n(n.P + n.F * !r(16)([].every, !0), "Array", {
      every: function (t) {
        return i(this, t, arguments[1]);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(110);
    n(n.P + n.F * !r(16)([].reduce, !0), "Array", {
      reduce: function (t) {
        return i(this, t, arguments.length, arguments[1], !1);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(110);
    n(n.P + n.F * !r(16)([].reduceRight, !0), "Array", {
      reduceRight: function (t) {
        return i(this, t, arguments.length, arguments[1], !0);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(49)(!1),
      o = [].indexOf,
      a = !!o && 1 / [1].indexOf(1, -0) < 0;
    n(n.P + n.F * (a || !r(16)(o)), "Array", {
      indexOf: function (t) {
        return a ? o.apply(this, arguments) || 0 : i(this, t, arguments[1]);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(15),
      o = r(19),
      a = r(6),
      s = [].lastIndexOf,
      u = !!s && 1 / [1].lastIndexOf(1, -0) < 0;
    n(n.P + n.F * (u || !r(16)(s)), "Array", {
      lastIndexOf: function (t) {
        if (u) return s.apply(this, arguments) || 0;
        var e = i(this),
          r = a(e.length),
          n = r - 1;
        for (
          arguments.length > 1 && (n = Math.min(n, o(arguments[1]))),
            n < 0 && (n = r + n);
          n >= 0;
          n--
        )
          if (n in e && e[n] === t) return n || 0;
        return -1;
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.P, "Array", { copyWithin: r(111) }), r(36)("copyWithin");
  },
  function (t, e, r) {
    var n = r(0);
    n(n.P, "Array", { fill: r(83) }), r(36)("fill");
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(5),
      o = !0;
    "find" in [] &&
      Array(1).find(function () {
        o = !1;
      }),
      n(n.P + n.F * o, "Array", {
        find: function (t) {
          return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
        },
      }),
      r(36)("find");
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(22)(6),
      o = "findIndex",
      a = !0;
    o in [] &&
      Array(1)[o](function () {
        a = !1;
      }),
      n(n.P + n.F * a, "Array", {
        findIndex: function (t) {
          return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
        },
      }),
      r(36)(o);
  },
  function (t, e, r) {
    r(41)("Array");
  },
  function (t, e, r) {
    var n = r(1),
      i = r(71),
      o = r(9).f,
      a = r(34).f,
      s = r(78),
      u = r(53),
      c = n.RegExp,
      f = c,
      l = c.prototype,
      h = /a/g,
      p = /a/g,
      d = new c(h) !== h;
    if (
      r(8) &&
      (!d ||
        r(2)(function () {
          return (
            (p[r(5)("match")] = !1),
            c(h) != h || c(p) == p || "/a/i" != c(h, "i")
          );
        }))
    ) {
      c = function (t, e) {
        var r = this instanceof c,
          n = s(t),
          o = void 0 === e;
        return !r && n && t.constructor === c && o
          ? t
          : i(
              d
                ? new f(n && !o ? t.source : t, e)
                : f(
                    (n = t instanceof c) ? t.source : t,
                    n && o ? u.call(t) : e
                  ),
              r ? this : l,
              c
            );
      };
      for (
        var v = function (t) {
            (t in c) ||
              o(c, t, {
                configurable: !0,
                get: function () {
                  return f[t];
                },
                set: function (e) {
                  f[t] = e;
                },
              });
          },
          y = a(f),
          g = 0;
        y.length > g;

      )
        v(y[g++]);
      (l.constructor = c), (c.prototype = l), r(11)(n, "RegExp", c);
    }
    r(41)("RegExp");
  },
  function (t, e, r) {
    "use strict";
    r(114);
    var n = r(3),
      i = r(53),
      o = r(8),
      a = /./.toString,
      s = function (t) {
        r(11)(RegExp.prototype, "toString", t, !0);
      };
    r(2)(function () {
      return "/a/b" != a.call({ source: "a", flags: "b" });
    })
      ? s(function () {
          var t = n(this);
          return "/".concat(
            t.source,
            "/",
            "flags" in t
              ? t.flags
              : !o && t instanceof RegExp
              ? i.call(t)
              : void 0
          );
        })
      : "toString" != a.name &&
        s(function () {
          return a.call(this);
        });
  },
  function (t, e, r) {
    "use strict";
    var n = r(3),
      i = r(6),
      o = r(86),
      a = r(54);
    r(55)("match", 1, function (t, e, r, s) {
      return [
        function (r) {
          var n = t(this),
            i = null == r ? void 0 : r[e];
          return void 0 !== i ? i.call(r, n) : new RegExp(r)[e](String(n));
        },
        function (t) {
          var e = s(r, t, this);
          if (e.done) return e.value;
          var u = n(t),
            c = String(this);
          if (!u.global) return a(u, c);
          var f = u.unicode;
          u.lastIndex = 0;
          for (var l, h = [], p = 0; null !== (l = a(u, c)); ) {
            var d = String(l[0]);
            (h[p] = d),
              "" === d && (u.lastIndex = o(c, i(u.lastIndex), f)),
              p++;
          }
          return 0 === p ? null : h;
        },
      ];
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(3),
      i = r(10),
      o = r(6),
      a = r(19),
      s = r(86),
      u = r(54),
      c = Math.max,
      f = Math.min,
      l = Math.floor,
      h = /\$([$&`']|\d\d?|<[^>]*>)/g,
      p = /\$([$&`']|\d\d?)/g;
    r(55)("replace", 2, function (t, e, r, d) {
      return [
        function (n, i) {
          var o = t(this),
            a = null == n ? void 0 : n[e];
          return void 0 !== a ? a.call(n, o, i) : r.call(String(o), n, i);
        },
        function (t, e) {
          var i = d(r, t, this, e);
          if (i.done) return i.value;
          var l = n(t),
            h = String(this),
            p = "function" == typeof e;
          p || (e = String(e));
          var y = l.global;
          if (y) {
            var g = l.unicode;
            l.lastIndex = 0;
          }
          for (var m = []; ; ) {
            var b = u(l, h);
            if (null === b) break;
            if ((m.push(b), !y)) break;
            "" === String(b[0]) && (l.lastIndex = s(h, o(l.lastIndex), g));
          }
          for (var w, _ = "", E = 0, T = 0; T < m.length; T++) {
            b = m[T];
            for (
              var x = String(b[0]),
                S = c(f(a(b.index), h.length), 0),
                A = [],
                I = 1;
              I < b.length;
              I++
            )
              A.push(void 0 === (w = b[I]) ? w : String(w));
            var O = b.groups;
            if (p) {
              var R = [x].concat(A, S, h);
              void 0 !== O && R.push(O);
              var P = String(e.apply(void 0, R));
            } else P = v(x, h, S, A, O, e);
            S >= E && ((_ += h.slice(E, S) + P), (E = S + x.length));
          }
          return _ + h.slice(E);
        },
      ];
      function v(t, e, n, o, a, s) {
        var u = n + t.length,
          c = o.length,
          f = p;
        return (
          void 0 !== a && ((a = i(a)), (f = h)),
          r.call(s, f, function (r, i) {
            var s;
            switch (i.charAt(0)) {
              case "$":
                return "$";
              case "&":
                return t;
              case "`":
                return e.slice(0, n);
              case "'":
                return e.slice(u);
              case "<":
                s = a[i.slice(1, -1)];
                break;
              default:
                var f = +i;
                if (0 === f) return r;
                if (f > c) {
                  var h = l(f / 10);
                  return 0 === h
                    ? r
                    : h <= c
                    ? void 0 === o[h - 1]
                      ? i.charAt(1)
                      : o[h - 1] + i.charAt(1)
                    : r;
                }
                s = o[f - 1];
            }
            return void 0 === s ? "" : s;
          })
        );
      }
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(3),
      i = r(99),
      o = r(54);
    r(55)("search", 1, function (t, e, r, a) {
      return [
        function (r) {
          var n = t(this),
            i = null == r ? void 0 : r[e];
          return void 0 !== i ? i.call(r, n) : new RegExp(r)[e](String(n));
        },
        function (t) {
          var e = a(r, t, this);
          if (e.done) return e.value;
          var s = n(t),
            u = String(this),
            c = s.lastIndex;
          i(c, 0) || (s.lastIndex = 0);
          var f = o(s, u);
          return (
            i(s.lastIndex, c) || (s.lastIndex = c), null === f ? -1 : f.index
          );
        },
      ];
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(78),
      i = r(3),
      o = r(47),
      a = r(86),
      s = r(6),
      u = r(54),
      c = r(85),
      f = r(2),
      l = Math.min,
      h = [].push,
      p = "length",
      d = !f(function () {
        RegExp(4294967295, "y");
      });
    r(55)("split", 2, function (t, e, r, f) {
      var v;
      return (
        (v =
          "c" == "abbc".split(/(b)*/)[1] ||
          4 != "test".split(/(?:)/, -1)[p] ||
          2 != "ab".split(/(?:ab)*/)[p] ||
          4 != ".".split(/(.?)(.?)/)[p] ||
          ".".split(/()()/)[p] > 1 ||
          "".split(/.?/)[p]
            ? function (t, e) {
                var i = String(this);
                if (void 0 === t && 0 === e) return [];
                if (!n(t)) return r.call(i, t, e);
                for (
                  var o,
                    a,
                    s,
                    u = [],
                    f =
                      (t.ignoreCase ? "i" : "") +
                      (t.multiline ? "m" : "") +
                      (t.unicode ? "u" : "") +
                      (t.sticky ? "y" : ""),
                    l = 0,
                    d = void 0 === e ? 4294967295 : e >>> 0,
                    v = new RegExp(t.source, f + "g");
                  (o = c.call(v, i)) &&
                  !(
                    (a = v.lastIndex) > l &&
                    (u.push(i.slice(l, o.index)),
                    o[p] > 1 && o.index < i[p] && h.apply(u, o.slice(1)),
                    (s = o[0][p]),
                    (l = a),
                    u[p] >= d)
                  );

                )
                  v.lastIndex === o.index && v.lastIndex++;
                return (
                  l === i[p]
                    ? (!s && v.test("")) || u.push("")
                    : u.push(i.slice(l)),
                  u[p] > d ? u.slice(0, d) : u
                );
              }
            : "0".split(void 0, 0)[p]
            ? function (t, e) {
                return void 0 === t && 0 === e ? [] : r.call(this, t, e);
              }
            : r),
        [
          function (r, n) {
            var i = t(this),
              o = null == r ? void 0 : r[e];
            return void 0 !== o ? o.call(r, i, n) : v.call(String(i), r, n);
          },
          function (t, e) {
            var n = f(v, t, this, e, v !== r);
            if (n.done) return n.value;
            var c = i(t),
              h = String(this),
              p = o(c, RegExp),
              y = c.unicode,
              g =
                (c.ignoreCase ? "i" : "") +
                (c.multiline ? "m" : "") +
                (c.unicode ? "u" : "") +
                (d ? "y" : "g"),
              m = new p(d ? c : "^(?:" + c.source + ")", g),
              b = void 0 === e ? 4294967295 : e >>> 0;
            if (0 === b) return [];
            if (0 === h.length) return null === u(m, h) ? [h] : [];
            for (var w = 0, _ = 0, E = []; _ < h.length; ) {
              m.lastIndex = d ? _ : 0;
              var T,
                x = u(m, d ? h : h.slice(_));
              if (
                null === x ||
                (T = l(s(m.lastIndex + (d ? 0 : _)), h.length)) === w
              )
                _ = a(h, _, y);
              else {
                if ((E.push(h.slice(w, _)), E.length === b)) return E;
                for (var S = 1; S <= x.length - 1; S++)
                  if ((E.push(x[S]), E.length === b)) return E;
                _ = w = T;
              }
            }
            return E.push(h.slice(w)), E;
          },
        ]
      );
    });
  },
  function (t, e, r) {
    var n = r(1),
      i = r(87).set,
      o = n.MutationObserver || n.WebKitMutationObserver,
      a = n.process,
      s = n.Promise,
      u = "process" == r(23)(a);
    t.exports = function () {
      var t,
        e,
        r,
        c = function () {
          var n, i;
          for (u && (n = a.domain) && n.exit(); t; ) {
            (i = t.fn), (t = t.next);
            try {
              i();
            } catch (n) {
              throw (t ? r() : (e = void 0), n);
            }
          }
          (e = void 0), n && n.enter();
        };
      if (u)
        r = function () {
          a.nextTick(c);
        };
      else if (!o || (n.navigator && n.navigator.standalone))
        if (s && s.resolve) {
          var f = s.resolve(void 0);
          r = function () {
            f.then(c);
          };
        } else
          r = function () {
            i.call(n, c);
          };
      else {
        var l = !0,
          h = document.createTextNode("");
        new o(c).observe(h, { characterData: !0 }),
          (r = function () {
            h.data = l = !l;
          });
      }
      return function (n) {
        var i = { fn: n, next: void 0 };
        e && (e.next = i), t || ((t = i), r()), (e = i);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return { e: !1, v: t() };
      } catch (t) {
        return { e: !0, v: t };
      }
    };
  },
  function (t, e, r) {
    "use strict";
    var n = r(118),
      i = r(37);
    t.exports = r(58)(
      "Map",
      function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : void 0);
        };
      },
      {
        get: function (t) {
          var e = n.getEntry(i(this, "Map"), t);
          return e && e.v;
        },
        set: function (t, e) {
          return n.def(i(this, "Map"), 0 === t ? 0 : t, e);
        },
      },
      n,
      !0
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(118),
      i = r(37);
    t.exports = r(58)(
      "Set",
      function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : void 0);
        };
      },
      {
        add: function (t) {
          return n.def(i(this, "Set"), (t = 0 === t ? 0 : t), t);
        },
      },
      n
    );
  },
  function (t, e, r) {
    "use strict";
    var n,
      i = r(1),
      o = r(22)(0),
      a = r(11),
      s = r(27),
      u = r(98),
      c = r(119),
      f = r(4),
      l = r(37),
      h = r(37),
      p = !i.ActiveXObject && "ActiveXObject" in i,
      d = s.getWeak,
      v = Object.isExtensible,
      y = c.ufstore,
      g = function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : void 0);
        };
      },
      m = {
        get: function (t) {
          if (f(t)) {
            var e = d(t);
            return !0 === e
              ? y(l(this, "WeakMap")).get(t)
              : e
              ? e[this._i]
              : void 0;
          }
        },
        set: function (t, e) {
          return c.def(l(this, "WeakMap"), t, e);
        },
      },
      b = (t.exports = r(58)("WeakMap", g, m, c, !0, !0));
    h &&
      p &&
      (u((n = c.getConstructor(g, "WeakMap")).prototype, m),
      (s.NEED = !0),
      o(["delete", "has", "get", "set"], function (t) {
        var e = b.prototype,
          r = e[t];
        a(e, t, function (e, i) {
          if (f(e) && !v(e)) {
            this._f || (this._f = new n());
            var o = this._f[t](e, i);
            return "set" == t ? this : o;
          }
          return r.call(this, e, i);
        });
      }));
  },
  function (t, e, r) {
    "use strict";
    var n = r(119),
      i = r(37);
    r(58)(
      "WeakSet",
      function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : void 0);
        };
      },
      {
        add: function (t) {
          return n.def(i(this, "WeakSet"), t, !0);
        },
      },
      n,
      !1,
      !0
    );
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(59),
      o = r(88),
      a = r(3),
      s = r(32),
      u = r(6),
      c = r(4),
      f = r(1).ArrayBuffer,
      l = r(47),
      h = o.ArrayBuffer,
      p = o.DataView,
      d = i.ABV && f.isView,
      v = h.prototype.slice,
      y = i.VIEW;
    n(n.G + n.W + n.F * (f !== h), { ArrayBuffer: h }),
      n(n.S + n.F * !i.CONSTR, "ArrayBuffer", {
        isView: function (t) {
          return (d && d(t)) || (c(t) && y in t);
        },
      }),
      n(
        n.P +
          n.U +
          n.F *
            r(2)(function () {
              return !new h(2).slice(1, void 0).byteLength;
            }),
        "ArrayBuffer",
        {
          slice: function (t, e) {
            if (void 0 !== v && void 0 === e) return v.call(a(this), t);
            for (
              var r = a(this).byteLength,
                n = s(t, r),
                i = s(void 0 === e ? r : e, r),
                o = new (l(this, h))(u(i - n)),
                c = new p(this),
                f = new p(o),
                d = 0;
              n < i;

            )
              f.setUint8(d++, c.getUint8(n++));
            return o;
          },
        }
      ),
      r(41)("ArrayBuffer");
  },
  function (t, e, r) {
    var n = r(0);
    n(n.G + n.W + n.F * !r(59).ABV, { DataView: r(88).DataView });
  },
  function (t, e, r) {
    r(25)("Int8", 1, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)("Uint8", 1, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)(
      "Uint8",
      1,
      function (t) {
        return function (e, r, n) {
          return t(this, e, r, n);
        };
      },
      !0
    );
  },
  function (t, e, r) {
    r(25)("Int16", 2, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)("Uint16", 2, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)("Int32", 4, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)("Uint32", 4, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)("Float32", 4, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    r(25)("Float64", 8, function (t) {
      return function (e, r, n) {
        return t(this, e, r, n);
      };
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(18),
      o = r(3),
      a = (r(1).Reflect || {}).apply,
      s = Function.apply;
    n(
      n.S +
        n.F *
          !r(2)(function () {
            a(function () {});
          }),
      "Reflect",
      {
        apply: function (t, e, r) {
          var n = i(t),
            u = o(r);
          return a ? a(n, e, u) : s.call(n, e, u);
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0),
      i = r(33),
      o = r(18),
      a = r(3),
      s = r(4),
      u = r(2),
      c = r(100),
      f = (r(1).Reflect || {}).construct,
      l = u(function () {
        function t() {}
        return !(f(function () {}, [], t) instanceof t);
      }),
      h = !u(function () {
        f(function () {});
      });
    n(n.S + n.F * (l || h), "Reflect", {
      construct: function (t, e) {
        o(t), a(e);
        var r = arguments.length < 3 ? t : o(arguments[2]);
        if (h && !l) return f(t, e, r);
        if (t == r) {
          switch (e.length) {
            case 0:
              return new t();
            case 1:
              return new t(e[0]);
            case 2:
              return new t(e[0], e[1]);
            case 3:
              return new t(e[0], e[1], e[2]);
            case 4:
              return new t(e[0], e[1], e[2], e[3]);
          }
          var n = [null];
          return n.push.apply(n, e), new (c.apply(t, n))();
        }
        var u = r.prototype,
          p = i(s(u) ? u : Object.prototype),
          d = Function.apply.call(t, p, e);
        return s(d) ? d : p;
      },
    });
  },
  function (t, e, r) {
    var n = r(9),
      i = r(0),
      o = r(3),
      a = r(26);
    i(
      i.S +
        i.F *
          r(2)(function () {
            Reflect.defineProperty(n.f({}, 1, { value: 1 }), 1, { value: 2 });
          }),
      "Reflect",
      {
        defineProperty: function (t, e, r) {
          o(t), (e = a(e, !0)), o(r);
          try {
            return n.f(t, e, r), !0;
          } catch (t) {
            return !1;
          }
        },
      }
    );
  },
  function (t, e, r) {
    var n = r(0),
      i = r(20).f,
      o = r(3);
    n(n.S, "Reflect", {
      deleteProperty: function (t, e) {
        var r = i(o(t), e);
        return !(r && !r.configurable) && delete t[e];
      },
    });
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(3),
      o = function (t) {
        (this._t = i(t)), (this._i = 0);
        var e,
          r = (this._k = []);
        for (e in t) r.push(e);
      };
    r(107)(o, "Object", function () {
      var t,
        e = this._k;
      do {
        if (this._i >= e.length) return { value: void 0, done: !0 };
      } while (!((t = e[this._i++]) in this._t));
      return { value: t, done: !1 };
    }),
      n(n.S, "Reflect", {
        enumerate: function (t) {
          return new o(t);
        },
      });
  },
  function (t, e, r) {
    var n = r(20),
      i = r(35),
      o = r(13),
      a = r(0),
      s = r(4),
      u = r(3);
    a(a.S, "Reflect", {
      get: function t(e, r) {
        var a,
          c,
          f = arguments.length < 3 ? e : arguments[2];
        return u(e) === f
          ? e[r]
          : (a = n.f(e, r))
          ? o(a, "value")
            ? a.value
            : void 0 !== a.get
            ? a.get.call(f)
            : void 0
          : s((c = i(e)))
          ? t(c, r, f)
          : void 0;
      },
    });
  },
  function (t, e, r) {
    var n = r(20),
      i = r(0),
      o = r(3);
    i(i.S, "Reflect", {
      getOwnPropertyDescriptor: function (t, e) {
        return n.f(o(t), e);
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(35),
      o = r(3);
    n(n.S, "Reflect", {
      getPrototypeOf: function (t) {
        return i(o(t));
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Reflect", {
      has: function (t, e) {
        return e in t;
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(3),
      o = Object.isExtensible;
    n(n.S, "Reflect", {
      isExtensible: function (t) {
        return i(t), !o || o(t);
      },
    });
  },
  function (t, e, r) {
    var n = r(0);
    n(n.S, "Reflect", { ownKeys: r(121) });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(3),
      o = Object.preventExtensions;
    n(n.S, "Reflect", {
      preventExtensions: function (t) {
        i(t);
        try {
          return o && o(t), !0;
        } catch (t) {
          return !1;
        }
      },
    });
  },
  function (t, e, r) {
    var n = r(9),
      i = r(20),
      o = r(35),
      a = r(13),
      s = r(0),
      u = r(28),
      c = r(3),
      f = r(4);
    s(s.S, "Reflect", {
      set: function t(e, r, s) {
        var l,
          h,
          p = arguments.length < 4 ? e : arguments[3],
          d = i.f(c(e), r);
        if (!d) {
          if (f((h = o(e)))) return t(h, r, s, p);
          d = u(0);
        }
        if (a(d, "value")) {
          if (!1 === d.writable || !f(p)) return !1;
          if ((l = i.f(p, r))) {
            if (l.get || l.set || !1 === l.writable) return !1;
            (l.value = s), n.f(p, r, l);
          } else n.f(p, r, u(0, s));
          return !0;
        }
        return void 0 !== d.set && (d.set.call(p, s), !0);
      },
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(69);
    i &&
      n(n.S, "Reflect", {
        setPrototypeOf: function (t, e) {
          i.check(t, e);
          try {
            return i.set(t, e), !0;
          } catch (t) {
            return !1;
          }
        },
      });
  },
  function (t, e, r) {
    r(278), (t.exports = r(7).Array.includes);
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(49)(!0);
    n(n.P, "Array", {
      includes: function (t) {
        return i(this, t, arguments.length > 1 ? arguments[1] : void 0);
      },
    }),
      r(36)("includes");
  },
  function (t, e, r) {
    r(280), (t.exports = r(7).Array.flatMap);
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(281),
      o = r(10),
      a = r(6),
      s = r(18),
      u = r(109);
    n(n.P, "Array", {
      flatMap: function (t) {
        var e,
          r,
          n = o(this);
        return (
          s(t),
          (e = a(n.length)),
          (r = u(n, 0)),
          i(r, n, n, e, 0, 1, t, arguments[1]),
          r
        );
      },
    }),
      r(36)("flatMap");
  },
  function (t, e, r) {
    "use strict";
    var n = r(51),
      i = r(4),
      o = r(6),
      a = r(17),
      s = r(5)("isConcatSpreadable");
    t.exports = function t(e, r, u, c, f, l, h, p) {
      for (var d, v, y = f, g = 0, m = !!h && a(h, p, 3); g < c; ) {
        if (g in u) {
          if (
            ((d = m ? m(u[g], g, r) : u[g]),
            (v = !1),
            i(d) && (v = void 0 !== (v = d[s]) ? !!v : n(d)),
            v && l > 0)
          )
            y = t(e, r, d, o(d.length), y, l - 1) - 1;
          else {
            if (y >= 9007199254740991) throw TypeError();
            e[y] = d;
          }
          y++;
        }
        g++;
      }
      return y;
    };
  },
  function (t, e, r) {
    r(283), (t.exports = r(7).String.padStart);
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(122),
      o = r(57),
      a = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(o);
    n(n.P + n.F * a, "String", {
      padStart: function (t) {
        return i(this, t, arguments.length > 1 ? arguments[1] : void 0, !0);
      },
    });
  },
  function (t, e, r) {
    r(285), (t.exports = r(7).String.padEnd);
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(122),
      o = r(57),
      a = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(o);
    n(n.P + n.F * a, "String", {
      padEnd: function (t) {
        return i(this, t, arguments.length > 1 ? arguments[1] : void 0, !1);
      },
    });
  },
  function (t, e, r) {
    r(287), (t.exports = r(7).String.trimLeft);
  },
  function (t, e, r) {
    "use strict";
    r(39)(
      "trimLeft",
      function (t) {
        return function () {
          return t(this, 1);
        };
      },
      "trimStart"
    );
  },
  function (t, e, r) {
    r(289), (t.exports = r(7).String.trimRight);
  },
  function (t, e, r) {
    "use strict";
    r(39)(
      "trimRight",
      function (t) {
        return function () {
          return t(this, 2);
        };
      },
      "trimEnd"
    );
  },
  function (t, e, r) {
    r(291), (t.exports = r(65).f("asyncIterator"));
  },
  function (t, e, r) {
    r(94)("asyncIterator");
  },
  function (t, e, r) {
    r(293), (t.exports = r(7).Object.getOwnPropertyDescriptors);
  },
  function (t, e, r) {
    var n = r(0),
      i = r(121),
      o = r(15),
      a = r(20),
      s = r(81);
    n(n.S, "Object", {
      getOwnPropertyDescriptors: function (t) {
        for (
          var e, r, n = o(t), u = a.f, c = i(n), f = {}, l = 0;
          c.length > l;

        )
          void 0 !== (r = u(n, (e = c[l++]))) && s(f, e, r);
        return f;
      },
    });
  },
  function (t, e, r) {
    r(295), (t.exports = r(7).Object.values);
  },
  function (t, e, r) {
    var n = r(0),
      i = r(123)(!1);
    n(n.S, "Object", {
      values: function (t) {
        return i(t);
      },
    });
  },
  function (t, e, r) {
    r(297), (t.exports = r(7).Object.entries);
  },
  function (t, e, r) {
    var n = r(0),
      i = r(123)(!0);
    n(n.S, "Object", {
      entries: function (t) {
        return i(t);
      },
    });
  },
  function (t, e, r) {
    "use strict";
    r(115), r(299), (t.exports = r(7).Promise.finally);
  },
  function (t, e, r) {
    "use strict";
    var n = r(0),
      i = r(7),
      o = r(1),
      a = r(47),
      s = r(117);
    n(n.P + n.R, "Promise", {
      finally: function (t) {
        var e = a(this, i.Promise || o.Promise),
          r = "function" == typeof t;
        return this.then(
          r
            ? function (r) {
                return s(e, t()).then(function () {
                  return r;
                });
              }
            : t,
          r
            ? function (r) {
                return s(e, t()).then(function () {
                  throw r;
                });
              }
            : t
        );
      },
    });
  },
  function (t, e, r) {
    r(301), r(302), r(303), (t.exports = r(7));
  },
  function (t, e, r) {
    var n = r(1),
      i = r(0),
      o = r(57),
      a = [].slice,
      s = /MSIE .\./.test(o),
      u = function (t) {
        return function (e, r) {
          var n = arguments.length > 2,
            i = !!n && a.call(arguments, 2);
          return t(
            n
              ? function () {
                  ("function" == typeof e ? e : Function(e)).apply(this, i);
                }
              : e,
            r
          );
        };
      };
    i(i.G + i.B + i.F * s, {
      setTimeout: u(n.setTimeout),
      setInterval: u(n.setInterval),
    });
  },
  function (t, e, r) {
    var n = r(0),
      i = r(87);
    n(n.G + n.B, { setImmediate: i.set, clearImmediate: i.clear });
  },
  function (t, e, r) {
    for (
      var n = r(84),
        i = r(31),
        o = r(11),
        a = r(1),
        s = r(14),
        u = r(40),
        c = r(5),
        f = c("iterator"),
        l = c("toStringTag"),
        h = u.Array,
        p = {
          CSSRuleList: !0,
          CSSStyleDeclaration: !1,
          CSSValueList: !1,
          ClientRectList: !1,
          DOMRectList: !1,
          DOMStringList: !1,
          DOMTokenList: !0,
          DataTransferItemList: !1,
          FileList: !1,
          HTMLAllCollection: !1,
          HTMLCollection: !1,
          HTMLFormElement: !1,
          HTMLSelectElement: !1,
          MediaList: !0,
          MimeTypeArray: !1,
          NamedNodeMap: !1,
          NodeList: !0,
          PaintRequestList: !1,
          Plugin: !1,
          PluginArray: !1,
          SVGLengthList: !1,
          SVGNumberList: !1,
          SVGPathSegList: !1,
          SVGPointList: !1,
          SVGStringList: !1,
          SVGTransformList: !1,
          SourceBufferList: !1,
          StyleSheetList: !0,
          TextTrackCueList: !1,
          TextTrackList: !1,
          TouchList: !1,
        },
        d = i(p),
        v = 0;
      v < d.length;
      v++
    ) {
      var y,
        g = d[v],
        m = p[g],
        b = a[g],
        w = b && b.prototype;
      if (w && (w[f] || s(w, f, h), w[l] || s(w, l, g), (u[g] = h), m))
        for (y in n) w[y] || o(w, y, n[y], !0);
    }
  },
  function (t, e, r) {
    var n = (function (t) {
      "use strict";
      var e = Object.prototype,
        r = e.hasOwnProperty,
        n = "function" == typeof Symbol ? Symbol : {},
        i = n.iterator || "@@iterator",
        o = n.asyncIterator || "@@asyncIterator",
        a = n.toStringTag || "@@toStringTag";
      function s(t, e, r) {
        return (
          Object.defineProperty(t, e, {
            value: r,
            enumerable: !0,
            configurable: !0,
            writable: !0,
          }),
          t[e]
        );
      }
      try {
        s({}, "");
      } catch (t) {
        s = function (t, e, r) {
          return (t[e] = r);
        };
      }
      function u(t, e, r, n) {
        var i = e && e.prototype instanceof l ? e : l,
          o = Object.create(i.prototype),
          a = new T(n || []);
        return (
          (o._invoke = (function (t, e, r) {
            var n = "suspendedStart";
            return function (i, o) {
              if ("executing" === n)
                throw new Error("Generator is already running");
              if ("completed" === n) {
                if ("throw" === i) throw o;
                return S();
              }
              for (r.method = i, r.arg = o; ; ) {
                var a = r.delegate;
                if (a) {
                  var s = w(a, r);
                  if (s) {
                    if (s === f) continue;
                    return s;
                  }
                }
                if ("next" === r.method) r.sent = r._sent = r.arg;
                else if ("throw" === r.method) {
                  if ("suspendedStart" === n) throw ((n = "completed"), r.arg);
                  r.dispatchException(r.arg);
                } else "return" === r.method && r.abrupt("return", r.arg);
                n = "executing";
                var u = c(t, e, r);
                if ("normal" === u.type) {
                  if (
                    ((n = r.done ? "completed" : "suspendedYield"), u.arg === f)
                  )
                    continue;
                  return { value: u.arg, done: r.done };
                }
                "throw" === u.type &&
                  ((n = "completed"), (r.method = "throw"), (r.arg = u.arg));
              }
            };
          })(t, r, a)),
          o
        );
      }
      function c(t, e, r) {
        try {
          return { type: "normal", arg: t.call(e, r) };
        } catch (t) {
          return { type: "throw", arg: t };
        }
      }
      t.wrap = u;
      var f = {};
      function l() {}
      function h() {}
      function p() {}
      var d = {};
      d[i] = function () {
        return this;
      };
      var v = Object.getPrototypeOf,
        y = v && v(v(x([])));
      y && y !== e && r.call(y, i) && (d = y);
      var g = (p.prototype = l.prototype = Object.create(d));
      function m(t) {
        ["next", "throw", "return"].forEach(function (e) {
          s(t, e, function (t) {
            return this._invoke(e, t);
          });
        });
      }
      function b(t, e) {
        var n;
        this._invoke = function (i, o) {
          function a() {
            return new e(function (n, a) {
              !(function n(i, o, a, s) {
                var u = c(t[i], t, o);
                if ("throw" !== u.type) {
                  var f = u.arg,
                    l = f.value;
                  return l && "object" == typeof l && r.call(l, "__await")
                    ? e.resolve(l.__await).then(
                        function (t) {
                          n("next", t, a, s);
                        },
                        function (t) {
                          n("throw", t, a, s);
                        }
                      )
                    : e.resolve(l).then(
                        function (t) {
                          (f.value = t), a(f);
                        },
                        function (t) {
                          return n("throw", t, a, s);
                        }
                      );
                }
                s(u.arg);
              })(i, o, n, a);
            });
          }
          return (n = n ? n.then(a, a) : a());
        };
      }
      function w(t, e) {
        var r = t.iterator[e.method];
        if (void 0 === r) {
          if (((e.delegate = null), "throw" === e.method)) {
            if (
              t.iterator.return &&
              ((e.method = "return"),
              (e.arg = void 0),
              w(t, e),
              "throw" === e.method)
            )
              return f;
            (e.method = "throw"),
              (e.arg = new TypeError(
                "The iterator does not provide a 'throw' method"
              ));
          }
          return f;
        }
        var n = c(r, t.iterator, e.arg);
        if ("throw" === n.type)
          return (e.method = "throw"), (e.arg = n.arg), (e.delegate = null), f;
        var i = n.arg;
        return i
          ? i.done
            ? ((e[t.resultName] = i.value),
              (e.next = t.nextLoc),
              "return" !== e.method && ((e.method = "next"), (e.arg = void 0)),
              (e.delegate = null),
              f)
            : i
          : ((e.method = "throw"),
            (e.arg = new TypeError("iterator result is not an object")),
            (e.delegate = null),
            f);
      }
      function _(t) {
        var e = { tryLoc: t[0] };
        1 in t && (e.catchLoc = t[1]),
          2 in t && ((e.finallyLoc = t[2]), (e.afterLoc = t[3])),
          this.tryEntries.push(e);
      }
      function E(t) {
        var e = t.completion || {};
        (e.type = "normal"), delete e.arg, (t.completion = e);
      }
      function T(t) {
        (this.tryEntries = [{ tryLoc: "root" }]),
          t.forEach(_, this),
          this.reset(!0);
      }
      function x(t) {
        if (t) {
          var e = t[i];
          if (e) return e.call(t);
          if ("function" == typeof t.next) return t;
          if (!isNaN(t.length)) {
            var n = -1,
              o = function e() {
                for (; ++n < t.length; )
                  if (r.call(t, n)) return (e.value = t[n]), (e.done = !1), e;
                return (e.value = void 0), (e.done = !0), e;
              };
            return (o.next = o);
          }
        }
        return { next: S };
      }
      function S() {
        return { value: void 0, done: !0 };
      }
      return (
        (h.prototype = g.constructor = p),
        (p.constructor = h),
        (h.displayName = s(p, a, "GeneratorFunction")),
        (t.isGeneratorFunction = function (t) {
          var e = "function" == typeof t && t.constructor;
          return (
            !!e &&
            (e === h || "GeneratorFunction" === (e.displayName || e.name))
          );
        }),
        (t.mark = function (t) {
          return (
            Object.setPrototypeOf
              ? Object.setPrototypeOf(t, p)
              : ((t.__proto__ = p), s(t, a, "GeneratorFunction")),
            (t.prototype = Object.create(g)),
            t
          );
        }),
        (t.awrap = function (t) {
          return { __await: t };
        }),
        m(b.prototype),
        (b.prototype[o] = function () {
          return this;
        }),
        (t.AsyncIterator = b),
        (t.async = function (e, r, n, i, o) {
          void 0 === o && (o = Promise);
          var a = new b(u(e, r, n, i), o);
          return t.isGeneratorFunction(r)
            ? a
            : a.next().then(function (t) {
                return t.done ? t.value : a.next();
              });
        }),
        m(g),
        s(g, a, "Generator"),
        (g[i] = function () {
          return this;
        }),
        (g.toString = function () {
          return "[object Generator]";
        }),
        (t.keys = function (t) {
          var e = [];
          for (var r in t) e.push(r);
          return (
            e.reverse(),
            function r() {
              for (; e.length; ) {
                var n = e.pop();
                if (n in t) return (r.value = n), (r.done = !1), r;
              }
              return (r.done = !0), r;
            }
          );
        }),
        (t.values = x),
        (T.prototype = {
          constructor: T,
          reset: function (t) {
            if (
              ((this.prev = 0),
              (this.next = 0),
              (this.sent = this._sent = void 0),
              (this.done = !1),
              (this.delegate = null),
              (this.method = "next"),
              (this.arg = void 0),
              this.tryEntries.forEach(E),
              !t)
            )
              for (var e in this)
                "t" === e.charAt(0) &&
                  r.call(this, e) &&
                  !isNaN(+e.slice(1)) &&
                  (this[e] = void 0);
          },
          stop: function () {
            this.done = !0;
            var t = this.tryEntries[0].completion;
            if ("throw" === t.type) throw t.arg;
            return this.rval;
          },
          dispatchException: function (t) {
            if (this.done) throw t;
            var e = this;
            function n(r, n) {
              return (
                (a.type = "throw"),
                (a.arg = t),
                (e.next = r),
                n && ((e.method = "next"), (e.arg = void 0)),
                !!n
              );
            }
            for (var i = this.tryEntries.length - 1; i >= 0; --i) {
              var o = this.tryEntries[i],
                a = o.completion;
              if ("root" === o.tryLoc) return n("end");
              if (o.tryLoc <= this.prev) {
                var s = r.call(o, "catchLoc"),
                  u = r.call(o, "finallyLoc");
                if (s && u) {
                  if (this.prev < o.catchLoc) return n(o.catchLoc, !0);
                  if (this.prev < o.finallyLoc) return n(o.finallyLoc);
                } else if (s) {
                  if (this.prev < o.catchLoc) return n(o.catchLoc, !0);
                } else {
                  if (!u)
                    throw new Error("try statement without catch or finally");
                  if (this.prev < o.finallyLoc) return n(o.finallyLoc);
                }
              }
            }
          },
          abrupt: function (t, e) {
            for (var n = this.tryEntries.length - 1; n >= 0; --n) {
              var i = this.tryEntries[n];
              if (
                i.tryLoc <= this.prev &&
                r.call(i, "finallyLoc") &&
                this.prev < i.finallyLoc
              ) {
                var o = i;
                break;
              }
            }
            o &&
              ("break" === t || "continue" === t) &&
              o.tryLoc <= e &&
              e <= o.finallyLoc &&
              (o = null);
            var a = o ? o.completion : {};
            return (
              (a.type = t),
              (a.arg = e),
              o
                ? ((this.method = "next"), (this.next = o.finallyLoc), f)
                : this.complete(a)
            );
          },
          complete: function (t, e) {
            if ("throw" === t.type) throw t.arg;
            return (
              "break" === t.type || "continue" === t.type
                ? (this.next = t.arg)
                : "return" === t.type
                ? ((this.rval = this.arg = t.arg),
                  (this.method = "return"),
                  (this.next = "end"))
                : "normal" === t.type && e && (this.next = e),
              f
            );
          },
          finish: function (t) {
            for (var e = this.tryEntries.length - 1; e >= 0; --e) {
              var r = this.tryEntries[e];
              if (r.finallyLoc === t)
                return this.complete(r.completion, r.afterLoc), E(r), f;
            }
          },
          catch: function (t) {
            for (var e = this.tryEntries.length - 1; e >= 0; --e) {
              var r = this.tryEntries[e];
              if (r.tryLoc === t) {
                var n = r.completion;
                if ("throw" === n.type) {
                  var i = n.arg;
                  E(r);
                }
                return i;
              }
            }
            throw new Error("illegal catch attempt");
          },
          delegateYield: function (t, e, r) {
            return (
              (this.delegate = { iterator: x(t), resultName: e, nextLoc: r }),
              "next" === this.method && (this.arg = void 0),
              f
            );
          },
        }),
        t
      );
    })(t.exports);
    try {
      regeneratorRuntime = n;
    } catch (t) {
      Function("r", "regeneratorRuntime = r")(n);
    }
  },
  function (t, e, r) {
    r(306), (t.exports = r(124).global);
  },
  function (t, e, r) {
    var n = r(307);
    n(n.G, { global: r(89) });
  },
  function (t, e, r) {
    var n = r(89),
      i = r(124),
      o = r(308),
      a = r(310),
      s = r(317),
      u = function (t, e, r) {
        var c,
          f,
          l,
          h = t & u.F,
          p = t & u.G,
          d = t & u.S,
          v = t & u.P,
          y = t & u.B,
          g = t & u.W,
          m = p ? i : i[e] || (i[e] = {}),
          b = m.prototype,
          w = p ? n : d ? n[e] : (n[e] || {}).prototype;
        for (c in (p && (r = e), r))
          ((f = !h && w && void 0 !== w[c]) && s(m, c)) ||
            ((l = f ? w[c] : r[c]),
            (m[c] =
              p && "function" != typeof w[c]
                ? r[c]
                : y && f
                ? o(l, n)
                : g && w[c] == l
                ? (function (t) {
                    var e = function (e, r, n) {
                      if (this instanceof t) {
                        switch (arguments.length) {
                          case 0:
                            return new t();
                          case 1:
                            return new t(e);
                          case 2:
                            return new t(e, r);
                        }
                        return new t(e, r, n);
                      }
                      return t.apply(this, arguments);
                    };
                    return (e.prototype = t.prototype), e;
                  })(l)
                : v && "function" == typeof l
                ? o(Function.call, l)
                : l),
            v &&
              (((m.virtual || (m.virtual = {}))[c] = l),
              t & u.R && b && !b[c] && a(b, c, l)));
      };
    (u.F = 1),
      (u.G = 2),
      (u.S = 4),
      (u.P = 8),
      (u.B = 16),
      (u.W = 32),
      (u.U = 64),
      (u.R = 128),
      (t.exports = u);
  },
  function (t, e, r) {
    var n = r(309);
    t.exports = function (t, e, r) {
      if ((n(t), void 0 === e)) return t;
      switch (r) {
        case 1:
          return function (r) {
            return t.call(e, r);
          };
        case 2:
          return function (r, n) {
            return t.call(e, r, n);
          };
        case 3:
          return function (r, n, i) {
            return t.call(e, r, n, i);
          };
      }
      return function () {
        return t.apply(e, arguments);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if ("function" != typeof t) throw TypeError(t + " is not a function!");
      return t;
    };
  },
  function (t, e, r) {
    var n = r(311),
      i = r(316);
    t.exports = r(91)
      ? function (t, e, r) {
          return n.f(t, e, i(1, r));
        }
      : function (t, e, r) {
          return (t[e] = r), t;
        };
  },
  function (t, e, r) {
    var n = r(312),
      i = r(313),
      o = r(315),
      a = Object.defineProperty;
    e.f = r(91)
      ? Object.defineProperty
      : function (t, e, r) {
          if ((n(t), (e = o(e, !0)), n(r), i))
            try {
              return a(t, e, r);
            } catch (t) {}
          if ("get" in r || "set" in r)
            throw TypeError("Accessors not supported!");
          return "value" in r && (t[e] = r.value), t;
        };
  },
  function (t, e, r) {
    var n = r(90);
    t.exports = function (t) {
      if (!n(t)) throw TypeError(t + " is not an object!");
      return t;
    };
  },
  function (t, e, r) {
    t.exports =
      !r(91) &&
      !r(125)(function () {
        return (
          7 !=
          Object.defineProperty(r(314)("div"), "a", {
            get: function () {
              return 7;
            },
          }).a
        );
      });
  },
  function (t, e, r) {
    var n = r(90),
      i = r(89).document,
      o = n(i) && n(i.createElement);
    t.exports = function (t) {
      return o ? i.createElement(t) : {};
    };
  },
  function (t, e, r) {
    var n = r(90);
    t.exports = function (t, e) {
      if (!n(t)) return t;
      var r, i;
      if (e && "function" == typeof (r = t.toString) && !n((i = r.call(t))))
        return i;
      if ("function" == typeof (r = t.valueOf) && !n((i = r.call(t)))) return i;
      if (!e && "function" == typeof (r = t.toString) && !n((i = r.call(t))))
        return i;
      throw TypeError("Can't convert object to primitive value");
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: e,
      };
    };
  },
  function (t, e) {
    var r = {}.hasOwnProperty;
    t.exports = function (t, e) {
      return r.call(t, e);
    };
  },
  function (t, e, r) {
    (function (t) {
      !(function (t) {
        var e = (function () {
            try {
              return !!Symbol.iterator;
            } catch (t) {
              return !1;
            }
          })(),
          r = function (t) {
            var r = {
              next: function () {
                var e = t.shift();
                return { done: void 0 === e, value: e };
              },
            };
            return (
              e &&
                (r[Symbol.iterator] = function () {
                  return r;
                }),
              r
            );
          },
          n = function (t) {
            return encodeURIComponent(t).replace(/%20/g, "+");
          },
          i = function (t) {
            return decodeURIComponent(String(t).replace(/\+/g, " "));
          };
        (function () {
          try {
            var e = t.URLSearchParams;
            return (
              "a=1" === new e("?a=1").toString() &&
              "function" == typeof e.prototype.set &&
              "function" == typeof e.prototype.entries
            );
          } catch (t) {
            return !1;
          }
        })() ||
          (function () {
            var i = function (t) {
                Object.defineProperty(this, "_entries", {
                  writable: !0,
                  value: {},
                });
                var e = typeof t;
                if ("undefined" === e);
                else if ("string" === e) "" !== t && this._fromString(t);
                else if (t instanceof i) {
                  var r = this;
                  t.forEach(function (t, e) {
                    r.append(e, t);
                  });
                } else {
                  if (null === t || "object" !== e)
                    throw new TypeError(
                      "Unsupported input's type for URLSearchParams"
                    );
                  if ("[object Array]" === Object.prototype.toString.call(t))
                    for (var n = 0; n < t.length; n++) {
                      var o = t[n];
                      if (
                        "[object Array]" !==
                          Object.prototype.toString.call(o) &&
                        2 === o.length
                      )
                        throw new TypeError(
                          "Expected [string, any] as entry at index " +
                            n +
                            " of URLSearchParams's input"
                        );
                      this.append(o[0], o[1]);
                    }
                  else
                    for (var a in t)
                      t.hasOwnProperty(a) && this.append(a, t[a]);
                }
              },
              o = i.prototype;
            (o.append = function (t, e) {
              t in this._entries
                ? this._entries[t].push(String(e))
                : (this._entries[t] = [String(e)]);
            }),
              (o.delete = function (t) {
                delete this._entries[t];
              }),
              (o.get = function (t) {
                return t in this._entries ? this._entries[t][0] : null;
              }),
              (o.getAll = function (t) {
                return t in this._entries ? this._entries[t].slice(0) : [];
              }),
              (o.has = function (t) {
                return t in this._entries;
              }),
              (o.set = function (t, e) {
                this._entries[t] = [String(e)];
              }),
              (o.forEach = function (t, e) {
                var r;
                for (var n in this._entries)
                  if (this._entries.hasOwnProperty(n)) {
                    r = this._entries[n];
                    for (var i = 0; i < r.length; i++) t.call(e, r[i], n, this);
                  }
              }),
              (o.keys = function () {
                var t = [];
                return (
                  this.forEach(function (e, r) {
                    t.push(r);
                  }),
                  r(t)
                );
              }),
              (o.values = function () {
                var t = [];
                return (
                  this.forEach(function (e) {
                    t.push(e);
                  }),
                  r(t)
                );
              }),
              (o.entries = function () {
                var t = [];
                return (
                  this.forEach(function (e, r) {
                    t.push([r, e]);
                  }),
                  r(t)
                );
              }),
              e && (o[Symbol.iterator] = o.entries),
              (o.toString = function () {
                var t = [];
                return (
                  this.forEach(function (e, r) {
                    t.push(n(r) + "=" + n(e));
                  }),
                  t.join("&")
                );
              }),
              (t.URLSearchParams = i);
          })();
        var o = t.URLSearchParams.prototype;
        "function" != typeof o.sort &&
          (o.sort = function () {
            var t = this,
              e = [];
            this.forEach(function (r, n) {
              e.push([n, r]), t._entries || t.delete(n);
            }),
              e.sort(function (t, e) {
                return t[0] < e[0] ? -1 : t[0] > e[0] ? 1 : 0;
              }),
              t._entries && (t._entries = {});
            for (var r = 0; r < e.length; r++) this.append(e[r][0], e[r][1]);
          }),
          "function" != typeof o._fromString &&
            Object.defineProperty(o, "_fromString", {
              enumerable: !1,
              configurable: !1,
              writable: !1,
              value: function (t) {
                if (this._entries) this._entries = {};
                else {
                  var e = [];
                  this.forEach(function (t, r) {
                    e.push(r);
                  });
                  for (var r = 0; r < e.length; r++) this.delete(e[r]);
                }
                var n,
                  o = (t = t.replace(/^\?/, "")).split("&");
                for (r = 0; r < o.length; r++)
                  (n = o[r].split("=")),
                    this.append(i(n[0]), n.length > 1 ? i(n[1]) : "");
              },
            });
      })(
        void 0 !== t
          ? t
          : "undefined" != typeof window
          ? window
          : "undefined" != typeof self
          ? self
          : this
      ),
        (function (t) {
          var e, r, n;
          if (
            ((function () {
              try {
                var e = new t.URL("b", "http://a");
                return (
                  (e.pathname = "c d"),
                  "http://a/c%20d" === e.href && e.searchParams
                );
              } catch (t) {
                return !1;
              }
            })() ||
              ((e = t.URL),
              (n = (r = function (e, r) {
                "string" != typeof e && (e = String(e)),
                  r && "string" != typeof r && (r = String(r));
                var n,
                  i = document;
                if (r && (void 0 === t.location || r !== t.location.href)) {
                  (r = r.toLowerCase()),
                    ((n = (i =
                      document.implementation.createHTMLDocument(
                        ""
                      )).createElement("base")).href = r),
                    i.head.appendChild(n);
                  try {
                    if (0 !== n.href.indexOf(r)) throw new Error(n.href);
                  } catch (t) {
                    throw new Error(
                      "URL unable to set base " + r + " due to " + t
                    );
                  }
                }
                var o = i.createElement("a");
                (o.href = e), n && (i.body.appendChild(o), (o.href = o.href));
                var a = i.createElement("input");
                if (
                  ((a.type = "url"),
                  (a.value = e),
                  ":" === o.protocol ||
                    !/:/.test(o.href) ||
                    (!a.checkValidity() && !r))
                )
                  throw new TypeError("Invalid URL");
                Object.defineProperty(this, "_anchorElement", { value: o });
                var s = new t.URLSearchParams(this.search),
                  u = !0,
                  c = !0,
                  f = this;
                ["append", "delete", "set"].forEach(function (t) {
                  var e = s[t];
                  s[t] = function () {
                    e.apply(s, arguments),
                      u && ((c = !1), (f.search = s.toString()), (c = !0));
                  };
                }),
                  Object.defineProperty(this, "searchParams", {
                    value: s,
                    enumerable: !0,
                  });
                var l = void 0;
                Object.defineProperty(this, "_updateSearchParams", {
                  enumerable: !1,
                  configurable: !1,
                  writable: !1,
                  value: function () {
                    this.search !== l &&
                      ((l = this.search),
                      c &&
                        ((u = !1),
                        this.searchParams._fromString(this.search),
                        (u = !0)));
                  },
                });
              }).prototype),
              ["hash", "host", "hostname", "port", "protocol"].forEach(
                function (t) {
                  !(function (t) {
                    Object.defineProperty(n, t, {
                      get: function () {
                        return this._anchorElement[t];
                      },
                      set: function (e) {
                        this._anchorElement[t] = e;
                      },
                      enumerable: !0,
                    });
                  })(t);
                }
              ),
              Object.defineProperty(n, "search", {
                get: function () {
                  return this._anchorElement.search;
                },
                set: function (t) {
                  (this._anchorElement.search = t), this._updateSearchParams();
                },
                enumerable: !0,
              }),
              Object.defineProperties(n, {
                toString: {
                  get: function () {
                    var t = this;
                    return function () {
                      return t.href;
                    };
                  },
                },
                href: {
                  get: function () {
                    return this._anchorElement.href.replace(/\?$/, "");
                  },
                  set: function (t) {
                    (this._anchorElement.href = t), this._updateSearchParams();
                  },
                  enumerable: !0,
                },
                pathname: {
                  get: function () {
                    return this._anchorElement.pathname.replace(/(^\/?)/, "/");
                  },
                  set: function (t) {
                    this._anchorElement.pathname = t;
                  },
                  enumerable: !0,
                },
                origin: {
                  get: function () {
                    var t = { "http:": 80, "https:": 443, "ftp:": 21 }[
                        this._anchorElement.protocol
                      ],
                      e =
                        this._anchorElement.port != t &&
                        "" !== this._anchorElement.port;
                    return (
                      this._anchorElement.protocol +
                      "//" +
                      this._anchorElement.hostname +
                      (e ? ":" + this._anchorElement.port : "")
                    );
                  },
                  enumerable: !0,
                },
                password: {
                  get: function () {
                    return "";
                  },
                  set: function (t) {},
                  enumerable: !0,
                },
                username: {
                  get: function () {
                    return "";
                  },
                  set: function (t) {},
                  enumerable: !0,
                },
              }),
              (r.createObjectURL = function (t) {
                return e.createObjectURL.apply(e, arguments);
              }),
              (r.revokeObjectURL = function (t) {
                return e.revokeObjectURL.apply(e, arguments);
              }),
              (t.URL = r)),
            void 0 !== t.location && !("origin" in t.location))
          ) {
            var i = function () {
              return (
                t.location.protocol +
                "//" +
                t.location.hostname +
                (t.location.port ? ":" + t.location.port : "")
              );
            };
            try {
              Object.defineProperty(t.location, "origin", {
                get: i,
                enumerable: !0,
              });
            } catch (e) {
              setInterval(function () {
                t.location.origin = i();
              }, 100);
            }
          }
        })(
          void 0 !== t
            ? t
            : "undefined" != typeof window
            ? window
            : "undefined" != typeof self
            ? self
            : this
        );
    }).call(this, r(60));
  },
  function (t, e, r) {
    const n = r(61),
      i = r(62),
      o = r(325),
      a = r(92),
      s = r(63),
      { setTimeout: u } = r(126),
      c = r(127),
      f = r(128),
      l = r(334),
      h = r(130),
      p = r(131),
      d = "https://www.youtube.com/watch?v=";
    (e.cache = new p()),
      (e.cookieCache = new p(864e5)),
      (e.watchPageCache = new p());
    class v extends Error {}
    const y = [
      "support.google.com/youtube/?p=age_restrictions",
      "youtube.com/t/community_guidelines",
    ];
    e.getBasicInfo = async (t, e) => {
      const r = Object.assign({}, a.defaultOptions, e.requestOptions);
      let n = await T(
        [t, e],
        (t) => {
          let e = s.playError(t.player_response, ["ERROR"], v),
            r = g(t.player_response);
          if (e || r)
            throw (console.log("-------getBasicInfo--------err"), e || r);
          return (
            t &&
            t.player_response &&
            (t.player_response.streamingData ||
              m(t.player_response) ||
              b(t.player_response))
          );
        },
        r,
        [N, P, L]
      );
      Object.assign(n, {
        formats: D(n.player_response),
        related_videos: l.getRelatedVideos(n),
      });
      const i = l.getMedia(n);
      let o = {
        author: l.getAuthor(n),
        media: i,
        likes: l.getLikes(n),
        dislikes: l.getDislikes(n),
        age_restricted: !!(
          i &&
          i.notice_url &&
          y.some((t) => i.notice_url.includes(t))
        ),
        video_url: d + t,
      };
      return (
        (n.videoDetails = l.cleanVideoDetails(
          Object.assign(
            {},
            n.player_response &&
              n.player_response.microformat &&
              n.player_response.microformat.playerMicroformatRenderer,
            n.player_response && n.player_response.videoDetails,
            o
          )
        )),
        n
      );
    };
    const g = (t) => {
        let e = t && t.playabilityStatus;
        return e &&
          "LOGIN_REQUIRED" === e.status &&
          e.messages &&
          e.messages.filter((t) => /This is a private video/.test(t)).length
          ? new v(e.reason || (e.messages && e.messages[0]))
          : null;
      },
      m = (t) => {
        let e = t.playabilityStatus;
        return (
          e &&
          "UNPLAYABLE" === e.status &&
          e.errorScreen &&
          e.errorScreen.playerLegacyDesktopYpcOfferRenderer
        );
      },
      b = (t) => {
        let e = t.playabilityStatus;
        return e && "LIVE_STREAM_OFFLINE" === e.status;
      },
      w = (t, e) => `${d + t}&hl=${e.lang || "en"}`,
      _ = (t, r) => {
        const n = w(t, r);
        return e.watchPageCache.getOrSet(n, () =>
          a(n, r.requestOptions).text()
        );
      },
      E = (t) => {
        let e =
          /<script\s+src="([^"]+)"(?:\s+type="text\/javascript")?\s+name="player_ias\/base"\s*>|"jsUrl":"([^"]+)"/.exec(
            t
          );
        return e ? e[1] || e[2] : null;
      },
      T = async (t, e, r, n) => {
        let i;
        for (let o of n)
          try {
            const n = await S(o, t.concat([i]), r);
            if (
              (n.player_response &&
                ((n.player_response.videoDetails = x(
                  i && i.player_response && i.player_response.videoDetails,
                  n.player_response.videoDetails
                )),
                (n.player_response = x(
                  i && i.player_response,
                  n.player_response
                ))),
              (i = x(i, n)),
              e(i, !1))
            )
              break;
          } catch (t) {
            if (
              (console.log("-------pipeline--------err", t),
              t instanceof v || o === n[n.length - 1])
            )
              throw t;
          }
        return i;
      },
      x = (t, e) => {
        if (!t || !e) return t || e;
        for (let [r, n] of Object.entries(e)) null != n && (t[r] = n);
        return t;
      },
      S = async (t, e, r) => {
        let n,
          i = 0;
        for (; i <= r.maxRetries; )
          try {
            n = await t(...e);
            break;
          } catch (t) {
            if (
              (console.log("-------retryFunc--------err", t),
              t instanceof v ||
                (t instanceof a.MinigetError && t.statusCode < 500) ||
                i >= r.maxRetries)
            )
              throw t;
            let e = Math.min(++i * r.backoff.inc, r.backoff.max);
            await new Promise((t) => u(t, e));
          }
        return n;
      },
      A = /^[)\]}'\s]+/,
      I = (t, e, r) => {
        if (!r || "object" == typeof r) return r;
        try {
          return (r = r.replace(A, "")), JSON.parse(r);
        } catch (r) {
          throw Error(`Error parsing ${e} in ${t}: ${r.message}`);
        }
      },
      O = (t, e, r, n, i, o = "") => {
        let a = s.between(r, n, i);
        if (!a) throw Error(`Could not find ${e} in ${t}`);
        return I(t, e, s.cutAfterJSON(`${o}${a}`));
      },
      R = (t, e) => {
        const r =
          e &&
          ((e.args && e.args.player_response) ||
            e.player_response ||
            e.playerResponse ||
            e.embedded_player_response);
        return I(t, "player_response", r);
      },
      P = async (t, r) => {
        const n = Object.assign({ headers: {} }, r.requestOptions);
        let i = n.headers.Cookie || n.headers.cookie;
        n.headers = Object.assign(
          {
            "x-youtube-client-name": "1",
            "x-youtube-client-version": "2.20201203.06.00",
            "x-youtube-identity-token": e.cookieCache.get(i || "browser") || "",
          },
          n.headers
        );
        const o = async (i, o) => {
          n.headers["x-youtube-identity-token"] ||
            (n.headers["x-youtube-identity-token"] = await ((t, r, n, i) =>
              e.cookieCache.getOrSet(n, async () => {
                let e = (await _(t, r)).match(
                  /(["'])ID_TOKEN\1[:,]\s?"([^"]+)"/
                );
                if (!e && i)
                  throw new v(
                    "Cookie header used in request, but unable to find YouTube identity token"
                  );
                return e && e[2];
              }))(t, r, i, o));
        };
        i && (await o(i, !0));
        const s = ((t, e) => w(t, e) + "&pbj=1")(t, r);
        let u = await a(s, n).text(),
          c = I("watch.json", "body", u);
        if (
          ("now" === c.reload && (await o("browser", !1)),
          "now" === c.reload || !Array.isArray(c))
        )
          throw Error("Unable to retrieve video metadata in watch.json");
        let f = c.reduce((t, e) => Object.assign(e, t), {});
        return (
          (f.player_response = R("watch.json", f)),
          (f.html5player = f.player && f.player.assets && f.player.assets.js),
          f
        );
      },
      N = async (t, e) => {
        let r = await _(t, e),
          n = { page: "watch" };
        try {
          n.player_response = O(
            "watch.html",
            "player_response",
            r,
            /\bytInitialPlayerResponse\s*=\s*\{/i,
            "\n",
            "{"
          );
        } catch (t) {
          console.log("-------getWatchHTMLPage--------err", t);
          let e = O(
            "watch.html",
            "player_response",
            r,
            /\bytplayer\.config\s*=\s*{/,
            "</script>",
            "{"
          );
          n.player_response = R("watch.html", e);
        }
        return (
          (n.response = O(
            "watch.html",
            "response",
            r,
            /\bytInitialData("\])?\s*=\s*\{/i,
            "\n",
            "{"
          )),
          (n.html5player = E(r)),
          n
        );
      },
      L = async (t, e) => {
        const r = n.format({
          protocol: "https",
          host: "www.youtube.com",
          pathname: "/get_video_info",
          query: {
            video_id: t,
            eurl: "https://youtube.googleapis.com/v/" + t,
            ps: "default",
            gl: "US",
            hl: e.lang || "en",
          },
        });
        let o = await a(r, e.requestOptions).text(),
          s = i.parse(o);
        return (s.player_response = R("get_video_info", s)), s;
      },
      D = (t) => {
        let e = [];
        return (
          t &&
            t.streamingData &&
            (e = e
              .concat(t.streamingData.formats || [])
              .concat(t.streamingData.adaptiveFormats || [])),
          e
        );
      };
    e.getInfo = async (t, r) => {
      let i = await e.getBasicInfo(t, r);
      const o =
        i.player_response &&
        i.player_response.streamingData &&
        (i.player_response.streamingData.dashManifestUrl ||
          i.player_response.streamingData.hlsManifestUrl);
      let s = [];
      if (i.formats.length) {
        if (
          ((i.html5player =
            i.html5player ||
            E(await _(t, r)) ||
            E(
              await ((t, e) => {
                const r = `${"https://www.youtube.com/embed/" + t}?hl=${
                  e.lang || "en"
                }`;
                return a(r, e.requestOptions).text();
              })(t, r)
            )),
          !i.html5player)
        )
          throw Error("Unable to find html5player file");
        const e = n.resolve(d, i.html5player);
        s.push(h.decipherFormats(i.formats, e, r));
      }
      if (o && i.player_response.streamingData.dashManifestUrl) {
        let t = i.player_response.streamingData.dashManifestUrl;
        s.push(F(t, r));
      }
      if (o && i.player_response.streamingData.hlsManifestUrl) {
        let t = i.player_response.streamingData.hlsManifestUrl;
        s.push(C(t, r));
      }
      let u = await Promise.all(s);
      return (
        (i.formats = Object.values(Object.assign({}, ...u))),
        (i.formats = i.formats.map(c.addFormatMeta)),
        i.formats.sort(c.sortFormats),
        (i.full = !0),
        i
      );
    };
    const F = (t, e) =>
        new Promise((r, i) => {
          let s = {};
          const u = o.parser(!1);
          let c;
          (u.onerror = i),
            (u.onopentag = (e) => {
              if ("ADAPTATIONSET" === e.name) c = e.attributes;
              else if ("REPRESENTATION" === e.name) {
                const r = parseInt(e.attributes.ID);
                isNaN(r) ||
                  (s[t] = Object.assign(
                    {
                      itag: r,
                      url: t,
                      bitrate: parseInt(e.attributes.BANDWIDTH),
                      mimeType: `${c.MIMETYPE}; codecs="${e.attributes.CODECS}"`,
                    },
                    e.attributes.HEIGHT
                      ? {
                          width: parseInt(e.attributes.WIDTH),
                          height: parseInt(e.attributes.HEIGHT),
                          fps: parseInt(e.attributes.FRAMERATE),
                        }
                      : { audioSampleRate: e.attributes.AUDIOSAMPLINGRATE }
                  ));
              }
            }),
            (u.onend = () => {
              r(s);
            });
          const f = a(n.resolve(d, t), e.requestOptions);
          f.setEncoding("utf8"),
            f.on("error", i),
            f.on("data", (t) => {
              u.write(t);
            }),
            f.on("end", u.close.bind(u));
        }),
      C = async (t, e) => {
        t = n.resolve(d, t);
        let r = await a(t, e.requestOptions).text(),
          i = {};
        return (
          r
            .split("\n")
            .filter((t) => /^https?:\/\//.test(t))
            .forEach((t) => {
              const e = parseInt(t.match(/\/itag\/(\d+)\//)[1]);
              i[t] = { itag: e, url: t };
            }),
          i
        );
      };
    for (let t of ["getBasicInfo", "getInfo"]) {
      const r = e[t];
      e[t] = (n, i = {}) => {
        let o = f.getVideoID(n);
        const a = [t, o, i.lang].join("-");
        return e.cache.getOrSet(a, () => r(o, i));
      };
    }
    (e.validateID = f.validateID),
      (e.validateURL = f.validateURL),
      (e.getURLVideoID = f.getURLVideoID),
      (e.getVideoID = f.getVideoID);
  },
  function (t, e, r) {
    (function (t, n) {
      var i;
      /*! https://mths.be/punycode v1.3.2 by @mathias */ !(function (o) {
        e && e.nodeType, t && t.nodeType;
        var a = "object" == typeof n && n;
        a.global !== a && a.window !== a && a.self;
        var s,
          u = 2147483647,
          c = /^xn--/,
          f = /[^\x20-\x7E]/,
          l = /[\x2E\u3002\uFF0E\uFF61]/g,
          h = {
            overflow: "Overflow: input needs wider integers to process",
            "not-basic": "Illegal input >= 0x80 (not a basic code point)",
            "invalid-input": "Invalid input",
          },
          p = Math.floor,
          d = String.fromCharCode;
        function v(t) {
          throw RangeError(h[t]);
        }
        function y(t, e) {
          for (var r = t.length, n = []; r--; ) n[r] = e(t[r]);
          return n;
        }
        function g(t, e) {
          var r = t.split("@"),
            n = "";
          return (
            r.length > 1 && ((n = r[0] + "@"), (t = r[1])),
            n + y((t = t.replace(l, ".")).split("."), e).join(".")
          );
        }
        function m(t) {
          for (var e, r, n = [], i = 0, o = t.length; i < o; )
            (e = t.charCodeAt(i++)) >= 55296 && e <= 56319 && i < o
              ? 56320 == (64512 & (r = t.charCodeAt(i++)))
                ? n.push(((1023 & e) << 10) + (1023 & r) + 65536)
                : (n.push(e), i--)
              : n.push(e);
          return n;
        }
        function b(t) {
          return y(t, function (t) {
            var e = "";
            return (
              t > 65535 &&
                ((e += d((((t -= 65536) >>> 10) & 1023) | 55296)),
                (t = 56320 | (1023 & t))),
              (e += d(t))
            );
          }).join("");
        }
        function w(t, e) {
          return t + 22 + 75 * (t < 26) - ((0 != e) << 5);
        }
        function _(t, e, r) {
          var n = 0;
          for (t = r ? p(t / 700) : t >> 1, t += p(t / e); t > 455; n += 36)
            t = p(t / 35);
          return p(n + (36 * t) / (t + 38));
        }
        function E(t) {
          var e,
            r,
            n,
            i,
            o,
            a,
            s,
            c,
            f,
            l,
            h,
            d = [],
            y = t.length,
            g = 0,
            m = 128,
            w = 72;
          for ((r = t.lastIndexOf("-")) < 0 && (r = 0), n = 0; n < r; ++n)
            t.charCodeAt(n) >= 128 && v("not-basic"), d.push(t.charCodeAt(n));
          for (i = r > 0 ? r + 1 : 0; i < y; ) {
            for (
              o = g, a = 1, s = 36;
              i >= y && v("invalid-input"),
                ((c =
                  (h = t.charCodeAt(i++)) - 48 < 10
                    ? h - 22
                    : h - 65 < 26
                    ? h - 65
                    : h - 97 < 26
                    ? h - 97
                    : 36) >= 36 ||
                  c > p((u - g) / a)) &&
                  v("overflow"),
                (g += c * a),
                !(c < (f = s <= w ? 1 : s >= w + 26 ? 26 : s - w));
              s += 36
            )
              a > p(u / (l = 36 - f)) && v("overflow"), (a *= l);
            (w = _(g - o, (e = d.length + 1), 0 == o)),
              p(g / e) > u - m && v("overflow"),
              (m += p(g / e)),
              (g %= e),
              d.splice(g++, 0, m);
          }
          return b(d);
        }
        function T(t) {
          var e,
            r,
            n,
            i,
            o,
            a,
            s,
            c,
            f,
            l,
            h,
            y,
            g,
            b,
            E,
            T = [];
          for (y = (t = m(t)).length, e = 128, r = 0, o = 72, a = 0; a < y; ++a)
            (h = t[a]) < 128 && T.push(d(h));
          for (n = i = T.length, i && T.push("-"); n < y; ) {
            for (s = u, a = 0; a < y; ++a) (h = t[a]) >= e && h < s && (s = h);
            for (
              s - e > p((u - r) / (g = n + 1)) && v("overflow"),
                r += (s - e) * g,
                e = s,
                a = 0;
              a < y;
              ++a
            )
              if (((h = t[a]) < e && ++r > u && v("overflow"), h == e)) {
                for (
                  c = r, f = 36;
                  !(c < (l = f <= o ? 1 : f >= o + 26 ? 26 : f - o));
                  f += 36
                )
                  (E = c - l),
                    (b = 36 - l),
                    T.push(d(w(l + (E % b), 0))),
                    (c = p(E / b));
                T.push(d(w(c, 0))), (o = _(r, g, n == i)), (r = 0), ++n;
              }
            ++r, ++e;
          }
          return T.join("");
        }
        (s = {
          version: "1.3.2",
          ucs2: { decode: m, encode: b },
          decode: E,
          encode: T,
          toASCII: function (t) {
            return g(t, function (t) {
              return f.test(t) ? "xn--" + T(t) : t;
            });
          },
          toUnicode: function (t) {
            return g(t, function (t) {
              return c.test(t) ? E(t.slice(4).toLowerCase()) : t;
            });
          },
        }),
          void 0 ===
            (i = function () {
              return s;
            }.call(e, r, e, t)) || (t.exports = i);
      })();
    }).call(this, r(321)(t), r(60));
  },
  function (t, e) {
    t.exports = function (t) {
      return (
        t.webpackPolyfill ||
          ((t.deprecate = function () {}),
          (t.paths = []),
          t.children || (t.children = []),
          Object.defineProperty(t, "loaded", {
            enumerable: !0,
            get: function () {
              return t.l;
            },
          }),
          Object.defineProperty(t, "id", {
            enumerable: !0,
            get: function () {
              return t.i;
            },
          }),
          (t.webpackPolyfill = 1)),
        t
      );
    };
  },
  function (t, e, r) {
    "use strict";
    t.exports = {
      isString: function (t) {
        return "string" == typeof t;
      },
      isObject: function (t) {
        return "object" == typeof t && null !== t;
      },
      isNull: function (t) {
        return null === t;
      },
      isNullOrUndefined: function (t) {
        return null == t;
      },
    };
  },
  function (t, e, r) {
    "use strict";
    function n(t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }
    t.exports = function (t, e, r, o) {
      (e = e || "&"), (r = r || "=");
      var a = {};
      if ("string" != typeof t || 0 === t.length) return a;
      var s = /\+/g;
      t = t.split(e);
      var u = 1e3;
      o && "number" == typeof o.maxKeys && (u = o.maxKeys);
      var c = t.length;
      u > 0 && c > u && (c = u);
      for (var f = 0; f < c; ++f) {
        var l,
          h,
          p,
          d,
          v = t[f].replace(s, "%20"),
          y = v.indexOf(r);
        y >= 0
          ? ((l = v.substr(0, y)), (h = v.substr(y + 1)))
          : ((l = v), (h = "")),
          (p = decodeURIComponent(l)),
          (d = decodeURIComponent(h)),
          n(a, p) ? (i(a[p]) ? a[p].push(d) : (a[p] = [a[p], d])) : (a[p] = d);
      }
      return a;
    };
    var i =
      Array.isArray ||
      function (t) {
        return "[object Array]" === Object.prototype.toString.call(t);
      };
  },
  function (t, e, r) {
    "use strict";
    var n = function (t) {
      switch (typeof t) {
        case "string":
          return t;
        case "boolean":
          return t ? "true" : "false";
        case "number":
          return isFinite(t) ? t : "";
        default:
          return "";
      }
    };
    t.exports = function (t, e, r, s) {
      return (
        (e = e || "&"),
        (r = r || "="),
        null === t && (t = void 0),
        "object" == typeof t
          ? o(a(t), function (a) {
              var s = encodeURIComponent(n(a)) + r;
              return i(t[a])
                ? o(t[a], function (t) {
                    return s + encodeURIComponent(n(t));
                  }).join(e)
                : s + encodeURIComponent(n(t[a]));
            }).join(e)
          : s
          ? encodeURIComponent(n(s)) + r + encodeURIComponent(n(t))
          : ""
      );
    };
    var i =
      Array.isArray ||
      function (t) {
        return "[object Array]" === Object.prototype.toString.call(t);
      };
    function o(t, e) {
      if (t.map) return t.map(e);
      for (var r = [], n = 0; n < t.length; n++) r.push(e(t[n], n));
      return r;
    }
    var a =
      Object.keys ||
      function (t) {
        var e = [];
        for (var r in t)
          Object.prototype.hasOwnProperty.call(t, r) && e.push(r);
        return e;
      };
  },
  function (t, e, r) {
    t.exports = r(326);
  },
  function (t, e, r) {
    (function (t) {
      !(function (e) {
        (e.parser = function (t, e) {
          return new n(t, e);
        }),
          (e.SAXParser = n),
          (e.SAXStream = a),
          (e.createStream = function (t, e) {
            return new a(t, e);
          }),
          (e.MAX_BUFFER_LENGTH = 65536);
        var r = [
          "comment",
          "sgmlDecl",
          "textNode",
          "tagName",
          "doctype",
          "procInstName",
          "procInstBody",
          "entity",
          "attribName",
          "attribValue",
          "cdata",
          "script",
        ];
        function n(t, i) {
          if (!(this instanceof n)) return new n(t, i);
          !(function (t) {
            for (var e = 0, n = r.length; e < n; e++) t[r[e]] = "";
          })(this),
            (this.q = this.c = ""),
            (this.bufferCheckPosition = e.MAX_BUFFER_LENGTH),
            (this.opt = i || {}),
            (this.opt.lowercase = this.opt.lowercase || this.opt.lowercasetags),
            (this.looseCase = this.opt.lowercase
              ? "toLowerCase"
              : "toUpperCase"),
            (this.tags = []),
            (this.closed = this.closedRoot = this.sawRoot = !1),
            (this.tag = this.error = null),
            (this.strict = !!t),
            (this.noscript = !(!t && !this.opt.noscript)),
            (this.state = _.BEGIN),
            (this.strictEntities = this.opt.strictEntities),
            (this.ENTITIES = this.strictEntities
              ? Object.create(e.XML_ENTITIES)
              : Object.create(e.ENTITIES)),
            (this.attribList = []),
            this.opt.xmlns && (this.ns = Object.create(u)),
            (this.trackPosition = !1 !== this.opt.position),
            this.trackPosition && (this.position = this.line = this.column = 0),
            T(this, "onready");
        }
        (e.EVENTS = [
          "text",
          "processinginstruction",
          "sgmldeclaration",
          "doctype",
          "comment",
          "opentagstart",
          "attribute",
          "opentag",
          "closetag",
          "opencdata",
          "cdata",
          "closecdata",
          "error",
          "end",
          "ready",
          "script",
          "opennamespace",
          "closenamespace",
        ]),
          Object.create ||
            (Object.create = function (t) {
              function e() {}
              return (e.prototype = t), new e();
            }),
          Object.keys ||
            (Object.keys = function (t) {
              var e = [];
              for (var r in t) t.hasOwnProperty(r) && e.push(r);
              return e;
            }),
          (n.prototype = {
            end: function () {
              O(this);
            },
            write: function (t) {
              if (this.error) throw this.error;
              if (this.closed)
                return I(
                  this,
                  "Cannot write after close. Assign an onready handler."
                );
              if (null === t) return O(this);
              "object" == typeof t && (t = t.toString());
              var n = 0,
                i = "";
              for (; (i = B(t, n++)), (this.c = i), i; )
                switch (
                  (this.trackPosition &&
                    (this.position++,
                    "\n" === i
                      ? (this.line++, (this.column = 0))
                      : this.column++),
                  this.state)
                ) {
                  case _.BEGIN:
                    if (((this.state = _.BEGIN_WHITESPACE), "\ufeff" === i))
                      continue;
                    M(this, i);
                    continue;
                  case _.BEGIN_WHITESPACE:
                    M(this, i);
                    continue;
                  case _.TEXT:
                    if (this.sawRoot && !this.closedRoot) {
                      for (var o = n - 1; i && "<" !== i && "&" !== i; )
                        (i = B(t, n++)) &&
                          this.trackPosition &&
                          (this.position++,
                          "\n" === i
                            ? (this.line++, (this.column = 0))
                            : this.column++);
                      this.textNode += t.substring(o, n - 1);
                    }
                    "<" !== i ||
                    (this.sawRoot && this.closedRoot && !this.strict)
                      ? (p(i) ||
                          (this.sawRoot && !this.closedRoot) ||
                          R(this, "Text data outside of root node."),
                        "&" === i
                          ? (this.state = _.TEXT_ENTITY)
                          : (this.textNode += i))
                      : ((this.state = _.OPEN_WAKA),
                        (this.startTagPosition = this.position));
                    continue;
                  case _.SCRIPT:
                    "<" === i
                      ? (this.state = _.SCRIPT_ENDING)
                      : (this.script += i);
                    continue;
                  case _.SCRIPT_ENDING:
                    "/" === i
                      ? (this.state = _.CLOSE_TAG)
                      : ((this.script += "<" + i), (this.state = _.SCRIPT));
                    continue;
                  case _.OPEN_WAKA:
                    if ("!" === i)
                      (this.state = _.SGML_DECL), (this.sgmlDecl = "");
                    else if (p(i));
                    else if (y(c, i))
                      (this.state = _.OPEN_TAG), (this.tagName = i);
                    else if ("/" === i)
                      (this.state = _.CLOSE_TAG), (this.tagName = "");
                    else if ("?" === i)
                      (this.state = _.PROC_INST),
                        (this.procInstName = this.procInstBody = "");
                    else {
                      if (
                        (R(this, "Unencoded <"),
                        this.startTagPosition + 1 < this.position)
                      ) {
                        var a = this.position - this.startTagPosition;
                        i = new Array(a).join(" ") + i;
                      }
                      (this.textNode += "<" + i), (this.state = _.TEXT);
                    }
                    continue;
                  case _.SGML_DECL:
                    "[CDATA[" === (this.sgmlDecl + i).toUpperCase()
                      ? (x(this, "onopencdata"),
                        (this.state = _.CDATA),
                        (this.sgmlDecl = ""),
                        (this.cdata = ""))
                      : this.sgmlDecl + i === "--"
                      ? ((this.state = _.COMMENT),
                        (this.comment = ""),
                        (this.sgmlDecl = ""))
                      : "DOCTYPE" === (this.sgmlDecl + i).toUpperCase()
                      ? ((this.state = _.DOCTYPE),
                        (this.doctype || this.sawRoot) &&
                          R(
                            this,
                            "Inappropriately located doctype declaration"
                          ),
                        (this.doctype = ""),
                        (this.sgmlDecl = ""))
                      : ">" === i
                      ? (x(this, "onsgmldeclaration", this.sgmlDecl),
                        (this.sgmlDecl = ""),
                        (this.state = _.TEXT))
                      : d(i)
                      ? ((this.state = _.SGML_DECL_QUOTED),
                        (this.sgmlDecl += i))
                      : (this.sgmlDecl += i);
                    continue;
                  case _.SGML_DECL_QUOTED:
                    i === this.q && ((this.state = _.SGML_DECL), (this.q = "")),
                      (this.sgmlDecl += i);
                    continue;
                  case _.DOCTYPE:
                    ">" === i
                      ? ((this.state = _.TEXT),
                        x(this, "ondoctype", this.doctype),
                        (this.doctype = !0))
                      : ((this.doctype += i),
                        "[" === i
                          ? (this.state = _.DOCTYPE_DTD)
                          : d(i) &&
                            ((this.state = _.DOCTYPE_QUOTED), (this.q = i)));
                    continue;
                  case _.DOCTYPE_QUOTED:
                    (this.doctype += i),
                      i === this.q && ((this.q = ""), (this.state = _.DOCTYPE));
                    continue;
                  case _.DOCTYPE_DTD:
                    (this.doctype += i),
                      "]" === i
                        ? (this.state = _.DOCTYPE)
                        : d(i) &&
                          ((this.state = _.DOCTYPE_DTD_QUOTED), (this.q = i));
                    continue;
                  case _.DOCTYPE_DTD_QUOTED:
                    (this.doctype += i),
                      i === this.q &&
                        ((this.state = _.DOCTYPE_DTD), (this.q = ""));
                    continue;
                  case _.COMMENT:
                    "-" === i
                      ? (this.state = _.COMMENT_ENDING)
                      : (this.comment += i);
                    continue;
                  case _.COMMENT_ENDING:
                    "-" === i
                      ? ((this.state = _.COMMENT_ENDED),
                        (this.comment = A(this.opt, this.comment)),
                        this.comment && x(this, "oncomment", this.comment),
                        (this.comment = ""))
                      : ((this.comment += "-" + i), (this.state = _.COMMENT));
                    continue;
                  case _.COMMENT_ENDED:
                    ">" !== i
                      ? (R(this, "Malformed comment"),
                        (this.comment += "--" + i),
                        (this.state = _.COMMENT))
                      : (this.state = _.TEXT);
                    continue;
                  case _.CDATA:
                    "]" === i
                      ? (this.state = _.CDATA_ENDING)
                      : (this.cdata += i);
                    continue;
                  case _.CDATA_ENDING:
                    "]" === i
                      ? (this.state = _.CDATA_ENDING_2)
                      : ((this.cdata += "]" + i), (this.state = _.CDATA));
                    continue;
                  case _.CDATA_ENDING_2:
                    ">" === i
                      ? (this.cdata && x(this, "oncdata", this.cdata),
                        x(this, "onclosecdata"),
                        (this.cdata = ""),
                        (this.state = _.TEXT))
                      : "]" === i
                      ? (this.cdata += "]")
                      : ((this.cdata += "]]" + i), (this.state = _.CDATA));
                    continue;
                  case _.PROC_INST:
                    "?" === i
                      ? (this.state = _.PROC_INST_ENDING)
                      : p(i)
                      ? (this.state = _.PROC_INST_BODY)
                      : (this.procInstName += i);
                    continue;
                  case _.PROC_INST_BODY:
                    if (!this.procInstBody && p(i)) continue;
                    "?" === i
                      ? (this.state = _.PROC_INST_ENDING)
                      : (this.procInstBody += i);
                    continue;
                  case _.PROC_INST_ENDING:
                    ">" === i
                      ? (x(this, "onprocessinginstruction", {
                          name: this.procInstName,
                          body: this.procInstBody,
                        }),
                        (this.procInstName = this.procInstBody = ""),
                        (this.state = _.TEXT))
                      : ((this.procInstBody += "?" + i),
                        (this.state = _.PROC_INST_BODY));
                    continue;
                  case _.OPEN_TAG:
                    y(f, i)
                      ? (this.tagName += i)
                      : (P(this),
                        ">" === i
                          ? D(this)
                          : "/" === i
                          ? (this.state = _.OPEN_TAG_SLASH)
                          : (p(i) || R(this, "Invalid character in tag name"),
                            (this.state = _.ATTRIB)));
                    continue;
                  case _.OPEN_TAG_SLASH:
                    ">" === i
                      ? (D(this, !0), F(this))
                      : (R(
                          this,
                          "Forward-slash in opening tag not followed by >"
                        ),
                        (this.state = _.ATTRIB));
                    continue;
                  case _.ATTRIB:
                    if (p(i)) continue;
                    ">" === i
                      ? D(this)
                      : "/" === i
                      ? (this.state = _.OPEN_TAG_SLASH)
                      : y(c, i)
                      ? ((this.attribName = i),
                        (this.attribValue = ""),
                        (this.state = _.ATTRIB_NAME))
                      : R(this, "Invalid attribute name");
                    continue;
                  case _.ATTRIB_NAME:
                    "=" === i
                      ? (this.state = _.ATTRIB_VALUE)
                      : ">" === i
                      ? (R(this, "Attribute without value"),
                        (this.attribValue = this.attribName),
                        L(this),
                        D(this))
                      : p(i)
                      ? (this.state = _.ATTRIB_NAME_SAW_WHITE)
                      : y(f, i)
                      ? (this.attribName += i)
                      : R(this, "Invalid attribute name");
                    continue;
                  case _.ATTRIB_NAME_SAW_WHITE:
                    if ("=" === i) this.state = _.ATTRIB_VALUE;
                    else {
                      if (p(i)) continue;
                      R(this, "Attribute without value"),
                        (this.tag.attributes[this.attribName] = ""),
                        (this.attribValue = ""),
                        x(this, "onattribute", {
                          name: this.attribName,
                          value: "",
                        }),
                        (this.attribName = ""),
                        ">" === i
                          ? D(this)
                          : y(c, i)
                          ? ((this.attribName = i),
                            (this.state = _.ATTRIB_NAME))
                          : (R(this, "Invalid attribute name"),
                            (this.state = _.ATTRIB));
                    }
                    continue;
                  case _.ATTRIB_VALUE:
                    if (p(i)) continue;
                    d(i)
                      ? ((this.q = i), (this.state = _.ATTRIB_VALUE_QUOTED))
                      : (R(this, "Unquoted attribute value"),
                        (this.state = _.ATTRIB_VALUE_UNQUOTED),
                        (this.attribValue = i));
                    continue;
                  case _.ATTRIB_VALUE_QUOTED:
                    if (i !== this.q) {
                      "&" === i
                        ? (this.state = _.ATTRIB_VALUE_ENTITY_Q)
                        : (this.attribValue += i);
                      continue;
                    }
                    L(this),
                      (this.q = ""),
                      (this.state = _.ATTRIB_VALUE_CLOSED);
                    continue;
                  case _.ATTRIB_VALUE_CLOSED:
                    p(i)
                      ? (this.state = _.ATTRIB)
                      : ">" === i
                      ? D(this)
                      : "/" === i
                      ? (this.state = _.OPEN_TAG_SLASH)
                      : y(c, i)
                      ? (R(this, "No whitespace between attributes"),
                        (this.attribName = i),
                        (this.attribValue = ""),
                        (this.state = _.ATTRIB_NAME))
                      : R(this, "Invalid attribute name");
                    continue;
                  case _.ATTRIB_VALUE_UNQUOTED:
                    if (!v(i)) {
                      "&" === i
                        ? (this.state = _.ATTRIB_VALUE_ENTITY_U)
                        : (this.attribValue += i);
                      continue;
                    }
                    L(this), ">" === i ? D(this) : (this.state = _.ATTRIB);
                    continue;
                  case _.CLOSE_TAG:
                    if (this.tagName)
                      ">" === i
                        ? F(this)
                        : y(f, i)
                        ? (this.tagName += i)
                        : this.script
                        ? ((this.script += "</" + this.tagName),
                          (this.tagName = ""),
                          (this.state = _.SCRIPT))
                        : (p(i) || R(this, "Invalid tagname in closing tag"),
                          (this.state = _.CLOSE_TAG_SAW_WHITE));
                    else {
                      if (p(i)) continue;
                      g(c, i)
                        ? this.script
                          ? ((this.script += "</" + i), (this.state = _.SCRIPT))
                          : R(this, "Invalid tagname in closing tag.")
                        : (this.tagName = i);
                    }
                    continue;
                  case _.CLOSE_TAG_SAW_WHITE:
                    if (p(i)) continue;
                    ">" === i
                      ? F(this)
                      : R(this, "Invalid characters in closing tag");
                    continue;
                  case _.TEXT_ENTITY:
                  case _.ATTRIB_VALUE_ENTITY_Q:
                  case _.ATTRIB_VALUE_ENTITY_U:
                    var s, u;
                    switch (this.state) {
                      case _.TEXT_ENTITY:
                        (s = _.TEXT), (u = "textNode");
                        break;
                      case _.ATTRIB_VALUE_ENTITY_Q:
                        (s = _.ATTRIB_VALUE_QUOTED), (u = "attribValue");
                        break;
                      case _.ATTRIB_VALUE_ENTITY_U:
                        (s = _.ATTRIB_VALUE_UNQUOTED), (u = "attribValue");
                    }
                    ";" === i
                      ? ((this[u] += C(this)),
                        (this.entity = ""),
                        (this.state = s))
                      : y(this.entity.length ? h : l, i)
                      ? (this.entity += i)
                      : (R(this, "Invalid character in entity name"),
                        (this[u] += "&" + this.entity + i),
                        (this.entity = ""),
                        (this.state = s));
                    continue;
                  default:
                    throw new Error(this, "Unknown state: " + this.state);
                }
              this.position >= this.bufferCheckPosition &&
                (function (t) {
                  for (
                    var n = Math.max(e.MAX_BUFFER_LENGTH, 10),
                      i = 0,
                      o = 0,
                      a = r.length;
                    o < a;
                    o++
                  ) {
                    var s = t[r[o]].length;
                    if (s > n)
                      switch (r[o]) {
                        case "textNode":
                          S(t);
                          break;
                        case "cdata":
                          x(t, "oncdata", t.cdata), (t.cdata = "");
                          break;
                        case "script":
                          x(t, "onscript", t.script), (t.script = "");
                          break;
                        default:
                          I(t, "Max buffer length exceeded: " + r[o]);
                      }
                    i = Math.max(i, s);
                  }
                  var u = e.MAX_BUFFER_LENGTH - i;
                  t.bufferCheckPosition = u + t.position;
                })(this);
              return this;
            },
            /*! http://mths.be/fromcodepoint v0.1.0 by @mathias */ resume:
              function () {
                return (this.error = null), this;
              },
            close: function () {
              return this.write(null);
            },
            flush: function () {
              var t;
              S((t = this)),
                "" !== t.cdata && (x(t, "oncdata", t.cdata), (t.cdata = "")),
                "" !== t.script &&
                  (x(t, "onscript", t.script), (t.script = ""));
            },
          });
        var i = function () {},
          o = e.EVENTS.filter(function (t) {
            return "error" !== t && "end" !== t;
          });
        function a(t, e) {
          if (!(this instanceof a)) return new a(t, e);
          i.apply(this),
            (this._parser = new n(t, e)),
            (this.writable = !0),
            (this.readable = !0);
          var r = this;
          (this._parser.onend = function () {
            r.emit("end");
          }),
            (this._parser.onerror = function (t) {
              r.emit("error", t), (r._parser.error = null);
            }),
            (this._decoder = null),
            o.forEach(function (t) {
              Object.defineProperty(r, "on" + t, {
                get: function () {
                  return r._parser["on" + t];
                },
                set: function (e) {
                  if (!e)
                    return (
                      r.removeAllListeners(t), (r._parser["on" + t] = e), e
                    );
                  r.on(t, e);
                },
                enumerable: !0,
                configurable: !1,
              });
            });
        }
        (a.prototype = Object.create(i.prototype, {
          constructor: { value: a },
        })),
          (a.prototype.write = function (e) {
            if (
              "function" == typeof t &&
              "function" == typeof t.isBuffer &&
              t.isBuffer(e)
            ) {
              if (!this._decoder) {
                this._decoder = new null("utf8");
              }
              e = this._decoder.write(e);
            }
            return this._parser.write(e.toString()), this.emit("data", e), !0;
          }),
          (a.prototype.end = function (t) {
            return t && t.length && this.write(t), this._parser.end(), !0;
          }),
          (a.prototype.on = function (t, e) {
            var r = this;
            return (
              r._parser["on" + t] ||
                -1 === o.indexOf(t) ||
                (r._parser["on" + t] = function () {
                  var e =
                    1 === arguments.length
                      ? [arguments[0]]
                      : Array.apply(null, arguments);
                  e.splice(0, 0, t), r.emit.apply(r, e);
                }),
              i.prototype.on.call(r, t, e)
            );
          });
        var s = "http://www.w3.org/XML/1998/namespace",
          u = { xml: s, xmlns: "http://www.w3.org/2000/xmlns/" },
          c =
            /[:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD]/,
          f =
            /[:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u00B7\u0300-\u036F\u203F-\u2040.\d-]/,
          l =
            /[#:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD]/,
          h =
            /[#:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u00B7\u0300-\u036F\u203F-\u2040.\d-]/;
        function p(t) {
          return " " === t || "\n" === t || "\r" === t || "\t" === t;
        }
        function d(t) {
          return '"' === t || "'" === t;
        }
        function v(t) {
          return ">" === t || p(t);
        }
        function y(t, e) {
          return t.test(e);
        }
        function g(t, e) {
          return !y(t, e);
        }
        var m,
          b,
          w,
          _ = 0;
        for (var E in ((e.STATE = {
          BEGIN: _++,
          BEGIN_WHITESPACE: _++,
          TEXT: _++,
          TEXT_ENTITY: _++,
          OPEN_WAKA: _++,
          SGML_DECL: _++,
          SGML_DECL_QUOTED: _++,
          DOCTYPE: _++,
          DOCTYPE_QUOTED: _++,
          DOCTYPE_DTD: _++,
          DOCTYPE_DTD_QUOTED: _++,
          COMMENT_STARTING: _++,
          COMMENT: _++,
          COMMENT_ENDING: _++,
          COMMENT_ENDED: _++,
          CDATA: _++,
          CDATA_ENDING: _++,
          CDATA_ENDING_2: _++,
          PROC_INST: _++,
          PROC_INST_BODY: _++,
          PROC_INST_ENDING: _++,
          OPEN_TAG: _++,
          OPEN_TAG_SLASH: _++,
          ATTRIB: _++,
          ATTRIB_NAME: _++,
          ATTRIB_NAME_SAW_WHITE: _++,
          ATTRIB_VALUE: _++,
          ATTRIB_VALUE_QUOTED: _++,
          ATTRIB_VALUE_CLOSED: _++,
          ATTRIB_VALUE_UNQUOTED: _++,
          ATTRIB_VALUE_ENTITY_Q: _++,
          ATTRIB_VALUE_ENTITY_U: _++,
          CLOSE_TAG: _++,
          CLOSE_TAG_SAW_WHITE: _++,
          SCRIPT: _++,
          SCRIPT_ENDING: _++,
        }),
        (e.XML_ENTITIES = { amp: "&", gt: ">", lt: "<", quot: '"', apos: "'" }),
        (e.ENTITIES = {
          amp: "&",
          gt: ">",
          lt: "<",
          quot: '"',
          apos: "'",
          AElig: 198,
          Aacute: 193,
          Acirc: 194,
          Agrave: 192,
          Aring: 197,
          Atilde: 195,
          Auml: 196,
          Ccedil: 199,
          ETH: 208,
          Eacute: 201,
          Ecirc: 202,
          Egrave: 200,
          Euml: 203,
          Iacute: 205,
          Icirc: 206,
          Igrave: 204,
          Iuml: 207,
          Ntilde: 209,
          Oacute: 211,
          Ocirc: 212,
          Ograve: 210,
          Oslash: 216,
          Otilde: 213,
          Ouml: 214,
          THORN: 222,
          Uacute: 218,
          Ucirc: 219,
          Ugrave: 217,
          Uuml: 220,
          Yacute: 221,
          aacute: 225,
          acirc: 226,
          aelig: 230,
          agrave: 224,
          aring: 229,
          atilde: 227,
          auml: 228,
          ccedil: 231,
          eacute: 233,
          ecirc: 234,
          egrave: 232,
          eth: 240,
          euml: 235,
          iacute: 237,
          icirc: 238,
          igrave: 236,
          iuml: 239,
          ntilde: 241,
          oacute: 243,
          ocirc: 244,
          ograve: 242,
          oslash: 248,
          otilde: 245,
          ouml: 246,
          szlig: 223,
          thorn: 254,
          uacute: 250,
          ucirc: 251,
          ugrave: 249,
          uuml: 252,
          yacute: 253,
          yuml: 255,
          copy: 169,
          reg: 174,
          nbsp: 160,
          iexcl: 161,
          cent: 162,
          pound: 163,
          curren: 164,
          yen: 165,
          brvbar: 166,
          sect: 167,
          uml: 168,
          ordf: 170,
          laquo: 171,
          not: 172,
          shy: 173,
          macr: 175,
          deg: 176,
          plusmn: 177,
          sup1: 185,
          sup2: 178,
          sup3: 179,
          acute: 180,
          micro: 181,
          para: 182,
          middot: 183,
          cedil: 184,
          ordm: 186,
          raquo: 187,
          frac14: 188,
          frac12: 189,
          frac34: 190,
          iquest: 191,
          times: 215,
          divide: 247,
          OElig: 338,
          oelig: 339,
          Scaron: 352,
          scaron: 353,
          Yuml: 376,
          fnof: 402,
          circ: 710,
          tilde: 732,
          Alpha: 913,
          Beta: 914,
          Gamma: 915,
          Delta: 916,
          Epsilon: 917,
          Zeta: 918,
          Eta: 919,
          Theta: 920,
          Iota: 921,
          Kappa: 922,
          Lambda: 923,
          Mu: 924,
          Nu: 925,
          Xi: 926,
          Omicron: 927,
          Pi: 928,
          Rho: 929,
          Sigma: 931,
          Tau: 932,
          Upsilon: 933,
          Phi: 934,
          Chi: 935,
          Psi: 936,
          Omega: 937,
          alpha: 945,
          beta: 946,
          gamma: 947,
          delta: 948,
          epsilon: 949,
          zeta: 950,
          eta: 951,
          theta: 952,
          iota: 953,
          kappa: 954,
          lambda: 955,
          mu: 956,
          nu: 957,
          xi: 958,
          omicron: 959,
          pi: 960,
          rho: 961,
          sigmaf: 962,
          sigma: 963,
          tau: 964,
          upsilon: 965,
          phi: 966,
          chi: 967,
          psi: 968,
          omega: 969,
          thetasym: 977,
          upsih: 978,
          piv: 982,
          ensp: 8194,
          emsp: 8195,
          thinsp: 8201,
          zwnj: 8204,
          zwj: 8205,
          lrm: 8206,
          rlm: 8207,
          ndash: 8211,
          mdash: 8212,
          lsquo: 8216,
          rsquo: 8217,
          sbquo: 8218,
          ldquo: 8220,
          rdquo: 8221,
          bdquo: 8222,
          dagger: 8224,
          Dagger: 8225,
          bull: 8226,
          hellip: 8230,
          permil: 8240,
          prime: 8242,
          Prime: 8243,
          lsaquo: 8249,
          rsaquo: 8250,
          oline: 8254,
          frasl: 8260,
          euro: 8364,
          image: 8465,
          weierp: 8472,
          real: 8476,
          trade: 8482,
          alefsym: 8501,
          larr: 8592,
          uarr: 8593,
          rarr: 8594,
          darr: 8595,
          harr: 8596,
          crarr: 8629,
          lArr: 8656,
          uArr: 8657,
          rArr: 8658,
          dArr: 8659,
          hArr: 8660,
          forall: 8704,
          part: 8706,
          exist: 8707,
          empty: 8709,
          nabla: 8711,
          isin: 8712,
          notin: 8713,
          ni: 8715,
          prod: 8719,
          sum: 8721,
          minus: 8722,
          lowast: 8727,
          radic: 8730,
          prop: 8733,
          infin: 8734,
          ang: 8736,
          and: 8743,
          or: 8744,
          cap: 8745,
          cup: 8746,
          int: 8747,
          there4: 8756,
          sim: 8764,
          cong: 8773,
          asymp: 8776,
          ne: 8800,
          equiv: 8801,
          le: 8804,
          ge: 8805,
          sub: 8834,
          sup: 8835,
          nsub: 8836,
          sube: 8838,
          supe: 8839,
          oplus: 8853,
          otimes: 8855,
          perp: 8869,
          sdot: 8901,
          lceil: 8968,
          rceil: 8969,
          lfloor: 8970,
          rfloor: 8971,
          lang: 9001,
          rang: 9002,
          loz: 9674,
          spades: 9824,
          clubs: 9827,
          hearts: 9829,
          diams: 9830,
        }),
        Object.keys(e.ENTITIES).forEach(function (t) {
          var r = e.ENTITIES[t],
            n = "number" == typeof r ? String.fromCharCode(r) : r;
          e.ENTITIES[t] = n;
        }),
        e.STATE))
          e.STATE[e.STATE[E]] = E;
        function T(t, e, r) {
          t[e] && t[e](r);
        }
        function x(t, e, r) {
          t.textNode && S(t), T(t, e, r);
        }
        function S(t) {
          (t.textNode = A(t.opt, t.textNode)),
            t.textNode && T(t, "ontext", t.textNode),
            (t.textNode = "");
        }
        function A(t, e) {
          return (
            t.trim && (e = e.trim()),
            t.normalize && (e = e.replace(/\s+/g, " ")),
            e
          );
        }
        function I(t, e) {
          return (
            S(t),
            t.trackPosition &&
              (e +=
                "\nLine: " +
                t.line +
                "\nColumn: " +
                t.column +
                "\nChar: " +
                t.c),
            (e = new Error(e)),
            (t.error = e),
            T(t, "onerror", e),
            t
          );
        }
        function O(t) {
          return (
            t.sawRoot && !t.closedRoot && R(t, "Unclosed root tag"),
            t.state !== _.BEGIN &&
              t.state !== _.BEGIN_WHITESPACE &&
              t.state !== _.TEXT &&
              I(t, "Unexpected end"),
            S(t),
            (t.c = ""),
            (t.closed = !0),
            T(t, "onend"),
            n.call(t, t.strict, t.opt),
            t
          );
        }
        function R(t, e) {
          if ("object" != typeof t || !(t instanceof n))
            throw new Error("bad call to strictFail");
          t.strict && I(t, e);
        }
        function P(t) {
          t.strict || (t.tagName = t.tagName[t.looseCase]());
          var e = t.tags[t.tags.length - 1] || t,
            r = (t.tag = { name: t.tagName, attributes: {} });
          t.opt.xmlns && (r.ns = e.ns),
            (t.attribList.length = 0),
            x(t, "onopentagstart", r);
        }
        function N(t, e) {
          var r = t.indexOf(":") < 0 ? ["", t] : t.split(":"),
            n = r[0],
            i = r[1];
          return (
            e && "xmlns" === t && ((n = "xmlns"), (i = "")),
            { prefix: n, local: i }
          );
        }
        function L(t) {
          if (
            (t.strict || (t.attribName = t.attribName[t.looseCase]()),
            -1 !== t.attribList.indexOf(t.attribName) ||
              t.tag.attributes.hasOwnProperty(t.attribName))
          )
            t.attribName = t.attribValue = "";
          else {
            if (t.opt.xmlns) {
              var e = N(t.attribName, !0),
                r = e.prefix,
                n = e.local;
              if ("xmlns" === r)
                if ("xml" === n && t.attribValue !== s)
                  R(
                    t,
                    "xml: prefix must be bound to " +
                      s +
                      "\nActual: " +
                      t.attribValue
                  );
                else if (
                  "xmlns" === n &&
                  "http://www.w3.org/2000/xmlns/" !== t.attribValue
                )
                  R(
                    t,
                    "xmlns: prefix must be bound to http://www.w3.org/2000/xmlns/\nActual: " +
                      t.attribValue
                  );
                else {
                  var i = t.tag,
                    o = t.tags[t.tags.length - 1] || t;
                  i.ns === o.ns && (i.ns = Object.create(o.ns)),
                    (i.ns[n] = t.attribValue);
                }
              t.attribList.push([t.attribName, t.attribValue]);
            } else
              (t.tag.attributes[t.attribName] = t.attribValue),
                x(t, "onattribute", {
                  name: t.attribName,
                  value: t.attribValue,
                });
            t.attribName = t.attribValue = "";
          }
        }
        function D(t, e) {
          if (t.opt.xmlns) {
            var r = t.tag,
              n = N(t.tagName);
            (r.prefix = n.prefix),
              (r.local = n.local),
              (r.uri = r.ns[n.prefix] || ""),
              r.prefix &&
                !r.uri &&
                (R(t, "Unbound namespace prefix: " + JSON.stringify(t.tagName)),
                (r.uri = n.prefix));
            var i = t.tags[t.tags.length - 1] || t;
            r.ns &&
              i.ns !== r.ns &&
              Object.keys(r.ns).forEach(function (e) {
                x(t, "onopennamespace", { prefix: e, uri: r.ns[e] });
              });
            for (var o = 0, a = t.attribList.length; o < a; o++) {
              var s = t.attribList[o],
                u = s[0],
                c = s[1],
                f = N(u, !0),
                l = f.prefix,
                h = f.local,
                p = "" === l ? "" : r.ns[l] || "",
                d = { name: u, value: c, prefix: l, local: h, uri: p };
              l &&
                "xmlns" !== l &&
                !p &&
                (R(t, "Unbound namespace prefix: " + JSON.stringify(l)),
                (d.uri = l)),
                (t.tag.attributes[u] = d),
                x(t, "onattribute", d);
            }
            t.attribList.length = 0;
          }
          (t.tag.isSelfClosing = !!e),
            (t.sawRoot = !0),
            t.tags.push(t.tag),
            x(t, "onopentag", t.tag),
            e ||
              (t.noscript || "script" !== t.tagName.toLowerCase()
                ? (t.state = _.TEXT)
                : (t.state = _.SCRIPT),
              (t.tag = null),
              (t.tagName = "")),
            (t.attribName = t.attribValue = ""),
            (t.attribList.length = 0);
        }
        function F(t) {
          if (!t.tagName)
            return (
              R(t, "Weird empty close tag."),
              (t.textNode += "</>"),
              void (t.state = _.TEXT)
            );
          if (t.script) {
            if ("script" !== t.tagName)
              return (
                (t.script += "</" + t.tagName + ">"),
                (t.tagName = ""),
                void (t.state = _.SCRIPT)
              );
            x(t, "onscript", t.script), (t.script = "");
          }
          var e = t.tags.length,
            r = t.tagName;
          t.strict || (r = r[t.looseCase]());
          for (var n = r; e--; ) {
            if (t.tags[e].name === n) break;
            R(t, "Unexpected close tag");
          }
          if (e < 0)
            return (
              R(t, "Unmatched closing tag: " + t.tagName),
              (t.textNode += "</" + t.tagName + ">"),
              void (t.state = _.TEXT)
            );
          t.tagName = r;
          for (var i = t.tags.length; i-- > e; ) {
            var o = (t.tag = t.tags.pop());
            (t.tagName = t.tag.name), x(t, "onclosetag", t.tagName);
            var a = {};
            for (var s in o.ns) a[s] = o.ns[s];
            var u = t.tags[t.tags.length - 1] || t;
            t.opt.xmlns &&
              o.ns !== u.ns &&
              Object.keys(o.ns).forEach(function (e) {
                var r = o.ns[e];
                x(t, "onclosenamespace", { prefix: e, uri: r });
              });
          }
          0 === e && (t.closedRoot = !0),
            (t.tagName = t.attribValue = t.attribName = ""),
            (t.attribList.length = 0),
            (t.state = _.TEXT);
        }
        function C(t) {
          var e,
            r = t.entity,
            n = r.toLowerCase(),
            i = "";
          return t.ENTITIES[r]
            ? t.ENTITIES[r]
            : t.ENTITIES[n]
            ? t.ENTITIES[n]
            : ("#" === (r = n).charAt(0) &&
                ("x" === r.charAt(1)
                  ? ((r = r.slice(2)), (i = (e = parseInt(r, 16)).toString(16)))
                  : ((r = r.slice(1)),
                    (i = (e = parseInt(r, 10)).toString(10)))),
              (r = r.replace(/^0+/, "")),
              isNaN(e) || i.toLowerCase() !== r
                ? (R(t, "Invalid character entity"), "&" + t.entity + ";")
                : String.fromCodePoint(e));
        }
        function M(t, e) {
          "<" === e
            ? ((t.state = _.OPEN_WAKA), (t.startTagPosition = t.position))
            : p(e) ||
              (R(t, "Non-whitespace before first tag."),
              (t.textNode = e),
              (t.state = _.TEXT));
        }
        function B(t, e) {
          var r = "";
          return e < t.length && (r = t.charAt(e)), r;
        }
        (_ = e.STATE),
          String.fromCodePoint ||
            ((m = String.fromCharCode),
            (b = Math.floor),
            (w = function () {
              var t,
                e,
                r = 16384,
                n = [],
                i = -1,
                o = arguments.length;
              if (!o) return "";
              for (var a = ""; ++i < o; ) {
                var s = Number(arguments[i]);
                if (!isFinite(s) || s < 0 || s > 1114111 || b(s) !== s)
                  throw RangeError("Invalid code point: " + s);
                s <= 65535
                  ? n.push(s)
                  : ((t = 55296 + ((s -= 65536) >> 10)),
                    (e = (s % 1024) + 56320),
                    n.push(t, e)),
                  (i + 1 === o || n.length > r) &&
                    ((a += m.apply(null, n)), (n.length = 0));
              }
              return a;
            }),
            Object.defineProperty
              ? Object.defineProperty(String, "fromCodePoint", {
                  value: w,
                  configurable: !0,
                  writable: !0,
                })
              : (String.fromCodePoint = w));
      })(e);
    }).call(this, r(327).Buffer);
  },
  function (t, e, r) {
    "use strict";
    (function (t) {
      /*!
       * The buffer module from node.js, for the browser.
       *
       * @author   Feross Aboukhadijeh <http://feross.org>
       * @license  MIT
       */
      var n = r(328),
        i = r(329),
        o = r(330);
      function a() {
        return u.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
      }
      function s(t, e) {
        if (a() < e) throw new RangeError("Invalid typed array length");
        return (
          u.TYPED_ARRAY_SUPPORT
            ? ((t = new Uint8Array(e)).__proto__ = u.prototype)
            : (null === t && (t = new u(e)), (t.length = e)),
          t
        );
      }
      function u(t, e, r) {
        if (!(u.TYPED_ARRAY_SUPPORT || this instanceof u))
          return new u(t, e, r);
        if ("number" == typeof t) {
          if ("string" == typeof e)
            throw new Error(
              "If encoding is specified then the first argument must be a string"
            );
          return l(this, t);
        }
        return c(this, t, e, r);
      }
      function c(t, e, r, n) {
        if ("number" == typeof e)
          throw new TypeError('"value" argument must not be a number');
        return "undefined" != typeof ArrayBuffer && e instanceof ArrayBuffer
          ? (function (t, e, r, n) {
              if ((e.byteLength, r < 0 || e.byteLength < r))
                throw new RangeError("'offset' is out of bounds");
              if (e.byteLength < r + (n || 0))
                throw new RangeError("'length' is out of bounds");
              e =
                void 0 === r && void 0 === n
                  ? new Uint8Array(e)
                  : void 0 === n
                  ? new Uint8Array(e, r)
                  : new Uint8Array(e, r, n);
              u.TYPED_ARRAY_SUPPORT
                ? ((t = e).__proto__ = u.prototype)
                : (t = h(t, e));
              return t;
            })(t, e, r, n)
          : "string" == typeof e
          ? (function (t, e, r) {
              ("string" == typeof r && "" !== r) || (r = "utf8");
              if (!u.isEncoding(r))
                throw new TypeError(
                  '"encoding" must be a valid string encoding'
                );
              var n = 0 | d(e, r),
                i = (t = s(t, n)).write(e, r);
              i !== n && (t = t.slice(0, i));
              return t;
            })(t, e, r)
          : (function (t, e) {
              if (u.isBuffer(e)) {
                var r = 0 | p(e.length);
                return 0 === (t = s(t, r)).length || e.copy(t, 0, 0, r), t;
              }
              if (e) {
                if (
                  ("undefined" != typeof ArrayBuffer &&
                    e.buffer instanceof ArrayBuffer) ||
                  "length" in e
                )
                  return "number" != typeof e.length || (n = e.length) != n
                    ? s(t, 0)
                    : h(t, e);
                if ("Buffer" === e.type && o(e.data)) return h(t, e.data);
              }
              var n;
              throw new TypeError(
                "First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object."
              );
            })(t, e);
      }
      function f(t) {
        if ("number" != typeof t)
          throw new TypeError('"size" argument must be a number');
        if (t < 0) throw new RangeError('"size" argument must not be negative');
      }
      function l(t, e) {
        if ((f(e), (t = s(t, e < 0 ? 0 : 0 | p(e))), !u.TYPED_ARRAY_SUPPORT))
          for (var r = 0; r < e; ++r) t[r] = 0;
        return t;
      }
      function h(t, e) {
        var r = e.length < 0 ? 0 : 0 | p(e.length);
        t = s(t, r);
        for (var n = 0; n < r; n += 1) t[n] = 255 & e[n];
        return t;
      }
      function p(t) {
        if (t >= a())
          throw new RangeError(
            "Attempt to allocate Buffer larger than maximum size: 0x" +
              a().toString(16) +
              " bytes"
          );
        return 0 | t;
      }
      function d(t, e) {
        if (u.isBuffer(t)) return t.length;
        if (
          "undefined" != typeof ArrayBuffer &&
          "function" == typeof ArrayBuffer.isView &&
          (ArrayBuffer.isView(t) || t instanceof ArrayBuffer)
        )
          return t.byteLength;
        "string" != typeof t && (t = "" + t);
        var r = t.length;
        if (0 === r) return 0;
        for (var n = !1; ; )
          switch (e) {
            case "ascii":
            case "latin1":
            case "binary":
              return r;
            case "utf8":
            case "utf-8":
            case void 0:
              return k(t).length;
            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return 2 * r;
            case "hex":
              return r >>> 1;
            case "base64":
              return V(t).length;
            default:
              if (n) return k(t).length;
              (e = ("" + e).toLowerCase()), (n = !0);
          }
      }
      function v(t, e, r) {
        var n = !1;
        if (((void 0 === e || e < 0) && (e = 0), e > this.length)) return "";
        if (((void 0 === r || r > this.length) && (r = this.length), r <= 0))
          return "";
        if ((r >>>= 0) <= (e >>>= 0)) return "";
        for (t || (t = "utf8"); ; )
          switch (t) {
            case "hex":
              return R(this, e, r);
            case "utf8":
            case "utf-8":
              return A(this, e, r);
            case "ascii":
              return I(this, e, r);
            case "latin1":
            case "binary":
              return O(this, e, r);
            case "base64":
              return S(this, e, r);
            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return P(this, e, r);
            default:
              if (n) throw new TypeError("Unknown encoding: " + t);
              (t = (t + "").toLowerCase()), (n = !0);
          }
      }
      function y(t, e, r) {
        var n = t[e];
        (t[e] = t[r]), (t[r] = n);
      }
      function g(t, e, r, n, i) {
        if (0 === t.length) return -1;
        if (
          ("string" == typeof r
            ? ((n = r), (r = 0))
            : r > 2147483647
            ? (r = 2147483647)
            : r < -2147483648 && (r = -2147483648),
          (r = +r),
          isNaN(r) && (r = i ? 0 : t.length - 1),
          r < 0 && (r = t.length + r),
          r >= t.length)
        ) {
          if (i) return -1;
          r = t.length - 1;
        } else if (r < 0) {
          if (!i) return -1;
          r = 0;
        }
        if (("string" == typeof e && (e = u.from(e, n)), u.isBuffer(e)))
          return 0 === e.length ? -1 : m(t, e, r, n, i);
        if ("number" == typeof e)
          return (
            (e &= 255),
            u.TYPED_ARRAY_SUPPORT &&
            "function" == typeof Uint8Array.prototype.indexOf
              ? i
                ? Uint8Array.prototype.indexOf.call(t, e, r)
                : Uint8Array.prototype.lastIndexOf.call(t, e, r)
              : m(t, [e], r, n, i)
          );
        throw new TypeError("val must be string, number or Buffer");
      }
      function m(t, e, r, n, i) {
        var o,
          a = 1,
          s = t.length,
          u = e.length;
        if (
          void 0 !== n &&
          ("ucs2" === (n = String(n).toLowerCase()) ||
            "ucs-2" === n ||
            "utf16le" === n ||
            "utf-16le" === n)
        ) {
          if (t.length < 2 || e.length < 2) return -1;
          (a = 2), (s /= 2), (u /= 2), (r /= 2);
        }
        function c(t, e) {
          return 1 === a ? t[e] : t.readUInt16BE(e * a);
        }
        if (i) {
          var f = -1;
          for (o = r; o < s; o++)
            if (c(t, o) === c(e, -1 === f ? 0 : o - f)) {
              if ((-1 === f && (f = o), o - f + 1 === u)) return f * a;
            } else -1 !== f && (o -= o - f), (f = -1);
        } else
          for (r + u > s && (r = s - u), o = r; o >= 0; o--) {
            for (var l = !0, h = 0; h < u; h++)
              if (c(t, o + h) !== c(e, h)) {
                l = !1;
                break;
              }
            if (l) return o;
          }
        return -1;
      }
      function b(t, e, r, n) {
        r = Number(r) || 0;
        var i = t.length - r;
        n ? (n = Number(n)) > i && (n = i) : (n = i);
        var o = e.length;
        if (o % 2 != 0) throw new TypeError("Invalid hex string");
        n > o / 2 && (n = o / 2);
        for (var a = 0; a < n; ++a) {
          var s = parseInt(e.substr(2 * a, 2), 16);
          if (isNaN(s)) return a;
          t[r + a] = s;
        }
        return a;
      }
      function w(t, e, r, n) {
        return q(k(e, t.length - r), t, r, n);
      }
      function _(t, e, r, n) {
        return q(
          (function (t) {
            for (var e = [], r = 0; r < t.length; ++r)
              e.push(255 & t.charCodeAt(r));
            return e;
          })(e),
          t,
          r,
          n
        );
      }
      function E(t, e, r, n) {
        return _(t, e, r, n);
      }
      function T(t, e, r, n) {
        return q(V(e), t, r, n);
      }
      function x(t, e, r, n) {
        return q(
          (function (t, e) {
            for (
              var r, n, i, o = [], a = 0;
              a < t.length && !((e -= 2) < 0);
              ++a
            )
              (r = t.charCodeAt(a)),
                (n = r >> 8),
                (i = r % 256),
                o.push(i),
                o.push(n);
            return o;
          })(e, t.length - r),
          t,
          r,
          n
        );
      }
      function S(t, e, r) {
        return 0 === e && r === t.length
          ? n.fromByteArray(t)
          : n.fromByteArray(t.slice(e, r));
      }
      function A(t, e, r) {
        r = Math.min(t.length, r);
        for (var n = [], i = e; i < r; ) {
          var o,
            a,
            s,
            u,
            c = t[i],
            f = null,
            l = c > 239 ? 4 : c > 223 ? 3 : c > 191 ? 2 : 1;
          if (i + l <= r)
            switch (l) {
              case 1:
                c < 128 && (f = c);
                break;
              case 2:
                128 == (192 & (o = t[i + 1])) &&
                  (u = ((31 & c) << 6) | (63 & o)) > 127 &&
                  (f = u);
                break;
              case 3:
                (o = t[i + 1]),
                  (a = t[i + 2]),
                  128 == (192 & o) &&
                    128 == (192 & a) &&
                    (u = ((15 & c) << 12) | ((63 & o) << 6) | (63 & a)) >
                      2047 &&
                    (u < 55296 || u > 57343) &&
                    (f = u);
                break;
              case 4:
                (o = t[i + 1]),
                  (a = t[i + 2]),
                  (s = t[i + 3]),
                  128 == (192 & o) &&
                    128 == (192 & a) &&
                    128 == (192 & s) &&
                    (u =
                      ((15 & c) << 18) |
                      ((63 & o) << 12) |
                      ((63 & a) << 6) |
                      (63 & s)) > 65535 &&
                    u < 1114112 &&
                    (f = u);
            }
          null === f
            ? ((f = 65533), (l = 1))
            : f > 65535 &&
              ((f -= 65536),
              n.push(((f >>> 10) & 1023) | 55296),
              (f = 56320 | (1023 & f))),
            n.push(f),
            (i += l);
        }
        return (function (t) {
          var e = t.length;
          if (e <= 4096) return String.fromCharCode.apply(String, t);
          var r = "",
            n = 0;
          for (; n < e; )
            r += String.fromCharCode.apply(String, t.slice(n, (n += 4096)));
          return r;
        })(n);
      }
      (e.Buffer = u),
        (e.SlowBuffer = function (t) {
          +t != t && (t = 0);
          return u.alloc(+t);
        }),
        (e.INSPECT_MAX_BYTES = 50),
        (u.TYPED_ARRAY_SUPPORT =
          void 0 !== t.TYPED_ARRAY_SUPPORT
            ? t.TYPED_ARRAY_SUPPORT
            : (function () {
                try {
                  var t = new Uint8Array(1);
                  return (
                    (t.__proto__ = {
                      __proto__: Uint8Array.prototype,
                      foo: function () {
                        return 42;
                      },
                    }),
                    42 === t.foo() &&
                      "function" == typeof t.subarray &&
                      0 === t.subarray(1, 1).byteLength
                  );
                } catch (t) {
                  return !1;
                }
              })()),
        (e.kMaxLength = a()),
        (u.poolSize = 8192),
        (u._augment = function (t) {
          return (t.__proto__ = u.prototype), t;
        }),
        (u.from = function (t, e, r) {
          return c(null, t, e, r);
        }),
        u.TYPED_ARRAY_SUPPORT &&
          ((u.prototype.__proto__ = Uint8Array.prototype),
          (u.__proto__ = Uint8Array),
          "undefined" != typeof Symbol &&
            Symbol.species &&
            u[Symbol.species] === u &&
            Object.defineProperty(u, Symbol.species, {
              value: null,
              configurable: !0,
            })),
        (u.alloc = function (t, e, r) {
          return (function (t, e, r, n) {
            return (
              f(e),
              e <= 0
                ? s(t, e)
                : void 0 !== r
                ? "string" == typeof n
                  ? s(t, e).fill(r, n)
                  : s(t, e).fill(r)
                : s(t, e)
            );
          })(null, t, e, r);
        }),
        (u.allocUnsafe = function (t) {
          return l(null, t);
        }),
        (u.allocUnsafeSlow = function (t) {
          return l(null, t);
        }),
        (u.isBuffer = function (t) {
          return !(null == t || !t._isBuffer);
        }),
        (u.compare = function (t, e) {
          if (!u.isBuffer(t) || !u.isBuffer(e))
            throw new TypeError("Arguments must be Buffers");
          if (t === e) return 0;
          for (
            var r = t.length, n = e.length, i = 0, o = Math.min(r, n);
            i < o;
            ++i
          )
            if (t[i] !== e[i]) {
              (r = t[i]), (n = e[i]);
              break;
            }
          return r < n ? -1 : n < r ? 1 : 0;
        }),
        (u.isEncoding = function (t) {
          switch (String(t).toLowerCase()) {
            case "hex":
            case "utf8":
            case "utf-8":
            case "ascii":
            case "latin1":
            case "binary":
            case "base64":
            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return !0;
            default:
              return !1;
          }
        }),
        (u.concat = function (t, e) {
          if (!o(t))
            throw new TypeError('"list" argument must be an Array of Buffers');
          if (0 === t.length) return u.alloc(0);
          var r;
          if (void 0 === e)
            for (e = 0, r = 0; r < t.length; ++r) e += t[r].length;
          var n = u.allocUnsafe(e),
            i = 0;
          for (r = 0; r < t.length; ++r) {
            var a = t[r];
            if (!u.isBuffer(a))
              throw new TypeError(
                '"list" argument must be an Array of Buffers'
              );
            a.copy(n, i), (i += a.length);
          }
          return n;
        }),
        (u.byteLength = d),
        (u.prototype._isBuffer = !0),
        (u.prototype.swap16 = function () {
          var t = this.length;
          if (t % 2 != 0)
            throw new RangeError("Buffer size must be a multiple of 16-bits");
          for (var e = 0; e < t; e += 2) y(this, e, e + 1);
          return this;
        }),
        (u.prototype.swap32 = function () {
          var t = this.length;
          if (t % 4 != 0)
            throw new RangeError("Buffer size must be a multiple of 32-bits");
          for (var e = 0; e < t; e += 4)
            y(this, e, e + 3), y(this, e + 1, e + 2);
          return this;
        }),
        (u.prototype.swap64 = function () {
          var t = this.length;
          if (t % 8 != 0)
            throw new RangeError("Buffer size must be a multiple of 64-bits");
          for (var e = 0; e < t; e += 8)
            y(this, e, e + 7),
              y(this, e + 1, e + 6),
              y(this, e + 2, e + 5),
              y(this, e + 3, e + 4);
          return this;
        }),
        (u.prototype.toString = function () {
          var t = 0 | this.length;
          return 0 === t
            ? ""
            : 0 === arguments.length
            ? A(this, 0, t)
            : v.apply(this, arguments);
        }),
        (u.prototype.equals = function (t) {
          if (!u.isBuffer(t)) throw new TypeError("Argument must be a Buffer");
          return this === t || 0 === u.compare(this, t);
        }),
        (u.prototype.inspect = function () {
          var t = "",
            r = e.INSPECT_MAX_BYTES;
          return (
            this.length > 0 &&
              ((t = this.toString("hex", 0, r).match(/.{2}/g).join(" ")),
              this.length > r && (t += " ... ")),
            "<Buffer " + t + ">"
          );
        }),
        (u.prototype.compare = function (t, e, r, n, i) {
          if (!u.isBuffer(t)) throw new TypeError("Argument must be a Buffer");
          if (
            (void 0 === e && (e = 0),
            void 0 === r && (r = t ? t.length : 0),
            void 0 === n && (n = 0),
            void 0 === i && (i = this.length),
            e < 0 || r > t.length || n < 0 || i > this.length)
          )
            throw new RangeError("out of range index");
          if (n >= i && e >= r) return 0;
          if (n >= i) return -1;
          if (e >= r) return 1;
          if (this === t) return 0;
          for (
            var o = (i >>>= 0) - (n >>>= 0),
              a = (r >>>= 0) - (e >>>= 0),
              s = Math.min(o, a),
              c = this.slice(n, i),
              f = t.slice(e, r),
              l = 0;
            l < s;
            ++l
          )
            if (c[l] !== f[l]) {
              (o = c[l]), (a = f[l]);
              break;
            }
          return o < a ? -1 : a < o ? 1 : 0;
        }),
        (u.prototype.includes = function (t, e, r) {
          return -1 !== this.indexOf(t, e, r);
        }),
        (u.prototype.indexOf = function (t, e, r) {
          return g(this, t, e, r, !0);
        }),
        (u.prototype.lastIndexOf = function (t, e, r) {
          return g(this, t, e, r, !1);
        }),
        (u.prototype.write = function (t, e, r, n) {
          if (void 0 === e) (n = "utf8"), (r = this.length), (e = 0);
          else if (void 0 === r && "string" == typeof e)
            (n = e), (r = this.length), (e = 0);
          else {
            if (!isFinite(e))
              throw new Error(
                "Buffer.write(string, encoding, offset[, length]) is no longer supported"
              );
            (e |= 0),
              isFinite(r)
                ? ((r |= 0), void 0 === n && (n = "utf8"))
                : ((n = r), (r = void 0));
          }
          var i = this.length - e;
          if (
            ((void 0 === r || r > i) && (r = i),
            (t.length > 0 && (r < 0 || e < 0)) || e > this.length)
          )
            throw new RangeError("Attempt to write outside buffer bounds");
          n || (n = "utf8");
          for (var o = !1; ; )
            switch (n) {
              case "hex":
                return b(this, t, e, r);
              case "utf8":
              case "utf-8":
                return w(this, t, e, r);
              case "ascii":
                return _(this, t, e, r);
              case "latin1":
              case "binary":
                return E(this, t, e, r);
              case "base64":
                return T(this, t, e, r);
              case "ucs2":
              case "ucs-2":
              case "utf16le":
              case "utf-16le":
                return x(this, t, e, r);
              default:
                if (o) throw new TypeError("Unknown encoding: " + n);
                (n = ("" + n).toLowerCase()), (o = !0);
            }
        }),
        (u.prototype.toJSON = function () {
          return {
            type: "Buffer",
            data: Array.prototype.slice.call(this._arr || this, 0),
          };
        });
      function I(t, e, r) {
        var n = "";
        r = Math.min(t.length, r);
        for (var i = e; i < r; ++i) n += String.fromCharCode(127 & t[i]);
        return n;
      }
      function O(t, e, r) {
        var n = "";
        r = Math.min(t.length, r);
        for (var i = e; i < r; ++i) n += String.fromCharCode(t[i]);
        return n;
      }
      function R(t, e, r) {
        var n = t.length;
        (!e || e < 0) && (e = 0), (!r || r < 0 || r > n) && (r = n);
        for (var i = "", o = e; o < r; ++o) i += j(t[o]);
        return i;
      }
      function P(t, e, r) {
        for (var n = t.slice(e, r), i = "", o = 0; o < n.length; o += 2)
          i += String.fromCharCode(n[o] + 256 * n[o + 1]);
        return i;
      }
      function N(t, e, r) {
        if (t % 1 != 0 || t < 0) throw new RangeError("offset is not uint");
        if (t + e > r)
          throw new RangeError("Trying to access beyond buffer length");
      }
      function L(t, e, r, n, i, o) {
        if (!u.isBuffer(t))
          throw new TypeError('"buffer" argument must be a Buffer instance');
        if (e > i || e < o)
          throw new RangeError('"value" argument is out of bounds');
        if (r + n > t.length) throw new RangeError("Index out of range");
      }
      function D(t, e, r, n) {
        e < 0 && (e = 65535 + e + 1);
        for (var i = 0, o = Math.min(t.length - r, 2); i < o; ++i)
          t[r + i] =
            (e & (255 << (8 * (n ? i : 1 - i)))) >>> (8 * (n ? i : 1 - i));
      }
      function F(t, e, r, n) {
        e < 0 && (e = 4294967295 + e + 1);
        for (var i = 0, o = Math.min(t.length - r, 4); i < o; ++i)
          t[r + i] = (e >>> (8 * (n ? i : 3 - i))) & 255;
      }
      function C(t, e, r, n, i, o) {
        if (r + n > t.length) throw new RangeError("Index out of range");
        if (r < 0) throw new RangeError("Index out of range");
      }
      function M(t, e, r, n, o) {
        return o || C(t, 0, r, 4), i.write(t, e, r, n, 23, 4), r + 4;
      }
      function B(t, e, r, n, o) {
        return o || C(t, 0, r, 8), i.write(t, e, r, n, 52, 8), r + 8;
      }
      (u.prototype.slice = function (t, e) {
        var r,
          n = this.length;
        if (
          ((t = ~~t) < 0 ? (t += n) < 0 && (t = 0) : t > n && (t = n),
          (e = void 0 === e ? n : ~~e) < 0
            ? (e += n) < 0 && (e = 0)
            : e > n && (e = n),
          e < t && (e = t),
          u.TYPED_ARRAY_SUPPORT)
        )
          (r = this.subarray(t, e)).__proto__ = u.prototype;
        else {
          var i = e - t;
          r = new u(i, void 0);
          for (var o = 0; o < i; ++o) r[o] = this[o + t];
        }
        return r;
      }),
        (u.prototype.readUIntLE = function (t, e, r) {
          (t |= 0), (e |= 0), r || N(t, e, this.length);
          for (var n = this[t], i = 1, o = 0; ++o < e && (i *= 256); )
            n += this[t + o] * i;
          return n;
        }),
        (u.prototype.readUIntBE = function (t, e, r) {
          (t |= 0), (e |= 0), r || N(t, e, this.length);
          for (var n = this[t + --e], i = 1; e > 0 && (i *= 256); )
            n += this[t + --e] * i;
          return n;
        }),
        (u.prototype.readUInt8 = function (t, e) {
          return e || N(t, 1, this.length), this[t];
        }),
        (u.prototype.readUInt16LE = function (t, e) {
          return e || N(t, 2, this.length), this[t] | (this[t + 1] << 8);
        }),
        (u.prototype.readUInt16BE = function (t, e) {
          return e || N(t, 2, this.length), (this[t] << 8) | this[t + 1];
        }),
        (u.prototype.readUInt32LE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            (this[t] | (this[t + 1] << 8) | (this[t + 2] << 16)) +
              16777216 * this[t + 3]
          );
        }),
        (u.prototype.readUInt32BE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            16777216 * this[t] +
              ((this[t + 1] << 16) | (this[t + 2] << 8) | this[t + 3])
          );
        }),
        (u.prototype.readIntLE = function (t, e, r) {
          (t |= 0), (e |= 0), r || N(t, e, this.length);
          for (var n = this[t], i = 1, o = 0; ++o < e && (i *= 256); )
            n += this[t + o] * i;
          return n >= (i *= 128) && (n -= Math.pow(2, 8 * e)), n;
        }),
        (u.prototype.readIntBE = function (t, e, r) {
          (t |= 0), (e |= 0), r || N(t, e, this.length);
          for (var n = e, i = 1, o = this[t + --n]; n > 0 && (i *= 256); )
            o += this[t + --n] * i;
          return o >= (i *= 128) && (o -= Math.pow(2, 8 * e)), o;
        }),
        (u.prototype.readInt8 = function (t, e) {
          return (
            e || N(t, 1, this.length),
            128 & this[t] ? -1 * (255 - this[t] + 1) : this[t]
          );
        }),
        (u.prototype.readInt16LE = function (t, e) {
          e || N(t, 2, this.length);
          var r = this[t] | (this[t + 1] << 8);
          return 32768 & r ? 4294901760 | r : r;
        }),
        (u.prototype.readInt16BE = function (t, e) {
          e || N(t, 2, this.length);
          var r = this[t + 1] | (this[t] << 8);
          return 32768 & r ? 4294901760 | r : r;
        }),
        (u.prototype.readInt32LE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            this[t] |
              (this[t + 1] << 8) |
              (this[t + 2] << 16) |
              (this[t + 3] << 24)
          );
        }),
        (u.prototype.readInt32BE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            (this[t] << 24) |
              (this[t + 1] << 16) |
              (this[t + 2] << 8) |
              this[t + 3]
          );
        }),
        (u.prototype.readFloatLE = function (t, e) {
          return e || N(t, 4, this.length), i.read(this, t, !0, 23, 4);
        }),
        (u.prototype.readFloatBE = function (t, e) {
          return e || N(t, 4, this.length), i.read(this, t, !1, 23, 4);
        }),
        (u.prototype.readDoubleLE = function (t, e) {
          return e || N(t, 8, this.length), i.read(this, t, !0, 52, 8);
        }),
        (u.prototype.readDoubleBE = function (t, e) {
          return e || N(t, 8, this.length), i.read(this, t, !1, 52, 8);
        }),
        (u.prototype.writeUIntLE = function (t, e, r, n) {
          ((t = +t), (e |= 0), (r |= 0), n) ||
            L(this, t, e, r, Math.pow(2, 8 * r) - 1, 0);
          var i = 1,
            o = 0;
          for (this[e] = 255 & t; ++o < r && (i *= 256); )
            this[e + o] = (t / i) & 255;
          return e + r;
        }),
        (u.prototype.writeUIntBE = function (t, e, r, n) {
          ((t = +t), (e |= 0), (r |= 0), n) ||
            L(this, t, e, r, Math.pow(2, 8 * r) - 1, 0);
          var i = r - 1,
            o = 1;
          for (this[e + i] = 255 & t; --i >= 0 && (o *= 256); )
            this[e + i] = (t / o) & 255;
          return e + r;
        }),
        (u.prototype.writeUInt8 = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 1, 255, 0),
            u.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)),
            (this[e] = 255 & t),
            e + 1
          );
        }),
        (u.prototype.writeUInt16LE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 2, 65535, 0),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = 255 & t), (this[e + 1] = t >>> 8))
              : D(this, t, e, !0),
            e + 2
          );
        }),
        (u.prototype.writeUInt16BE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 2, 65535, 0),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 8), (this[e + 1] = 255 & t))
              : D(this, t, e, !1),
            e + 2
          );
        }),
        (u.prototype.writeUInt32LE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 4, 4294967295, 0),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e + 3] = t >>> 24),
                (this[e + 2] = t >>> 16),
                (this[e + 1] = t >>> 8),
                (this[e] = 255 & t))
              : F(this, t, e, !0),
            e + 4
          );
        }),
        (u.prototype.writeUInt32BE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 4, 4294967295, 0),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 24),
                (this[e + 1] = t >>> 16),
                (this[e + 2] = t >>> 8),
                (this[e + 3] = 255 & t))
              : F(this, t, e, !1),
            e + 4
          );
        }),
        (u.prototype.writeIntLE = function (t, e, r, n) {
          if (((t = +t), (e |= 0), !n)) {
            var i = Math.pow(2, 8 * r - 1);
            L(this, t, e, r, i - 1, -i);
          }
          var o = 0,
            a = 1,
            s = 0;
          for (this[e] = 255 & t; ++o < r && (a *= 256); )
            t < 0 && 0 === s && 0 !== this[e + o - 1] && (s = 1),
              (this[e + o] = (((t / a) >> 0) - s) & 255);
          return e + r;
        }),
        (u.prototype.writeIntBE = function (t, e, r, n) {
          if (((t = +t), (e |= 0), !n)) {
            var i = Math.pow(2, 8 * r - 1);
            L(this, t, e, r, i - 1, -i);
          }
          var o = r - 1,
            a = 1,
            s = 0;
          for (this[e + o] = 255 & t; --o >= 0 && (a *= 256); )
            t < 0 && 0 === s && 0 !== this[e + o + 1] && (s = 1),
              (this[e + o] = (((t / a) >> 0) - s) & 255);
          return e + r;
        }),
        (u.prototype.writeInt8 = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 1, 127, -128),
            u.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)),
            t < 0 && (t = 255 + t + 1),
            (this[e] = 255 & t),
            e + 1
          );
        }),
        (u.prototype.writeInt16LE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 2, 32767, -32768),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = 255 & t), (this[e + 1] = t >>> 8))
              : D(this, t, e, !0),
            e + 2
          );
        }),
        (u.prototype.writeInt16BE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 2, 32767, -32768),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 8), (this[e + 1] = 255 & t))
              : D(this, t, e, !1),
            e + 2
          );
        }),
        (u.prototype.writeInt32LE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 4, 2147483647, -2147483648),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = 255 & t),
                (this[e + 1] = t >>> 8),
                (this[e + 2] = t >>> 16),
                (this[e + 3] = t >>> 24))
              : F(this, t, e, !0),
            e + 4
          );
        }),
        (u.prototype.writeInt32BE = function (t, e, r) {
          return (
            (t = +t),
            (e |= 0),
            r || L(this, t, e, 4, 2147483647, -2147483648),
            t < 0 && (t = 4294967295 + t + 1),
            u.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 24),
                (this[e + 1] = t >>> 16),
                (this[e + 2] = t >>> 8),
                (this[e + 3] = 255 & t))
              : F(this, t, e, !1),
            e + 4
          );
        }),
        (u.prototype.writeFloatLE = function (t, e, r) {
          return M(this, t, e, !0, r);
        }),
        (u.prototype.writeFloatBE = function (t, e, r) {
          return M(this, t, e, !1, r);
        }),
        (u.prototype.writeDoubleLE = function (t, e, r) {
          return B(this, t, e, !0, r);
        }),
        (u.prototype.writeDoubleBE = function (t, e, r) {
          return B(this, t, e, !1, r);
        }),
        (u.prototype.copy = function (t, e, r, n) {
          if (
            (r || (r = 0),
            n || 0 === n || (n = this.length),
            e >= t.length && (e = t.length),
            e || (e = 0),
            n > 0 && n < r && (n = r),
            n === r)
          )
            return 0;
          if (0 === t.length || 0 === this.length) return 0;
          if (e < 0) throw new RangeError("targetStart out of bounds");
          if (r < 0 || r >= this.length)
            throw new RangeError("sourceStart out of bounds");
          if (n < 0) throw new RangeError("sourceEnd out of bounds");
          n > this.length && (n = this.length),
            t.length - e < n - r && (n = t.length - e + r);
          var i,
            o = n - r;
          if (this === t && r < e && e < n)
            for (i = o - 1; i >= 0; --i) t[i + e] = this[i + r];
          else if (o < 1e3 || !u.TYPED_ARRAY_SUPPORT)
            for (i = 0; i < o; ++i) t[i + e] = this[i + r];
          else Uint8Array.prototype.set.call(t, this.subarray(r, r + o), e);
          return o;
        }),
        (u.prototype.fill = function (t, e, r, n) {
          if ("string" == typeof t) {
            if (
              ("string" == typeof e
                ? ((n = e), (e = 0), (r = this.length))
                : "string" == typeof r && ((n = r), (r = this.length)),
              1 === t.length)
            ) {
              var i = t.charCodeAt(0);
              i < 256 && (t = i);
            }
            if (void 0 !== n && "string" != typeof n)
              throw new TypeError("encoding must be a string");
            if ("string" == typeof n && !u.isEncoding(n))
              throw new TypeError("Unknown encoding: " + n);
          } else "number" == typeof t && (t &= 255);
          if (e < 0 || this.length < e || this.length < r)
            throw new RangeError("Out of range index");
          if (r <= e) return this;
          var o;
          if (
            ((e >>>= 0),
            (r = void 0 === r ? this.length : r >>> 0),
            t || (t = 0),
            "number" == typeof t)
          )
            for (o = e; o < r; ++o) this[o] = t;
          else {
            var a = u.isBuffer(t) ? t : k(new u(t, n).toString()),
              s = a.length;
            for (o = 0; o < r - e; ++o) this[o + e] = a[o % s];
          }
          return this;
        });
      var U = /[^+\/0-9A-Za-z-_]/g;
      function j(t) {
        return t < 16 ? "0" + t.toString(16) : t.toString(16);
      }
      function k(t, e) {
        var r;
        e = e || 1 / 0;
        for (var n = t.length, i = null, o = [], a = 0; a < n; ++a) {
          if ((r = t.charCodeAt(a)) > 55295 && r < 57344) {
            if (!i) {
              if (r > 56319) {
                (e -= 3) > -1 && o.push(239, 191, 189);
                continue;
              }
              if (a + 1 === n) {
                (e -= 3) > -1 && o.push(239, 191, 189);
                continue;
              }
              i = r;
              continue;
            }
            if (r < 56320) {
              (e -= 3) > -1 && o.push(239, 191, 189), (i = r);
              continue;
            }
            r = 65536 + (((i - 55296) << 10) | (r - 56320));
          } else i && (e -= 3) > -1 && o.push(239, 191, 189);
          if (((i = null), r < 128)) {
            if ((e -= 1) < 0) break;
            o.push(r);
          } else if (r < 2048) {
            if ((e -= 2) < 0) break;
            o.push((r >> 6) | 192, (63 & r) | 128);
          } else if (r < 65536) {
            if ((e -= 3) < 0) break;
            o.push((r >> 12) | 224, ((r >> 6) & 63) | 128, (63 & r) | 128);
          } else {
            if (!(r < 1114112)) throw new Error("Invalid code point");
            if ((e -= 4) < 0) break;
            o.push(
              (r >> 18) | 240,
              ((r >> 12) & 63) | 128,
              ((r >> 6) & 63) | 128,
              (63 & r) | 128
            );
          }
        }
        return o;
      }
      function V(t) {
        return n.toByteArray(
          (function (t) {
            if (
              (t = (function (t) {
                return t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, "");
              })(t).replace(U, "")).length < 2
            )
              return "";
            for (; t.length % 4 != 0; ) t += "=";
            return t;
          })(t)
        );
      }
      function q(t, e, r, n) {
        for (var i = 0; i < n && !(i + r >= e.length || i >= t.length); ++i)
          e[i + r] = t[i];
        return i;
      }
    }).call(this, r(60));
  },
  function (t, e, r) {
    "use strict";
    (e.byteLength = function (t) {
      var e = c(t),
        r = e[0],
        n = e[1];
      return (3 * (r + n)) / 4 - n;
    }),
      (e.toByteArray = function (t) {
        var e,
          r,
          n = c(t),
          a = n[0],
          s = n[1],
          u = new o(
            (function (t, e, r) {
              return (3 * (e + r)) / 4 - r;
            })(0, a, s)
          ),
          f = 0,
          l = s > 0 ? a - 4 : a;
        for (r = 0; r < l; r += 4)
          (e =
            (i[t.charCodeAt(r)] << 18) |
            (i[t.charCodeAt(r + 1)] << 12) |
            (i[t.charCodeAt(r + 2)] << 6) |
            i[t.charCodeAt(r + 3)]),
            (u[f++] = (e >> 16) & 255),
            (u[f++] = (e >> 8) & 255),
            (u[f++] = 255 & e);
        2 === s &&
          ((e = (i[t.charCodeAt(r)] << 2) | (i[t.charCodeAt(r + 1)] >> 4)),
          (u[f++] = 255 & e));
        1 === s &&
          ((e =
            (i[t.charCodeAt(r)] << 10) |
            (i[t.charCodeAt(r + 1)] << 4) |
            (i[t.charCodeAt(r + 2)] >> 2)),
          (u[f++] = (e >> 8) & 255),
          (u[f++] = 255 & e));
        return u;
      }),
      (e.fromByteArray = function (t) {
        for (
          var e, r = t.length, i = r % 3, o = [], a = 0, s = r - i;
          a < s;
          a += 16383
        )
          o.push(f(t, a, a + 16383 > s ? s : a + 16383));
        1 === i
          ? ((e = t[r - 1]), o.push(n[e >> 2] + n[(e << 4) & 63] + "=="))
          : 2 === i &&
            ((e = (t[r - 2] << 8) + t[r - 1]),
            o.push(n[e >> 10] + n[(e >> 4) & 63] + n[(e << 2) & 63] + "="));
        return o.join("");
      });
    for (
      var n = [],
        i = [],
        o = "undefined" != typeof Uint8Array ? Uint8Array : Array,
        a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        s = 0,
        u = a.length;
      s < u;
      ++s
    )
      (n[s] = a[s]), (i[a.charCodeAt(s)] = s);
    function c(t) {
      var e = t.length;
      if (e % 4 > 0)
        throw new Error("Invalid string. Length must be a multiple of 4");
      var r = t.indexOf("=");
      return -1 === r && (r = e), [r, r === e ? 0 : 4 - (r % 4)];
    }
    function f(t, e, r) {
      for (var i, o, a = [], s = e; s < r; s += 3)
        (i =
          ((t[s] << 16) & 16711680) +
          ((t[s + 1] << 8) & 65280) +
          (255 & t[s + 2])),
          a.push(
            n[((o = i) >> 18) & 63] +
              n[(o >> 12) & 63] +
              n[(o >> 6) & 63] +
              n[63 & o]
          );
      return a.join("");
    }
    (i["-".charCodeAt(0)] = 62), (i["_".charCodeAt(0)] = 63);
  },
  function (t, e) {
    /*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
    (e.read = function (t, e, r, n, i) {
      var o,
        a,
        s = 8 * i - n - 1,
        u = (1 << s) - 1,
        c = u >> 1,
        f = -7,
        l = r ? i - 1 : 0,
        h = r ? -1 : 1,
        p = t[e + l];
      for (
        l += h, o = p & ((1 << -f) - 1), p >>= -f, f += s;
        f > 0;
        o = 256 * o + t[e + l], l += h, f -= 8
      );
      for (
        a = o & ((1 << -f) - 1), o >>= -f, f += n;
        f > 0;
        a = 256 * a + t[e + l], l += h, f -= 8
      );
      if (0 === o) o = 1 - c;
      else {
        if (o === u) return a ? NaN : (1 / 0) * (p ? -1 : 1);
        (a += Math.pow(2, n)), (o -= c);
      }
      return (p ? -1 : 1) * a * Math.pow(2, o - n);
    }),
      (e.write = function (t, e, r, n, i, o) {
        var a,
          s,
          u,
          c = 8 * o - i - 1,
          f = (1 << c) - 1,
          l = f >> 1,
          h = 23 === i ? Math.pow(2, -24) - Math.pow(2, -77) : 0,
          p = n ? 0 : o - 1,
          d = n ? 1 : -1,
          v = e < 0 || (0 === e && 1 / e < 0) ? 1 : 0;
        for (
          e = Math.abs(e),
            isNaN(e) || e === 1 / 0
              ? ((s = isNaN(e) ? 1 : 0), (a = f))
              : ((a = Math.floor(Math.log(e) / Math.LN2)),
                e * (u = Math.pow(2, -a)) < 1 && (a--, (u *= 2)),
                (e += a + l >= 1 ? h / u : h * Math.pow(2, 1 - l)) * u >= 2 &&
                  (a++, (u /= 2)),
                a + l >= f
                  ? ((s = 0), (a = f))
                  : a + l >= 1
                  ? ((s = (e * u - 1) * Math.pow(2, i)), (a += l))
                  : ((s = e * Math.pow(2, l - 1) * Math.pow(2, i)), (a = 0)));
          i >= 8;
          t[r + p] = 255 & s, p += d, s /= 256, i -= 8
        );
        for (
          a = (a << i) | s, c += i;
          c > 0;
          t[r + p] = 255 & a, p += d, a /= 256, c -= 8
        );
        t[r + p - d] |= 128 * v;
      });
  },
  function (t, e) {
    var r = {}.toString;
    t.exports =
      Array.isArray ||
      function (t) {
        return "[object Array]" == r.call(t);
      };
  },
  function (t, e) {
    var r,
      n,
      i = (t.exports = {});
    function o() {
      throw new Error("setTimeout has not been defined");
    }
    function a() {
      throw new Error("clearTimeout has not been defined");
    }
    function s(t) {
      if (r === setTimeout) return setTimeout(t, 0);
      if ((r === o || !r) && setTimeout)
        return (r = setTimeout), setTimeout(t, 0);
      try {
        return r(t, 0);
      } catch (e) {
        try {
          return r.call(null, t, 0);
        } catch (e) {
          return r.call(this, t, 0);
        }
      }
    }
    !(function () {
      try {
        r = "function" == typeof setTimeout ? setTimeout : o;
      } catch (t) {
        r = o;
      }
      try {
        n = "function" == typeof clearTimeout ? clearTimeout : a;
      } catch (t) {
        n = a;
      }
    })();
    var u,
      c = [],
      f = !1,
      l = -1;
    function h() {
      f &&
        u &&
        ((f = !1), u.length ? (c = u.concat(c)) : (l = -1), c.length && p());
    }
    function p() {
      if (!f) {
        var t = s(h);
        f = !0;
        for (var e = c.length; e; ) {
          for (u = c, c = []; ++l < e; ) u && u[l].run();
          (l = -1), (e = c.length);
        }
        (u = null),
          (f = !1),
          (function (t) {
            if (n === clearTimeout) return clearTimeout(t);
            if ((n === a || !n) && clearTimeout)
              return (n = clearTimeout), clearTimeout(t);
            try {
              n(t);
            } catch (e) {
              try {
                return n.call(null, t);
              } catch (e) {
                return n.call(this, t);
              }
            }
          })(t);
      }
    }
    function d(t, e) {
      (this.fun = t), (this.array = e);
    }
    function v() {}
    (i.nextTick = function (t) {
      var e = new Array(arguments.length - 1);
      if (arguments.length > 1)
        for (var r = 1; r < arguments.length; r++) e[r - 1] = arguments[r];
      c.push(new d(t, e)), 1 !== c.length || f || s(p);
    }),
      (d.prototype.run = function () {
        this.fun.apply(null, this.array);
      }),
      (i.title = "browser"),
      (i.browser = !0),
      (i.env = {}),
      (i.argv = []),
      (i.version = ""),
      (i.versions = {}),
      (i.on = v),
      (i.addListener = v),
      (i.once = v),
      (i.off = v),
      (i.removeListener = v),
      (i.removeAllListeners = v),
      (i.emit = v),
      (i.prependListener = v),
      (i.prependOnceListener = v),
      (i.listeners = function (t) {
        return [];
      }),
      (i.binding = function (t) {
        throw new Error("process.binding is not supported");
      }),
      (i.cwd = function () {
        return "/";
      }),
      (i.chdir = function (t) {
        throw new Error("process.chdir is not supported");
      }),
      (i.umask = function () {
        return 0;
      });
  },
  function (t) {
    t.exports = JSON.parse(
      '{"name":"react-native-ytdl","version":"4.2.1","description":"YouTube video and audio stream extractor for react native.","main":"index.js","dependencies":{"querystring":"^0.2.0","url":"~0.10.1"},"scripts":{"clone-and-patch":"npm run clone-node-ytdl-core && npm run apply-patches && npm run copy-to-project-root && npm run copy-to-test-app && npm run clean-temp","clone-node-ytdl-core":"./__AUTO_PATCHER__/shell_scripts/clone_node_ytdl_core_to_temp_dir.sh","apply-patches":"./__AUTO_PATCHER__/shell_scripts/apply_custom_implementations_to_temp_dir.sh","copy-to-project-root":"./__AUTO_PATCHER__/shell_scripts/copy_patches_from_temp_dir_to_project_root.sh","copy-to-test-app":"./__AUTO_PATCHER__/shell_scripts/copy_patches_from_temp_dir_to_test_app.sh","clean-temp":"./__AUTO_PATCHER__/shell_scripts/clean_temp_dir.sh","test":"echo \\"Error: no test specified\\" && exit 1"},"repository":{"type":"git","url":"git+https://github.com/ytdl-js/react-native-ytdl.git"},"keywords":["react","native","youtube","downloader","audio","video","stream","extractor","ytdl"],"author":"Abel Tesfaye","license":"ISC","bugs":{"url":"https://github.com/ytdl-js/react-native-ytdl/issues"},"homepage":"https://github.com/ytdl-js/react-native-ytdl#readme"}'
    );
  },
  function (t, e) {
    t.exports = {
      5: {
        mimeType: 'video/flv; codecs="Sorenson H.283, mp3"',
        qualityLabel: "240p",
        bitrate: 25e4,
        audioBitrate: 64,
      },
      6: {
        mimeType: 'video/flv; codecs="Sorenson H.263, mp3"',
        qualityLabel: "270p",
        bitrate: 8e5,
        audioBitrate: 64,
      },
      13: {
        mimeType: 'video/3gp; codecs="MPEG-4 Visual, aac"',
        qualityLabel: null,
        bitrate: 5e5,
        audioBitrate: null,
      },
      17: {
        mimeType: 'video/3gp; codecs="MPEG-4 Visual, aac"',
        qualityLabel: "144p",
        bitrate: 5e4,
        audioBitrate: 24,
      },
      18: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 96,
      },
      22: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 192,
      },
      34: {
        mimeType: 'video/flv; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 128,
      },
      35: {
        mimeType: 'video/flv; codecs="H.264, aac"',
        qualityLabel: "480p",
        bitrate: 8e5,
        audioBitrate: 128,
      },
      36: {
        mimeType: 'video/3gp; codecs="MPEG-4 Visual, aac"',
        qualityLabel: "240p",
        bitrate: 175e3,
        audioBitrate: 32,
      },
      37: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "1080p",
        bitrate: 3e6,
        audioBitrate: 192,
      },
      38: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "3072p",
        bitrate: 35e5,
        audioBitrate: 192,
      },
      43: {
        mimeType: 'video/webm; codecs="VP8, vorbis"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 128,
      },
      44: {
        mimeType: 'video/webm; codecs="VP8, vorbis"',
        qualityLabel: "480p",
        bitrate: 1e6,
        audioBitrate: 128,
      },
      45: {
        mimeType: 'video/webm; codecs="VP8, vorbis"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 192,
      },
      46: {
        mimeType: 'audio/webm; codecs="vp8, vorbis"',
        qualityLabel: "1080p",
        bitrate: null,
        audioBitrate: 192,
      },
      82: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 96,
      },
      83: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "240p",
        bitrate: 5e5,
        audioBitrate: 96,
      },
      84: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 192,
      },
      85: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "1080p",
        bitrate: 3e6,
        audioBitrate: 192,
      },
      91: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "144p",
        bitrate: 1e5,
        audioBitrate: 48,
      },
      92: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "240p",
        bitrate: 15e4,
        audioBitrate: 48,
      },
      93: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 128,
      },
      94: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "480p",
        bitrate: 8e5,
        audioBitrate: 128,
      },
      95: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 15e5,
        audioBitrate: 256,
      },
      96: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "1080p",
        bitrate: 25e5,
        audioBitrate: 256,
      },
      100: {
        mimeType: 'audio/webm; codecs="VP8, vorbis"',
        qualityLabel: "360p",
        bitrate: null,
        audioBitrate: 128,
      },
      101: {
        mimeType: 'audio/webm; codecs="VP8, vorbis"',
        qualityLabel: "360p",
        bitrate: null,
        audioBitrate: 192,
      },
      102: {
        mimeType: 'audio/webm; codecs="VP8, vorbis"',
        qualityLabel: "720p",
        bitrate: null,
        audioBitrate: 192,
      },
      120: {
        mimeType: 'video/flv; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 128,
      },
      127: {
        mimeType: 'audio/ts; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 96,
      },
      128: {
        mimeType: 'audio/ts; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 96,
      },
      132: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "240p",
        bitrate: 15e4,
        audioBitrate: 48,
      },
      133: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "240p",
        bitrate: 2e5,
        audioBitrate: null,
      },
      134: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "360p",
        bitrate: 3e5,
        audioBitrate: null,
      },
      135: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "480p",
        bitrate: 5e5,
        audioBitrate: null,
      },
      136: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "720p",
        bitrate: 1e6,
        audioBitrate: null,
      },
      137: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "1080p",
        bitrate: 25e5,
        audioBitrate: null,
      },
      138: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "4320p",
        bitrate: 135e5,
        audioBitrate: null,
      },
      139: {
        mimeType: 'audio/mp4; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 48,
      },
      140: {
        mimeType: 'audio/m4a; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 128,
      },
      141: {
        mimeType: 'audio/mp4; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 256,
      },
      151: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 5e4,
        audioBitrate: 24,
      },
      160: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "144p",
        bitrate: 1e5,
        audioBitrate: null,
      },
      171: {
        mimeType: 'audio/webm; codecs="vorbis"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 128,
      },
      172: {
        mimeType: 'audio/webm; codecs="vorbis"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 192,
      },
      242: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "240p",
        bitrate: 1e5,
        audioBitrate: null,
      },
      243: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "360p",
        bitrate: 25e4,
        audioBitrate: null,
      },
      244: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "480p",
        bitrate: 5e5,
        audioBitrate: null,
      },
      247: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "720p",
        bitrate: 7e5,
        audioBitrate: null,
      },
      248: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1080p",
        bitrate: 15e5,
        audioBitrate: null,
      },
      249: {
        mimeType: 'audio/webm; codecs="opus"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 48,
      },
      250: {
        mimeType: 'audio/webm; codecs="opus"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 64,
      },
      251: {
        mimeType: 'audio/webm; codecs="opus"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 160,
      },
      264: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "1440p",
        bitrate: 4e6,
        audioBitrate: null,
      },
      266: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "2160p",
        bitrate: 125e5,
        audioBitrate: null,
      },
      271: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1440p",
        bitrate: 9e6,
        audioBitrate: null,
      },
      272: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "4320p",
        bitrate: 2e7,
        audioBitrate: null,
      },
      278: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "144p 30fps",
        bitrate: 8e4,
        audioBitrate: null,
      },
      298: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "720p",
        bitrate: 3e6,
        audioBitrate: null,
      },
      299: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "1080p",
        bitrate: 55e5,
        audioBitrate: null,
      },
      300: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 1318e3,
        audioBitrate: 48,
      },
      302: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "720p HFR",
        bitrate: 25e5,
        audioBitrate: null,
      },
      303: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1080p HFR",
        bitrate: 5e6,
        audioBitrate: null,
      },
      308: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1440p HFR",
        bitrate: 1e7,
        audioBitrate: null,
      },
      313: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "2160p",
        bitrate: 13e6,
        audioBitrate: null,
      },
      315: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "2160p HFR",
        bitrate: 2e7,
        audioBitrate: null,
      },
      330: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "144p HDR, HFR",
        bitrate: 8e4,
        audioBitrate: null,
      },
      331: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "240p HDR, HFR",
        bitrate: 1e5,
        audioBitrate: null,
      },
      332: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "360p HDR, HFR",
        bitrate: 25e4,
        audioBitrate: null,
      },
      333: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "240p HDR, HFR",
        bitrate: 5e5,
        audioBitrate: null,
      },
      334: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "720p HDR, HFR",
        bitrate: 1e6,
        audioBitrate: null,
      },
      335: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1080p HDR, HFR",
        bitrate: 15e5,
        audioBitrate: null,
      },
      336: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1440p HDR, HFR",
        bitrate: 5e6,
        audioBitrate: null,
      },
      337: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "2160p HDR, HFR",
        bitrate: 12e6,
        audioBitrate: null,
      },
    };
  },
  function (t, e, r) {
    const n = r(63),
      i = r(62),
      o = r(61),
      { parseTimestamp: a } = r(129),
      s = "https://www.youtube.com/watch?v=",
      u = { song: { name: "Music", url: "https://music.youtube.com/" } },
      c = (t) => (t ? (t.runs ? t.runs[0].text : t.simpleText) : null);
    e.getMedia = (t) => {
      let e = {},
        r = [];
      try {
        r =
          t.response.contents.twoColumnWatchNextResults.results.results
            .contents;
      } catch (t) {
        console.log("-------getMedia--------err", t);
      }
      let n = r.find((t) => t.videoSecondaryInfoRenderer);
      if (!n) return {};
      try {
        let t = (
          n.metadataRowContainer ||
          n.videoSecondaryInfoRenderer.metadataRowContainer
        ).metadataRowContainerRenderer.rows;
        for (let r of t)
          if (r.metadataRowRenderer) {
            let t = c(r.metadataRowRenderer.title).toLowerCase(),
              n = r.metadataRowRenderer.contents[0];
            e[t] = c(n);
            let i = n.runs;
            i &&
              i[0].navigationEndpoint &&
              (e[t + "_url"] = o.resolve(
                s,
                i[0].navigationEndpoint.commandMetadata.webCommandMetadata.url
              )),
              t in u && ((e.category = u[t].name), (e.category_url = u[t].url));
          } else if (r.richMetadataRowRenderer) {
            let t = r.richMetadataRowRenderer.contents,
              n = t.filter(
                (t) =>
                  "RICH_METADATA_RENDERER_STYLE_BOX_ART" ===
                  t.richMetadataRenderer.style
              );
            for (let { richMetadataRenderer: t } of n) {
              let r = t;
              e.year = c(r.subtitle);
              let n = c(r.callToAction).split(" ")[1];
              (e[n] = c(r.title)),
                (e[n + "_url"] = o.resolve(
                  s,
                  r.endpoint.commandMetadata.webCommandMetadata.url
                )),
                (e.thumbnails = r.thumbnail.thumbnails);
            }
            let i = t.filter(
              (t) =>
                "RICH_METADATA_RENDERER_STYLE_TOPIC" ===
                t.richMetadataRenderer.style
            );
            for (let { richMetadataRenderer: t } of i) {
              let r = t;
              (e.category = c(r.title)),
                (e.category_url = o.resolve(
                  s,
                  r.endpoint.commandMetadata.webCommandMetadata.url
                ));
            }
          }
      } catch (t) {
        console.log("-------getMedia--------err2", t);
      }
      return e;
    };
    const f = (t) =>
      !(!t || !t.find((t) => "Verified" === t.metadataBadgeRenderer.tooltip));
    (e.getAuthor = (t) => {
      let e,
        r,
        i = [],
        a = !1;
      try {
        let u =
          t.response.contents.twoColumnWatchNextResults.results.results.contents.find(
            (t) =>
              t.videoSecondaryInfoRenderer &&
              t.videoSecondaryInfoRenderer.owner &&
              t.videoSecondaryInfoRenderer.owner.videoOwnerRenderer
          ).videoSecondaryInfoRenderer.owner.videoOwnerRenderer;
        (e = u.navigationEndpoint.browseEndpoint.browseId),
          (i = u.thumbnail.thumbnails.map(
            (t) => ((t.url = o.resolve(s, t.url)), t)
          )),
          (r = n.parseAbbreviatedNumber(c(u.subscriberCountText))),
          (a = f(u.badges));
      } catch (t) {
        console.log("-------getAuthor--------err", t);
      }
      try {
        let u =
            t.player_response.microformat &&
            t.player_response.microformat.playerMicroformatRenderer,
          c =
            (u && u.channelId) || e || t.player_response.videoDetails.channelId,
          f = {
            id: c,
            name: u
              ? u.ownerChannelName
              : t.player_response.videoDetails.author,
            user: u ? u.ownerProfileUrl.split("/").slice(-1)[0] : null,
            channel_url: "https://www.youtube.com/channel/" + c,
            external_channel_url: u
              ? "https://www.youtube.com/channel/" + u.externalChannelId
              : "",
            user_url: u ? o.resolve(s, u.ownerProfileUrl) : "",
            thumbnails: i,
            verified: a,
            subscriber_count: r,
          };
        return (
          i.length &&
            n.deprecate(
              f,
              "avatar",
              f.thumbnails[0].url,
              "author.avatar",
              "author.thumbnails[0].url"
            ),
          f
        );
      } catch (t) {
        return console.log("-------getAuthor--------err2", t), {};
      }
    }),
      (e.getRelatedVideos = (t) => {
        let e = [],
          r = [];
        try {
          e = t.response.webWatchNextResponseExtensionData.relatedVideoArgs
            .split(",")
            .map((t) => i.parse(t));
        } catch (t) {
          console.log("-------getRelatedVideos--------err2", t);
        }
        try {
          r =
            t.response.contents.twoColumnWatchNextResults.secondaryResults
              .secondaryResults.results;
        } catch (t) {
          return console.log("-------getRelatedVideos--------err", t), [];
        }
        let u = [];
        for (let t of r || []) {
          let r = t.compactVideoRenderer;
          if (r)
            try {
              let t = c(r.viewCountText),
                i = c(r.shortViewCountText),
                l = e.find((t) => t.id === r.videoId);
              /^\d/.test(i) || (i = (l && l.short_view_count_text) || ""),
                (t = (/^\d/.test(t) ? t : i).split(" ")[0]);
              let h =
                  r.shortBylineText.runs[0].navigationEndpoint.browseEndpoint,
                p = h.browseId,
                d = c(r.shortBylineText),
                v = (h.canonicalBaseUrl || "").split("/").slice(-1)[0],
                y = {
                  id: r.videoId,
                  title: c(r.title),
                  published: c(r.publishedTimeText),
                  author: {
                    id: p,
                    name: d,
                    user: v,
                    channel_url: "https://www.youtube.com/channel/" + p,
                    user_url: "https://www.youtube.com/user/" + v,
                    thumbnails: r.channelThumbnail.thumbnails.map(
                      (t) => ((t.url = o.resolve(s, t.url)), t)
                    ),
                    verified: f(r.ownerBadges),
                    [Symbol.toPrimitive]: () => (
                      console.warn(
                        "`relatedVideo.author` will be removed in a near future release, use `relatedVideo.author.name` instead."
                      ),
                      y.author.name
                    ),
                  },
                  short_view_count_text: i.split(" ")[0],
                  view_count: t.replace(/,/g, ""),
                  length_seconds: r.lengthText
                    ? Math.floor(a(c(r.lengthText)) / 1e3)
                    : e && "" + e.length_seconds,
                  thumbnails: r.thumbnail.thumbnails,
                  isLive: !(
                    !r.badges ||
                    !r.badges.find(
                      (t) => "LIVE NOW" === t.metadataBadgeRenderer.label
                    )
                  ),
                };
              n.deprecate(
                y,
                "author_thumbnail",
                y.author.thumbnails[0].url,
                "relatedVideo.author_thumbnail",
                "relatedVideo.author.thumbnails[0].url"
              ),
                n.deprecate(
                  y,
                  "ucid",
                  y.author.id,
                  "relatedVideo.ucid",
                  "relatedVideo.author.id"
                ),
                n.deprecate(
                  y,
                  "video_thumbnail",
                  y.thumbnails[0].url,
                  "relatedVideo.video_thumbnail",
                  "relatedVideo.thumbnails[0].url"
                ),
                u.push(y);
            } catch (t) {
              console.log("-------getRelatedVideos--------err2", t);
            }
        }
        return u;
      }),
      (e.getLikes = (t) => {
        try {
          let e =
            t.response.contents.twoColumnWatchNextResults.results.results.contents
              .find((t) => t.videoPrimaryInfoRenderer)
              .videoPrimaryInfoRenderer.videoActions.menuRenderer.topLevelButtons.find(
                (t) =>
                  t.toggleButtonRenderer &&
                  "LIKE" === t.toggleButtonRenderer.defaultIcon.iconType
              );
          return parseInt(
            e.toggleButtonRenderer.defaultText.accessibility.accessibilityData.label.replace(
              /\D+/g,
              ""
            )
          );
        } catch (t) {
          return console.log("-------getLikes--------err", t), null;
        }
      }),
      (e.getDislikes = (t) => {
        try {
          let e =
            t.response.contents.twoColumnWatchNextResults.results.results.contents
              .find((t) => t.videoPrimaryInfoRenderer)
              .videoPrimaryInfoRenderer.videoActions.menuRenderer.topLevelButtons.find(
                (t) =>
                  t.toggleButtonRenderer &&
                  "DISLIKE" === t.toggleButtonRenderer.defaultIcon.iconType
              );
          return parseInt(
            e.toggleButtonRenderer.defaultText.accessibility.accessibilityData.label.replace(
              /\D+/g,
              ""
            )
          );
        } catch (t) {
          return console.log("-------getDislikes--------err", t), null;
        }
      }),
      (e.cleanVideoDetails = (t) => (
        (t.thumbnails = t.thumbnail.thumbnails),
        delete t.thumbnail,
        n.deprecate(
          t,
          "thumbnail",
          { thumbnails: t.thumbnails },
          "videoDetails.thumbnail.thumbnails",
          "videoDetails.thumbnails"
        ),
        (t.description = t.shortDescription || c(t.description)),
        delete t.shortDescription,
        n.deprecate(
          t,
          "shortDescription",
          t.description,
          "videoDetails.shortDescription",
          "videoDetails.description"
        ),
        t
      ));
  },
  function (t, e, r) {
    "use strict";
    r.r(e);
    r(132), r(318);
    const n = r(319),
      i = r(63),
      o = r(127),
      a = r(128),
      s = r(130),
      { parseTimestamp: u } = r(129),
      c = (t, e) => c.getInfo(t, e).then((t) => f(t, e));
    (c.getBasicInfo = n.getBasicInfo),
      (c.getInfo = n.getInfo),
      (c.chooseFormat = o.chooseFormat),
      (c.filterFormats = o.filterFormats),
      (c.validateID = a.validateID),
      (c.validateURL = a.validateURL),
      (c.getURLVideoID = a.getURLVideoID),
      (c.getVideoID = a.getVideoID),
      (c.cache = { sig: s.cache, info: n.cache, cookie: n.cookieCache });
    const f = (t, e) =>
      new Promise(async (r, n) => {
        e = e || {};
        let a,
          s = i.playError(t.player_response, [
            "UNPLAYABLE",
            "LIVE_STREAM_OFFLINE",
            "LOGIN_REQUIRED",
          ]);
        if (s) return void n(s);
        if (!t.formats.length)
          return void n(Error("This video is unavailable"));
        try {
          a = o.chooseFormat(t.formats, e);
        } catch (t) {
          return void n(t);
        }
        const c = [];
        if (a.isHLS || a.isDashMPD) c.push({ url: a.url, headers: [] });
        else {
          e.begin && (a.url += "&begin=" + u(e.begin));
          const t = { url: a.url, headers: [] };
          e.range &&
            (e.range.start || e.range.end) &&
            t.headers.push({
              Range: `bytes=${e.range.start || "0"}-${e.range.end || ""}`,
            }),
            c.push(t);
        }
        r(c);
      });
    var l = c;
    window.ytdl = l;
  },
]);

(function () {
  if (typeof window.objYoutube === "object") {
    return;
  }

  class objYoutube {
    constructor(links) {
      this.indexPlay = -1;
      if (!Array.isArray(links) || links.length < 1) {
        this.links = [];
      } else {
        this.links = [...links];
      }

      this.prepareURL = "";
      this._prepareData = [];
      this._countFailed = 0;

      this.isGetLinkAfterFirst = false;
      this.isGetLinkBeforeEnd = false;

      this._currentTitle = "";

      // console.log("======constructor=====");

      return true;
    }
    changeLinks(links) {
      if (!Array.isArray(links) || links.length < 1) {
        this.links = [];
        return false;
      }
      this.indexPlay = -1;
      this.links = [...links];

      return true;
    }

    getPlaylist() {
      window.backWorker.postMessage(
        window.workerId,
        JSON.stringify({
          cmd: "currentList",
          data: this.links,
        })
      );
      return;
    }
    clear() {
      this.links = [];
      this.indexPlay = -1;
      this._isPlaying = false;
      this._totalTime = 0;
      this._currentTime = 0;
      return true;
    }
    onDuration(currentTime, totalTime) {
      this._currentTime = currentTime;

      window.backWorker.postMessage(
        window.workerId,
        JSON.stringify({
          cmd: "observer",
          data: {
            state: "duration",
            currentTime: Math.round(currentTime),
            totalTime,
          },
        })
      );
      if (this.isGetLinkAfterFirst !== true && currentTime > 5) {
        this._prepareData = [];
        this._countFailed = 0;

        this.isGetLinkAfterFirst = true;
      } else if (
        this.isGetLinkBeforeEnd !== true &&
        currentTime + 30 > totalTime
      ) {
        this.isGetLinkBeforeEnd = true;
      } else {
        return;
      }
      // console.log("<<<on duration check link");
      this.checkLink(1);
    }
    checkLink(indexCommand = 0, urlCheck = "", isNew = false) {
      const listYoutube = this.links;
      // chi co 1 bai thi bo qua
      let url;
      if (urlCheck.length === 0) {
        if (listYoutube.length < 2) {
          return;
        }
        // tien hanh lay link nhac sau 5 giay dau tien
        const indexPlay = this.getNextIndexPlay(indexCommand);
        const youtubeURL = listYoutube[indexPlay];
        if (!youtubeURL || !youtubeURL.watchUrl) {
          return;
        }
        url = youtubeURL.watchUrl;
      } else {
        url = urlCheck;
      }
      const _that = this;
      // console.log("checkLink-----<<< 1", url);
      this.getLink(
        url,
        function (data) {
          _that._prepareData.push({
            ...data,
          });
          window.backWorker.command(
            window.workerId,
            JSON.stringify({
              ...data,
              command: "prepare-audio",
              type: "media",
            })
          );
        },
        isNew
      );
    }

    onPlay(totalTime) {
      // this.prepareURL = "";

      this._totalTime = totalTime;

      this._isPlaying = true;
      this._prepareData = [];
      this.isGetLinkAfterFirst = false;
      this.isGetLinkBeforeEnd = false;

      // console.log("window.workerId", window.workerId);
      // push local noti
      window.backWorker.command(
        window.workerId,
        JSON.stringify({
          type: "notification",
          action: "push",
          title: "Music Metanode",
          body: "Playing: " + this._currentTitle,
          delay: 10,
        })
      );
    }

    playAtIndex(index) {
      // console.log("-------------------call playAtIndex - 1");
      // console.log("-------------------call playAtIndex", index);
      this.currentAudioPlaying = null;
      this._prepareCMD = 1;
      this.indexPlay = index;
      const info = this.links[index];
      if (typeof info.watchUrl !== "string") {
        // console.log("======playAtIndex=====", "ignore-2");
        return;
      }
      if (info && this.prepareURL && info.watchUrl === this.prepareURL) {
        this.indexPlay = indexPlay;
        this._prepareCMD = false;
        const data = this._prepareData[this._prepareData.length - 1];
        this._currentTitle = data.title;
        this.currentAudioPlaying = data;

        // console.log("PlayAudio ------> ");

        window.backWorker.command(
          window.workerId,
          JSON.stringify({
            ...data,
            command: "play-audio",
            type: "media",
          })
        );
        return;
      }
      this.checkLink(1, info.watchUrl, true);
    }
    pause() {
      window.backWorker.command(
        window.workerId,
        JSON.stringify({
          command: "pause-audio",
          type: "media",
        })
      );
    }

    play() {
      window.backWorker.command(
        window.workerId,
        JSON.stringify({
          command: "play-audio",
          type: "media",
        })
      );
    }

    onPause() {
      this._isPlaying = false;

      // console.log("window.workerId--------pause", window.workerId);
      // push local noti
      window.backWorker.command(
        window.workerId,
        JSON.stringify({
          type: "notification",
          action: "push",
          title: "Music Metanode",
          body: "Pause: " + this._currentTitle,
          delay: 0,
        })
      );

      this.getState("observer");
    }
    getNextIndexPlay(isNext) {
      let indexPlay = this.indexPlay;
      // console.log("-----getNextIndexPlay-current-index", indexPlay);
      // console.log("-----isNext", isNext);
      const listYoutube = this.links;
      if (!Array.isArray(listYoutube) || listYoutube.length < 1) {
        // console.log("------------links-empty");
        return 0;
      }

      if (isNext === -1) {
        indexPlay--;
        if (indexPlay < 0) {
          indexPlay = listYoutube.length - 1;
        }
      } else if (isNext === 1) {
        indexPlay++;
        if (indexPlay >= listYoutube.length - 1) {
          indexPlay = 0;
        }
      }
      if (indexPlay < 0) {
        indexPlay = 0;
      }
      // console.log("-----getNextIndexPlay - result", indexPlay);
      return indexPlay;
    }

    getState(cmd = "") {
      if (cmd === "") {
        // console.log("this.links -> ", JSON.stringify(this.links));
        let indexPlay = this.indexPlay;
        // console.log("g1 - ", indexPlay);
        const link = this.links[indexPlay];
        // console.log("g2 - ", JSON.stringify(link.title));
        window.backWorker.postMessage(
          window.workerId,
          JSON.stringify({
            cmd: "currentState",
            data: {
              state: this._isPlaying === true ? "playing" : "pause",
              totalTime: this._totalTime,
              currentTime: this._currentTime,
              index: indexPlay,
              info: link,
              workerId: window.workerId,
            },
          })
        );

        return;
      }

      if (this._isPlaying !== true) {
        window.backWorker.postMessage(
          window.workerId,
          JSON.stringify({
            cmd: "observer",
            data: {
              state: "pause",
              currentTime: this._currentTime,
            },
          })
        );

        return;
      }

      let indexPlay = this.indexPlay;
      const link = this.links[indexPlay];

      window.backWorker.postMessage(
        window.workerId,
        JSON.stringify({
          cmd: "observer",
          data: {
            state: "playing",
            totalTime: this._totalTime,
            index: indexPlay,
            info: link,
          },
        })
      );
    }
    getLink(youtubeURL, cb, isNew) {
      // console.log("getLink - isNew -> ", isNew);
      // console.log("getInfo-----start------", youtubeURL);
      window.ytdl
        .getInfo(youtubeURL)
        .then((info) => {
          // console.log("getInfo----end-------");
          if (typeof info !== "object" || info === null) {
            cb(false);
            return;
          }
          const videoDetails = info.videoDetails;
          const title = videoDetails.title;
          const uploadDate = videoDetails.uploadDate;
          const thumbnail =
            videoDetails.thumbnails[videoDetails.thumbnails.length - 2].url;
          const ownerChannelName = videoDetails.ownerChannelName;
          const lengthSeconds = videoDetails.lengthSeconds;
          const isLive = videoDetails.isLive || false;
          let audioFormats = window.ytdl.filterFormats(
            info.formats,
            "audioonly"
          );
          if (!Array.isArray(audioFormats)) {
            cb(false);
            return;
          }
          const urls = [];
          // let durationLive = 0;
          audioFormats.forEach((url) => {
            if (url.hasAudio === 0 || url.hasVideo === 1) {
              return;
            }
            if (["mp4", "mp3"].indexOf(url.container) < 0) {
              return;
            }
            urls.push(url);
          });

          if (urls.length < 1) {
            cb(false);
            return;
          }

          const dataMedia = {
            videoDetails,
            // urls,
            url: urls[0].url,
            title,
            isLive,
            uploadDate: uploadDate,
            thumbnail: thumbnail,
            owner: ownerChannelName,
            duration: lengthSeconds,
            watchUrl: youtubeURL,
          };
          if (isNew) {
            this.prepareURL = youtubeURL;
            this._prepareData.push({
              ...dataMedia,
            });
            this.indexPlay = -1;
          }
          this.links = [
            dataMedia,
            ...info.related_videos.map((v) => {
              // console.log("id <<<< ", v.id);
              return {
                title: v.title,
                isLive: v.isLive,
                owner: v.author.name,
                thumbnail: v.thumbnails[0].url,
                duration: v.length_seconds,
                watchUrl: "https://www.youtube.com/watch?v=" + v.id,
              };
            }),
          ];
          console.log("this.links --->", this.links);
          if (this.currentAudioPlaying) {
            this.links.unshift(this.currentAudioPlaying);
          }

          window.backWorker.postMessage(
            window.workerId,
            JSON.stringify({
              cmd: "playListsChange",
              data: [...this.links],
            })
          );

          // console.log("list link update");
          // if (this.indexPlay > 0) {
          //   this.indexPlay = 0;
          // }
          cb(dataMedia);
        })
        .catch((e) => {
          // console.log("getInfo-----err------", e);
          // cb(false);
        });
    }
    prepareOnPlay() {
      // console.log("prepareOnPlay-----ok------", this._prepareCMD);

      if (this._prepareCMD === false) {
        return;
      }
      // let indexPlay = this.getNextIndexPlay(this._prepareCMD);

      // this.indexPlay = indexPlay;

      this._prepareCMD = false;
      const data = this._prepareData[this._prepareData.length - 1];
      this._currentTitle = data.title;
      this.currentAudioPlaying = data;
      window.backWorker.command(
        window.workerId,
        JSON.stringify({
          ...data,
          command: "play-audio",
          type: "media",
        })
      );
    }
    prepareOnFailed() {
      // console.log("prepareOnFailed-----------");
      // tien hanh remove phan tu cuoi cung va them phan tu moi vao

      if (this._prepareData.length > 0) {
        this._prepareData.splice(-1);
      } else {
        this._prepareData = [];
      }
      this.checkLink(1);
    }
    onFailed() {
      // console.log("onFailed-----------");

      this.prepareURL = "";
      this._prepareData = [];

      this.isGetLinkAfterFirst = false;
      this.isGetLinkBeforeEnd = false;

      this._countFailed++;
      if (this._countFailed > 2) {
        this.nextPlay(1);
      } else {
        this._prepareCMD = 0;
        this.nextPlay(0);
      }
    }
    nextPlay(indexCommand = 1) {
      this.currentAudioPlaying = null;
      if (indexCommand != 0) {
        this._prepareCMD = indexCommand;
      }
      let indexPlay = this.getNextIndexPlay(indexCommand);
      const listYoutube = this.links;
      // console.log(
      //   "next----> in list -->>>",
      //   this.links.map((v) => v.watchUrl)
      // );
      const youtubeURL = listYoutube[indexPlay];
      if (!youtubeURL) {
        this.indexPlay = -1;
        this.nextPlay(1);
        return;
      }
      // console.log("<<<NEXT youtubeURL>>>", youtubeURL.watchUrl);

      if (
        youtubeURL &&
        this.prepareURL &&
        youtubeURL.watchUrl === this.prepareURL
      ) {
        this.indexPlay = indexPlay;
        this._prepareCMD = false;
        const data = this._prepareData[this._prepareData.length - 1];
        this._currentTitle = data.title;
        this.currentAudioPlaying = data;

        // console.log("PlayAudio ------> ");

        window.backWorker.command(
          window.workerId,
          JSON.stringify({
            ...data,
            command: "play-audio",
            type: "media",
          })
        );
        return;
      }
      // console.log("Check link ----- 0");
      this.checkLink(indexCommand, "", true);
    }
  }

  if (!"backWorker" in window) {
    // console.log("-------not found worker");
    return;
  }

  window.objYoutube = new objYoutube();

  var n = (e) => {
    console.log("---client---start-receive--url", typeof e, e);
    // window.removeEventListener("message", n)
    window.backWorker.postMessage(
      window.workerId,
      JSON.stringify({
        cmd: "pong-from:",
        rdata: e,
      })
    );
  };
  window.addEventListener("message", n, !1);

  // console.log("ok-loaded-ytdl");
})();
