(function () {
  window.console = {
    log: function (...arguments) {
      window.webkit.messageHandlers.callbackHandler.postMessage(
        JSON.stringify({
          command: "test-click",
          ...arguments,
        })
      );
    },
  };

  let tiemout_register;

  var n = (e) => {
    // window.webkit.messageHandlers.callbackHandler.postMessage(
    //   JSON.stringify({
    //     command: "test-click",
    //     ok: e.data,
    //   })
    // );

    if (typeof e.data !== "string") {
      return;
    }
    const tmps = e.data.split("|");

    if (tmps[0] !== "backWorker") {
      return;
    }
    if (tmps[tmps.length - 1] !== "end") {
      return;
    }
    if (tmps[1] === "id") {
      window.workerId = tmps[2];
      clearTimeout(tiemout_register);
    }
    // window.removeEventListener("message", n), resolve(s);
  };
  window.addEventListener("message", n, !1);
  const sendEvent = function (action, content) {
    if (action !== "register" && typeof window.workerId !== "string") {
      alert("------err-not-found-worker");
      return false;
    }

    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        workerId: window.workerId,
        command: "backWorker",
        action,
        content,
      })
    );
  };
  window.backWorker = {
    register: function (obj) {
      // trrong vong 1 giay can nhan lai workerid
      tiemout_register = setTimeout(function () {
        alert("can not register worker");
      }, 1000);

      sendEvent("register", obj);
    },
    execute: function (js) {
      // lenh js chay
      sendEvent("execute", {
        js,
      });
    },
    postMessage: function (obj) {
      // lenh js chay
      sendEvent("postMessage", obj);
    },
    action: function (cmd) {
      // lenh chay cac lenh trong register nhu next, back, ...
      sendEvent("action", {
        cmd,
      });
    },
    command: function (command) {
      // command la danh cho cac script de thuc thu nhu notification
      sendEvent("command", command);
    },
    stop: function () {
      sendEvent("stop", {});
    },
    fetch: (e) =>
      new Promise((resolve, reject) => {
        console.log("------start-send--url", e);
        var n = (e) => {
          console.log("------start-receive--url", e);
          if ("url" !== e.data) {
            return;
          }
          let s = atob(window.abc);

          console.log("------start-receive--data", s.length);
          window.removeEventListener("message", n), resolve(s);
          console.log("--------ok---la");
        };
        window.addEventListener("message", n, !1);

        window.webkit.messageHandlers.callbackHandler.postMessage(
          JSON.stringify({
            command: "get-url",
            url: e,
          })
        );

        console.log("----ok--data--e.data-listen--");
      }),
  };

  window.backWorker.register({
    type: "media",
    // content: '', // or link: '',
    link: "https://gitlab.com/QuangFiIt/upload_app/-/raw/main/script/quick_js_bridging.js",
    // link: "http://10.0.0.21:5500/quick_js_bridging.js",
    // init: 'window.objYoutube.changeLinks(["https://www.youtube.com/watch?v=me7KyKQKj6k","https://www.youtube.com/watch?v=cZR2XRbkOr0","https://www.youtube.com/watch?v=-GsAwVgUsQw","https://www.youtube.com/watch?v=JnfRWwdIZLg"]);',
    // init: 'window.objYoutube.changeLinks(["https://www.youtube.com/watch?v=HEOg8S0scZk","https://www.youtube.com/watch?v=me7KyKQKj6k","https://www.youtube.com/watch?v=cZR2XRbkOr0","https://www.youtube.com/watch?v=-GsAwVgUsQw","https://www.youtube.com/watch?v=JnfRWwdIZLg"]);',
    // timeouts: [
    //   {
    //     time: 2,
    //     execute: "window.objYoutube.nextPlay(1);",
    //   },
    // ],
    next: "window.objYoutube.nextPlay(1);",
    back: "window.objYoutube.nextPlay(-1);",
    onDuration: "window.objYoutube.onDuration({currentTime}, {totalTime});", // binding
    // onPause: '',
    onFailed: "window.objYoutube.onFailed()",
    onPlay: "window.objYoutube.onPlay()",
    onStop: "window.objYoutube.clear();",
    replay: "window.objYoutube.nextPlay(0);",

    prepareOnPlay: "window.objYoutube.prepareOnPlay();",
    prepareOnFailed: "window.objYoutube.prepareOnFailed();",
  });

  // communicate

  var n = (e) => {
    console.log("---main---start-receive--url", typeof e.data, e.data);
    if (!e || typeof e.data !== "object") {
      return;
    }
    const cmd = e.data && e.data.cmd;
    console.log("cmd -> ", cmd);
    if (cmd === "getInfo") {
      //   const data = e.data.data;
      window.backWorker.execute(`window.objYoutube.nextPlay(1);`);
    }
  };
  window.addEventListener("message", n, !1);
  setTimeout(() => {
    console.log("--------logping-1");
    window.backWorker.postMessage("ping-1");
  }, 2000);

  window.getVideoInfo = function (youtubeURL) {
    const a = function (data) {
      console.log("----data", JSON.stringify(data));

      window.backWorker.postMessage(
        window.workerId,
        JSON.stringify({
          cmd: "getInfo",
          data: data,
        })
      );
      console.log("---------b");
    };
    window.backWorker.execute(
      `window.objYoutube.getLink('${youtubeURL}', ${a.toString()}, true)`
    );
    // window.backWorker.execute(
    //   `window.objYoutube.changeLinks(['${youtubeURL}']);`
    // );

    // window.backWorker.execute(`window.objYoutube.nextPlay(1);`);
  };

  // setTimeout(() => {
  //   // push local noti
  //   window.backWorker.command({
  //     type: 'nfc',
  //     action: 'id',
  //     title: 'Please tag ne...',
  //   });
  // }, 1000);
  var currentLocation = document.location.href;

  var lastUrl = location.href;
  new MutationObserver(() => {
    const url = location.href;
    if (url !== lastUrl) {
      lastUrl = url;
      window.onUrlChange();
    }
  }).observe(document, { subtree: true, childList: true });

  window.callPauseVideo = function () {
    document.querySelectorAll("video").forEach((vid) => {
      vid.muted = true;
      vid.autoplay = false;
      if (vid.paused === true) {
        // clearInterval(window._toCallPause);
        return;
      }
      vid.pause();
      vid.currentTime = vid.duration - 2;
    });
    var player = document.getElementById("player-container-id");
    if (player != null) {
      var node = document.createElement("div");
      node.style =
        "position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;  background-color: transparent";
      player.appendChild(node);
    }
  };
  window.handlePause = function () {
    window.callPauseVideo();
    window._toCallPause = setInterval(function () {
      window.callPauseVideo();
    }, 300);
  };

  window.onUrlChange = function onUrlChange() {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: "change-url",
        value: {
          currentLocation,
          url: document.location.href,
        },
      })
    );
  };
  window.handlePause();
})();
