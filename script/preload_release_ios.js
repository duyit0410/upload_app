(function () {
  window.objHandleEvent = [];
  const messageListen = function (event) {
    if (
      typeof event.data !== "object" ||
      typeof event.data.action !== "string" ||
      event.data.action !== "cmd"
    ) {
      return;
    }

    const data = event.data;

    const id = data.id;

    if (typeof window.objHandleEvent[id] !== "object") {
      return;
    }

    if (data.type == "click") {
      window.objHandleEvent[id].click();

      // const elem = window.objHandleEvent[id];
      // var evt = document.createEvent("MouseEvents");
      // evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, elem);
      // elem.dispatchEvent(evt);
    } else {
      const elem = window.objHandleEvent[id];

      const lastValue = elem.value;
      elem.value = data.value;

      // const e = new Event("change");
      // elem.dispatchEvent(e);

      // for reactjs
      const tracker = elem._valueTracker;
      if (tracker) {
        const event = new Event("input", { bubbles: true });
        tracker.setValue(lastValue);
        elem.dispatchEvent(event);
      } else {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        elem.dispatchEvent(evt);
      }
      // end for reactjs

      // var keyboardEvent = document.createEvent('KeyboardEvent');
      // var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? 'initKeyboardEvent' : 'initKeyEvent';

      // keyboardEvent[initMethod](
      //   'keyup', // event type: keydown, keyup, keypress
      //   true, // bubbles
      //   true, // cancelable
      //   window, // view: should be window
      //   false, // ctrlKey
      //   false, // altKey
      //   false, // shiftKey
      //   false, // metaKey
      //   40, // keyCode: unsigned long - the virtual key code, else 0
      //   0, // charCode: unsigned long - the Unicode character associated with the depressed key, else 0
      // );
      // elem.dispatchEvent(keyboardEvent);
    }
  };

  window.removeEventListener("message", messageListen);
  window.addEventListener("message", messageListen);

  // var doc = document.getElementById('FileFrame').contentWindow.document;
  var doc = window.document;
  // doc.open();
  // doc.write('<style>* {margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;}</style>');
  // doc.close();

  var t = (function () {
    const arr = document.getElementsByClassName("meta-input");
    if (typeof arr !== "object" || arr.length < 1) {
      return null;
    }
    return arr[0];
  })();

  if (t === null) {
    return;
  }
  console.log("tttt", t);

  const parentCSS = {};
  var o = getComputedStyle(doc.body);
  for (var i = 0; i < o.length; i++) {
    parentCSS[o[i]] = o.getPropertyValue(o[i]);
  }

  // lay doi tuong dang focus
  const activeTextarea = document.activeElement;
  // gán cho thẻ data
  activeTextarea.setAttribute("data-focus", "1");

  function dumpCSSText(element) {
    var s = "";
    var o = getComputedStyle(element);
    for (var i = 0; i < o.length; i++) {
      const value = o.getPropertyValue(o[i]);

      if (parentCSS[o[i]] == value) {
        continue;
      }

      s += "-webkit-" + o[i] + ":" + value + ";";
      s += o[i] + ":" + value + ";";
    }
    return s;
  }

  function recurseAndAdd(el) {
    if (el.nodeType == 3) {
      return el.cloneNode(false);
    }

    var children = el.childNodes;

    const nel = el.cloneNode(false);

    for (let i = 0; i < children.length; i++) {
      nel.appendChild(recurseAndAdd(children[i]));
    }

    // query lay CSS
    const cssEl = dumpCSSText(el);
    if (typeof el.name === "string") {
      window.objHandleEvent.push(el);
      const id = window.objHandleEvent.length - 1;

      nel.setAttribute("onClick", "handleEvent(this, " + id + ");");
      nel.setAttribute("onChange", "handleEvent(this, " + id + ");");
      nel.setAttribute("onKeyUp", "handleEvent(this, " + id + ");");
    }
    if (nel.getAttribute("data-focus") === "1") {
      nel.setAttribute("autofocus", "true");
    }
    nel.setAttribute("style", cssEl);
    // nel.setAttribute("fi", "ok");
    nel.removeAttribute("class");
    nel.removeAttribute("id");

    return nel;
  }

  const html = recurseAndAdd(t).outerHTML;

  if (html === window.lastHTML) {
    return;
  }

  window.lastHTML = html;

  const a =
    `<!DOCTYPE html><html><head><title>MetanodeBottom</title>
    <meta charset='utf-8' name='viewport' content= 'width=device-width, initial-scale=1.0'></meta>
    <style>
    body >:first-child {position:unset !important}
    * {margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;}</style>
    </head><body>` +
    html +
    `
    <script>
      console.log('123');
      window.handleEvent = function (e, id) {
        console.log(id, e.tagName, e.name, e.value);
        window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify({ 
          command: 'handle-event',
          value: {
            event: {
              action: 'cmd', id, type: event.type, tagName: e.tagName, name: e.name, value: e.value
            }
          }
        }) );
        console.log('id, e.tagName, e.name, e.value');
      }
      document.querySelector('[data-focus]').focus();
    </script></body></html>`;
  console.log(a);

  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "bottom-content",
      value: {
        html: a,
      },
    })
  );

  // doc.open();
  // doc.write(a);
  // doc.close();
})();
