(function () {
  window.objHandleEvent = [];

  function postContentToNative(a, height) {
    if (window.lastHTML === a) {
      return;
    }
    window.lastHTML = a;
    if (a === "") {
      window.isHidden = true;
    } else {
      window.isHidden = false;
    }

    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: "bottom-content",
        value: {
          html: a,
          height,
        },
      })
    );
  }

  const messageListen = function (event) {
    if (
      typeof event.data !== "object" ||
      typeof event.data.action !== "string" ||
      event.data.action !== "cmd"
    ) {
      return;
    }

    const data = event.data;

    const id = data.id;

    if (typeof window.objHandleEvent[id] !== "object") {
      return;
    }

    if (data.type == "click") {
      window.objHandleEvent[id].click();

      // const elem = window.objHandleEvent[id];
      // var evt = document.createEvent("MouseEvents");
      // evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, elem);
      // elem.dispatchEvent(evt);
    } else {
      const elem = window.objHandleEvent[id];

      const lastValue = elem.value;
      elem.value = data.value;

      // const e = new Event("change");
      // elem.dispatchEvent(e);

      // for reactjs
      const tracker = elem._valueTracker;
      if (tracker) {
        const event = new Event("input", { bubbles: true });
        tracker.setValue(lastValue);
        elem.dispatchEvent(event);
      } else {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        elem.dispatchEvent(evt);
      }
      // end for reactjs

      // var keyboardEvent = document.createEvent('KeyboardEvent');
      // var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? 'initKeyboardEvent' : 'initKeyEvent';

      // keyboardEvent[initMethod](
      //   'keyup', // event type: keydown, keyup, keypress
      //   true, // bubbles
      //   true, // cancelable
      //   window, // view: should be window
      //   false, // ctrlKey
      //   false, // altKey
      //   false, // shiftKey
      //   false, // metaKey
      //   40, // keyCode: unsigned long - the virtual key code, else 0
      //   0, // charCode: unsigned long - the Unicode character associated with the depressed key, else 0
      // );
      // elem.dispatchEvent(keyboardEvent);
    }
  };

  window.removeEventListener("message", messageListen);
  window.addEventListener("message", messageListen);

  if (!document.body.contains(document.getElementById("FileFrame"))) {
    var ifrm = document.createElement("iframe");
    ifrm.setAttribute("src", "about:blank");
    ifrm.style.width = "0px";
    ifrm.style.height = "0px";
    ifrm.style.position = "fixed";
    ifrm.style.top = "0px";
    ifrm.style.right = "0px";
    ifrm.style.zIndex = 99999;
    ifrm.style.display = "block";
    ifrm.id = "FileFrame";
    document.body.appendChild(ifrm);
  }
  var doc = document.getElementById("FileFrame").contentWindow.document;
  doc.open();
  doc.write(
    "<style>* {margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;}</style>"
  );
  doc.close();

  var t = (function () {
    const arr = document.getElementsByClassName("meta-input");
    if (typeof arr !== "object" || arr.length < 1) {
      return null;
    }
    return arr[0];
  })();

  if (t === null) {
    postContentToNative("", 0);
    // if (!window.isHidden) {
    //   window.lastHTML = null;
    //   window.webkit.messageHandlers.callbackHandler.postMessage(
    //     JSON.stringify({
    //       command: "hiden-bottom",
    //     })
    //   );
    // }
    // window.isHidden = true;
    return;
  }
  // console.log("tttt", t);

  let height = 0;
  (function () {
    var obj = t.getBoundingClientRect();
    height = obj.height;
    if (height > 1) {
      return;
    }
    var o = getComputedStyle(t);
    for (var i = 0; i < o.length; i++) {
      if (o[i] !== "height" && o[i] !== "min-height") {
        continue;
      }

      let h = o.getPropertyValue(o[i]);

      if (typeof h === "string") {
        h = h.replace("px", "");
        try {
          h *= 1;
        } catch (e) {
          h = 0;
        }
        if (!isNaN(h) && typeof h === "number") {
          height = h;
        }
      }
    }
  })();

  const fontFamilyNeeds = [];

  const parentCSS = {};
  var o = getComputedStyle(doc.body);
  for (var i = 0; i < o.length; i++) {
    const name = o[i];
    const value = o.getPropertyValue(name);

    if (name === "font-family" && fontFamilyNeeds.indexOf(value) < 0) {
      fontFamilyNeeds.push(value);
    }
    parentCSS[name] = value;
  }

  // lay doi tuong dang focus
  const activeTextarea = document.activeElement;
  // gán cho thẻ data
  activeTextarea.setAttribute("data-focus", "1");

  function dumpCSSText(element) {
    var s = "";
    var o = getComputedStyle(element);
    for (var i = 0; i < o.length; i++) {
      const name = o[i];
      const value = o.getPropertyValue(name);

      if (parentCSS[name] == value) {
        continue;
      }

      if (name === "font-family" && fontFamilyNeeds.indexOf(value) < 0) {
        fontFamilyNeeds.push(value);
      }

      s += "-webkit-" + name + ":" + value + ";";
      s += name + ":" + value + ";";
    }
    return s;
  }

  function recurseAndAdd(el) {
    if (el.nodeType == 3) {
      return el.cloneNode(false);
    }

    var children = el.childNodes;

    const nel = el.cloneNode(false);

    for (let i = 0; i < children.length; i++) {
      nel.appendChild(recurseAndAdd(children[i]));
    }

    // query lay CSS
    let cssEl = dumpCSSText(el);
    if (typeof el.name === "string") {
      window.objHandleEvent.push(el);
      const id = window.objHandleEvent.length - 1;

      nel.setAttribute("onClick", "handleEvent(this, " + id + ");");
      nel.setAttribute("onChange", "handleEvent(this, " + id + ");");
      nel.setAttribute("onKeyUp", "handleEvent(this, " + id + ");");
    }
    if (nel.getAttribute("data-focus") === "1") {
      nel.setAttribute("autofocus", "true");
    }
    // if (nel.className === "text") {
    //   cssEl += "; min-width: 100%;";
    // }
    nel.setAttribute("style", cssEl);
    // nel.setAttribute("fi", "ok");
    nel.removeAttribute("class");
    nel.removeAttribute("id");
    if (el === t) {
      nel.setAttribute("id", "root");
    }

    return nel;
  }

  const html = recurseAndAdd(t).outerHTML;

  console.log("fontFamilyNeeds", fontFamilyNeeds);

  const fontsText = [];
  if (fontFamilyNeeds.length > 0) {
    const styleSheets = Array.from(document.styleSheets);

    const fontsFamily = [];
    styleSheets.forEach((styleSheet) => {
      let cssRules;

      try {
        cssRules = styleSheet.cssRules;
      } catch (e) {
        cssRules = null;
      }

      if (cssRules === null || cssRules.length < 1) {
        return;
      }

      // all the font-faces rules
      for (var i = 0; i < cssRules.length; i++) {
        const css = cssRules[i];
        const cssText = css.cssText;

        const isFontFace = cssText.startsWith("@font-face");

        if (!isFontFace) {
          continue;
        }
        if (fontsText.indexOf(cssText) > -1) {
          continue;
        }

        const fontFamily = css.style.fontFamily;

        if (fontFamilyNeeds.indexOf(fontFamily) < 0) {
          continue;
        }

        fontsText.push(cssText);

        fontsFamily.push(fontFamily);
      }
    });

    console.log(fontsFamily);
    console.log(fontsText);

    // lay font hien tai dang co,
  }

  const a =
    `
      <!DOCTYPE html>
        <html>
          <head>
            <title>MetanodeBottom</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <style>
              button, input[type="submit"], input[type="reset"] {
                background: none;
                color: inherit;
                border: none;
                padding: 0;
                font: inherit;
                cursor: pointer;
                outline: inherit;
              }
              #root {position:unset !important; left: unset !important; opacity: 1 !important }
              *:focus {
                outline: none;
              }
              * {margin: 0;padding: 0;border: 0;font-size: 100%;font-family: system-ui;vertical-align: baseline;}
              ${fontsText.join("\n")}
            </style>

            
          </head>
          <body>` +
    html +
    `
      <script>
                
      window.handleEvent = function (e, id) {
        console.log(id, e.tagName, e.name, e.value, '<<<<<<<<<<Handle Click Event >>>>>>>>>>>>');
        window.webkit.messageHandlers.callbackHandler.postMessage(JSON.stringify({ 
          command: 'handle-event',
          value: {
            event: {
              action: 'cmd', id, type: event.type, tagName: e.tagName, name: e.name, value: e.value
            }
          }
        }) );
          
        }
        try {
          document.querySelector('[data-focus]').focus();
        } catch(e) {
          console.log("err", e);
        }
      </script>
    </body></html>`;

  postContentToNative(a, height);
})();
