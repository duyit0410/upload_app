window.fetch2 = (e) =>
  new Promise((a) => {
    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: "get-url",
        url: e,
      })
    );
    var n = (e) => {
      if ("url" !== e.data) {
        return;
      }
      let s = atob(window.abc);
      (s = s.replace(/AAAAaaaaBBBB/g, "`")), console.log("----ok--data----", s);
      window.removeEventListener("message", n), a(s);
    };
    window.addEventListener("message", n, !1);
  });

!(function (t) {
  var e = {};
  function n(r) {
    if (e[r]) return e[r].exports;
    var o = (e[r] = { i: r, l: !1, exports: {} });
    return t[r].call(o.exports, o, o.exports, n), (o.l = !0), o.exports;
  }
  (n.m = t),
    (n.c = e),
    (n.d = function (t, e, r) {
      n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: r });
    }),
    (n.r = function (t) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(t, "__esModule", { value: !0 });
    }),
    (n.t = function (t, e) {
      if ((1 & e && (t = n(t)), 8 & e)) return t;
      if (4 & e && "object" == typeof t && t && t.__esModule) return t;
      var r = Object.create(null);
      if (
        (n.r(r),
        Object.defineProperty(r, "default", { enumerable: !0, value: t }),
        2 & e && "string" != typeof t)
      )
        for (var o in t)
          n.d(
            r,
            o,
            function (e) {
              return t[e];
            }.bind(null, o)
          );
      return r;
    }),
    (n.n = function (t) {
      var e =
        t && t.__esModule
          ? function () {
              return t["default"];
            }
          : function () {
              return t;
            };
      return n.d(e, "a", e), e;
    }),
    (n.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }),
    (n.p = ""),
    n((n.s = 132));
})([
  function (t, e, n) {
    var r = n(1),
      o = n(7),
      i = n(14),
      a = n(11),
      u = n(17),
      c = function s(t, e, n) {
        var c,
          f,
          l,
          p,
          h = t & s.F,
          d = t & s.G,
          y = t & s.P,
          m = t & s.B,
          v = d ? r : t & s.S ? r[e] || (r[e] = {}) : (r[e] || {}).prototype,
          g = d ? o : o[e] || (o[e] = {}),
          b = g.prototype || (g.prototype = {});
        for (c in (d && (n = e), n))
          (l = ((f = !h && v && v[c] !== undefined) ? v : n)[c]),
            (p =
              m && f
                ? u(l, r)
                : y && "function" == typeof l
                ? u(Function.call, l)
                : l),
            v && a(v, c, l, t & s.U),
            g[c] != l && i(g, c, p),
            y && b[c] != l && (b[c] = l);
      };
    (r.core = o),
      (c.F = 1),
      (c.G = 2),
      (c.S = 4),
      (c.P = 8),
      (c.B = 16),
      (c.W = 32),
      (c.U = 64),
      (c.R = 128),
      (t.exports = c);
  },
  function (t, e) {
    var n = (t.exports =
      "undefined" != typeof window && window.Math == Math
        ? window
        : "undefined" != typeof self && self.Math == Math
        ? self
        : Function("return this")());
    "number" == typeof __g && (__g = n);
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return !!t();
      } catch (e) {
        return !0;
      }
    };
  },
  function (t, e, n) {
    var r = n(4);
    t.exports = function (t) {
      if (!r(t)) throw TypeError(t + " is not an object!");
      return t;
    };
  },
  function (t, e) {
    function n(t) {
      return (n =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    t.exports = function (t) {
      return "object" === n(t) ? null !== t : "function" == typeof t;
    };
  },
  function (t, e, n) {
    var r = n(49)("wks"),
      o = n(29),
      i = n(1).Symbol,
      a = "function" == typeof i;
    (t.exports = function (t) {
      return r[t] || (r[t] = (a && i[t]) || (a ? i : o)("Symbol." + t));
    }).store = r;
  },
  function (t, e, n) {
    var r = n(19),
      o = Math.min;
    t.exports = function (t) {
      return t > 0 ? o(r(t), 9007199254740991) : 0;
    };
  },
  function (t, e) {
    var n = (t.exports = { version: "2.6.12" });
    "number" == typeof __e && (__e = n);
  },
  function (t, e, n) {
    t.exports = !n(2)(function () {
      return (
        7 !=
        Object.defineProperty({}, "a", {
          get: function () {
            return 7;
          },
        }).a
      );
    });
  },
  function (t, e, n) {
    var r = n(3),
      o = n(93),
      i = n(26),
      a = Object.defineProperty;
    e.f = n(8)
      ? Object.defineProperty
      : function (t, e, n) {
          if ((r(t), (e = i(e, !0)), r(n), o))
            try {
              return a(t, e, n);
            } catch (u) {}
          if ("get" in n || "set" in n)
            throw TypeError("Accessors not supported!");
          return "value" in n && (t[e] = n.value), t;
        };
  },
  function (t, e, n) {
    var r = n(24);
    t.exports = function (t) {
      return Object(r(t));
    };
  },
  function (t, e, n) {
    var r = n(1),
      o = n(14),
      i = n(13),
      a = n(29)("src"),
      u = n(137),
      c = "toString",
      s = ("" + u).split(c);
    (n(7).inspectSource = function (t) {
      return u.call(t);
    }),
      (t.exports = function (t, e, n, u) {
        var c = "function" == typeof n;
        c && (i(n, "name") || o(n, "name", e)),
          t[e] !== n &&
            (c && (i(n, a) || o(n, a, t[e] ? "" + t[e] : s.join(String(e)))),
            t === r
              ? (t[e] = n)
              : u
              ? t[e]
                ? (t[e] = n)
                : o(t, e, n)
              : (delete t[e], o(t, e, n)));
      })(Function.prototype, c, function () {
        return ("function" == typeof this && this[a]) || u.call(this);
      });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(2),
      i = n(24),
      a = /"/g,
      u = function (t, e, n, r) {
        var o = String(i(t)),
          u = "<" + e;
        return (
          "" !== n &&
            (u += " " + n + '="' + String(r).replace(a, "&quot;") + '"'),
          u + ">" + o + "</" + e + ">"
        );
      };
    t.exports = function (t, e) {
      var n = {};
      (n[t] = e(u)),
        r(
          r.P +
            r.F *
              o(function () {
                var e = ""[t]('"');
                return e !== e.toLowerCase() || e.split('"').length > 3;
              }),
          "String",
          n
        );
    };
  },
  function (t, e) {
    var n = {}.hasOwnProperty;
    t.exports = function (t, e) {
      return n.call(t, e);
    };
  },
  function (t, e, n) {
    var r = n(9),
      o = n(28);
    t.exports = n(8)
      ? function (t, e, n) {
          return r.f(t, e, o(1, n));
        }
      : function (t, e, n) {
          return (t[e] = n), t;
        };
  },
  function (t, e, n) {
    var r = n(44),
      o = n(24);
    t.exports = function (t) {
      return r(o(t));
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(2);
    t.exports = function (t, e) {
      return (
        !!t &&
        r(function () {
          e ? t.call(null, function () {}, 1) : t.call(null);
        })
      );
    };
  },
  function (t, e, n) {
    var r = n(18);
    t.exports = function (t, e, n) {
      if ((r(t), e === undefined)) return t;
      switch (n) {
        case 1:
          return function (n) {
            return t.call(e, n);
          };
        case 2:
          return function (n, r) {
            return t.call(e, n, r);
          };
        case 3:
          return function (n, r, o) {
            return t.call(e, n, r, o);
          };
      }
      return function () {
        return t.apply(e, arguments);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if ("function" != typeof t) throw TypeError(t + " is not a function!");
      return t;
    };
  },
  function (t, e) {
    var n = Math.ceil,
      r = Math.floor;
    t.exports = function (t) {
      return isNaN((t = +t)) ? 0 : (t > 0 ? r : n)(t);
    };
  },
  function (t, e, n) {
    var r = n(45),
      o = n(28),
      i = n(15),
      a = n(26),
      u = n(13),
      c = n(93),
      s = Object.getOwnPropertyDescriptor;
    e.f = n(8)
      ? s
      : function (t, e) {
          if (((t = i(t)), (e = a(e, !0)), c))
            try {
              return s(t, e);
            } catch (n) {}
          if (u(t, e)) return o(!r.f.call(t, e), t[e]);
        };
  },
  function (t, e, n) {
    var r = n(0),
      o = n(7),
      i = n(2);
    t.exports = function (t, e) {
      var n = (o.Object || {})[t] || Object[t],
        a = {};
      (a[t] = e(n)),
        r(
          r.S +
            r.F *
              i(function () {
                n(1);
              }),
          "Object",
          a
        );
    };
  },
  function (t, e, n) {
    var r = n(17),
      o = n(44),
      i = n(10),
      a = n(6),
      u = n(109);
    t.exports = function (t, e) {
      var n = 1 == t,
        c = 2 == t,
        s = 3 == t,
        f = 4 == t,
        l = 6 == t,
        p = 5 == t || l,
        h = e || u;
      return function (e, u, d) {
        for (
          var y,
            m,
            v = i(e),
            g = o(v),
            b = r(u, d, 3),
            w = a(g.length),
            _ = 0,
            E = n ? h(e, w) : c ? h(e, 0) : undefined;
          w > _;
          _++
        )
          if ((p || _ in g) && ((m = b((y = g[_]), _, v)), t))
            if (n) E[_] = m;
            else if (m)
              switch (t) {
                case 3:
                  return !0;
                case 5:
                  return y;
                case 6:
                  return _;
                case 2:
                  E.push(y);
              }
            else if (f) return !1;
        return l ? -1 : s || f ? f : E;
      };
    };
  },
  function (t, e) {
    var n = {}.toString;
    t.exports = function (t) {
      return n.call(t).slice(8, -1);
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if (t == undefined) throw TypeError("Can't call method on  " + t);
      return t;
    };
  },
  function (t, e, n) {
    "use strict";
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    if (n(8)) {
      var o = n(30),
        i = n(1),
        a = n(2),
        u = n(0),
        c = n(60),
        s = n(87),
        f = n(17),
        l = n(42),
        p = n(28),
        h = n(14),
        d = n(43),
        y = n(19),
        m = n(6),
        v = n(120),
        g = n(32),
        b = n(26),
        w = n(13),
        _ = n(46),
        E = n(4),
        T = n(10),
        S = n(79),
        x = n(33),
        A = n(35),
        R = n(34).f,
        I = n(81),
        O = n(29),
        P = n(5),
        L = n(22),
        N = n(50),
        D = n(47),
        F = n(83),
        M = n(40),
        C = n(53),
        B = n(41),
        j = n(82),
        U = n(111),
        k = n(9),
        V = n(20),
        q = k.f,
        Y = V.f,
        G = i.RangeError,
        H = i.TypeError,
        W = i.Uint8Array,
        $ = "ArrayBuffer",
        z = "SharedArrayBuffer",
        X = "BYTES_PER_ELEMENT",
        K = Array.prototype,
        Q = s.ArrayBuffer,
        J = s.DataView,
        Z = L(0),
        tt = L(2),
        et = L(3),
        nt = L(4),
        rt = L(5),
        ot = L(6),
        it = N(!0),
        at = N(!1),
        ut = F.values,
        ct = F.keys,
        st = F.entries,
        ft = K.lastIndexOf,
        lt = K.reduce,
        pt = K.reduceRight,
        ht = K.join,
        dt = K.sort,
        yt = K.slice,
        mt = K.toString,
        vt = K.toLocaleString,
        gt = P("iterator"),
        bt = P("toStringTag"),
        wt = O("typed_constructor"),
        _t = O("def_constructor"),
        Et = c.CONSTR,
        Tt = c.TYPED,
        St = c.VIEW,
        xt = "Wrong length!",
        At = L(1, function (t, e) {
          return Lt(D(t, t[_t]), e);
        }),
        Rt = a(function () {
          return 1 === new W(new Uint16Array([1]).buffer)[0];
        }),
        It =
          !!W &&
          !!W.prototype.set &&
          a(function () {
            new W(1).set({});
          }),
        Ot = function (t, e) {
          var n = y(t);
          if (n < 0 || n % e) throw G("Wrong offset!");
          return n;
        },
        Pt = function (t) {
          if (E(t) && Tt in t) return t;
          throw H(t + " is not a typed array!");
        },
        Lt = function (t, e) {
          if (!E(t) || !(wt in t))
            throw H("It is not a typed array constructor!");
          return new t(e);
        },
        Nt = function (t, e) {
          return Dt(D(t, t[_t]), e);
        },
        Dt = function (t, e) {
          for (var n = 0, r = e.length, o = Lt(t, r); r > n; ) o[n] = e[n++];
          return o;
        },
        Ft = function (t, e, n) {
          q(t, e, {
            get: function () {
              return this._d[n];
            },
          });
        },
        Mt = function (t) {
          var e,
            n,
            r,
            o,
            i,
            a,
            u = T(t),
            c = arguments.length,
            s = c > 1 ? arguments[1] : undefined,
            l = s !== undefined,
            p = I(u);
          if (p != undefined && !S(p)) {
            for (a = p.call(u), r = [], e = 0; !(i = a.next()).done; e++)
              r.push(i.value);
            u = r;
          }
          for (
            l && c > 2 && (s = f(s, arguments[2], 2)),
              e = 0,
              n = m(u.length),
              o = Lt(this, n);
            n > e;
            e++
          )
            o[e] = l ? s(u[e], e) : u[e];
          return o;
        },
        Ct = function () {
          for (var t = 0, e = arguments.length, n = Lt(this, e); e > t; )
            n[t] = arguments[t++];
          return n;
        },
        Bt =
          !!W &&
          a(function () {
            vt.call(new W(1));
          }),
        jt = function () {
          return vt.apply(Bt ? yt.call(Pt(this)) : Pt(this), arguments);
        },
        Ut = {
          copyWithin: function (t, e) {
            return U.call(
              Pt(this),
              t,
              e,
              arguments.length > 2 ? arguments[2] : undefined
            );
          },
          every: function (t) {
            return nt(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          fill: function (t) {
            return j.apply(Pt(this), arguments);
          },
          filter: function (t) {
            return Nt(
              this,
              tt(Pt(this), t, arguments.length > 1 ? arguments[1] : undefined)
            );
          },
          find: function (t) {
            return rt(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          findIndex: function (t) {
            return ot(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          forEach: function (t) {
            Z(Pt(this), t, arguments.length > 1 ? arguments[1] : undefined);
          },
          indexOf: function (t) {
            return at(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          includes: function (t) {
            return it(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          join: function (t) {
            return ht.apply(Pt(this), arguments);
          },
          lastIndexOf: function (t) {
            return ft.apply(Pt(this), arguments);
          },
          map: function (t) {
            return At(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          reduce: function (t) {
            return lt.apply(Pt(this), arguments);
          },
          reduceRight: function (t) {
            return pt.apply(Pt(this), arguments);
          },
          reverse: function () {
            for (
              var t, e = this, n = Pt(e).length, r = Math.floor(n / 2), o = 0;
              o < r;

            )
              (t = e[o]), (e[o++] = e[--n]), (e[n] = t);
            return e;
          },
          some: function (t) {
            return et(
              Pt(this),
              t,
              arguments.length > 1 ? arguments[1] : undefined
            );
          },
          sort: function (t) {
            return dt.call(Pt(this), t);
          },
          subarray: function (t, e) {
            var n = Pt(this),
              r = n.length,
              o = g(t, r);
            return new (D(n, n[_t]))(
              n.buffer,
              n.byteOffset + o * n.BYTES_PER_ELEMENT,
              m((e === undefined ? r : g(e, r)) - o)
            );
          },
        },
        kt = function (t, e) {
          return Nt(this, yt.call(Pt(this), t, e));
        },
        Vt = function (t) {
          Pt(this);
          var e = Ot(arguments[1], 1),
            n = this.length,
            r = T(t),
            o = m(r.length),
            i = 0;
          if (o + e > n) throw G(xt);
          for (; i < o; ) this[e + i] = r[i++];
        },
        qt = {
          entries: function () {
            return st.call(Pt(this));
          },
          keys: function () {
            return ct.call(Pt(this));
          },
          values: function () {
            return ut.call(Pt(this));
          },
        },
        Yt = function (t, e) {
          return (
            E(t) &&
            t[Tt] &&
            "symbol" != r(e) &&
            e in t &&
            String(+e) == String(e)
          );
        },
        Gt = function (t, e) {
          return Yt(t, (e = b(e, !0))) ? p(2, t[e]) : Y(t, e);
        },
        Ht = function (t, e, n) {
          return !(Yt(t, (e = b(e, !0))) && E(n) && w(n, "value")) ||
            w(n, "get") ||
            w(n, "set") ||
            n.configurable ||
            (w(n, "writable") && !n.writable) ||
            (w(n, "enumerable") && !n.enumerable)
            ? q(t, e, n)
            : ((t[e] = n.value), t);
        };
      Et || ((V.f = Gt), (k.f = Ht)),
        u(u.S + u.F * !Et, "Object", {
          getOwnPropertyDescriptor: Gt,
          defineProperty: Ht,
        }),
        a(function () {
          mt.call({});
        }) &&
          (mt = vt =
            function () {
              return ht.call(this);
            });
      var Wt = d({}, Ut);
      d(Wt, qt),
        h(Wt, gt, qt.values),
        d(Wt, {
          slice: kt,
          set: Vt,
          constructor: function () {},
          toString: mt,
          toLocaleString: jt,
        }),
        Ft(Wt, "buffer", "b"),
        Ft(Wt, "byteOffset", "o"),
        Ft(Wt, "byteLength", "l"),
        Ft(Wt, "length", "e"),
        q(Wt, bt, {
          get: function () {
            return this[Tt];
          },
        }),
        (t.exports = function (t, e, n, r) {
          var s = t + ((r = !!r) ? "Clamped" : "") + "Array",
            f = "get" + t,
            p = "set" + t,
            d = i[s],
            y = d || {},
            g = d && A(d),
            b = !d || !c.ABV,
            w = {},
            T = d && d.prototype,
            S = function (t, n) {
              q(t, n, {
                get: function () {
                  return (function (t, n) {
                    var r = t._d;
                    return r.v[f](n * e + r.o, Rt);
                  })(this, n);
                },
                set: function (t) {
                  return (function (t, n, o) {
                    var i = t._d;
                    r &&
                      (o =
                        (o = Math.round(o)) < 0 ? 0 : o > 255 ? 255 : 255 & o),
                      i.v[p](n * e + i.o, o, Rt);
                  })(this, n, t);
                },
                enumerable: !0,
              });
            };
          b
            ? ((d = n(function (t, n, r, o) {
                l(t, d, s, "_d");
                var i,
                  a,
                  u,
                  c,
                  f = 0,
                  p = 0;
                if (E(n)) {
                  if (!(n instanceof Q || (c = _(n)) == $ || c == z))
                    return Tt in n ? Dt(d, n) : Mt.call(d, n);
                  (i = n), (p = Ot(r, e));
                  var y = n.byteLength;
                  if (o === undefined) {
                    if (y % e) throw G(xt);
                    if ((a = y - p) < 0) throw G(xt);
                  } else if ((a = m(o) * e) + p > y) throw G(xt);
                  u = a / e;
                } else (u = v(n)), (i = new Q((a = u * e)));
                for (
                  h(t, "_d", { b: i, o: p, l: a, e: u, v: new J(i) });
                  f < u;

                )
                  S(t, f++);
              })),
              (T = d.prototype = x(Wt)),
              h(T, "constructor", d))
            : (a(function () {
                d(1);
              }) &&
                a(function () {
                  new d(-1);
                }) &&
                C(function (t) {
                  new d(), new d(null), new d(1.5), new d(t);
                }, !0)) ||
              ((d = n(function (t, n, r, o) {
                var i;
                return (
                  l(t, d, s),
                  E(n)
                    ? n instanceof Q || (i = _(n)) == $ || i == z
                      ? o !== undefined
                        ? new y(n, Ot(r, e), o)
                        : r !== undefined
                        ? new y(n, Ot(r, e))
                        : new y(n)
                      : Tt in n
                      ? Dt(d, n)
                      : Mt.call(d, n)
                    : new y(v(n))
                );
              })),
              Z(
                g !== Function.prototype ? R(y).concat(R(g)) : R(y),
                function (t) {
                  t in d || h(d, t, y[t]);
                }
              ),
              (d.prototype = T),
              o || (T.constructor = d));
          var I = T[gt],
            O = !!I && ("values" == I.name || I.name == undefined),
            P = qt.values;
          h(d, wt, !0),
            h(T, Tt, s),
            h(T, St, !0),
            h(T, _t, d),
            (r ? new d(1)[bt] == s : bt in T) ||
              q(T, bt, {
                get: function () {
                  return s;
                },
              }),
            (w[s] = d),
            u(u.G + u.W + u.F * (d != y), w),
            u(u.S, s, { BYTES_PER_ELEMENT: e }),
            u(
              u.S +
                u.F *
                  a(function () {
                    y.of.call(d, 1);
                  }),
              s,
              { from: Mt, of: Ct }
            ),
            X in T || h(T, X, e),
            u(u.P, s, Ut),
            B(s),
            u(u.P + u.F * It, s, { set: Vt }),
            u(u.P + u.F * !O, s, qt),
            o || T.toString == mt || (T.toString = mt),
            u(
              u.P +
                u.F *
                  a(function () {
                    new d(1).slice();
                  }),
              s,
              { slice: kt }
            ),
            u(
              u.P +
                u.F *
                  (a(function () {
                    return (
                      [1, 2].toLocaleString() != new d([1, 2]).toLocaleString()
                    );
                  }) ||
                    !a(function () {
                      T.toLocaleString.call([1, 2]);
                    })),
              s,
              { toLocaleString: jt }
            ),
            (M[s] = O ? I : P),
            o || O || h(T, gt, P);
        });
    } else t.exports = function () {};
  },
  function (t, e, n) {
    var r = n(4);
    t.exports = function (t, e) {
      if (!r(t)) return t;
      var n, o;
      if (e && "function" == typeof (n = t.toString) && !r((o = n.call(t))))
        return o;
      if ("function" == typeof (n = t.valueOf) && !r((o = n.call(t)))) return o;
      if (!e && "function" == typeof (n = t.toString) && !r((o = n.call(t))))
        return o;
      throw TypeError("Can't convert object to primitive value");
    };
  },
  function (t, e, n) {
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    var o = n(29)("meta"),
      i = n(4),
      a = n(13),
      u = n(9).f,
      c = 0,
      s =
        Object.isExtensible ||
        function () {
          return !0;
        },
      f = !n(2)(function () {
        return s(Object.preventExtensions({}));
      }),
      l = function (t) {
        u(t, o, { value: { i: "O" + ++c, w: {} } });
      },
      p = (t.exports = {
        KEY: o,
        NEED: !1,
        fastKey: function (t, e) {
          if (!i(t))
            return "symbol" == r(t)
              ? t
              : ("string" == typeof t ? "S" : "P") + t;
          if (!a(t, o)) {
            if (!s(t)) return "F";
            if (!e) return "E";
            l(t);
          }
          return t[o].i;
        },
        getWeak: function (t, e) {
          if (!a(t, o)) {
            if (!s(t)) return !0;
            if (!e) return !1;
            l(t);
          }
          return t[o].w;
        },
        onFreeze: function (t) {
          return f && p.NEED && s(t) && !a(t, o) && l(t), t;
        },
      });
  },
  function (t, e) {
    t.exports = function (t, e) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: e,
      };
    };
  },
  function (t, e) {
    var n = 0,
      r = Math.random();
    t.exports = function (t) {
      return "Symbol(".concat(
        t === undefined ? "" : t,
        ")_",
        (++n + r).toString(36)
      );
    };
  },
  function (t, e) {
    t.exports = !1;
  },
  function (t, e, n) {
    var r = n(95),
      o = n(66);
    t.exports =
      Object.keys ||
      function (t) {
        return r(t, o);
      };
  },
  function (t, e, n) {
    var r = n(19),
      o = Math.max,
      i = Math.min;
    t.exports = function (t, e) {
      return (t = r(t)) < 0 ? o(t + e, 0) : i(t, e);
    };
  },
  function (t, e, n) {
    var r = n(3),
      o = n(96),
      i = n(66),
      a = n(65)("IE_PROTO"),
      u = function () {},
      c = function () {
        var t,
          e = n(63)("iframe"),
          r = i.length;
        for (
          e.style.display = "none",
            n(67).appendChild(e),
            e.src = "javascript:",
            (t = e.contentWindow.document).open(),
            t.write("<script>document.F=Object</script>"),
            t.close(),
            c = t.F;
          r--;

        )
          delete c.prototype[i[r]];
        return c();
      };
    t.exports =
      Object.create ||
      function (t, e) {
        var n;
        return (
          null !== t
            ? ((u.prototype = r(t)),
              (n = new u()),
              (u.prototype = null),
              (n[a] = t))
            : (n = c()),
          e === undefined ? n : o(n, e)
        );
      };
  },
  function (t, e, n) {
    var r = n(95),
      o = n(66).concat("length", "prototype");
    e.f =
      Object.getOwnPropertyNames ||
      function (t) {
        return r(t, o);
      };
  },
  function (t, e, n) {
    var r = n(13),
      o = n(10),
      i = n(65)("IE_PROTO"),
      a = Object.prototype;
    t.exports =
      Object.getPrototypeOf ||
      function (t) {
        return (
          (t = o(t)),
          r(t, i)
            ? t[i]
            : "function" == typeof t.constructor && t instanceof t.constructor
            ? t.constructor.prototype
            : t instanceof Object
            ? a
            : null
        );
      };
  },
  function (t, e, n) {
    var r = n(5)("unscopables"),
      o = Array.prototype;
    o[r] == undefined && n(14)(o, r, {}),
      (t.exports = function (t) {
        o[r][t] = !0;
      });
  },
  function (t, e, n) {
    var r = n(4);
    t.exports = function (t, e) {
      if (!r(t) || t._t !== e)
        throw TypeError("Incompatible receiver, " + e + " required!");
      return t;
    };
  },
  function (t, e, n) {
    var r = n(9).f,
      o = n(13),
      i = n(5)("toStringTag");
    t.exports = function (t, e, n) {
      t &&
        !o((t = n ? t : t.prototype), i) &&
        r(t, i, { configurable: !0, value: e });
    };
  },
  function (t, e, n) {
    var r = n(0),
      o = n(24),
      i = n(2),
      a = n(69),
      u = "[" + a + "]",
      c = RegExp("^" + u + u + "*"),
      s = RegExp(u + u + "*$"),
      f = function (t, e, n) {
        var o = {},
          u = i(function () {
            return !!a[t]() || "​" != "​"[t]();
          }),
          c = (o[t] = u ? e(l) : a[t]);
        n && (o[n] = c), r(r.P + r.F * u, "String", o);
      },
      l = (f.trim = function (t, e) {
        return (
          (t = String(o(t))),
          1 & e && (t = t.replace(c, "")),
          2 & e && (t = t.replace(s, "")),
          t
        );
      });
    t.exports = f;
  },
  function (t, e) {
    t.exports = {};
  },
  function (t, e, n) {
    "use strict";
    var r = n(1),
      o = n(9),
      i = n(8),
      a = n(5)("species");
    t.exports = function (t) {
      var e = r[t];
      i &&
        e &&
        !e[a] &&
        o.f(e, a, {
          configurable: !0,
          get: function () {
            return this;
          },
        });
    };
  },
  function (t, e) {
    t.exports = function (t, e, n, r) {
      if (!(t instanceof e) || (r !== undefined && r in t))
        throw TypeError(n + ": incorrect invocation!");
      return t;
    };
  },
  function (t, e, n) {
    var r = n(11);
    t.exports = function (t, e, n) {
      for (var o in e) r(t, o, e[o], n);
      return t;
    };
  },
  function (t, e, n) {
    var r = n(23);
    t.exports = Object("z").propertyIsEnumerable(0)
      ? Object
      : function (t) {
          return "String" == r(t) ? t.split("") : Object(t);
        };
  },
  function (t, e) {
    e.f = {}.propertyIsEnumerable;
  },
  function (t, e, n) {
    var r = n(23),
      o = n(5)("toStringTag"),
      i =
        "Arguments" ==
        r(
          (function () {
            return arguments;
          })()
        );
    t.exports = function (t) {
      var e, n, a;
      return t === undefined
        ? "Undefined"
        : null === t
        ? "Null"
        : "string" ==
          typeof (n = (function (t, e) {
            try {
              return t[e];
            } catch (n) {}
          })((e = Object(t)), o))
        ? n
        : i
        ? r(e)
        : "Object" == (a = r(e)) && "function" == typeof e.callee
        ? "Arguments"
        : a;
    };
  },
  function (t, e, n) {
    var r = n(3),
      o = n(18),
      i = n(5)("species");
    t.exports = function (t, e) {
      var n,
        a = r(t).constructor;
      return a === undefined || (n = r(a)[i]) == undefined ? e : o(n);
    };
  },
  function (t, e) {
    function n(t) {
      return (n =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    var r;
    r = (function () {
      return this;
    })();
    try {
      r = r || new Function("return this")();
    } catch (o) {
      "object" === ("undefined" == typeof window ? "undefined" : n(window)) &&
        (r = window);
    }
    t.exports = r;
  },
  function (t, e, n) {
    var r = n(7),
      o = n(1),
      i = "__core-js_shared__",
      a = o[i] || (o[i] = {});
    (t.exports = function (t, e) {
      return a[t] || (a[t] = e !== undefined ? e : {});
    })("versions", []).push({
      version: r.version,
      mode: n(30) ? "pure" : "global",
      copyright: "© 2020 Denis Pushkarev (zloirock.ru)",
    });
  },
  function (t, e, n) {
    var r = n(15),
      o = n(6),
      i = n(32);
    t.exports = function (t) {
      return function (e, n, a) {
        var u,
          c = r(e),
          s = o(c.length),
          f = i(a, s);
        if (t && n != n) {
          for (; s > f; ) if ((u = c[f++]) != u) return !0;
        } else
          for (; s > f; f++)
            if ((t || f in c) && c[f] === n) return t || f || 0;
        return !t && -1;
      };
    };
  },
  function (t, e) {
    e.f = Object.getOwnPropertySymbols;
  },
  function (t, e, n) {
    var r = n(23);
    t.exports =
      Array.isArray ||
      function (t) {
        return "Array" == r(t);
      };
  },
  function (t, e, n) {
    var r = n(5)("iterator"),
      o = !1;
    try {
      var i = [7][r]();
      (i["return"] = function () {
        o = !0;
      }),
        Array.from(i, function () {
          throw 2;
        });
    } catch (a) {}
    t.exports = function (t, e) {
      if (!e && !o) return !1;
      var n = !1;
      try {
        var i = [7],
          u = i[r]();
        (u.next = function () {
          return { done: (n = !0) };
        }),
          (i[r] = function () {
            return u;
          }),
          t(i);
      } catch (a) {}
      return n;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(3);
    t.exports = function () {
      var t = r(this),
        e = "";
      return (
        t.global && (e += "g"),
        t.ignoreCase && (e += "i"),
        t.multiline && (e += "m"),
        t.unicode && (e += "u"),
        t.sticky && (e += "y"),
        e
      );
    };
  },
  function (t, e, n) {
    "use strict";
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    var o = n(46),
      i = RegExp.prototype.exec;
    t.exports = function (t, e) {
      var n = t.exec;
      if ("function" == typeof n) {
        var a = n.call(t, e);
        if ("object" !== r(a))
          throw new TypeError(
            "RegExp exec method returned something other than an Object or null"
          );
        return a;
      }
      if ("RegExp" !== o(t))
        throw new TypeError("RegExp#exec called on incompatible receiver");
      return i.call(t, e);
    };
  },
  function (t, e, n) {
    "use strict";
    n(113);
    var r = n(11),
      o = n(14),
      i = n(2),
      a = n(24),
      u = n(5),
      c = n(84),
      s = u("species"),
      f = !i(function () {
        var t = /./;
        return (
          (t.exec = function () {
            var t = [];
            return (t.groups = { a: "7" }), t;
          }),
          "7" !== "".replace(t, "$<a>")
        );
      }),
      l = (function () {
        var t = /(?:)/,
          e = t.exec;
        t.exec = function () {
          return e.apply(this, arguments);
        };
        var n = "ab".split(t);
        return 2 === n.length && "a" === n[0] && "b" === n[1];
      })();
    t.exports = function (t, e, n) {
      var p = u(t),
        h = !i(function () {
          var e = {};
          return (
            (e[p] = function () {
              return 7;
            }),
            7 != ""[t](e)
          );
        }),
        d = h
          ? !i(function () {
              var e = !1,
                n = /a/;
              return (
                (n.exec = function () {
                  return (e = !0), null;
                }),
                "split" === t &&
                  ((n.constructor = {}),
                  (n.constructor[s] = function () {
                    return n;
                  })),
                n[p](""),
                !e
              );
            })
          : undefined;
      if (!h || !d || ("replace" === t && !f) || ("split" === t && !l)) {
        var y = /./[p],
          m = n(a, p, ""[t], function (t, e, n, r, o) {
            return e.exec === c
              ? h && !o
                ? { done: !0, value: y.call(e, n, r) }
                : { done: !0, value: t.call(n, e, r) }
              : { done: !1 };
          }),
          v = m[0],
          g = m[1];
        r(String.prototype, t, v),
          o(
            RegExp.prototype,
            p,
            2 == e
              ? function (t, e) {
                  return g.call(t, this, e);
                }
              : function (t) {
                  return g.call(t, this);
                }
          );
      }
    };
  },
  function (t, e, n) {
    var r = n(17),
      o = n(108),
      i = n(79),
      a = n(3),
      u = n(6),
      c = n(81),
      s = {},
      f = {};
    ((e = t.exports =
      function (t, e, n, l, p) {
        var h,
          d,
          y,
          m,
          v = p
            ? function () {
                return t;
              }
            : c(t),
          g = r(n, l, e ? 2 : 1),
          b = 0;
        if ("function" != typeof v) throw TypeError(t + " is not iterable!");
        if (i(v)) {
          for (h = u(t.length); h > b; b++)
            if ((m = e ? g(a((d = t[b]))[0], d[1]) : g(t[b])) === s || m === f)
              return m;
        } else
          for (y = v.call(t); !(d = y.next()).done; )
            if ((m = o(y, g, d.value, e)) === s || m === f) return m;
      }).BREAK = s),
      (e.RETURN = f);
  },
  function (t, e, n) {
    var r = n(1).navigator;
    t.exports = (r && r.userAgent) || "";
  },
  function (t, e, n) {
    "use strict";
    var r = n(1),
      o = n(0),
      i = n(11),
      a = n(43),
      u = n(27),
      c = n(57),
      s = n(42),
      f = n(4),
      l = n(2),
      p = n(53),
      h = n(38),
      d = n(70);
    t.exports = function (t, e, n, y, m, v) {
      var g = r[t],
        b = g,
        w = m ? "set" : "add",
        _ = b && b.prototype,
        E = {},
        T = function (t) {
          var e = _[t];
          i(
            _,
            t,
            "delete" == t || "has" == t
              ? function (t) {
                  return !(v && !f(t)) && e.call(this, 0 === t ? 0 : t);
                }
              : "get" == t
              ? function (t) {
                  return v && !f(t) ? undefined : e.call(this, 0 === t ? 0 : t);
                }
              : "add" == t
              ? function (t) {
                  return e.call(this, 0 === t ? 0 : t), this;
                }
              : function (t, n) {
                  return e.call(this, 0 === t ? 0 : t, n), this;
                }
          );
        };
      if (
        "function" == typeof b &&
        (v ||
          (_.forEach &&
            !l(function () {
              new b().entries().next();
            })))
      ) {
        var S = new b(),
          x = S[w](v ? {} : -0, 1) != S,
          A = l(function () {
            S.has(1);
          }),
          R = p(function (t) {
            new b(t);
          }),
          I =
            !v &&
            l(function () {
              for (var t = new b(), e = 5; e--; ) t[w](e, e);
              return !t.has(-0);
            });
        R ||
          (((b = e(function (e, n) {
            s(e, b, t);
            var r = d(new g(), e, b);
            return n != undefined && c(n, m, r[w], r), r;
          })).prototype = _),
          (_.constructor = b)),
          (A || I) && (T("delete"), T("has"), m && T("get")),
          (I || x) && T(w),
          v && _.clear && delete _.clear;
      } else
        (b = y.getConstructor(e, t, m, w)), a(b.prototype, n), (u.NEED = !0);
      return (
        h(b, t),
        (E[t] = b),
        o(o.G + o.W + o.F * (b != g), E),
        v || y.setStrong(b, t, m),
        b
      );
    };
  },
  function (t, e, n) {
    for (
      var r,
        o = n(1),
        i = n(14),
        a = n(29),
        u = a("typed_array"),
        c = a("view"),
        s = !(!o.ArrayBuffer || !o.DataView),
        f = s,
        l = 0,
        p =
          "Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array".split(
            ","
          );
      l < 9;

    )
      (r = o[p[l++]])
        ? (i(r.prototype, u, !0), i(r.prototype, c, !0))
        : (f = !1);
    t.exports = { ABV: s, CONSTR: f, TYPED: u, VIEW: c };
  },
  function (t, e) {
    function n(t) {
      return (n =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    function r(t, e) {
      return !e || ("object" !== n(e) && "function" != typeof e)
        ? (function (t) {
            if (void 0 === t)
              throw new ReferenceError(
                "this hasn't been initialised - super() hasn't been called"
              );
            return t;
          })(t)
        : e;
    }
    function o(t) {
      var e = "function" == typeof Map ? new Map() : undefined;
      return (o = function (t) {
        if (
          null === t ||
          ((n = t), -1 === Function.toString.call(n).indexOf("[native code]"))
        )
          return t;
        var n;
        if ("function" != typeof t)
          throw new TypeError(
            "Super expression must either be null or a function"
          );
        if (void 0 !== e) {
          if (e.has(t)) return e.get(t);
          e.set(t, r);
        }
        function r() {
          return i(t, arguments, c(this).constructor);
        }
        return (
          (r.prototype = Object.create(t.prototype, {
            constructor: {
              value: r,
              enumerable: !1,
              writable: !0,
              configurable: !0,
            },
          })),
          u(r, t)
        );
      })(t);
    }
    function i(t, e, n) {
      return (i = a()
        ? Reflect.construct
        : function (t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var o = new (Function.bind.apply(t, r))();
            return n && u(o, n.prototype), o;
          }).apply(null, arguments);
    }
    function a() {
      if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
      if (Reflect.construct.sham) return !1;
      if ("function" == typeof Proxy) return !0;
      try {
        return (
          Boolean.prototype.valueOf.call(
            Reflect.construct(Boolean, [], function () {})
          ),
          !0
        );
      } catch (t) {
        return !1;
      }
    }
    function u(t, e) {
      return (u =
        Object.setPrototypeOf ||
        function (t, e) {
          return (t.__proto__ = e), t;
        })(t, e);
    }
    function c(t) {
      return (c = Object.setPrototypeOf
        ? Object.getPrototypeOf
        : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t);
          })(t);
    }
    function s(t, e) {
      var n = Object.keys(t);
      if (Object.getOwnPropertySymbols) {
        var r = Object.getOwnPropertySymbols(t);
        e &&
          (r = r.filter(function (e) {
            return Object.getOwnPropertyDescriptor(t, e).enumerable;
          })),
          n.push.apply(n, r);
      }
      return n;
    }
    function f(t) {
      for (var e = 1; e < arguments.length; e++) {
        var n = null != arguments[e] ? arguments[e] : {};
        e % 2
          ? s(Object(n), !0).forEach(function (e) {
              l(t, e, n[e]);
            })
          : Object.getOwnPropertyDescriptors
          ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
          : s(Object(n)).forEach(function (e) {
              Object.defineProperty(
                t,
                e,
                Object.getOwnPropertyDescriptor(n, e)
              );
            });
      }
      return t;
    }
    function l(t, e, n) {
      return (
        e in t
          ? Object.defineProperty(t, e, {
              value: n,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            })
          : (t[e] = n),
        t
      );
    }
    new Set([429, 503]);
    var p = function (t) {
      var e =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : {},
        n = f({}, e);
      n.headers = f({ "Content-Type": "text/plain;charset=UTF-8" }, n.headers);
      var r = window.fetch2(t, n);
      return {
        on: function (t, e) {
          switch (t) {
            case "data":
              r.then(e);
              break;
            case "error":
              r["catch"](e);
              break;
            case "end":
              r["finally"](e);
              break;
            default:
              console.warn(
                "react-native-ytdl: miniget: unknown event listener received: ".concat(
                  t
                )
              );
          }
        },
        setEncoding: function () {
          console.warn(
            "react-native-ytdl: miniget: will not use specified encoding since request has already been made. Currently using utf8 encoding."
          );
        },
        text: function () {
          return r;
        },
      };
    };
    (p.MinigetError = (function (t) {
      !(function (t, e) {
        if ("function" != typeof e && null !== e)
          throw new TypeError(
            "Super expression must either be null or a function"
          );
        (t.prototype = Object.create(e && e.prototype, {
          constructor: { value: t, writable: !0, configurable: !0 },
        })),
          e && u(t, e);
      })(i, t);
      var e,
        n,
        o =
          ((e = i),
          (n = a()),
          function () {
            var t,
              o = c(e);
            if (n) {
              var i = c(this).constructor;
              t = Reflect.construct(o, arguments, i);
            } else t = o.apply(this, arguments);
            return r(this, t);
          });
      function i(t) {
        return (
          (function (t, e) {
            if (!(t instanceof e))
              throw new TypeError("Cannot call a class as a function");
          })(this, i),
          o.call(this, t)
        );
      }
      return i;
    })(o(Error))),
      (p.defaultOptions = {
        maxRedirects: 10,
        maxRetries: 5,
        maxReconnects: 0,
        backoff: { inc: 100, max: 1e4 },
      }),
      (t.exports = p);
  },
  function (t, e, n) {
    function r(t, e) {
      return (
        (function (t) {
          if (Array.isArray(t)) return t;
        })(t) ||
        (function (t, e) {
          var n =
            t &&
            (("undefined" != typeof Symbol && t[Symbol.iterator]) ||
              t["@@iterator"]);
          if (null == n) return;
          var r,
            o,
            i = [],
            a = !0,
            u = !1;
          try {
            for (
              n = n.call(t);
              !(a = (r = n.next()).done) &&
              (i.push(r.value), !e || i.length !== e);
              a = !0
            );
          } catch (c) {
            (u = !0), (o = c);
          } finally {
            try {
              a || null == n["return"] || n["return"]();
            } finally {
              if (u) throw o;
            }
          }
          return i;
        })(t, e) ||
        (function (t, e) {
          if (!t) return;
          if ("string" == typeof t) return o(t, e);
          var n = Object.prototype.toString.call(t).slice(8, -1);
          "Object" === n && t.constructor && (n = t.constructor.name);
          if ("Map" === n || "Set" === n) return Array.from(t);
          if (
            "Arguments" === n ||
            /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
          )
            return o(t, e);
        })(t, e) ||
        (function () {
          throw new TypeError(
            "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        })()
      );
    }
    function o(t, e) {
      (null == e || e > t.length) && (e = t.length);
      for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
      return r;
    }
    n(61);
    (e.between = function (t, e, n) {
      var r;
      if (e instanceof RegExp) {
        var o = t.match(e);
        if (!o) return "";
        r = o.index + o[0].length;
      } else {
        if (-1 === (r = t.indexOf(e))) return "";
        r += e.length;
      }
      return -1 === (r = (t = t.slice(r)).indexOf(n))
        ? ""
        : (t = t.slice(0, r));
    }),
      (e.parseAbbreviatedNumber = function (t) {
        var e = t
          .replace(",", ".")
          .replace(" ", "")
          .match(/([\d,.]+)([MK]?)/);
        if (e) {
          var n = r(e, 3),
            o = n[1],
            i = n[2];
          return (
            (o = parseFloat(o)),
            Math.round("M" === i ? 1e6 * o : "K" === i ? 1e3 * o : o)
          );
        }
        return null;
      }),
      (e.cutAfterJSON = function (t) {
        var e, n;
        if (
          ("[" === t[0]
            ? ((e = "["), (n = "]"))
            : "{" === t[0] && ((e = "{"), (n = "}")),
          !e)
        )
          throw new Error(
            "Can't cut unsupported JSON (need to begin with [ or { ) but got: ".concat(
              t[0]
            )
          );
        var r,
          o = !1,
          i = !1,
          a = 0;
        for (r = 0; r < t.length; r++)
          if ('"' !== t[r] || i) {
            if (
              ((i = "\\" === t[r] && !i),
              !o && (t[r] === e ? a++ : t[r] === n && a--, 0 === a))
            )
              return t.substr(0, r + 1);
          } else o = !o;
        throw Error(
          "Can't cut unsupported JSON (no matching closing bracket found)"
        );
      }),
      (e.playError = function (t, e) {
        var n =
            arguments.length > 2 && arguments[2] !== undefined
              ? arguments[2]
              : Error,
          r = t && t.playabilityStatus;
        return r && e.includes(r.status)
          ? new n(r.reason || (r.messages && r.messages[0]))
          : null;
      }),
      (e.deprecate = function (t, e, n, r, o) {
        Object.defineProperty(t, e, {
          get: function () {
            return (
              console.warn(
                "`".concat(r, "` will be removed in a near future release, ") +
                  "use `".concat(o, "` instead.")
              ),
              n
            );
          },
        });
      }),
      (e.checkForUpdates = function () {
        return null;
      });
  },
  function (t, e, n) {
    var r = n(4),
      o = n(1).document,
      i = r(o) && r(o.createElement);
    t.exports = function (t) {
      return i ? o.createElement(t) : {};
    };
  },
  function (t, e, n) {
    e.f = n(5);
  },
  function (t, e, n) {
    var r = n(49)("keys"),
      o = n(29);
    t.exports = function (t) {
      return r[t] || (r[t] = o(t));
    };
  },
  function (t, e) {
    t.exports =
      "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(
        ","
      );
  },
  function (t, e, n) {
    var r = n(1).document;
    t.exports = r && r.documentElement;
  },
  function (t, e, n) {
    var r = n(4),
      o = n(3),
      i = function (t, e) {
        if ((o(t), !r(e) && null !== e))
          throw TypeError(e + ": can't set as prototype!");
      };
    t.exports = {
      set:
        Object.setPrototypeOf ||
        ("__proto__" in {}
          ? (function (t, e, r) {
              try {
                (r = n(17)(
                  Function.call,
                  n(20).f(Object.prototype, "__proto__").set,
                  2
                ))(t, []),
                  (e = !(t instanceof Array));
              } catch (o) {
                e = !0;
              }
              return function (t, n) {
                return i(t, n), e ? (t.__proto__ = n) : r(t, n), t;
              };
            })({}, !1)
          : undefined),
      check: i,
    };
  },
  function (t, e) {
    t.exports = "\t\n\x0B\f\r   ᠎             　\u2028\u2029\ufeff";
  },
  function (t, e, n) {
    var r = n(4),
      o = n(68).set;
    t.exports = function (t, e, n) {
      var i,
        a = e.constructor;
      return (
        a !== n &&
          "function" == typeof a &&
          (i = a.prototype) !== n.prototype &&
          r(i) &&
          o &&
          o(t, i),
        t
      );
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(19),
      o = n(24);
    t.exports = function (t) {
      var e = String(o(this)),
        n = "",
        i = r(t);
      if (i < 0 || i == Infinity) throw RangeError("Count can't be negative");
      for (; i > 0; (i >>>= 1) && (e += e)) 1 & i && (n += e);
      return n;
    };
  },
  function (t, e) {
    t.exports =
      Math.sign ||
      function (t) {
        return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
      };
  },
  function (t, e) {
    var n = Math.expm1;
    t.exports =
      !n ||
      n(10) > 22025.465794806718 ||
      n(10) < 22025.465794806718 ||
      -2e-17 != n(-2e-17)
        ? function (t) {
            return 0 == (t = +t)
              ? t
              : t > -1e-6 && t < 1e-6
              ? t + (t * t) / 2
              : Math.exp(t) - 1;
          }
        : n;
  },
  function (t, e, n) {
    var r = n(19),
      o = n(24);
    t.exports = function (t) {
      return function (e, n) {
        var i,
          a,
          u = String(o(e)),
          c = r(n),
          s = u.length;
        return c < 0 || c >= s
          ? t
            ? ""
            : undefined
          : (i = u.charCodeAt(c)) < 55296 ||
            i > 56319 ||
            c + 1 === s ||
            (a = u.charCodeAt(c + 1)) < 56320 ||
            a > 57343
          ? t
            ? u.charAt(c)
            : i
          : t
          ? u.slice(c, c + 2)
          : a - 56320 + ((i - 55296) << 10) + 65536;
      };
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(30),
      o = n(0),
      i = n(11),
      a = n(14),
      u = n(40),
      c = n(107),
      s = n(38),
      f = n(35),
      l = n(5)("iterator"),
      p = !([].keys && "next" in [].keys()),
      h = "keys",
      d = "values",
      y = function () {
        return this;
      };
    t.exports = function (t, e, n, m, v, g, b) {
      c(n, e, m);
      var w,
        _,
        E,
        T = function (t) {
          if (!p && t in R) return R[t];
          switch (t) {
            case h:
            case d:
              return function () {
                return new n(this, t);
              };
          }
          return function () {
            return new n(this, t);
          };
        },
        S = e + " Iterator",
        x = v == d,
        A = !1,
        R = t.prototype,
        I = R[l] || R["@@iterator"] || (v && R[v]),
        O = I || T(v),
        P = v ? (x ? T("entries") : O) : undefined,
        L = ("Array" == e && R.entries) || I;
      if (
        (L &&
          (E = f(L.call(new t()))) !== Object.prototype &&
          E.next &&
          (s(E, S, !0), r || "function" == typeof E[l] || a(E, l, y)),
        x &&
          I &&
          I.name !== d &&
          ((A = !0),
          (O = function () {
            return I.call(this);
          })),
        (r && !b) || (!p && !A && R[l]) || a(R, l, O),
        (u[e] = O),
        (u[S] = y),
        v)
      )
        if (((w = { values: x ? O : T(d), keys: g ? O : T(h), entries: P }), b))
          for (_ in w) _ in R || i(R, _, w[_]);
        else o(o.P + o.F * (p || A), e, w);
      return w;
    };
  },
  function (t, e, n) {
    var r = n(77),
      o = n(24);
    t.exports = function (t, e, n) {
      if (r(e)) throw TypeError("String#" + n + " doesn't accept regex!");
      return String(o(t));
    };
  },
  function (t, e, n) {
    var r = n(4),
      o = n(23),
      i = n(5)("match");
    t.exports = function (t) {
      var e;
      return r(t) && ((e = t[i]) !== undefined ? !!e : "RegExp" == o(t));
    };
  },
  function (t, e, n) {
    var r = n(5)("match");
    t.exports = function (t) {
      var e = /./;
      try {
        "/./"[t](e);
      } catch (n) {
        try {
          return (e[r] = !1), !"/./"[t](e);
        } catch (o) {}
      }
      return !0;
    };
  },
  function (t, e, n) {
    var r = n(40),
      o = n(5)("iterator"),
      i = Array.prototype;
    t.exports = function (t) {
      return t !== undefined && (r.Array === t || i[o] === t);
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(9),
      o = n(28);
    t.exports = function (t, e, n) {
      e in t ? r.f(t, e, o(0, n)) : (t[e] = n);
    };
  },
  function (t, e, n) {
    var r = n(46),
      o = n(5)("iterator"),
      i = n(40);
    t.exports = n(7).getIteratorMethod = function (t) {
      if (t != undefined) return t[o] || t["@@iterator"] || i[r(t)];
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(10),
      o = n(32),
      i = n(6);
    t.exports = function (t) {
      for (
        var e = r(this),
          n = i(e.length),
          a = arguments.length,
          u = o(a > 1 ? arguments[1] : undefined, n),
          c = a > 2 ? arguments[2] : undefined,
          s = c === undefined ? n : o(c, n);
        s > u;

      )
        e[u++] = t;
      return e;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(36),
      o = n(112),
      i = n(40),
      a = n(15);
    (t.exports = n(75)(
      Array,
      "Array",
      function (t, e) {
        (this._t = a(t)), (this._i = 0), (this._k = e);
      },
      function () {
        var t = this._t,
          e = this._k,
          n = this._i++;
        return !t || n >= t.length
          ? ((this._t = undefined), o(1))
          : o(0, "keys" == e ? n : "values" == e ? t[n] : [n, t[n]]);
      },
      "values"
    )),
      (i.Arguments = i.Array),
      r("keys"),
      r("values"),
      r("entries");
  },
  function (t, e, n) {
    "use strict";
    var r,
      o,
      i = n(54),
      a = RegExp.prototype.exec,
      u = String.prototype.replace,
      c = a,
      s =
        ((r = /a/),
        (o = /b*/g),
        a.call(r, "a"),
        a.call(o, "a"),
        0 !== r.lastIndex || 0 !== o.lastIndex),
      f = /()??/.exec("")[1] !== undefined;
    (s || f) &&
      (c = function (t) {
        var e,
          n,
          r,
          o,
          c = this;
        return (
          f && (n = new RegExp("^" + c.source + "$(?!\\s)", i.call(c))),
          s && (e = c.lastIndex),
          (r = a.call(c, t)),
          s && r && (c.lastIndex = c.global ? r.index + r[0].length : e),
          f &&
            r &&
            r.length > 1 &&
            u.call(r[0], n, function () {
              for (o = 1; o < arguments.length - 2; o++)
                arguments[o] === undefined && (r[o] = undefined);
            }),
          r
        );
      }),
      (t.exports = c);
  },
  function (t, e, n) {
    "use strict";
    var r = n(74)(!0);
    t.exports = function (t, e, n) {
      return e + (n ? r(t, e).length : 1);
    };
  },
  function (t, e, n) {
    var r,
      o,
      i,
      a = n(17),
      u = n(101),
      c = n(67),
      s = n(63),
      f = n(1),
      l = f.process,
      p = f.setImmediate,
      h = f.clearImmediate,
      d = f.MessageChannel,
      y = f.Dispatch,
      m = 0,
      v = {},
      g = "onreadystatechange",
      b = function () {
        var t = +this;
        if (v.hasOwnProperty(t)) {
          var e = v[t];
          delete v[t], e();
        }
      },
      w = function (t) {
        b.call(t.data);
      };
    (p && h) ||
      ((p = function (t) {
        for (var e = [], n = 1; arguments.length > n; ) e.push(arguments[n++]);
        return (
          (v[++m] = function () {
            u("function" == typeof t ? t : Function(t), e);
          }),
          r(m),
          m
        );
      }),
      (h = function (t) {
        delete v[t];
      }),
      "process" == n(23)(l)
        ? (r = function (t) {
            l.nextTick(a(b, t, 1));
          })
        : y && y.now
        ? (r = function (t) {
            y.now(a(b, t, 1));
          })
        : d
        ? ((i = (o = new d()).port2),
          (o.port1.onmessage = w),
          (r = a(i.postMessage, i, 1)))
        : f.addEventListener &&
          "function" == typeof postMessage &&
          !f.importScripts
        ? ((r = function (t) {
            f.postMessage(t + "", "*");
          }),
          f.addEventListener("message", w, !1))
        : (r =
            g in s("script")
              ? function (t) {
                  c.appendChild(s("script")).onreadystatechange = function () {
                    c.removeChild(this), b.call(t);
                  };
                }
              : function (t) {
                  setTimeout(a(b, t, 1), 0);
                })),
      (t.exports = { set: p, clear: h });
  },
  function (t, e, n) {
    "use strict";
    var r = n(1),
      o = n(8),
      i = n(30),
      a = n(60),
      u = n(14),
      c = n(43),
      s = n(2),
      f = n(42),
      l = n(19),
      p = n(6),
      h = n(120),
      d = n(34).f,
      y = n(9).f,
      m = n(82),
      v = n(38),
      g = "ArrayBuffer",
      b = "DataView",
      w = "Wrong index!",
      _ = r.ArrayBuffer,
      E = r.DataView,
      T = r.Math,
      S = r.RangeError,
      x = r.Infinity,
      A = _,
      R = T.abs,
      I = T.pow,
      O = T.floor,
      P = T.log,
      L = T.LN2,
      N = "buffer",
      D = "byteLength",
      F = "byteOffset",
      M = o ? "_b" : N,
      C = o ? "_l" : D,
      B = o ? "_o" : F;
    function j(t, e, n) {
      var r,
        o,
        i,
        a = new Array(n),
        u = 8 * n - e - 1,
        c = (1 << u) - 1,
        s = c >> 1,
        f = 23 === e ? I(2, -24) - I(2, -77) : 0,
        l = 0,
        p = t < 0 || (0 === t && 1 / t < 0) ? 1 : 0;
      for (
        (t = R(t)) != t || t === x
          ? ((o = t != t ? 1 : 0), (r = c))
          : ((r = O(P(t) / L)),
            t * (i = I(2, -r)) < 1 && (r--, (i *= 2)),
            (t += r + s >= 1 ? f / i : f * I(2, 1 - s)) * i >= 2 &&
              (r++, (i /= 2)),
            r + s >= c
              ? ((o = 0), (r = c))
              : r + s >= 1
              ? ((o = (t * i - 1) * I(2, e)), (r += s))
              : ((o = t * I(2, s - 1) * I(2, e)), (r = 0)));
        e >= 8;
        a[l++] = 255 & o, o /= 256, e -= 8
      );
      for (r = (r << e) | o, u += e; u > 0; a[l++] = 255 & r, r /= 256, u -= 8);
      return (a[--l] |= 128 * p), a;
    }
    function U(t, e, n) {
      var r,
        o = 8 * n - e - 1,
        i = (1 << o) - 1,
        a = i >> 1,
        u = o - 7,
        c = n - 1,
        s = t[c--],
        f = 127 & s;
      for (s >>= 7; u > 0; f = 256 * f + t[c], c--, u -= 8);
      for (
        r = f & ((1 << -u) - 1), f >>= -u, u += e;
        u > 0;
        r = 256 * r + t[c], c--, u -= 8
      );
      if (0 === f) f = 1 - a;
      else {
        if (f === i) return r ? NaN : s ? -x : x;
        (r += I(2, e)), (f -= a);
      }
      return (s ? -1 : 1) * r * I(2, f - e);
    }
    function k(t) {
      return (t[3] << 24) | (t[2] << 16) | (t[1] << 8) | t[0];
    }
    function V(t) {
      return [255 & t];
    }
    function q(t) {
      return [255 & t, (t >> 8) & 255];
    }
    function Y(t) {
      return [255 & t, (t >> 8) & 255, (t >> 16) & 255, (t >> 24) & 255];
    }
    function G(t) {
      return j(t, 52, 8);
    }
    function H(t) {
      return j(t, 23, 4);
    }
    function W(t, e, n) {
      y(t.prototype, e, {
        get: function () {
          return this[n];
        },
      });
    }
    function $(t, e, n, r) {
      var o = h(+n);
      if (o + e > t[C]) throw S(w);
      var i = t[M]._b,
        a = o + t[B],
        u = i.slice(a, a + e);
      return r ? u : u.reverse();
    }
    function z(t, e, n, r, o, i) {
      var a = h(+n);
      if (a + e > t[C]) throw S(w);
      for (var u = t[M]._b, c = a + t[B], s = r(+o), f = 0; f < e; f++)
        u[c + f] = s[i ? f : e - f - 1];
    }
    if (a.ABV) {
      if (
        !s(function () {
          _(1);
        }) ||
        !s(function () {
          new _(-1);
        }) ||
        s(function () {
          return new _(), new _(1.5), new _(NaN), _.name != g;
        })
      ) {
        for (
          var X,
            K = ((_ = function (t) {
              return f(this, _), new A(h(t));
            }).prototype = A.prototype),
            Q = d(A),
            J = 0;
          Q.length > J;

        )
          (X = Q[J++]) in _ || u(_, X, A[X]);
        i || (K.constructor = _);
      }
      var Z = new E(new _(2)),
        tt = E.prototype.setInt8;
      Z.setInt8(0, 2147483648),
        Z.setInt8(1, 2147483649),
        (!Z.getInt8(0) && Z.getInt8(1)) ||
          c(
            E.prototype,
            {
              setInt8: function (t, e) {
                tt.call(this, t, (e << 24) >> 24);
              },
              setUint8: function (t, e) {
                tt.call(this, t, (e << 24) >> 24);
              },
            },
            !0
          );
    } else
      (_ = function (t) {
        f(this, _, g);
        var e = h(t);
        (this._b = m.call(new Array(e), 0)), (this[C] = e);
      }),
        (E = function (t, e, n) {
          f(this, E, b), f(t, _, b);
          var r = t[C],
            o = l(e);
          if (o < 0 || o > r) throw S("Wrong offset!");
          if (o + (n = n === undefined ? r - o : p(n)) > r)
            throw S("Wrong length!");
          (this[M] = t), (this[B] = o), (this[C] = n);
        }),
        o && (W(_, D, "_l"), W(E, N, "_b"), W(E, D, "_l"), W(E, F, "_o")),
        c(E.prototype, {
          getInt8: function (t) {
            return ($(this, 1, t)[0] << 24) >> 24;
          },
          getUint8: function (t) {
            return $(this, 1, t)[0];
          },
          getInt16: function (t) {
            var e = $(this, 2, t, arguments[1]);
            return (((e[1] << 8) | e[0]) << 16) >> 16;
          },
          getUint16: function (t) {
            var e = $(this, 2, t, arguments[1]);
            return (e[1] << 8) | e[0];
          },
          getInt32: function (t) {
            return k($(this, 4, t, arguments[1]));
          },
          getUint32: function (t) {
            return k($(this, 4, t, arguments[1])) >>> 0;
          },
          getFloat32: function (t) {
            return U($(this, 4, t, arguments[1]), 23, 4);
          },
          getFloat64: function (t) {
            return U($(this, 8, t, arguments[1]), 52, 8);
          },
          setInt8: function (t, e) {
            z(this, 1, t, V, e);
          },
          setUint8: function (t, e) {
            z(this, 1, t, V, e);
          },
          setInt16: function (t, e) {
            z(this, 2, t, q, e, arguments[2]);
          },
          setUint16: function (t, e) {
            z(this, 2, t, q, e, arguments[2]);
          },
          setInt32: function (t, e) {
            z(this, 4, t, Y, e, arguments[2]);
          },
          setUint32: function (t, e) {
            z(this, 4, t, Y, e, arguments[2]);
          },
          setFloat32: function (t, e) {
            z(this, 4, t, H, e, arguments[2]);
          },
          setFloat64: function (t, e) {
            z(this, 8, t, G, e, arguments[2]);
          },
        });
    v(_, g),
      v(E, b),
      u(E.prototype, a.VIEW, !0),
      (e.ArrayBuffer = _),
      (e.DataView = E);
  },
  function (t, e) {
    var n = (t.exports =
      "undefined" != typeof window && window.Math == Math
        ? window
        : "undefined" != typeof self && self.Math == Math
        ? self
        : Function("return this")());
    "number" == typeof __g && (__g = n);
  },
  function (t, e) {
    function n(t) {
      return (n =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    t.exports = function (t) {
      return "object" === n(t) ? null !== t : "function" == typeof t;
    };
  },
  function (t, e, n) {
    t.exports = !n(125)(function () {
      return (
        7 !=
        Object.defineProperty({}, "a", {
          get: function () {
            return 7;
          },
        }).a
      );
    });
  },
  function (t, e, n) {
    "use strict";
    (e.decode = e.parse = n(325)), (e.encode = e.stringify = n(326));
  },
  function (t, e) {
    e.parseTimestamp = function (t) {
      var e = { ms: 1, s: 1e3, m: 6e4, h: 36e5 };
      if ("number" == typeof t) return t;
      if (/^\d+$/.test(t)) return +t;
      var n = /^(?:(?:(\d+):)?(\d{1,2}):)?(\d{1,2})(?:\.(\d{3}))?$/.exec(t);
      if (n)
        return (
          +(n[1] || 0) * e.h + +(n[2] || 0) * e.m + +n[3] * e.s + +(n[4] || 0)
        );
      for (var r, o = 0, i = /(-?\d+)(ms|s|m|h)/g; null != (r = i.exec(t)); )
        o += +r[1] * e[r[2]];
      return o;
    };
  },
  function (t, e, n) {
    t.exports =
      !n(8) &&
      !n(2)(function () {
        return (
          7 !=
          Object.defineProperty(n(63)("div"), "a", {
            get: function () {
              return 7;
            },
          }).a
        );
      });
  },
  function (t, e, n) {
    var r = n(1),
      o = n(7),
      i = n(30),
      a = n(64),
      u = n(9).f;
    t.exports = function (t) {
      var e = o.Symbol || (o.Symbol = i ? {} : r.Symbol || {});
      "_" == t.charAt(0) || t in e || u(e, t, { value: a.f(t) });
    };
  },
  function (t, e, n) {
    var r = n(13),
      o = n(15),
      i = n(50)(!1),
      a = n(65)("IE_PROTO");
    t.exports = function (t, e) {
      var n,
        u = o(t),
        c = 0,
        s = [];
      for (n in u) n != a && r(u, n) && s.push(n);
      for (; e.length > c; ) r(u, (n = e[c++])) && (~i(s, n) || s.push(n));
      return s;
    };
  },
  function (t, e, n) {
    var r = n(9),
      o = n(3),
      i = n(31);
    t.exports = n(8)
      ? Object.defineProperties
      : function (t, e) {
          o(t);
          for (var n, a = i(e), u = a.length, c = 0; u > c; )
            r.f(t, (n = a[c++]), e[n]);
          return t;
        };
  },
  function (t, e, n) {
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    var o = n(15),
      i = n(34).f,
      a = {}.toString,
      u =
        "object" == ("undefined" == typeof window ? "undefined" : r(window)) &&
        window &&
        Object.getOwnPropertyNames
          ? Object.getOwnPropertyNames(window)
          : [];
    t.exports.f = function (t) {
      return u && "[object Window]" == a.call(t)
        ? (function (t) {
            try {
              return i(t);
            } catch (e) {
              return u.slice();
            }
          })(t)
        : i(o(t));
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(8),
      o = n(31),
      i = n(51),
      a = n(45),
      u = n(10),
      c = n(44),
      s = Object.assign;
    t.exports =
      !s ||
      n(2)(function () {
        var t = {},
          e = {},
          n = Symbol(),
          r = "abcdefghijklmnopqrst";
        return (
          (t[n] = 7),
          r.split("").forEach(function (t) {
            e[t] = t;
          }),
          7 != s({}, t)[n] || Object.keys(s({}, e)).join("") != r
        );
      })
        ? function (t, e) {
            for (
              var n = u(t), s = arguments.length, f = 1, l = i.f, p = a.f;
              s > f;

            )
              for (
                var h,
                  d = c(arguments[f++]),
                  y = l ? o(d).concat(l(d)) : o(d),
                  m = y.length,
                  v = 0;
                m > v;

              )
                (h = y[v++]), (r && !p.call(d, h)) || (n[h] = d[h]);
            return n;
          }
        : s;
  },
  function (t, e) {
    t.exports =
      Object.is ||
      function (t, e) {
        return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e;
      };
  },
  function (t, e, n) {
    "use strict";
    var r = n(18),
      o = n(4),
      i = n(101),
      a = [].slice,
      u = {},
      c = function (t, e, n) {
        if (!(e in u)) {
          for (var r = [], o = 0; o < e; o++) r[o] = "a[" + o + "]";
          u[e] = Function("F,a", "return new F(" + r.join(",") + ")");
        }
        return u[e](t, n);
      };
    t.exports =
      Function.bind ||
      function (t) {
        var e = r(this),
          n = a.call(arguments, 1),
          u = function s() {
            var r = n.concat(a.call(arguments));
            return this instanceof s ? c(e, r.length, r) : i(e, r, t);
          };
        return o(e.prototype) && (u.prototype = e.prototype), u;
      };
  },
  function (t, e) {
    t.exports = function (t, e, n) {
      var r = n === undefined;
      switch (e.length) {
        case 0:
          return r ? t() : t.call(n);
        case 1:
          return r ? t(e[0]) : t.call(n, e[0]);
        case 2:
          return r ? t(e[0], e[1]) : t.call(n, e[0], e[1]);
        case 3:
          return r ? t(e[0], e[1], e[2]) : t.call(n, e[0], e[1], e[2]);
        case 4:
          return r
            ? t(e[0], e[1], e[2], e[3])
            : t.call(n, e[0], e[1], e[2], e[3]);
      }
      return t.apply(n, e);
    };
  },
  function (t, e, n) {
    var r = n(1).parseInt,
      o = n(39).trim,
      i = n(69),
      a = /^[-+]?0[xX]/;
    t.exports =
      8 !== r(i + "08") || 22 !== r(i + "0x16")
        ? function (t, e) {
            var n = o(String(t), 3);
            return r(n, e >>> 0 || (a.test(n) ? 16 : 10));
          }
        : r;
  },
  function (t, e, n) {
    var r = n(1).parseFloat,
      o = n(39).trim;
    t.exports =
      1 / r(n(69) + "-0") != -Infinity
        ? function (t) {
            var e = o(String(t), 3),
              n = r(e);
            return 0 === n && "-" == e.charAt(0) ? -0 : n;
          }
        : r;
  },
  function (t, e, n) {
    var r = n(23);
    t.exports = function (t, e) {
      if ("number" != typeof t && "Number" != r(t)) throw TypeError(e);
      return +t;
    };
  },
  function (t, e, n) {
    var r = n(4),
      o = Math.floor;
    t.exports = function (t) {
      return !r(t) && isFinite(t) && o(t) === t;
    };
  },
  function (t, e) {
    t.exports =
      Math.log1p ||
      function (t) {
        return (t = +t) > -1e-8 && t < 1e-8 ? t - (t * t) / 2 : Math.log(1 + t);
      };
  },
  function (t, e, n) {
    "use strict";
    var r = n(33),
      o = n(28),
      i = n(38),
      a = {};
    n(14)(a, n(5)("iterator"), function () {
      return this;
    }),
      (t.exports = function (t, e, n) {
        (t.prototype = r(a, { next: o(1, n) })), i(t, e + " Iterator");
      });
  },
  function (t, e, n) {
    var r = n(3);
    t.exports = function (t, e, n, o) {
      try {
        return o ? e(r(n)[0], n[1]) : e(n);
      } catch (a) {
        var i = t["return"];
        throw (i !== undefined && r(i.call(t)), a);
      }
    };
  },
  function (t, e, n) {
    var r = n(227);
    t.exports = function (t, e) {
      return new (r(t))(e);
    };
  },
  function (t, e, n) {
    var r = n(18),
      o = n(10),
      i = n(44),
      a = n(6);
    t.exports = function (t, e, n, u, c) {
      r(e);
      var s = o(t),
        f = i(s),
        l = a(s.length),
        p = c ? l - 1 : 0,
        h = c ? -1 : 1;
      if (n < 2)
        for (;;) {
          if (p in f) {
            (u = f[p]), (p += h);
            break;
          }
          if (((p += h), c ? p < 0 : l <= p))
            throw TypeError("Reduce of empty array with no initial value");
        }
      for (; c ? p >= 0 : l > p; p += h) p in f && (u = e(u, f[p], p, s));
      return u;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(10),
      o = n(32),
      i = n(6);
    t.exports =
      [].copyWithin ||
      function (t, e) {
        var n = r(this),
          a = i(n.length),
          u = o(t, a),
          c = o(e, a),
          s = arguments.length > 2 ? arguments[2] : undefined,
          f = Math.min((s === undefined ? a : o(s, a)) - c, a - u),
          l = 1;
        for (
          c < u && u < c + f && ((l = -1), (c += f - 1), (u += f - 1));
          f-- > 0;

        )
          c in n ? (n[u] = n[c]) : delete n[u], (u += l), (c += l);
        return n;
      };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return { value: e, done: !!t };
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(84);
    n(0)({ target: "RegExp", proto: !0, forced: r !== /./.exec }, { exec: r });
  },
  function (t, e, n) {
    n(8) &&
      "g" != /./g.flags &&
      n(9).f(RegExp.prototype, "flags", { configurable: !0, get: n(54) });
  },
  function (t, e, n) {
    "use strict";
    var r,
      o,
      i,
      a,
      u = n(30),
      c = n(1),
      s = n(17),
      f = n(46),
      l = n(0),
      p = n(4),
      h = n(18),
      d = n(42),
      y = n(57),
      m = n(47),
      v = n(86).set,
      g = n(247)(),
      b = n(116),
      w = n(248),
      _ = n(58),
      E = n(117),
      T = "Promise",
      S = c.TypeError,
      x = c.process,
      A = x && x.versions,
      R = (A && A.v8) || "",
      I = c.Promise,
      O = "process" == f(x),
      P = function () {},
      L = (o = b.f),
      N = !!(function () {
        try {
          var t = I.resolve(1),
            e = ((t.constructor = {})[n(5)("species")] = function (t) {
              t(P, P);
            });
          return (
            (O || "function" == typeof PromiseRejectionEvent) &&
            t.then(P) instanceof e &&
            0 !== R.indexOf("6.6") &&
            -1 === _.indexOf("Chrome/66")
          );
        } catch (r) {}
      })(),
      D = function (t) {
        var e;
        return !(!p(t) || "function" != typeof (e = t.then)) && e;
      },
      F = function (t, e) {
        if (!t._n) {
          t._n = !0;
          var n = t._c;
          g(function () {
            for (
              var r = t._v,
                o = 1 == t._s,
                i = 0,
                a = function (e) {
                  var n,
                    i,
                    a,
                    u = o ? e.ok : e.fail,
                    c = e.resolve,
                    s = e.reject,
                    f = e.domain;
                  try {
                    u
                      ? (o || (2 == t._h && B(t), (t._h = 1)),
                        !0 === u
                          ? (n = r)
                          : (f && f.enter(),
                            (n = u(r)),
                            f && (f.exit(), (a = !0))),
                        n === e.promise
                          ? s(S("Promise-chain cycle"))
                          : (i = D(n))
                          ? i.call(n, c, s)
                          : c(n))
                      : s(r);
                  } catch (l) {
                    f && !a && f.exit(), s(l);
                  }
                };
              n.length > i;

            )
              a(n[i++]);
            (t._c = []), (t._n = !1), e && !t._h && M(t);
          });
        }
      },
      M = function (t) {
        v.call(c, function () {
          var e,
            n,
            r,
            o = t._v,
            i = C(t);
          if (
            (i &&
              ((e = w(function () {
                O
                  ? x.emit("unhandledRejection", o, t)
                  : (n = c.onunhandledrejection)
                  ? n({ promise: t, reason: o })
                  : (r = c.console) &&
                    r.error &&
                    r.error("Unhandled promise rejection", o);
              })),
              (t._h = O || C(t) ? 2 : 1)),
            (t._a = undefined),
            i && e.e)
          )
            throw e.v;
        });
      },
      C = function (t) {
        return 1 !== t._h && 0 === (t._a || t._c).length;
      },
      B = function (t) {
        v.call(c, function () {
          var e;
          O
            ? x.emit("rejectionHandled", t)
            : (e = c.onrejectionhandled) && e({ promise: t, reason: t._v });
        });
      },
      j = function (t) {
        var e = this;
        e._d ||
          ((e._d = !0),
          ((e = e._w || e)._v = t),
          (e._s = 2),
          e._a || (e._a = e._c.slice()),
          F(e, !0));
      },
      U = function k(t) {
        var e,
          n = this;
        if (!n._d) {
          (n._d = !0), (n = n._w || n);
          try {
            if (n === t) throw S("Promise can't be resolved itself");
            (e = D(t))
              ? g(function () {
                  var r = { _w: n, _d: !1 };
                  try {
                    e.call(t, s(k, r, 1), s(j, r, 1));
                  } catch (o) {
                    j.call(r, o);
                  }
                })
              : ((n._v = t), (n._s = 1), F(n, !1));
          } catch (r) {
            j.call({ _w: n, _d: !1 }, r);
          }
        }
      };
    N ||
      ((I = function (t) {
        d(this, I, T, "_h"), h(t), r.call(this);
        try {
          t(s(U, this, 1), s(j, this, 1));
        } catch (e) {
          j.call(this, e);
        }
      }),
      ((r = function (t) {
        (this._c = []),
          (this._a = undefined),
          (this._s = 0),
          (this._d = !1),
          (this._v = undefined),
          (this._h = 0),
          (this._n = !1);
      }).prototype = n(43)(I.prototype, {
        then: function (t, e) {
          var n = L(m(this, I));
          return (
            (n.ok = "function" != typeof t || t),
            (n.fail = "function" == typeof e && e),
            (n.domain = O ? x.domain : undefined),
            this._c.push(n),
            this._a && this._a.push(n),
            this._s && F(this, !1),
            n.promise
          );
        },
        catch: function (t) {
          return this.then(undefined, t);
        },
      })),
      (i = function () {
        var t = new r();
        (this.promise = t),
          (this.resolve = s(U, t, 1)),
          (this.reject = s(j, t, 1));
      }),
      (b.f = L =
        function (t) {
          return t === I || t === a ? new i(t) : o(t);
        })),
      l(l.G + l.W + l.F * !N, { Promise: I }),
      n(38)(I, T),
      n(41)(T),
      (a = n(7).Promise),
      l(l.S + l.F * !N, T, {
        reject: function (t) {
          var e = L(this);
          return (0, e.reject)(t), e.promise;
        },
      }),
      l(l.S + l.F * (u || !N), T, {
        resolve: function (t) {
          return E(u && this === a ? I : this, t);
        },
      }),
      l(
        l.S +
          l.F *
            !(
              N &&
              n(53)(function (t) {
                I.all(t)["catch"](P);
              })
            ),
        T,
        {
          all: function (t) {
            var e = this,
              n = L(e),
              r = n.resolve,
              o = n.reject,
              i = w(function () {
                var n = [],
                  i = 0,
                  a = 1;
                y(t, !1, function (t) {
                  var u = i++,
                    c = !1;
                  n.push(undefined),
                    a++,
                    e.resolve(t).then(function (t) {
                      c || ((c = !0), (n[u] = t), --a || r(n));
                    }, o);
                }),
                  --a || r(n);
              });
            return i.e && o(i.v), n.promise;
          },
          race: function (t) {
            var e = this,
              n = L(e),
              r = n.reject,
              o = w(function () {
                y(t, !1, function (t) {
                  e.resolve(t).then(n.resolve, r);
                });
              });
            return o.e && r(o.v), n.promise;
          },
        }
      );
  },
  function (t, e, n) {
    "use strict";
    var r = n(18);
    function o(t) {
      var e, n;
      (this.promise = new t(function (t, r) {
        if (e !== undefined || n !== undefined)
          throw TypeError("Bad Promise constructor");
        (e = t), (n = r);
      })),
        (this.resolve = r(e)),
        (this.reject = r(n));
    }
    t.exports.f = function (t) {
      return new o(t);
    };
  },
  function (t, e, n) {
    var r = n(3),
      o = n(4),
      i = n(116);
    t.exports = function (t, e) {
      if ((r(t), o(e) && e.constructor === t)) return e;
      var n = i.f(t);
      return (0, n.resolve)(e), n.promise;
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(9).f,
      o = n(33),
      i = n(43),
      a = n(17),
      u = n(42),
      c = n(57),
      s = n(75),
      f = n(112),
      l = n(41),
      p = n(8),
      h = n(27).fastKey,
      d = n(37),
      y = p ? "_s" : "size",
      m = function (t, e) {
        var n,
          r = h(e);
        if ("F" !== r) return t._i[r];
        for (n = t._f; n; n = n.n) if (n.k == e) return n;
      };
    t.exports = {
      getConstructor: function (t, e, n, s) {
        var f = t(function (t, r) {
          u(t, f, e, "_i"),
            (t._t = e),
            (t._i = o(null)),
            (t._f = undefined),
            (t._l = undefined),
            (t[y] = 0),
            r != undefined && c(r, n, t[s], t);
        });
        return (
          i(f.prototype, {
            clear: function () {
              for (var t = d(this, e), n = t._i, r = t._f; r; r = r.n)
                (r.r = !0), r.p && (r.p = r.p.n = undefined), delete n[r.i];
              (t._f = t._l = undefined), (t[y] = 0);
            },
            delete: function (t) {
              var n = d(this, e),
                r = m(n, t);
              if (r) {
                var o = r.n,
                  i = r.p;
                delete n._i[r.i],
                  (r.r = !0),
                  i && (i.n = o),
                  o && (o.p = i),
                  n._f == r && (n._f = o),
                  n._l == r && (n._l = i),
                  n[y]--;
              }
              return !!r;
            },
            forEach: function (t) {
              d(this, e);
              for (
                var n,
                  r = a(t, arguments.length > 1 ? arguments[1] : undefined, 3);
                (n = n ? n.n : this._f);

              )
                for (r(n.v, n.k, this); n && n.r; ) n = n.p;
            },
            has: function (t) {
              return !!m(d(this, e), t);
            },
          }),
          p &&
            r(f.prototype, "size", {
              get: function () {
                return d(this, e)[y];
              },
            }),
          f
        );
      },
      def: function (t, e, n) {
        var r,
          o,
          i = m(t, e);
        return (
          i
            ? (i.v = n)
            : ((t._l = i =
                {
                  i: (o = h(e, !0)),
                  k: e,
                  v: n,
                  p: (r = t._l),
                  n: undefined,
                  r: !1,
                }),
              t._f || (t._f = i),
              r && (r.n = i),
              t[y]++,
              "F" !== o && (t._i[o] = i)),
          t
        );
      },
      getEntry: m,
      setStrong: function (t, e, n) {
        s(
          t,
          e,
          function (t, n) {
            (this._t = d(t, e)), (this._k = n), (this._l = undefined);
          },
          function () {
            for (var t = this, e = t._k, n = t._l; n && n.r; ) n = n.p;
            return t._t && (t._l = n = n ? n.n : t._t._f)
              ? f(0, "keys" == e ? n.k : "values" == e ? n.v : [n.k, n.v])
              : ((t._t = undefined), f(1));
          },
          n ? "entries" : "values",
          !n,
          !0
        ),
          l(e);
      },
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(43),
      o = n(27).getWeak,
      i = n(3),
      a = n(4),
      u = n(42),
      c = n(57),
      s = n(22),
      f = n(13),
      l = n(37),
      p = s(5),
      h = s(6),
      d = 0,
      y = function (t) {
        return t._l || (t._l = new m());
      },
      m = function () {
        this.a = [];
      },
      v = function (t, e) {
        return p(t.a, function (t) {
          return t[0] === e;
        });
      };
    (m.prototype = {
      get: function (t) {
        var e = v(this, t);
        if (e) return e[1];
      },
      has: function (t) {
        return !!v(this, t);
      },
      set: function (t, e) {
        var n = v(this, t);
        n ? (n[1] = e) : this.a.push([t, e]);
      },
      delete: function (t) {
        var e = h(this.a, function (e) {
          return e[0] === t;
        });
        return ~e && this.a.splice(e, 1), !!~e;
      },
    }),
      (t.exports = {
        getConstructor: function (t, e, n, i) {
          var s = t(function (t, r) {
            u(t, s, e, "_i"),
              (t._t = e),
              (t._i = d++),
              (t._l = undefined),
              r != undefined && c(r, n, t[i], t);
          });
          return (
            r(s.prototype, {
              delete: function (t) {
                if (!a(t)) return !1;
                var n = o(t);
                return !0 === n
                  ? y(l(this, e))["delete"](t)
                  : n && f(n, this._i) && delete n[this._i];
              },
              has: function (t) {
                if (!a(t)) return !1;
                var n = o(t);
                return !0 === n ? y(l(this, e)).has(t) : n && f(n, this._i);
              },
            }),
            s
          );
        },
        def: function (t, e, n) {
          var r = o(i(e), !0);
          return !0 === r ? y(t).set(e, n) : (r[t._i] = n), t;
        },
        ufstore: y,
      });
  },
  function (t, e, n) {
    var r = n(19),
      o = n(6);
    t.exports = function (t) {
      if (t === undefined) return 0;
      var e = r(t),
        n = o(e);
      if (e !== n) throw RangeError("Wrong length!");
      return n;
    };
  },
  function (t, e, n) {
    var r = n(34),
      o = n(51),
      i = n(3),
      a = n(1).Reflect;
    t.exports =
      (a && a.ownKeys) ||
      function (t) {
        var e = r.f(i(t)),
          n = o.f;
        return n ? e.concat(n(t)) : e;
      };
  },
  function (t, e, n) {
    var r = n(6),
      o = n(71),
      i = n(24);
    t.exports = function (t, e, n, a) {
      var u = String(i(t)),
        c = u.length,
        s = n === undefined ? " " : String(n),
        f = r(e);
      if (f <= c || "" == s) return u;
      var l = f - c,
        p = o.call(s, Math.ceil(l / s.length));
      return p.length > l && (p = p.slice(0, l)), a ? p + u : u + p;
    };
  },
  function (t, e, n) {
    var r = n(8),
      o = n(31),
      i = n(15),
      a = n(45).f;
    t.exports = function (t) {
      return function (e) {
        for (var n, u = i(e), c = o(u), s = c.length, f = 0, l = []; s > f; )
          (n = c[f++]), (r && !a.call(u, n)) || l.push(t ? [n, u[n]] : u[n]);
        return l;
      };
    };
  },
  function (t, e) {
    var n = (t.exports = { version: "2.6.12" });
    "number" == typeof __e && (__e = n);
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return !!t();
      } catch (e) {
        return !0;
      }
    };
  },
  function (t, e, n) {
    (function (e) {
      var n = {
        setTimeout: function () {
          var t,
            n = (t = e).setTimeout.apply(t, arguments),
            r = {
              unref: function () {
                return n;
              },
            };
          return r;
        },
      };
      t.exports = n;
    }.call(this, n(48)));
  },
  function (t, e, n) {
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    function o(t, e) {
      var n =
        ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
      if (!n) {
        if (
          Array.isArray(t) ||
          (n = (function (t, e) {
            if (!t) return;
            if ("string" == typeof t) return i(t, e);
            var n = Object.prototype.toString.call(t).slice(8, -1);
            "Object" === n && t.constructor && (n = t.constructor.name);
            if ("Map" === n || "Set" === n) return Array.from(t);
            if (
              "Arguments" === n ||
              /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
            )
              return i(t, e);
          })(t)) ||
          (e && t && "number" == typeof t.length)
        ) {
          n && (t = n);
          var r = 0,
            o = function () {};
          return {
            s: o,
            n: function () {
              return r >= t.length ? { done: !0 } : { done: !1, value: t[r++] };
            },
            e: function (t) {
              throw t;
            },
            f: o,
          };
        }
        throw new TypeError(
          "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      }
      var a,
        u = !0,
        c = !1;
      return {
        s: function () {
          n = n.call(t);
        },
        n: function () {
          var t = n.next();
          return (u = t.done), t;
        },
        e: function (t) {
          (c = !0), (a = t);
        },
        f: function () {
          try {
            u || null == n["return"] || n["return"]();
          } finally {
            if (c) throw a;
          }
        },
      };
    }
    function i(t, e) {
      (null == e || e > t.length) && (e = t.length);
      for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
      return r;
    }
    var a = n(62),
      u = n(333),
      c = ["mp4a", "mp3", "vorbis", "aac", "opus", "flac"],
      s = [
        "mp4v",
        "avc1",
        "Sorenson H.283",
        "MPEG-4 Visual",
        "VP8",
        "VP9",
        "H.264",
      ],
      f = function (t) {
        return t.bitrate || 0;
      },
      l = function (t) {
        return s.findIndex(function (e) {
          return t.codecs && t.codecs.includes(e);
        });
      },
      p = function (t) {
        return t.audioBitrate || 0;
      },
      h = function (t) {
        return c.findIndex(function (e) {
          return t.codecs && t.codecs.includes(e);
        });
      },
      d = function (t, e, n) {
        var r,
          i = 0,
          a = o(n);
        try {
          for (a.s(); !(r = a.n()).done; ) {
            var u = r.value;
            if (0 !== (i = u(e) - u(t))) break;
          }
        } catch (c) {
          a.e(c);
        } finally {
          a.f();
        }
        return i;
      },
      y = function (t, e) {
        return d(t, e, [
          function (t) {
            return parseInt(t.qualityLabel);
          },
          f,
          l,
        ]);
      },
      m = function (t, e) {
        return d(t, e, [p, h]);
      };
    (e.sortFormats = function (t, e) {
      return d(t, e, [
        function (t) {
          return +!!t.isHLS;
        },
        function (t) {
          return +!!t.isDashMPD;
        },
        function (t) {
          return +(t.contentLength > 0);
        },
        function (t) {
          return +(t.hasVideo && t.hasAudio);
        },
        function (t) {
          return +t.hasVideo;
        },
        function (t) {
          return parseInt(t.qualityLabel) || 0;
        },
        f,
        p,
        l,
        h,
      ]);
    }),
      (e.chooseFormat = function (t, n) {
        if ("object" === r(n.format)) {
          if (!n.format.url)
            throw Error("Invalid format given, did you use `ytdl.getInfo()`?");
          return n.format;
        }
        var o;
        n.filter && (t = e.filterFormats(t, n.filter));
        var i = n.quality || "highest";
        switch (i) {
          case "highest":
            o = t[0];
            break;
          case "lowest":
            o = t[t.length - 1];
            break;
          case "highestaudio":
            (t = e.filterFormats(t, "audio")).sort(m), (o = t[0]);
            break;
          case "lowestaudio":
            (t = e.filterFormats(t, "audio")).sort(m), (o = t[t.length - 1]);
            break;
          case "highestvideo":
            (t = e.filterFormats(t, "video")).sort(y), (o = t[0]);
            break;
          case "lowestvideo":
            (t = e.filterFormats(t, "video")).sort(y), (o = t[t.length - 1]);
            break;
          default:
            o = v(i, t);
        }
        if (!o) throw Error("No such format found: ".concat(i));
        return o;
      });
    var v = function (t, e) {
      var n = function (t) {
        return e.find(function (e) {
          return "".concat(e.itag) === "".concat(t);
        });
      };
      return Array.isArray(t)
        ? n(
            t.find(function (t) {
              return n(t);
            })
          )
        : n(t);
    };
    (e.filterFormats = function (t, e) {
      var n;
      switch (e) {
        case "videoandaudio":
        case "audioandvideo":
          n = function (t) {
            return t.hasVideo && t.hasAudio;
          };
          break;
        case "video":
          n = function (t) {
            return t.hasVideo;
          };
          break;
        case "videoonly":
          n = function (t) {
            return t.hasVideo && !t.hasAudio;
          };
          break;
        case "audio":
          n = function (t) {
            return t.hasAudio;
          };
          break;
        case "audioonly":
          n = function (t) {
            return !t.hasVideo && t.hasAudio;
          };
          break;
        default:
          if ("function" != typeof e)
            throw TypeError("Given filter (".concat(e, ") is not supported"));
          n = e;
      }
      return t.filter(function (t) {
        return !!t.url && n(t);
      });
    }),
      (e.addFormatMeta = function (t) {
        return (
          ((t = Object.assign({}, u[t.itag], t)).hasVideo = !!t.qualityLabel),
          (t.hasAudio = !!t.audioBitrate),
          (t.container = t.mimeType
            ? t.mimeType.split(";")[0].split("/")[1]
            : null),
          (t.codecs = t.mimeType
            ? a.between(t.mimeType, 'codecs="', '"')
            : null),
          (t.videoCodec =
            t.hasVideo && t.codecs ? t.codecs.split(", ")[0] : null),
          (t.audioCodec =
            t.hasAudio && t.codecs ? t.codecs.split(", ").slice(-1)[0] : null),
          (t.isLive = /\bsource[/=]yt_live_broadcast\b/.test(t.url)),
          (t.isHLS = /\/manifest\/hls_(variant|playlist)\//.test(t.url)),
          (t.isDashMPD = /\/manifest\/dash\//.test(t.url)),
          t
        );
      });
  },
  function (t, e) {
    var n = new Set([
        "youtube.com",
        "www.youtube.com",
        "m.youtube.com",
        "music.youtube.com",
        "gaming.youtube.com",
      ]),
      r = /^https?:\/\/(youtu\.be\/|(www\.)?youtube.com\/(embed|v|shorts)\/)/;
    e.getURLVideoID = function (t) {
      var o = new URL(t),
        a = o.searchParams.get("v");
      if (r.test(t) && !a) {
        var u = o.pathname.split("/");
        a = u[u.length - 1];
      } else if (o.hostname && !n.has(o.hostname))
        throw Error("Not a YouTube domain");
      if (!a) throw Error("No video id found: ".concat(t));
      if (((a = a.substring(0, 11)), !e.validateID(a)))
        throw TypeError(
          "Video id (".concat(a, ") does not match expected ") +
            "format (".concat(i.toString(), ")")
        );
      return a;
    };
    var o = /^https?:\/\//;
    e.getVideoID = function (t) {
      if (e.validateID(t)) return t;
      if (o.test(t)) return e.getURLVideoID(t);
      throw Error("No video id found: ".concat(t));
    };
    var i = /^[a-zA-Z0-9-_]{11}$/;
    (e.validateID = function (t) {
      return i.test(t);
    }),
      (e.validateURL = function (t) {
        try {
          return e.getURLVideoID(t), !0;
        } catch (n) {
          return !1;
        }
      });
  },
  function (t, e, n) {
    function r(t, e, n, r, o, i, a) {
      try {
        var u = t[i](a),
          c = u.value;
      } catch (s) {
        return void n(s);
      }
      u.done ? e(c) : Promise.resolve(c).then(r, o);
    }
    function o(t) {
      return function () {
        var e = this,
          n = arguments;
        return new Promise(function (o, i) {
          var a = t.apply(e, n);
          function u(t) {
            r(a, o, i, u, c, "next", t);
          }
          function c(t) {
            r(a, o, i, u, c, "throw", t);
          }
          u(undefined);
        });
      };
    }
    var i = n(61),
      a = n(91),
      u = n(130);
    (e.cache = new u()),
      (e.getTokens = function (t, n) {
        return e.cache.getOrSet(
          t,
          o(
            regeneratorRuntime.mark(function r() {
              var o, a;
              return regeneratorRuntime.wrap(function (r) {
                for (;;)
                  switch ((r.prev = r.next)) {
                    case 0:
                      return (r.next = 2), i(t, n.requestOptions).text();
                    case 2:
                      if (
                        ((o = r.sent), (a = e.extractActions(o)) && a.length)
                      ) {
                        r.next = 6;
                        break;
                      }
                      throw Error(
                        "Could not extract signature deciphering actions"
                      );
                    case 6:
                      return e.cache.set(t, a), r.abrupt("return", a);
                    case 8:
                    case "end":
                      return r.stop();
                  }
              }, r);
            })
          )
        );
      }),
      (e.decipher = function (t, e) {
        e = e.split("");
        for (var n = 0, r = t.length; n < r; n++) {
          var o = t[n],
            i = void 0;
          switch (o[0]) {
            case "r":
              e = e.reverse();
              break;
            case "w":
              (i = ~~o.slice(1)), (e = c(e, i));
              break;
            case "s":
              (i = ~~o.slice(1)), (e = e.slice(i));
              break;
            case "p":
              (i = ~~o.slice(1)), e.splice(0, i);
          }
        }
        return e.join("");
      });
    var c = function (t, e) {
        var n = t[0];
        return (t[0] = t[e % t.length]), (t[e] = n), t;
      },
      s = "[a-zA-Z_\\$][a-zA-Z_0-9]*",
      f = "(?:"
        .concat("'[^'\\\\]*(:?\\\\[\\s\\S][^'\\\\]*)*'", "|")
        .concat('"[^"\\\\]*(:?\\\\[\\s\\S][^"\\\\]*)*"', ")"),
      l = "(?:".concat(s, "|").concat(f, ")"),
      p = "(?:\\.".concat(s, "|\\[").concat(f, "\\])"),
      h = "(?:''|\"\")",
      d = ":function\\(a\\)\\{(?:return )?a\\.reverse\\(\\)\\}",
      y = ":function\\(a,b\\)\\{return a\\.slice\\(b\\)\\}",
      m = ":function\\(a,b\\)\\{a\\.splice\\(0,b\\)\\}",
      v =
        ":function\\(a,b\\)\\{var c=a\\[0\\];a\\[0\\]=a\\[b(?:%a\\.length)?\\];a\\[b(?:%a\\.length)?\\]=c(?:;return a)?\\}",
      g = new RegExp(
        "var ("
          .concat(s, ")=\\{((?:(?:")
          .concat(l)
          .concat(d, "|")
          .concat(l)
          .concat(y, "|")
          .concat(l)
          .concat(m, "|")
          .concat(l)
          .concat(v, "),?\\r?\\n?)+)\\};")
      ),
      b = new RegExp(
        ""
          .concat(
            "function(?: ".concat(s, ")?\\(a\\)\\{") +
              "a=a\\.split\\(".concat(h, "\\);\\s*") +
              "((?:(?:a=)?".concat(s)
          )
          .concat(p, "\\(a,\\d+\\);)+)") +
          "return a\\.join\\(".concat(h, "\\)") +
          "\\}"
      ),
      w = new RegExp("(?:^|,)(".concat(l, ")").concat(d), "m"),
      _ = new RegExp("(?:^|,)(".concat(l, ")").concat(y), "m"),
      E = new RegExp("(?:^|,)(".concat(l, ")").concat(m), "m"),
      T = new RegExp("(?:^|,)(".concat(l, ")").concat(v), "m");
    (e.extractActions = function (t) {
      var e = g.exec(t),
        n = b.exec(t);
      if (!e || !n) return null;
      for (
        var r = e[1].replace(/\$/g, "\\$"),
          o = e[2].replace(/\$/g, "\\$"),
          i = n[1].replace(/\$/g, "\\$"),
          a = w.exec(o),
          u = a && a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, ""),
          c =
            (a = _.exec(o)) &&
            a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, ""),
          s =
            (a = E.exec(o)) &&
            a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, ""),
          f =
            (a = T.exec(o)) &&
            a[1].replace(/\$/g, "\\$").replace(/\$|^'|^"|'$|"$/g, ""),
          l = "(".concat([u, c, s, f].join("|"), ")"),
          p =
            "(?:a=)?"
              .concat(r, "(?:\\.")
              .concat(l, "|\\['")
              .concat(l, "'\\]|\\[\"")
              .concat(l, '"\\])') + "\\(a,(\\d+)\\)",
          h = new RegExp(p, "g"),
          d = [];
        null !== (a = h.exec(i));

      ) {
        switch (a[1] || a[2] || a[3]) {
          case f:
            d.push("w".concat(a[4]));
            break;
          case u:
            d.push("r");
            break;
          case c:
            d.push("s".concat(a[4]));
            break;
          case s:
            d.push("p".concat(a[4]));
        }
      }
      return d;
    }),
      (e.setDownloadURL = function (t, e) {
        var n;
        if (t.url) {
          n = t.url;
          try {
            n = decodeURIComponent(n);
          } catch (o) {
            return;
          }
          var r = new URL(n);
          r.searchParams.set("ratebypass", "yes"),
            e && r.searchParams.set(t.sp || "signature", e),
            (t.url = r.toString());
        }
      }),
      (e.decipherFormats = (function () {
        var t = o(
          regeneratorRuntime.mark(function n(t, r, o) {
            var i, u;
            return regeneratorRuntime.wrap(function (n) {
              for (;;)
                switch ((n.prev = n.next)) {
                  case 0:
                    return (i = {}), (n.next = 3), e.getTokens(r, o);
                  case 3:
                    return (
                      (u = n.sent),
                      t.forEach(function (t) {
                        var n = t.signatureCipher || t.cipher;
                        n &&
                          (Object.assign(t, a.parse(n)),
                          delete t.signatureCipher,
                          delete t.cipher);
                        var r = u && t.s ? e.decipher(u, t.s) : null;
                        e.setDownloadURL(t, r), (i[t.url] = t);
                      }),
                      n.abrupt("return", i)
                    );
                  case 6:
                  case "end":
                    return n.stop();
                }
            }, n);
          })
        );
        return function (e, n, r) {
          return t.apply(this, arguments);
        };
      })());
  },
  function (t, e, n) {
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    function o(t, e) {
      var n =
        ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
      if (!n) {
        if (
          Array.isArray(t) ||
          (n = (function (t, e) {
            if (!t) return;
            if ("string" == typeof t) return i(t, e);
            var n = Object.prototype.toString.call(t).slice(8, -1);
            "Object" === n && t.constructor && (n = t.constructor.name);
            if ("Map" === n || "Set" === n) return Array.from(t);
            if (
              "Arguments" === n ||
              /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
            )
              return i(t, e);
          })(t)) ||
          (e && t && "number" == typeof t.length)
        ) {
          n && (t = n);
          var r = 0,
            o = function () {};
          return {
            s: o,
            n: function () {
              return r >= t.length ? { done: !0 } : { done: !1, value: t[r++] };
            },
            e: function (t) {
              throw t;
            },
            f: o,
          };
        }
        throw new TypeError(
          "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      }
      var a,
        u = !0,
        c = !1;
      return {
        s: function () {
          n = n.call(t);
        },
        n: function () {
          var t = n.next();
          return (u = t.done), t;
        },
        e: function (t) {
          (c = !0), (a = t);
        },
        f: function () {
          try {
            u || null == n["return"] || n["return"]();
          } finally {
            if (c) throw a;
          }
        },
      };
    }
    function i(t, e) {
      (null == e || e > t.length) && (e = t.length);
      for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
      return r;
    }
    function a(t, e, n, r, o, i, a) {
      try {
        var u = t[i](a),
          c = u.value;
      } catch (s) {
        return void n(s);
      }
      u.done ? e(c) : Promise.resolve(c).then(r, o);
    }
    function u(t, e) {
      if (!(t instanceof e))
        throw new TypeError("Cannot call a class as a function");
    }
    function c(t, e) {
      for (var n = 0; n < e.length; n++) {
        var r = e[n];
        (r.enumerable = r.enumerable || !1),
          (r.configurable = !0),
          "value" in r && (r.writable = !0),
          Object.defineProperty(t, r.key, r);
      }
    }
    function s(t, e, n) {
      return (s =
        "undefined" != typeof Reflect && Reflect.get
          ? Reflect.get
          : function (t, e, n) {
              var r = (function (t, e) {
                for (
                  ;
                  !Object.prototype.hasOwnProperty.call(t, e) &&
                  null !== (t = y(t));

                );
                return t;
              })(t, e);
              if (r) {
                var o = Object.getOwnPropertyDescriptor(r, e);
                return o.get ? o.get.call(n) : o.value;
              }
            })(t, e, n || t);
    }
    function f(t, e) {
      return !e || ("object" !== r(e) && "function" != typeof e)
        ? (function (t) {
            if (void 0 === t)
              throw new ReferenceError(
                "this hasn't been initialised - super() hasn't been called"
              );
            return t;
          })(t)
        : e;
    }
    function l(t) {
      var e = "function" == typeof Map ? new Map() : undefined;
      return (l = function (t) {
        if (
          null === t ||
          ((n = t), -1 === Function.toString.call(n).indexOf("[native code]"))
        )
          return t;
        var n;
        if ("function" != typeof t)
          throw new TypeError(
            "Super expression must either be null or a function"
          );
        if (void 0 !== e) {
          if (e.has(t)) return e.get(t);
          e.set(t, r);
        }
        function r() {
          return p(t, arguments, y(this).constructor);
        }
        return (
          (r.prototype = Object.create(t.prototype, {
            constructor: {
              value: r,
              enumerable: !1,
              writable: !0,
              configurable: !0,
            },
          })),
          d(r, t)
        );
      })(t);
    }
    function p(t, e, n) {
      return (p = h()
        ? Reflect.construct
        : function (t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var o = new (Function.bind.apply(t, r))();
            return n && d(o, n.prototype), o;
          }).apply(null, arguments);
    }
    function h() {
      if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
      if (Reflect.construct.sham) return !1;
      if ("function" == typeof Proxy) return !0;
      try {
        return (
          Boolean.prototype.valueOf.call(
            Reflect.construct(Boolean, [], function () {})
          ),
          !0
        );
      } catch (t) {
        return !1;
      }
    }
    function d(t, e) {
      return (d =
        Object.setPrototypeOf ||
        function (t, e) {
          return (t.__proto__ = e), t;
        })(t, e);
    }
    function y(t) {
      return (y = Object.setPrototypeOf
        ? Object.getPrototypeOf
        : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t);
          })(t);
    }
    var m = n(126).setTimeout;
    t.exports = (function (t) {
      !(function (t, e) {
        if ("function" != typeof e && null !== e)
          throw new TypeError(
            "Super expression must either be null or a function"
          );
        (t.prototype = Object.create(e && e.prototype, {
          constructor: { value: t, writable: !0, configurable: !0 },
        })),
          e && d(t, e);
      })(v, t);
      var e,
        n,
        r,
        i,
        l,
        p =
          ((e = v),
          (n = h()),
          function () {
            var t,
              r = y(e);
            if (n) {
              var o = y(this).constructor;
              t = Reflect.construct(r, arguments, o);
            } else t = r.apply(this, arguments);
            return f(this, t);
          });
      function v() {
        var t,
          e =
            arguments.length > 0 && arguments[0] !== undefined
              ? arguments[0]
              : 1e3;
        return u(this, v), ((t = p.call(this)).timeout = e), t;
      }
      return (
        (r = v),
        (i = [
          {
            key: "set",
            value: function (t, e) {
              this.has(t) &&
                clearTimeout(s(y(v.prototype), "get", this).call(this, t).tid),
                s(y(v.prototype), "set", this).call(this, t, {
                  tid: m(this["delete"].bind(this, t), this.timeout).unref(),
                  value: e,
                });
            },
          },
          {
            key: "get",
            value: function (t) {
              var e = s(y(v.prototype), "get", this).call(this, t);
              return e ? e.value : null;
            },
          },
          {
            key: "getOrSet",
            value: function (t, e) {
              var n = this;
              if (this.has(t)) return this.get(t);
              var r = e();
              return (
                this.set(t, r),
                (function (t) {
                  return function () {
                    var e = this,
                      n = arguments;
                    return new Promise(function (r, o) {
                      var i = t.apply(e, n);
                      function u(t) {
                        a(i, r, o, u, c, "next", t);
                      }
                      function c(t) {
                        a(i, r, o, u, c, "throw", t);
                      }
                      u(undefined);
                    });
                  };
                })(
                  regeneratorRuntime.mark(function o() {
                    return regeneratorRuntime.wrap(
                      function (e) {
                        for (;;)
                          switch ((e.prev = e.next)) {
                            case 0:
                              return (e.prev = 0), (e.next = 3), r;
                            case 3:
                              e.next = 8;
                              break;
                            case 5:
                              (e.prev = 5),
                                (e.t0 = e["catch"](0)),
                                n["delete"](t);
                            case 8:
                            case "end":
                              return e.stop();
                          }
                      },
                      o,
                      null,
                      [[0, 5]]
                    );
                  })
                )(),
                r
              );
            },
          },
          {
            key: "delete",
            value: function (t) {
              var e = s(y(v.prototype), "get", this).call(this, t);
              e &&
                (clearTimeout(e.tid),
                s(y(v.prototype), "delete", this).call(this, t));
            },
          },
          {
            key: "clear",
            value: function () {
              var t,
                e = o(this.values());
              try {
                for (e.s(); !(t = e.n()).done; ) {
                  var n = t.value;
                  clearTimeout(n.tid);
                }
              } catch (r) {
                e.e(r);
              } finally {
                e.f();
              }
              s(y(v.prototype), "clear", this).call(this);
            },
          },
        ]) && c(r.prototype, i),
        l && c(r, l),
        v
      );
    })(l(Map));
  },
  function (t, e, n) {
    (function (e) {
      var r = n(324),
        o = n(62),
        i = n(127),
        a = n(128),
        u = n(129),
        c = n(61),
        s = n(92),
        f = n(92).parseTimestamp,
        l = function y(t, e) {
          var n = p(e);
          return (
            y.getInfo(t, e).then(function (t) {
              d(n, t, e);
            }, n.emit.bind(n, "error")),
            n
          );
        };
      (t.exports = l),
        (l.getBasicInfo = r.getBasicInfo),
        (l.getInfo = r.getInfo),
        (l.chooseFormat = i.chooseFormat),
        (l.filterFormats = i.filterFormats),
        (l.validateID = a.validateID),
        (l.validateURL = a.validateURL),
        (l.getURLVideoID = a.getURLVideoID),
        (l.getVideoID = a.getVideoID),
        (l.cache = {
          sig: u.cache,
          info: r.cache,
          watch: r.watchPageCache,
          cookie: r.cookieCache,
        }),
        (l.version = n(335).version);
      var p = function (t) {},
        h = function (t, e, n) {
          [
            "abort",
            "request",
            "response",
            "error",
            "redirect",
            "retry",
            "reconnect",
          ].forEach(function (n) {
            t.prependListener(n, e.emit.bind(e, n));
          }),
            t.pipe(e, { end: n });
        },
        d = function (t, e, n) {
          n = n || {};
          var r = o.playError(e.player_response, [
            "UNPLAYABLE",
            "LIVE_STREAM_OFFLINE",
            "LOGIN_REQUIRED",
          ]);
          if (r) t.emit("error", r);
          else if (e.formats.length) {
            var a;
            try {
              a = i.chooseFormat(e.formats, n);
            } catch (_) {
              return void t.emit("error", _);
            }
            if ((t.emit("info", e, a), !t.destroyed)) {
              var u,
                l,
                p = 0,
                d = function (e) {
                  (p += e.length), t.emit("progress", e.length, p, u);
                },
                y = n.dlChunkSize || 10485760,
                m = !0;
              if (a.isHLS || a.isDashMPD)
                (l = s(a.url, {
                  chunkReadahead: +e.live_chunk_readahead,
                  begin: n.begin || (a.isLive && Date.now()),
                  liveBuffer: n.liveBuffer,
                  requestOptions: n.requestOptions,
                  parser: a.isDashMPD ? "dash-mpd" : "m3u8",
                  id: a.itag,
                })).on("progress", function (e, n) {
                  t.emit("progress", e.size, e.num, n);
                }),
                  h(l, t, m);
              else {
                var v = Object.assign({}, n.requestOptions, {
                  maxReconnects: 6,
                  maxRetries: 3,
                  backoff: { inc: 500, max: 1e4 },
                });
                if (!(0 === y || (a.hasAudio && a.hasVideo))) {
                  var g = (n.range && n.range.start) || 0,
                    b = g + y,
                    w = n.range && n.range.end;
                  u = n.range
                    ? (w ? w + 1 : parseInt(a.contentLength)) - g
                    : parseInt(a.contentLength);
                  !(function e() {
                    !w && b >= u && (b = 0),
                      w && b > w && (b = w),
                      (m = !b || b === w),
                      (v.headers = Object.assign({}, v.headers, {
                        Range: "bytes=".concat(g, "-").concat(b || ""),
                      })),
                      (l = c(a.url, v)).on("data", d),
                      l.on("end", function () {
                        t.destroyed ||
                          (b && b !== w && ((g = b + 1), (b += y), e()));
                      }),
                      h(l, t, m);
                  })();
                } else
                  n.begin && (a.url += "&begin=".concat(f(n.begin))),
                    n.range &&
                      (n.range.start || n.range.end) &&
                      (v.headers = Object.assign({}, v.headers, {
                        Range: "bytes="
                          .concat(n.range.start || "0", "-")
                          .concat(n.range.end || ""),
                      })),
                    (l = c(a.url, v)).on("response", function (e) {
                      t.destroyed ||
                        (u = u || parseInt(e.headers["content-length"]));
                    }),
                    l.on("data", d),
                    h(l, t, m);
              }
              t._destroy = function () {
                (t.destroyed = !0), l.destroy(), l.end();
              };
            }
          } else t.emit("error", Error("This video is unavailable"));
        };
      l.downloadFromInfo = function (t, n) {
        var r = p(n);
        if (!t.full)
          throw Error(
            "Cannot use `ytdl.downloadFromInfo()` when called with info from `ytdl.getBasicInfo()`"
          );
        return (
          e(function () {
            d(r, t, n);
          }),
          r
        );
      };
    }.call(this, n(321).setImmediate));
  },
  function (t, e, n) {
    "use strict";
    n.r(e);
    n(133), n(320);
    var r = n(131),
      o = n.n(r);
    window.ytdl = o.a;
  },
  function (t, e, n) {
    "use strict";
    n(134);
    var r,
      o = (r = n(307)) && r.__esModule ? r : { default: r };
    o["default"]._babelPolyfill &&
      "undefined" != typeof console &&
      console.warn &&
      console.warn(
        "@babel/polyfill is loaded more than once on this page. This is probably not desirable/intended and may have consequences if different versions of the polyfills are applied sequentially. If you do need to load the polyfill more than once, use @babel/polyfill/noConflict instead to bypass the warning."
      ),
      (o["default"]._babelPolyfill = !0);
  },
  function (t, e, n) {
    "use strict";
    n(135),
      n(278),
      n(280),
      n(283),
      n(285),
      n(287),
      n(289),
      n(291),
      n(293),
      n(295),
      n(297),
      n(299),
      n(301),
      n(305);
  },
  function (t, e, n) {
    n(136),
      n(139),
      n(140),
      n(141),
      n(142),
      n(143),
      n(144),
      n(145),
      n(146),
      n(147),
      n(148),
      n(149),
      n(150),
      n(151),
      n(152),
      n(153),
      n(154),
      n(155),
      n(156),
      n(157),
      n(158),
      n(159),
      n(160),
      n(161),
      n(162),
      n(163),
      n(164),
      n(165),
      n(166),
      n(167),
      n(168),
      n(169),
      n(170),
      n(171),
      n(172),
      n(173),
      n(174),
      n(175),
      n(176),
      n(177),
      n(178),
      n(179),
      n(180),
      n(182),
      n(183),
      n(184),
      n(185),
      n(186),
      n(187),
      n(188),
      n(189),
      n(190),
      n(191),
      n(192),
      n(193),
      n(194),
      n(195),
      n(196),
      n(197),
      n(198),
      n(199),
      n(200),
      n(201),
      n(202),
      n(203),
      n(204),
      n(205),
      n(206),
      n(207),
      n(208),
      n(209),
      n(210),
      n(211),
      n(212),
      n(213),
      n(214),
      n(215),
      n(217),
      n(218),
      n(220),
      n(221),
      n(222),
      n(223),
      n(224),
      n(225),
      n(226),
      n(228),
      n(229),
      n(230),
      n(231),
      n(232),
      n(233),
      n(234),
      n(235),
      n(236),
      n(237),
      n(238),
      n(239),
      n(240),
      n(83),
      n(241),
      n(113),
      n(242),
      n(114),
      n(243),
      n(244),
      n(245),
      n(246),
      n(115),
      n(249),
      n(250),
      n(251),
      n(252),
      n(253),
      n(254),
      n(255),
      n(256),
      n(257),
      n(258),
      n(259),
      n(260),
      n(261),
      n(262),
      n(263),
      n(264),
      n(265),
      n(266),
      n(267),
      n(268),
      n(269),
      n(270),
      n(271),
      n(272),
      n(273),
      n(274),
      n(275),
      n(276),
      n(277),
      (t.exports = n(7));
  },
  function (t, e, n) {
    "use strict";
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    var o = n(1),
      i = n(13),
      a = n(8),
      u = n(0),
      c = n(11),
      s = n(27).KEY,
      f = n(2),
      l = n(49),
      p = n(38),
      h = n(29),
      d = n(5),
      y = n(64),
      m = n(94),
      v = n(138),
      g = n(52),
      b = n(3),
      w = n(4),
      _ = n(10),
      E = n(15),
      T = n(26),
      S = n(28),
      x = n(33),
      A = n(97),
      R = n(20),
      I = n(51),
      O = n(9),
      P = n(31),
      L = R.f,
      N = O.f,
      D = A.f,
      F = o.Symbol,
      M = o.JSON,
      C = M && M.stringify,
      B = d("_hidden"),
      j = d("toPrimitive"),
      U = {}.propertyIsEnumerable,
      k = l("symbol-registry"),
      V = l("symbols"),
      q = l("op-symbols"),
      Y = Object.prototype,
      G = "function" == typeof F && !!I.f,
      H = o.QObject,
      W = !H || !H.prototype || !H.prototype.findChild,
      $ =
        a &&
        f(function () {
          return (
            7 !=
            x(
              N({}, "a", {
                get: function () {
                  return N(this, "a", { value: 7 }).a;
                },
              })
            ).a
          );
        })
          ? function (t, e, n) {
              var r = L(Y, e);
              r && delete Y[e], N(t, e, n), r && t !== Y && N(Y, e, r);
            }
          : N,
      z = function (t) {
        var e = (V[t] = x(F.prototype));
        return (e._k = t), e;
      },
      X =
        G && "symbol" == r(F.iterator)
          ? function (t) {
              return "symbol" == r(t);
            }
          : function (t) {
              return t instanceof F;
            },
      K = function (t, e, n) {
        return (
          t === Y && K(q, e, n),
          b(t),
          (e = T(e, !0)),
          b(n),
          i(V, e)
            ? (n.enumerable
                ? (i(t, B) && t[B][e] && (t[B][e] = !1),
                  (n = x(n, { enumerable: S(0, !1) })))
                : (i(t, B) || N(t, B, S(1, {})), (t[B][e] = !0)),
              $(t, e, n))
            : N(t, e, n)
        );
      },
      Q = function (t, e) {
        b(t);
        for (var n, r = v((e = E(e))), o = 0, i = r.length; i > o; )
          K(t, (n = r[o++]), e[n]);
        return t;
      },
      J = function (t) {
        var e = U.call(this, (t = T(t, !0)));
        return (
          !(this === Y && i(V, t) && !i(q, t)) &&
          (!(e || !i(this, t) || !i(V, t) || (i(this, B) && this[B][t])) || e)
        );
      },
      Z = function (t, e) {
        if (((t = E(t)), (e = T(e, !0)), t !== Y || !i(V, e) || i(q, e))) {
          var n = L(t, e);
          return (
            !n || !i(V, e) || (i(t, B) && t[B][e]) || (n.enumerable = !0), n
          );
        }
      },
      tt = function (t) {
        for (var e, n = D(E(t)), r = [], o = 0; n.length > o; )
          i(V, (e = n[o++])) || e == B || e == s || r.push(e);
        return r;
      },
      et = function (t) {
        for (
          var e, n = t === Y, r = D(n ? q : E(t)), o = [], a = 0;
          r.length > a;

        )
          !i(V, (e = r[a++])) || (n && !i(Y, e)) || o.push(V[e]);
        return o;
      };
    G ||
      (c(
        (F = function () {
          if (this instanceof F)
            throw TypeError("Symbol is not a constructor!");
          var t = h(arguments.length > 0 ? arguments[0] : undefined),
            e = function n(e) {
              this === Y && n.call(q, e),
                i(this, B) && i(this[B], t) && (this[B][t] = !1),
                $(this, t, S(1, e));
            };
          return a && W && $(Y, t, { configurable: !0, set: e }), z(t);
        }).prototype,
        "toString",
        function () {
          return this._k;
        }
      ),
      (R.f = Z),
      (O.f = K),
      (n(34).f = A.f = tt),
      (n(45).f = J),
      (I.f = et),
      a && !n(30) && c(Y, "propertyIsEnumerable", J, !0),
      (y.f = function (t) {
        return z(d(t));
      })),
      u(u.G + u.W + u.F * !G, { Symbol: F });
    for (
      var nt =
          "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(
            ","
          ),
        rt = 0;
      nt.length > rt;

    )
      d(nt[rt++]);
    for (var ot = P(d.store), it = 0; ot.length > it; ) m(ot[it++]);
    u(u.S + u.F * !G, "Symbol", {
      for: function (t) {
        return i(k, (t += "")) ? k[t] : (k[t] = F(t));
      },
      keyFor: function (t) {
        if (!X(t)) throw TypeError(t + " is not a symbol!");
        for (var e in k) if (k[e] === t) return e;
      },
      useSetter: function () {
        W = !0;
      },
      useSimple: function () {
        W = !1;
      },
    }),
      u(u.S + u.F * !G, "Object", {
        create: function (t, e) {
          return e === undefined ? x(t) : Q(x(t), e);
        },
        defineProperty: K,
        defineProperties: Q,
        getOwnPropertyDescriptor: Z,
        getOwnPropertyNames: tt,
        getOwnPropertySymbols: et,
      });
    var at = f(function () {
      I.f(1);
    });
    u(u.S + u.F * at, "Object", {
      getOwnPropertySymbols: function (t) {
        return I.f(_(t));
      },
    }),
      M &&
        u(
          u.S +
            u.F *
              (!G ||
                f(function () {
                  var t = F();
                  return (
                    "[null]" != C([t]) ||
                    "{}" != C({ a: t }) ||
                    "{}" != C(Object(t))
                  );
                })),
          "JSON",
          {
            stringify: function (t) {
              for (var e, n, r = [t], o = 1; arguments.length > o; )
                r.push(arguments[o++]);
              if (((n = e = r[1]), (w(e) || t !== undefined) && !X(t)))
                return (
                  g(e) ||
                    (e = function (t, e) {
                      if (
                        ("function" == typeof n && (e = n.call(this, t, e)),
                        !X(e))
                      )
                        return e;
                    }),
                  (r[1] = e),
                  C.apply(M, r)
                );
            },
          }
        ),
      F.prototype[j] || n(14)(F.prototype, j, F.prototype.valueOf),
      p(F, "Symbol"),
      p(Math, "Math", !0),
      p(o.JSON, "JSON", !0);
  },
  function (t, e, n) {
    t.exports = n(49)("native-function-to-string", Function.toString);
  },
  function (t, e, n) {
    var r = n(31),
      o = n(51),
      i = n(45);
    t.exports = function (t) {
      var e = r(t),
        n = o.f;
      if (n)
        for (var a, u = n(t), c = i.f, s = 0; u.length > s; )
          c.call(t, (a = u[s++])) && e.push(a);
      return e;
    };
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Object", { create: n(33) });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S + r.F * !n(8), "Object", { defineProperty: n(9).f });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S + r.F * !n(8), "Object", { defineProperties: n(96) });
  },
  function (t, e, n) {
    var r = n(15),
      o = n(20).f;
    n(21)("getOwnPropertyDescriptor", function () {
      return function (t, e) {
        return o(r(t), e);
      };
    });
  },
  function (t, e, n) {
    var r = n(10),
      o = n(35);
    n(21)("getPrototypeOf", function () {
      return function (t) {
        return o(r(t));
      };
    });
  },
  function (t, e, n) {
    var r = n(10),
      o = n(31);
    n(21)("keys", function () {
      return function (t) {
        return o(r(t));
      };
    });
  },
  function (t, e, n) {
    n(21)("getOwnPropertyNames", function () {
      return n(97).f;
    });
  },
  function (t, e, n) {
    var r = n(4),
      o = n(27).onFreeze;
    n(21)("freeze", function (t) {
      return function (e) {
        return t && r(e) ? t(o(e)) : e;
      };
    });
  },
  function (t, e, n) {
    var r = n(4),
      o = n(27).onFreeze;
    n(21)("seal", function (t) {
      return function (e) {
        return t && r(e) ? t(o(e)) : e;
      };
    });
  },
  function (t, e, n) {
    var r = n(4),
      o = n(27).onFreeze;
    n(21)("preventExtensions", function (t) {
      return function (e) {
        return t && r(e) ? t(o(e)) : e;
      };
    });
  },
  function (t, e, n) {
    var r = n(4);
    n(21)("isFrozen", function (t) {
      return function (e) {
        return !r(e) || (!!t && t(e));
      };
    });
  },
  function (t, e, n) {
    var r = n(4);
    n(21)("isSealed", function (t) {
      return function (e) {
        return !r(e) || (!!t && t(e));
      };
    });
  },
  function (t, e, n) {
    var r = n(4);
    n(21)("isExtensible", function (t) {
      return function (e) {
        return !!r(e) && (!t || t(e));
      };
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S + r.F, "Object", { assign: n(98) });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Object", { is: n(99) });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Object", { setPrototypeOf: n(68).set });
  },
  function (t, e, n) {
    "use strict";
    var r = n(46),
      o = {};
    (o[n(5)("toStringTag")] = "z"),
      o + "" != "[object z]" &&
        n(11)(
          Object.prototype,
          "toString",
          function () {
            return "[object " + r(this) + "]";
          },
          !0
        );
  },
  function (t, e, n) {
    var r = n(0);
    r(r.P, "Function", { bind: n(100) });
  },
  function (t, e, n) {
    var r = n(9).f,
      o = Function.prototype,
      i = /^\s*function ([^ (]*)/,
      a = "name";
    a in o ||
      (n(8) &&
        r(o, a, {
          configurable: !0,
          get: function () {
            try {
              return ("" + this).match(i)[1];
            } catch (t) {
              return "";
            }
          },
        }));
  },
  function (t, e, n) {
    "use strict";
    var r = n(4),
      o = n(35),
      i = n(5)("hasInstance"),
      a = Function.prototype;
    i in a ||
      n(9).f(a, i, {
        value: function (t) {
          if ("function" != typeof this || !r(t)) return !1;
          if (!r(this.prototype)) return t instanceof this;
          for (; (t = o(t)); ) if (this.prototype === t) return !0;
          return !1;
        },
      });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(102);
    r(r.G + r.F * (parseInt != o), { parseInt: o });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(103);
    r(r.G + r.F * (parseFloat != o), { parseFloat: o });
  },
  function (t, e, n) {
    "use strict";
    var r = n(1),
      o = n(13),
      i = n(23),
      a = n(70),
      u = n(26),
      c = n(2),
      s = n(34).f,
      f = n(20).f,
      l = n(9).f,
      p = n(39).trim,
      h = "Number",
      d = r.Number,
      y = d,
      m = d.prototype,
      v = i(n(33)(m)) == h,
      g = "trim" in String.prototype,
      b = function (t) {
        var e = u(t, !1);
        if ("string" == typeof e && e.length > 2) {
          var n,
            r,
            o,
            i = (e = g ? e.trim() : p(e, 3)).charCodeAt(0);
          if (43 === i || 45 === i) {
            if (88 === (n = e.charCodeAt(2)) || 120 === n) return NaN;
          } else if (48 === i) {
            switch (e.charCodeAt(1)) {
              case 66:
              case 98:
                (r = 2), (o = 49);
                break;
              case 79:
              case 111:
                (r = 8), (o = 55);
                break;
              default:
                return +e;
            }
            for (var a, c = e.slice(2), s = 0, f = c.length; s < f; s++)
              if ((a = c.charCodeAt(s)) < 48 || a > o) return NaN;
            return parseInt(c, r);
          }
        }
        return +e;
      };
    if (!d(" 0o1") || !d("0b1") || d("+0x1")) {
      d = function (t) {
        var e = arguments.length < 1 ? 0 : t,
          n = this;
        return n instanceof d &&
          (v
            ? c(function () {
                m.valueOf.call(n);
              })
            : i(n) != h)
          ? a(new y(b(e)), n, d)
          : b(e);
      };
      for (
        var w,
          _ = n(8)
            ? s(y)
            : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(
                ","
              ),
          E = 0;
        _.length > E;
        E++
      )
        o(y, (w = _[E])) && !o(d, w) && l(d, w, f(y, w));
      (d.prototype = m), (m.constructor = d), n(11)(r, h, d);
    }
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(19),
      i = n(104),
      a = n(71),
      u = (1).toFixed,
      c = Math.floor,
      s = [0, 0, 0, 0, 0, 0],
      f = "Number.toFixed: incorrect invocation!",
      l = "0",
      p = function (t, e) {
        for (var n = -1, r = e; ++n < 6; )
          (r += t * s[n]), (s[n] = r % 1e7), (r = c(r / 1e7));
      },
      h = function (t) {
        for (var e = 6, n = 0; --e >= 0; )
          (n += s[e]), (s[e] = c(n / t)), (n = (n % t) * 1e7);
      },
      d = function () {
        for (var t = 6, e = ""; --t >= 0; )
          if ("" !== e || 0 === t || 0 !== s[t]) {
            var n = String(s[t]);
            e = "" === e ? n : e + a.call(l, 7 - n.length) + n;
          }
        return e;
      },
      y = function m(t, e, n) {
        return 0 === e
          ? n
          : e % 2 == 1
          ? m(t, e - 1, n * t)
          : m(t * t, e / 2, n);
      };
    r(
      r.P +
        r.F *
          ((!!u &&
            ("0.000" !== (8e-5).toFixed(3) ||
              "1" !== (0.9).toFixed(0) ||
              "1.25" !== (1.255).toFixed(2) ||
              "1000000000000000128" !== (0xde0b6b3a7640080).toFixed(0))) ||
            !n(2)(function () {
              u.call({});
            })),
      "Number",
      {
        toFixed: function (t) {
          var e,
            n,
            r,
            u,
            c = i(this, f),
            s = o(t),
            m = "",
            v = l;
          if (s < 0 || s > 20) throw RangeError(f);
          if (c != c) return "NaN";
          if (c <= -1e21 || c >= 1e21) return String(c);
          if ((c < 0 && ((m = "-"), (c = -c)), c > 1e-21))
            if (
              ((n =
                (e =
                  (function (t) {
                    for (var e = 0, n = t; n >= 4096; ) (e += 12), (n /= 4096);
                    for (; n >= 2; ) (e += 1), (n /= 2);
                    return e;
                  })(c * y(2, 69, 1)) - 69) < 0
                  ? c * y(2, -e, 1)
                  : c / y(2, e, 1)),
              (n *= 4503599627370496),
              (e = 52 - e) > 0)
            ) {
              for (p(0, n), r = s; r >= 7; ) p(1e7, 0), (r -= 7);
              for (p(y(10, r, 1), 0), r = e - 1; r >= 23; )
                h(1 << 23), (r -= 23);
              h(1 << r), p(1, 1), h(2), (v = d());
            } else p(0, n), p(1 << -e, 0), (v = d() + a.call(l, s));
          return (v =
            s > 0
              ? m +
                ((u = v.length) <= s
                  ? "0." + a.call(l, s - u) + v
                  : v.slice(0, u - s) + "." + v.slice(u - s))
              : m + v);
        },
      }
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(2),
      i = n(104),
      a = (1).toPrecision;
    r(
      r.P +
        r.F *
          (o(function () {
            return "1" !== a.call(1, undefined);
          }) ||
            !o(function () {
              a.call({});
            })),
      "Number",
      {
        toPrecision: function (t) {
          var e = i(this, "Number#toPrecision: incorrect invocation!");
          return t === undefined ? a.call(e) : a.call(e, t);
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Number", { EPSILON: Math.pow(2, -52) });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(1).isFinite;
    r(r.S, "Number", {
      isFinite: function (t) {
        return "number" == typeof t && o(t);
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Number", { isInteger: n(105) });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Number", {
      isNaN: function (t) {
        return t != t;
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(105),
      i = Math.abs;
    r(r.S, "Number", {
      isSafeInteger: function (t) {
        return o(t) && i(t) <= 9007199254740991;
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Number", { MAX_SAFE_INTEGER: 9007199254740991 });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Number", { MIN_SAFE_INTEGER: -9007199254740991 });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(103);
    r(r.S + r.F * (Number.parseFloat != o), "Number", { parseFloat: o });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(102);
    r(r.S + r.F * (Number.parseInt != o), "Number", { parseInt: o });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(106),
      i = Math.sqrt,
      a = Math.acosh;
    r(
      r.S +
        r.F *
          !(
            a &&
            710 == Math.floor(a(Number.MAX_VALUE)) &&
            a(Infinity) == Infinity
          ),
      "Math",
      {
        acosh: function (t) {
          return (t = +t) < 1
            ? NaN
            : t > 94906265.62425156
            ? Math.log(t) + Math.LN2
            : o(t - 1 + i(t - 1) * i(t + 1));
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0),
      o = Math.asinh;
    r(r.S + r.F * !(o && 1 / o(0) > 0), "Math", {
      asinh: function i(t) {
        return isFinite((t = +t)) && 0 != t
          ? t < 0
            ? -i(-t)
            : Math.log(t + Math.sqrt(t * t + 1))
          : t;
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = Math.atanh;
    r(r.S + r.F * !(o && 1 / o(-0) < 0), "Math", {
      atanh: function (t) {
        return 0 == (t = +t) ? t : Math.log((1 + t) / (1 - t)) / 2;
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(72);
    r(r.S, "Math", {
      cbrt: function (t) {
        return o((t = +t)) * Math.pow(Math.abs(t), 1 / 3);
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
      clz32: function (t) {
        return (t >>>= 0)
          ? 31 - Math.floor(Math.log(t + 0.5) * Math.LOG2E)
          : 32;
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = Math.exp;
    r(r.S, "Math", {
      cosh: function (t) {
        return (o((t = +t)) + o(-t)) / 2;
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(73);
    r(r.S + r.F * (o != Math.expm1), "Math", { expm1: o });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", { fround: n(181) });
  },
  function (t, e, n) {
    var r = n(72),
      o = Math.pow,
      i = o(2, -52),
      a = o(2, -23),
      u = o(2, 127) * (2 - a),
      c = o(2, -126);
    t.exports =
      Math.fround ||
      function (t) {
        var e,
          n,
          o = Math.abs(t),
          s = r(t);
        return o < c
          ? s * (o / c / a + 1 / i - 1 / i) * c * a
          : (n = (e = (1 + a / i) * o) - (e - o)) > u || n != n
          ? s * Infinity
          : s * n;
      };
  },
  function (t, e, n) {
    var r = n(0),
      o = Math.abs;
    r(r.S, "Math", {
      hypot: function (t, e) {
        for (var n, r, i = 0, a = 0, u = arguments.length, c = 0; a < u; )
          c < (n = o(arguments[a++]))
            ? ((i = i * (r = c / n) * r + 1), (c = n))
            : (i += n > 0 ? (r = n / c) * r : n);
        return c === Infinity ? Infinity : c * Math.sqrt(i);
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = Math.imul;
    r(
      r.S +
        r.F *
          n(2)(function () {
            return -5 != o(4294967295, 5) || 2 != o.length;
          }),
      "Math",
      {
        imul: function (t, e) {
          var n = 65535,
            r = +t,
            o = +e,
            i = n & r,
            a = n & o;
          return (
            0 |
            (i * a +
              ((((n & (r >>> 16)) * a + i * (n & (o >>> 16))) << 16) >>> 0))
          );
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
      log10: function (t) {
        return Math.log(t) * Math.LOG10E;
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", { log1p: n(106) });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
      log2: function (t) {
        return Math.log(t) / Math.LN2;
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", { sign: n(72) });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(73),
      i = Math.exp;
    r(
      r.S +
        r.F *
          n(2)(function () {
            return -2e-17 != !Math.sinh(-2e-17);
          }),
      "Math",
      {
        sinh: function (t) {
          return Math.abs((t = +t)) < 1
            ? (o(t) - o(-t)) / 2
            : (i(t - 1) - i(-t - 1)) * (Math.E / 2);
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0),
      o = n(73),
      i = Math.exp;
    r(r.S, "Math", {
      tanh: function (t) {
        var e = o((t = +t)),
          n = o(-t);
        return e == Infinity
          ? 1
          : n == Infinity
          ? -1
          : (e - n) / (i(t) + i(-t));
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Math", {
      trunc: function (t) {
        return (t > 0 ? Math.floor : Math.ceil)(t);
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(32),
      i = String.fromCharCode,
      a = String.fromCodePoint;
    r(r.S + r.F * (!!a && 1 != a.length), "String", {
      fromCodePoint: function (t) {
        for (var e, n = [], r = arguments.length, a = 0; r > a; ) {
          if (((e = +arguments[a++]), o(e, 1114111) !== e))
            throw RangeError(e + " is not a valid code point");
          n.push(
            e < 65536
              ? i(e)
              : i(55296 + ((e -= 65536) >> 10), (e % 1024) + 56320)
          );
        }
        return n.join("");
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(15),
      i = n(6);
    r(r.S, "String", {
      raw: function (t) {
        for (
          var e = o(t.raw),
            n = i(e.length),
            r = arguments.length,
            a = [],
            u = 0;
          n > u;

        )
          a.push(String(e[u++])), u < r && a.push(String(arguments[u]));
        return a.join("");
      },
    });
  },
  function (t, e, n) {
    "use strict";
    n(39)("trim", function (t) {
      return function () {
        return t(this, 3);
      };
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(74)(!0);
    n(75)(
      String,
      "String",
      function (t) {
        (this._t = String(t)), (this._i = 0);
      },
      function () {
        var t,
          e = this._t,
          n = this._i;
        return n >= e.length
          ? { value: undefined, done: !0 }
          : ((t = r(e, n)), (this._i += t.length), { value: t, done: !1 });
      }
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(74)(!1);
    r(r.P, "String", {
      codePointAt: function (t) {
        return o(this, t);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(6),
      i = n(76),
      a = "endsWith",
      u = "".endsWith;
    r(r.P + r.F * n(78)(a), "String", {
      endsWith: function (t) {
        var e = i(this, t, a),
          n = arguments.length > 1 ? arguments[1] : undefined,
          r = o(e.length),
          c = n === undefined ? r : Math.min(o(n), r),
          s = String(t);
        return u ? u.call(e, s, c) : e.slice(c - s.length, c) === s;
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(76),
      i = "includes";
    r(r.P + r.F * n(78)(i), "String", {
      includes: function (t) {
        return !!~o(this, t, i).indexOf(
          t,
          arguments.length > 1 ? arguments[1] : undefined
        );
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.P, "String", { repeat: n(71) });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(6),
      i = n(76),
      a = "startsWith",
      u = "".startsWith;
    r(r.P + r.F * n(78)(a), "String", {
      startsWith: function (t) {
        var e = i(this, t, a),
          n = o(
            Math.min(arguments.length > 1 ? arguments[1] : undefined, e.length)
          ),
          r = String(t);
        return u ? u.call(e, r, n) : e.slice(n, n + r.length) === r;
      },
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("anchor", function (t) {
      return function (e) {
        return t(this, "a", "name", e);
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("big", function (t) {
      return function () {
        return t(this, "big", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("blink", function (t) {
      return function () {
        return t(this, "blink", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("bold", function (t) {
      return function () {
        return t(this, "b", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("fixed", function (t) {
      return function () {
        return t(this, "tt", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("fontcolor", function (t) {
      return function (e) {
        return t(this, "font", "color", e);
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("fontsize", function (t) {
      return function (e) {
        return t(this, "font", "size", e);
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("italics", function (t) {
      return function () {
        return t(this, "i", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("link", function (t) {
      return function (e) {
        return t(this, "a", "href", e);
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("small", function (t) {
      return function () {
        return t(this, "small", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("strike", function (t) {
      return function () {
        return t(this, "strike", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("sub", function (t) {
      return function () {
        return t(this, "sub", "", "");
      };
    });
  },
  function (t, e, n) {
    "use strict";
    n(12)("sup", function (t) {
      return function () {
        return t(this, "sup", "", "");
      };
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Date", {
      now: function () {
        return new Date().getTime();
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(10),
      i = n(26);
    r(
      r.P +
        r.F *
          n(2)(function () {
            return (
              null !== new Date(NaN).toJSON() ||
              1 !==
                Date.prototype.toJSON.call({
                  toISOString: function () {
                    return 1;
                  },
                })
            );
          }),
      "Date",
      {
        toJSON: function (t) {
          var e = o(this),
            n = i(e);
          return "number" != typeof n || isFinite(n) ? e.toISOString() : null;
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0),
      o = n(216);
    r(r.P + r.F * (Date.prototype.toISOString !== o), "Date", {
      toISOString: o,
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(2),
      o = Date.prototype.getTime,
      i = Date.prototype.toISOString,
      a = function (t) {
        return t > 9 ? t : "0" + t;
      };
    t.exports =
      r(function () {
        return "0385-07-25T07:06:39.999Z" != i.call(new Date(-50000000000001));
      }) ||
      !r(function () {
        i.call(new Date(NaN));
      })
        ? function () {
            if (!isFinite(o.call(this))) throw RangeError("Invalid time value");
            var t = this,
              e = t.getUTCFullYear(),
              n = t.getUTCMilliseconds(),
              r = e < 0 ? "-" : e > 9999 ? "+" : "";
            return (
              r +
              ("00000" + Math.abs(e)).slice(r ? -6 : -4) +
              "-" +
              a(t.getUTCMonth() + 1) +
              "-" +
              a(t.getUTCDate()) +
              "T" +
              a(t.getUTCHours()) +
              ":" +
              a(t.getUTCMinutes()) +
              ":" +
              a(t.getUTCSeconds()) +
              "." +
              (n > 99 ? n : "0" + a(n)) +
              "Z"
            );
          }
        : i;
  },
  function (t, e, n) {
    var r = Date.prototype,
      o = "Invalid Date",
      i = "toString",
      a = r.toString,
      u = r.getTime;
    new Date(NaN) + "" != o &&
      n(11)(r, i, function () {
        var t = u.call(this);
        return t == t ? a.call(this) : o;
      });
  },
  function (t, e, n) {
    var r = n(5)("toPrimitive"),
      o = Date.prototype;
    r in o || n(14)(o, r, n(219));
  },
  function (t, e, n) {
    "use strict";
    var r = n(3),
      o = n(26),
      i = "number";
    t.exports = function (t) {
      if ("string" !== t && t !== i && "default" !== t)
        throw TypeError("Incorrect hint");
      return o(r(this), t != i);
    };
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Array", { isArray: n(52) });
  },
  function (t, e, n) {
    "use strict";
    var r = n(17),
      o = n(0),
      i = n(10),
      a = n(108),
      u = n(79),
      c = n(6),
      s = n(80),
      f = n(81);
    o(
      o.S +
        o.F *
          !n(53)(function (t) {
            Array.from(t);
          }),
      "Array",
      {
        from: function (t) {
          var e,
            n,
            o,
            l,
            p = i(t),
            h = "function" == typeof this ? this : Array,
            d = arguments.length,
            y = d > 1 ? arguments[1] : undefined,
            m = y !== undefined,
            v = 0,
            g = f(p);
          if (
            (m && (y = r(y, d > 2 ? arguments[2] : undefined, 2)),
            g == undefined || (h == Array && u(g)))
          )
            for (n = new h((e = c(p.length))); e > v; v++)
              s(n, v, m ? y(p[v], v) : p[v]);
          else
            for (l = g.call(p), n = new h(); !(o = l.next()).done; v++)
              s(n, v, m ? a(l, y, [o.value, v], !0) : o.value);
          return (n.length = v), n;
        },
      }
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(80);
    r(
      r.S +
        r.F *
          n(2)(function () {
            function t() {}
            return !(Array.of.call(t) instanceof t);
          }),
      "Array",
      {
        of: function () {
          for (
            var t = 0,
              e = arguments.length,
              n = new ("function" == typeof this ? this : Array)(e);
            e > t;

          )
            o(n, t, arguments[t++]);
          return (n.length = e), n;
        },
      }
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(15),
      i = [].join;
    r(r.P + r.F * (n(44) != Object || !n(16)(i)), "Array", {
      join: function (t) {
        return i.call(o(this), t === undefined ? "," : t);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(67),
      i = n(23),
      a = n(32),
      u = n(6),
      c = [].slice;
    r(
      r.P +
        r.F *
          n(2)(function () {
            o && c.call(o);
          }),
      "Array",
      {
        slice: function (t, e) {
          var n = u(this.length),
            r = i(this);
          if (((e = e === undefined ? n : e), "Array" == r))
            return c.call(this, t, e);
          for (
            var o = a(t, n), s = a(e, n), f = u(s - o), l = new Array(f), p = 0;
            p < f;
            p++
          )
            l[p] = "String" == r ? this.charAt(o + p) : this[o + p];
          return l;
        },
      }
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(18),
      i = n(10),
      a = n(2),
      u = [].sort,
      c = [1, 2, 3];
    r(
      r.P +
        r.F *
          (a(function () {
            c.sort(undefined);
          }) ||
            !a(function () {
              c.sort(null);
            }) ||
            !n(16)(u)),
      "Array",
      {
        sort: function (t) {
          return t === undefined ? u.call(i(this)) : u.call(i(this), o(t));
        },
      }
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(0),
      i = n(16)([].forEach, !0);
    r(r.P + r.F * !i, "Array", {
      forEach: function (t) {
        return o(this, t, arguments[1]);
      },
    });
  },
  function (t, e, n) {
    var r = n(4),
      o = n(52),
      i = n(5)("species");
    t.exports = function (t) {
      var e;
      return (
        o(t) &&
          ("function" != typeof (e = t.constructor) ||
            (e !== Array && !o(e.prototype)) ||
            (e = undefined),
          r(e) && null === (e = e[i]) && (e = undefined)),
        e === undefined ? Array : e
      );
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(1);
    r(r.P + r.F * !n(16)([].map, !0), "Array", {
      map: function (t) {
        return o(this, t, arguments[1]);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(2);
    r(r.P + r.F * !n(16)([].filter, !0), "Array", {
      filter: function (t) {
        return o(this, t, arguments[1]);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(3);
    r(r.P + r.F * !n(16)([].some, !0), "Array", {
      some: function (t) {
        return o(this, t, arguments[1]);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(4);
    r(r.P + r.F * !n(16)([].every, !0), "Array", {
      every: function (t) {
        return o(this, t, arguments[1]);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(110);
    r(r.P + r.F * !n(16)([].reduce, !0), "Array", {
      reduce: function (t) {
        return o(this, t, arguments.length, arguments[1], !1);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(110);
    r(r.P + r.F * !n(16)([].reduceRight, !0), "Array", {
      reduceRight: function (t) {
        return o(this, t, arguments.length, arguments[1], !0);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(50)(!1),
      i = [].indexOf,
      a = !!i && 1 / [1].indexOf(1, -0) < 0;
    r(r.P + r.F * (a || !n(16)(i)), "Array", {
      indexOf: function (t) {
        return a ? i.apply(this, arguments) || 0 : o(this, t, arguments[1]);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(15),
      i = n(19),
      a = n(6),
      u = [].lastIndexOf,
      c = !!u && 1 / [1].lastIndexOf(1, -0) < 0;
    r(r.P + r.F * (c || !n(16)(u)), "Array", {
      lastIndexOf: function (t) {
        if (c) return u.apply(this, arguments) || 0;
        var e = o(this),
          n = a(e.length),
          r = n - 1;
        for (
          arguments.length > 1 && (r = Math.min(r, i(arguments[1]))),
            r < 0 && (r = n + r);
          r >= 0;
          r--
        )
          if (r in e && e[r] === t) return r || 0;
        return -1;
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.P, "Array", { copyWithin: n(111) }), n(36)("copyWithin");
  },
  function (t, e, n) {
    var r = n(0);
    r(r.P, "Array", { fill: n(82) }), n(36)("fill");
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(5),
      i = "find",
      a = !0;
    i in [] &&
      Array(1).find(function () {
        a = !1;
      }),
      r(r.P + r.F * a, "Array", {
        find: function (t) {
          return o(this, t, arguments.length > 1 ? arguments[1] : undefined);
        },
      }),
      n(36)(i);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(22)(6),
      i = "findIndex",
      a = !0;
    i in [] &&
      Array(1)[i](function () {
        a = !1;
      }),
      r(r.P + r.F * a, "Array", {
        findIndex: function (t) {
          return o(this, t, arguments.length > 1 ? arguments[1] : undefined);
        },
      }),
      n(36)(i);
  },
  function (t, e, n) {
    n(41)("Array");
  },
  function (t, e, n) {
    var r = n(1),
      o = n(70),
      i = n(9).f,
      a = n(34).f,
      u = n(77),
      c = n(54),
      s = r.RegExp,
      f = s,
      l = s.prototype,
      p = /a/g,
      h = /a/g,
      d = new s(p) !== p;
    if (
      n(8) &&
      (!d ||
        n(2)(function () {
          return (
            (h[n(5)("match")] = !1),
            s(p) != p || s(h) == h || "/a/i" != s(p, "i")
          );
        }))
    ) {
      s = function (t, e) {
        var n = this instanceof s,
          r = u(t),
          i = e === undefined;
        return !n && r && t.constructor === s && i
          ? t
          : o(
              d
                ? new f(r && !i ? t.source : t, e)
                : f(
                    (r = t instanceof s) ? t.source : t,
                    r && i ? c.call(t) : e
                  ),
              n ? this : l,
              s
            );
      };
      for (
        var y = function (t) {
            (t in s) ||
              i(s, t, {
                configurable: !0,
                get: function () {
                  return f[t];
                },
                set: function (e) {
                  f[t] = e;
                },
              });
          },
          m = a(f),
          v = 0;
        m.length > v;

      )
        y(m[v++]);
      (l.constructor = s), (s.prototype = l), n(11)(r, "RegExp", s);
    }
    n(41)("RegExp");
  },
  function (t, e, n) {
    "use strict";
    n(114);
    var r = n(3),
      o = n(54),
      i = n(8),
      a = "toString",
      u = /./.toString,
      c = function (t) {
        n(11)(RegExp.prototype, a, t, !0);
      };
    n(2)(function () {
      return "/a/b" != u.call({ source: "a", flags: "b" });
    })
      ? c(function () {
          var t = r(this);
          return "/".concat(
            t.source,
            "/",
            "flags" in t
              ? t.flags
              : !i && t instanceof RegExp
              ? o.call(t)
              : undefined
          );
        })
      : u.name != a &&
        c(function () {
          return u.call(this);
        });
  },
  function (t, e, n) {
    "use strict";
    var r = n(3),
      o = n(6),
      i = n(85),
      a = n(55);
    n(56)("match", 1, function (t, e, n, u) {
      return [
        function (n) {
          var r = t(this),
            o = n == undefined ? undefined : n[e];
          return o !== undefined ? o.call(n, r) : new RegExp(n)[e](String(r));
        },
        function (t) {
          var e = u(n, t, this);
          if (e.done) return e.value;
          var c = r(t),
            s = String(this);
          if (!c.global) return a(c, s);
          var f = c.unicode;
          c.lastIndex = 0;
          for (var l, p = [], h = 0; null !== (l = a(c, s)); ) {
            var d = String(l[0]);
            (p[h] = d),
              "" === d && (c.lastIndex = i(s, o(c.lastIndex), f)),
              h++;
          }
          return 0 === h ? null : p;
        },
      ];
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(3),
      o = n(10),
      i = n(6),
      a = n(19),
      u = n(85),
      c = n(55),
      s = Math.max,
      f = Math.min,
      l = Math.floor,
      p = /\$([$&`']|\d\d?|<[^>]*>)/g,
      h = /\$([$&`']|\d\d?)/g;
    n(56)("replace", 2, function (t, e, n, d) {
      return [
        function (r, o) {
          var i = t(this),
            a = r == undefined ? undefined : r[e];
          return a !== undefined ? a.call(r, i, o) : n.call(String(i), r, o);
        },
        function (t, e) {
          var o = d(n, t, this, e);
          if (o.done) return o.value;
          var l = r(t),
            p = String(this),
            h = "function" == typeof e;
          h || (e = String(e));
          var m = l.global;
          if (m) {
            var v = l.unicode;
            l.lastIndex = 0;
          }
          for (var g = []; ; ) {
            var b = c(l, p);
            if (null === b) break;
            if ((g.push(b), !m)) break;
            "" === String(b[0]) && (l.lastIndex = u(p, i(l.lastIndex), v));
          }
          for (var w, _ = "", E = 0, T = 0; T < g.length; T++) {
            b = g[T];
            for (
              var S = String(b[0]),
                x = s(f(a(b.index), p.length), 0),
                A = [],
                R = 1;
              R < b.length;
              R++
            )
              A.push((w = b[R]) === undefined ? w : String(w));
            var I = b.groups;
            if (h) {
              var O = [S].concat(A, x, p);
              I !== undefined && O.push(I);
              var P = String(e.apply(undefined, O));
            } else P = y(S, p, x, A, I, e);
            x >= E && ((_ += p.slice(E, x) + P), (E = x + S.length));
          }
          return _ + p.slice(E);
        },
      ];
      function y(t, e, r, i, a, u) {
        var c = r + t.length,
          s = i.length,
          f = h;
        return (
          a !== undefined && ((a = o(a)), (f = p)),
          n.call(u, f, function (n, o) {
            var u;
            switch (o.charAt(0)) {
              case "$":
                return "$";
              case "&":
                return t;
              case "`":
                return e.slice(0, r);
              case "'":
                return e.slice(c);
              case "<":
                u = a[o.slice(1, -1)];
                break;
              default:
                var f = +o;
                if (0 === f) return n;
                if (f > s) {
                  var p = l(f / 10);
                  return 0 === p
                    ? n
                    : p <= s
                    ? i[p - 1] === undefined
                      ? o.charAt(1)
                      : i[p - 1] + o.charAt(1)
                    : n;
                }
                u = i[f - 1];
            }
            return u === undefined ? "" : u;
          })
        );
      }
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(3),
      o = n(99),
      i = n(55);
    n(56)("search", 1, function (t, e, n, a) {
      return [
        function (n) {
          var r = t(this),
            o = n == undefined ? undefined : n[e];
          return o !== undefined ? o.call(n, r) : new RegExp(n)[e](String(r));
        },
        function (t) {
          var e = a(n, t, this);
          if (e.done) return e.value;
          var u = r(t),
            c = String(this),
            s = u.lastIndex;
          o(s, 0) || (u.lastIndex = 0);
          var f = i(u, c);
          return (
            o(u.lastIndex, s) || (u.lastIndex = s), null === f ? -1 : f.index
          );
        },
      ];
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(77),
      o = n(3),
      i = n(47),
      a = n(85),
      u = n(6),
      c = n(55),
      s = n(84),
      f = n(2),
      l = Math.min,
      p = [].push,
      h = 4294967295,
      d = !f(function () {
        RegExp(h, "y");
      });
    n(56)("split", 2, function (t, e, n, f) {
      var y;
      return (
        (y =
          "c" == "abbc".split(/(b)*/)[1] ||
          4 != "test".split(/(?:)/, -1).length ||
          2 != "ab".split(/(?:ab)*/).length ||
          4 != ".".split(/(.?)(.?)/).length ||
          ".".split(/()()/).length > 1 ||
          "".split(/.?/).length
            ? function (t, e) {
                var o = String(this);
                if (t === undefined && 0 === e) return [];
                if (!r(t)) return n.call(o, t, e);
                for (
                  var i,
                    a,
                    u,
                    c = [],
                    f =
                      (t.ignoreCase ? "i" : "") +
                      (t.multiline ? "m" : "") +
                      (t.unicode ? "u" : "") +
                      (t.sticky ? "y" : ""),
                    l = 0,
                    d = e === undefined ? h : e >>> 0,
                    y = new RegExp(t.source, f + "g");
                  (i = s.call(y, o)) &&
                  !(
                    (a = y.lastIndex) > l &&
                    (c.push(o.slice(l, i.index)),
                    i.length > 1 &&
                      i.index < o.length &&
                      p.apply(c, i.slice(1)),
                    (u = i[0].length),
                    (l = a),
                    c.length >= d)
                  );

                )
                  y.lastIndex === i.index && y.lastIndex++;
                return (
                  l === o.length
                    ? (!u && y.test("")) || c.push("")
                    : c.push(o.slice(l)),
                  c.length > d ? c.slice(0, d) : c
                );
              }
            : "0".split(undefined, 0).length
            ? function (t, e) {
                return t === undefined && 0 === e ? [] : n.call(this, t, e);
              }
            : n),
        [
          function (n, r) {
            var o = t(this),
              i = n == undefined ? undefined : n[e];
            return i !== undefined ? i.call(n, o, r) : y.call(String(o), n, r);
          },
          function (t, e) {
            var r = f(y, t, this, e, y !== n);
            if (r.done) return r.value;
            var s = o(t),
              p = String(this),
              m = i(s, RegExp),
              v = s.unicode,
              g =
                (s.ignoreCase ? "i" : "") +
                (s.multiline ? "m" : "") +
                (s.unicode ? "u" : "") +
                (d ? "y" : "g"),
              b = new m(d ? s : "^(?:" + s.source + ")", g),
              w = e === undefined ? h : e >>> 0;
            if (0 === w) return [];
            if (0 === p.length) return null === c(b, p) ? [p] : [];
            for (var _ = 0, E = 0, T = []; E < p.length; ) {
              b.lastIndex = d ? E : 0;
              var S,
                x = c(b, d ? p : p.slice(E));
              if (
                null === x ||
                (S = l(u(b.lastIndex + (d ? 0 : E)), p.length)) === _
              )
                E = a(p, E, v);
              else {
                if ((T.push(p.slice(_, E)), T.length === w)) return T;
                for (var A = 1; A <= x.length - 1; A++)
                  if ((T.push(x[A]), T.length === w)) return T;
                E = _ = S;
              }
            }
            return T.push(p.slice(_)), T;
          },
        ]
      );
    });
  },
  function (t, e, n) {
    var r = n(1),
      o = n(86).set,
      i = r.MutationObserver || r.WebKitMutationObserver,
      a = r.process,
      u = r.Promise,
      c = "process" == n(23)(a);
    t.exports = function () {
      var t,
        e,
        n,
        s = function () {
          var r, o;
          for (c && (r = a.domain) && r.exit(); t; ) {
            (o = t.fn), (t = t.next);
            try {
              o();
            } catch (i) {
              throw (t ? n() : (e = undefined), i);
            }
          }
          (e = undefined), r && r.enter();
        };
      if (c)
        n = function () {
          a.nextTick(s);
        };
      else if (!i || (r.navigator && r.navigator.standalone))
        if (u && u.resolve) {
          var f = u.resolve(undefined);
          n = function () {
            f.then(s);
          };
        } else
          n = function () {
            o.call(r, s);
          };
      else {
        var l = !0,
          p = document.createTextNode("");
        new i(s).observe(p, { characterData: !0 }),
          (n = function () {
            p.data = l = !l;
          });
      }
      return function (r) {
        var o = { fn: r, next: undefined };
        e && (e.next = o), t || ((t = o), n()), (e = o);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      try {
        return { e: !1, v: t() };
      } catch (e) {
        return { e: !0, v: e };
      }
    };
  },
  function (t, e, n) {
    "use strict";
    var r = n(118),
      o = n(37),
      i = "Map";
    t.exports = n(59)(
      i,
      function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : undefined);
        };
      },
      {
        get: function (t) {
          var e = r.getEntry(o(this, i), t);
          return e && e.v;
        },
        set: function (t, e) {
          return r.def(o(this, i), 0 === t ? 0 : t, e);
        },
      },
      r,
      !0
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(118),
      o = n(37);
    t.exports = n(59)(
      "Set",
      function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : undefined);
        };
      },
      {
        add: function (t) {
          return r.def(o(this, "Set"), (t = 0 === t ? 0 : t), t);
        },
      },
      r
    );
  },
  function (t, e, n) {
    "use strict";
    var r,
      o = n(1),
      i = n(22)(0),
      a = n(11),
      u = n(27),
      c = n(98),
      s = n(119),
      f = n(4),
      l = n(37),
      p = n(37),
      h = !o.ActiveXObject && "ActiveXObject" in o,
      d = "WeakMap",
      y = u.getWeak,
      m = Object.isExtensible,
      v = s.ufstore,
      g = function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : undefined);
        };
      },
      b = {
        get: function (t) {
          if (f(t)) {
            var e = y(t);
            return !0 === e ? v(l(this, d)).get(t) : e ? e[this._i] : undefined;
          }
        },
        set: function (t, e) {
          return s.def(l(this, d), t, e);
        },
      },
      w = (t.exports = n(59)(d, g, b, s, !0, !0));
    p &&
      h &&
      (c((r = s.getConstructor(g, d)).prototype, b),
      (u.NEED = !0),
      i(["delete", "has", "get", "set"], function (t) {
        var e = w.prototype,
          n = e[t];
        a(e, t, function (e, o) {
          if (f(e) && !m(e)) {
            this._f || (this._f = new r());
            var i = this._f[t](e, o);
            return "set" == t ? this : i;
          }
          return n.call(this, e, o);
        });
      }));
  },
  function (t, e, n) {
    "use strict";
    var r = n(119),
      o = n(37),
      i = "WeakSet";
    n(59)(
      i,
      function (t) {
        return function () {
          return t(this, arguments.length > 0 ? arguments[0] : undefined);
        };
      },
      {
        add: function (t) {
          return r.def(o(this, i), t, !0);
        },
      },
      r,
      !1,
      !0
    );
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(60),
      i = n(87),
      a = n(3),
      u = n(32),
      c = n(6),
      s = n(4),
      f = n(1).ArrayBuffer,
      l = n(47),
      p = i.ArrayBuffer,
      h = i.DataView,
      d = o.ABV && f.isView,
      y = p.prototype.slice,
      m = o.VIEW,
      v = "ArrayBuffer";
    r(r.G + r.W + r.F * (f !== p), { ArrayBuffer: p }),
      r(r.S + r.F * !o.CONSTR, v, {
        isView: function (t) {
          return (d && d(t)) || (s(t) && m in t);
        },
      }),
      r(
        r.P +
          r.U +
          r.F *
            n(2)(function () {
              return !new p(2).slice(1, undefined).byteLength;
            }),
        v,
        {
          slice: function (t, e) {
            if (y !== undefined && e === undefined) return y.call(a(this), t);
            for (
              var n = a(this).byteLength,
                r = u(t, n),
                o = u(e === undefined ? n : e, n),
                i = new (l(this, p))(c(o - r)),
                s = new h(this),
                f = new h(i),
                d = 0;
              r < o;

            )
              f.setUint8(d++, s.getUint8(r++));
            return i;
          },
        }
      ),
      n(41)(v);
  },
  function (t, e, n) {
    var r = n(0);
    r(r.G + r.W + r.F * !n(60).ABV, { DataView: n(87).DataView });
  },
  function (t, e, n) {
    n(25)("Int8", 1, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)("Uint8", 1, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)(
      "Uint8",
      1,
      function (t) {
        return function (e, n, r) {
          return t(this, e, n, r);
        };
      },
      !0
    );
  },
  function (t, e, n) {
    n(25)("Int16", 2, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)("Uint16", 2, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)("Int32", 4, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)("Uint32", 4, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)("Float32", 4, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    n(25)("Float64", 8, function (t) {
      return function (e, n, r) {
        return t(this, e, n, r);
      };
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(18),
      i = n(3),
      a = (n(1).Reflect || {}).apply,
      u = Function.apply;
    r(
      r.S +
        r.F *
          !n(2)(function () {
            a(function () {});
          }),
      "Reflect",
      {
        apply: function (t, e, n) {
          var r = o(t),
            c = i(n);
          return a ? a(r, e, c) : u.call(r, e, c);
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0),
      o = n(33),
      i = n(18),
      a = n(3),
      u = n(4),
      c = n(2),
      s = n(100),
      f = (n(1).Reflect || {}).construct,
      l = c(function () {
        function t() {}
        return !(f(function () {}, [], t) instanceof t);
      }),
      p = !c(function () {
        f(function () {});
      });
    r(r.S + r.F * (l || p), "Reflect", {
      construct: function (t, e) {
        i(t), a(e);
        var n = arguments.length < 3 ? t : i(arguments[2]);
        if (p && !l) return f(t, e, n);
        if (t == n) {
          switch (e.length) {
            case 0:
              return new t();
            case 1:
              return new t(e[0]);
            case 2:
              return new t(e[0], e[1]);
            case 3:
              return new t(e[0], e[1], e[2]);
            case 4:
              return new t(e[0], e[1], e[2], e[3]);
          }
          var r = [null];
          return r.push.apply(r, e), new (s.apply(t, r))();
        }
        var c = n.prototype,
          h = o(u(c) ? c : Object.prototype),
          d = Function.apply.call(t, h, e);
        return u(d) ? d : h;
      },
    });
  },
  function (t, e, n) {
    var r = n(9),
      o = n(0),
      i = n(3),
      a = n(26);
    o(
      o.S +
        o.F *
          n(2)(function () {
            Reflect.defineProperty(r.f({}, 1, { value: 1 }), 1, { value: 2 });
          }),
      "Reflect",
      {
        defineProperty: function (t, e, n) {
          i(t), (e = a(e, !0)), i(n);
          try {
            return r.f(t, e, n), !0;
          } catch (o) {
            return !1;
          }
        },
      }
    );
  },
  function (t, e, n) {
    var r = n(0),
      o = n(20).f,
      i = n(3);
    r(r.S, "Reflect", {
      deleteProperty: function (t, e) {
        var n = o(i(t), e);
        return !(n && !n.configurable) && delete t[e];
      },
    });
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(3),
      i = function (t) {
        (this._t = o(t)), (this._i = 0);
        var e,
          n = (this._k = []);
        for (e in t) n.push(e);
      };
    n(107)(i, "Object", function () {
      var t,
        e = this,
        n = e._k;
      do {
        if (e._i >= n.length) return { value: undefined, done: !0 };
      } while (!((t = n[e._i++]) in e._t));
      return { value: t, done: !1 };
    }),
      r(r.S, "Reflect", {
        enumerate: function (t) {
          return new i(t);
        },
      });
  },
  function (t, e, n) {
    var r = n(20),
      o = n(35),
      i = n(13),
      a = n(0),
      u = n(4),
      c = n(3);
    a(a.S, "Reflect", {
      get: function s(t, e) {
        var n,
          a,
          f = arguments.length < 3 ? t : arguments[2];
        return c(t) === f
          ? t[e]
          : (n = r.f(t, e))
          ? i(n, "value")
            ? n.value
            : n.get !== undefined
            ? n.get.call(f)
            : undefined
          : u((a = o(t)))
          ? s(a, e, f)
          : void 0;
      },
    });
  },
  function (t, e, n) {
    var r = n(20),
      o = n(0),
      i = n(3);
    o(o.S, "Reflect", {
      getOwnPropertyDescriptor: function (t, e) {
        return r.f(i(t), e);
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(35),
      i = n(3);
    r(r.S, "Reflect", {
      getPrototypeOf: function (t) {
        return o(i(t));
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Reflect", {
      has: function (t, e) {
        return e in t;
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(3),
      i = Object.isExtensible;
    r(r.S, "Reflect", {
      isExtensible: function (t) {
        return o(t), !i || i(t);
      },
    });
  },
  function (t, e, n) {
    var r = n(0);
    r(r.S, "Reflect", { ownKeys: n(121) });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(3),
      i = Object.preventExtensions;
    r(r.S, "Reflect", {
      preventExtensions: function (t) {
        o(t);
        try {
          return i && i(t), !0;
        } catch (e) {
          return !1;
        }
      },
    });
  },
  function (t, e, n) {
    var r = n(9),
      o = n(20),
      i = n(35),
      a = n(13),
      u = n(0),
      c = n(28),
      s = n(3),
      f = n(4);
    u(u.S, "Reflect", {
      set: function l(t, e, n) {
        var u,
          p,
          h = arguments.length < 4 ? t : arguments[3],
          d = o.f(s(t), e);
        if (!d) {
          if (f((p = i(t)))) return l(p, e, n, h);
          d = c(0);
        }
        if (a(d, "value")) {
          if (!1 === d.writable || !f(h)) return !1;
          if ((u = o.f(h, e))) {
            if (u.get || u.set || !1 === u.writable) return !1;
            (u.value = n), r.f(h, e, u);
          } else r.f(h, e, c(0, n));
          return !0;
        }
        return d.set !== undefined && (d.set.call(h, n), !0);
      },
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(68);
    o &&
      r(r.S, "Reflect", {
        setPrototypeOf: function (t, e) {
          o.check(t, e);
          try {
            return o.set(t, e), !0;
          } catch (n) {
            return !1;
          }
        },
      });
  },
  function (t, e, n) {
    n(279), (t.exports = n(7).Array.includes);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(50)(!0);
    r(r.P, "Array", {
      includes: function (t) {
        return o(this, t, arguments.length > 1 ? arguments[1] : undefined);
      },
    }),
      n(36)("includes");
  },
  function (t, e, n) {
    n(281), (t.exports = n(7).Array.flatMap);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(282),
      i = n(10),
      a = n(6),
      u = n(18),
      c = n(109);
    r(r.P, "Array", {
      flatMap: function (t) {
        var e,
          n,
          r = i(this);
        return (
          u(t),
          (e = a(r.length)),
          (n = c(r, 0)),
          o(n, r, r, e, 0, 1, t, arguments[1]),
          n
        );
      },
    }),
      n(36)("flatMap");
  },
  function (t, e, n) {
    "use strict";
    var r = n(52),
      o = n(4),
      i = n(6),
      a = n(17),
      u = n(5)("isConcatSpreadable");
    t.exports = function c(t, e, n, s, f, l, p, h) {
      for (var d, y, m = f, v = 0, g = !!p && a(p, h, 3); v < s; ) {
        if (v in n) {
          if (
            ((d = g ? g(n[v], v, e) : n[v]),
            (y = !1),
            o(d) && (y = (y = d[u]) !== undefined ? !!y : r(d)),
            y && l > 0)
          )
            m = c(t, e, d, i(d.length), m, l - 1) - 1;
          else {
            if (m >= 9007199254740991) throw TypeError();
            t[m] = d;
          }
          m++;
        }
        v++;
      }
      return m;
    };
  },
  function (t, e, n) {
    n(284), (t.exports = n(7).String.padStart);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(122),
      i = n(58),
      a = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(i);
    r(r.P + r.F * a, "String", {
      padStart: function (t) {
        return o(this, t, arguments.length > 1 ? arguments[1] : undefined, !0);
      },
    });
  },
  function (t, e, n) {
    n(286), (t.exports = n(7).String.padEnd);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(122),
      i = n(58),
      a = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(i);
    r(r.P + r.F * a, "String", {
      padEnd: function (t) {
        return o(this, t, arguments.length > 1 ? arguments[1] : undefined, !1);
      },
    });
  },
  function (t, e, n) {
    n(288), (t.exports = n(7).String.trimLeft);
  },
  function (t, e, n) {
    "use strict";
    n(39)(
      "trimLeft",
      function (t) {
        return function () {
          return t(this, 1);
        };
      },
      "trimStart"
    );
  },
  function (t, e, n) {
    n(290), (t.exports = n(7).String.trimRight);
  },
  function (t, e, n) {
    "use strict";
    n(39)(
      "trimRight",
      function (t) {
        return function () {
          return t(this, 2);
        };
      },
      "trimEnd"
    );
  },
  function (t, e, n) {
    n(292), (t.exports = n(64).f("asyncIterator"));
  },
  function (t, e, n) {
    n(94)("asyncIterator");
  },
  function (t, e, n) {
    n(294), (t.exports = n(7).Object.getOwnPropertyDescriptors);
  },
  function (t, e, n) {
    var r = n(0),
      o = n(121),
      i = n(15),
      a = n(20),
      u = n(80);
    r(r.S, "Object", {
      getOwnPropertyDescriptors: function (t) {
        for (
          var e, n, r = i(t), c = a.f, s = o(r), f = {}, l = 0;
          s.length > l;

        )
          (n = c(r, (e = s[l++]))) !== undefined && u(f, e, n);
        return f;
      },
    });
  },
  function (t, e, n) {
    n(296), (t.exports = n(7).Object.values);
  },
  function (t, e, n) {
    var r = n(0),
      o = n(123)(!1);
    r(r.S, "Object", {
      values: function (t) {
        return o(t);
      },
    });
  },
  function (t, e, n) {
    n(298), (t.exports = n(7).Object.entries);
  },
  function (t, e, n) {
    var r = n(0),
      o = n(123)(!0);
    r(r.S, "Object", {
      entries: function (t) {
        return o(t);
      },
    });
  },
  function (t, e, n) {
    "use strict";
    n(115), n(300), (t.exports = n(7).Promise["finally"]);
  },
  function (t, e, n) {
    "use strict";
    var r = n(0),
      o = n(7),
      i = n(1),
      a = n(47),
      u = n(117);
    r(r.P + r.R, "Promise", {
      finally: function (t) {
        var e = a(this, o.Promise || i.Promise),
          n = "function" == typeof t;
        return this.then(
          n
            ? function (n) {
                return u(e, t()).then(function () {
                  return n;
                });
              }
            : t,
          n
            ? function (n) {
                return u(e, t()).then(function () {
                  throw n;
                });
              }
            : t
        );
      },
    });
  },
  function (t, e, n) {
    n(302), n(303), n(304), (t.exports = n(7));
  },
  function (t, e, n) {
    var r = n(1),
      o = n(0),
      i = n(58),
      a = [].slice,
      u = /MSIE .\./.test(i),
      c = function (t) {
        return function (e, n) {
          var r = arguments.length > 2,
            o = !!r && a.call(arguments, 2);
          return t(
            r
              ? function () {
                  ("function" == typeof e ? e : Function(e)).apply(this, o);
                }
              : e,
            n
          );
        };
      };
    o(o.G + o.B + o.F * u, {
      setTimeout: c(r.setTimeout),
      setInterval: c(r.setInterval),
    });
  },
  function (t, e, n) {
    var r = n(0),
      o = n(86);
    r(r.G + r.B, { setImmediate: o.set, clearImmediate: o.clear });
  },
  function (t, e, n) {
    for (
      var r = n(83),
        o = n(31),
        i = n(11),
        a = n(1),
        u = n(14),
        c = n(40),
        s = n(5),
        f = s("iterator"),
        l = s("toStringTag"),
        p = c.Array,
        h = {
          CSSRuleList: !0,
          CSSStyleDeclaration: !1,
          CSSValueList: !1,
          ClientRectList: !1,
          DOMRectList: !1,
          DOMStringList: !1,
          DOMTokenList: !0,
          DataTransferItemList: !1,
          FileList: !1,
          HTMLAllCollection: !1,
          HTMLCollection: !1,
          HTMLFormElement: !1,
          HTMLSelectElement: !1,
          MediaList: !0,
          MimeTypeArray: !1,
          NamedNodeMap: !1,
          NodeList: !0,
          PaintRequestList: !1,
          Plugin: !1,
          PluginArray: !1,
          SVGLengthList: !1,
          SVGNumberList: !1,
          SVGPathSegList: !1,
          SVGPointList: !1,
          SVGStringList: !1,
          SVGTransformList: !1,
          SourceBufferList: !1,
          StyleSheetList: !0,
          TextTrackCueList: !1,
          TextTrackList: !1,
          TouchList: !1,
        },
        d = o(h),
        y = 0;
      y < d.length;
      y++
    ) {
      var m,
        v = d[y],
        g = h[v],
        b = a[v],
        w = b && b.prototype;
      if (w && (w[f] || u(w, f, p), w[l] || u(w, l, v), (c[v] = p), g))
        for (m in r) w[m] || i(w, m, r[m], !0);
    }
  },
  function (t, e, n) {
    (function (t) {
      function e(t) {
        return (e =
          "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
            ? function (t) {
                return typeof t;
              }
            : function (t) {
                return t &&
                  "function" == typeof Symbol &&
                  t.constructor === Symbol &&
                  t !== Symbol.prototype
                  ? "symbol"
                  : typeof t;
              })(t);
      }
      var n = (function (t) {
        "use strict";
        var n,
          r = Object.prototype,
          o = r.hasOwnProperty,
          i = "function" == typeof Symbol ? Symbol : {},
          a = i.iterator || "@@iterator",
          u = i.asyncIterator || "@@asyncIterator",
          c = i.toStringTag || "@@toStringTag";
        function s(t, e, n) {
          return (
            Object.defineProperty(t, e, {
              value: n,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            }),
            t[e]
          );
        }
        try {
          s({}, "");
        } catch (N) {
          s = function (t, e, n) {
            return (t[e] = n);
          };
        }
        function f(t, e, n, r) {
          var o = e && e.prototype instanceof v ? e : v,
            i = Object.create(o.prototype),
            a = new O(r || []);
          return (
            (i._invoke = (function (t, e, n) {
              var r = p;
              return function (o, i) {
                if (r === d) throw new Error("Generator is already running");
                if (r === y) {
                  if ("throw" === o) throw i;
                  return L();
                }
                for (n.method = o, n.arg = i; ; ) {
                  var a = n.delegate;
                  if (a) {
                    var u = A(a, n);
                    if (u) {
                      if (u === m) continue;
                      return u;
                    }
                  }
                  if ("next" === n.method) n.sent = n._sent = n.arg;
                  else if ("throw" === n.method) {
                    if (r === p) throw ((r = y), n.arg);
                    n.dispatchException(n.arg);
                  } else "return" === n.method && n.abrupt("return", n.arg);
                  r = d;
                  var c = l(t, e, n);
                  if ("normal" === c.type) {
                    if (((r = n.done ? y : h), c.arg === m)) continue;
                    return { value: c.arg, done: n.done };
                  }
                  "throw" === c.type &&
                    ((r = y), (n.method = "throw"), (n.arg = c.arg));
                }
              };
            })(t, n, a)),
            i
          );
        }
        function l(t, e, n) {
          try {
            return { type: "normal", arg: t.call(e, n) };
          } catch (N) {
            return { type: "throw", arg: N };
          }
        }
        t.wrap = f;
        var p = "suspendedStart",
          h = "suspendedYield",
          d = "executing",
          y = "completed",
          m = {};
        function v() {}
        function g() {}
        function b() {}
        var w = {};
        w[a] = function () {
          return this;
        };
        var _ = Object.getPrototypeOf,
          E = _ && _(_(P([])));
        E && E !== r && o.call(E, a) && (w = E);
        var T = (b.prototype = v.prototype = Object.create(w));
        function S(t) {
          ["next", "throw", "return"].forEach(function (e) {
            s(t, e, function (t) {
              return this._invoke(e, t);
            });
          });
        }
        function x(t, n) {
          function r(i, a, u, c) {
            var s = l(t[i], t, a);
            if ("throw" !== s.type) {
              var f = s.arg,
                p = f.value;
              return p && "object" === e(p) && o.call(p, "__await")
                ? n.resolve(p.__await).then(
                    function (t) {
                      r("next", t, u, c);
                    },
                    function (t) {
                      r("throw", t, u, c);
                    }
                  )
                : n.resolve(p).then(
                    function (t) {
                      (f.value = t), u(f);
                    },
                    function (t) {
                      return r("throw", t, u, c);
                    }
                  );
            }
            c(s.arg);
          }
          var i;
          this._invoke = function (t, e) {
            function o() {
              return new n(function (n, o) {
                r(t, e, n, o);
              });
            }
            return (i = i ? i.then(o, o) : o());
          };
        }
        function A(t, e) {
          var r = t.iterator[e.method];
          if (r === n) {
            if (((e.delegate = null), "throw" === e.method)) {
              if (
                t.iterator["return"] &&
                ((e.method = "return"),
                (e.arg = n),
                A(t, e),
                "throw" === e.method)
              )
                return m;
              (e.method = "throw"),
                (e.arg = new TypeError(
                  "The iterator does not provide a 'throw' method"
                ));
            }
            return m;
          }
          var o = l(r, t.iterator, e.arg);
          if ("throw" === o.type)
            return (
              (e.method = "throw"), (e.arg = o.arg), (e.delegate = null), m
            );
          var i = o.arg;
          return i
            ? i.done
              ? ((e[t.resultName] = i.value),
                (e.next = t.nextLoc),
                "return" !== e.method && ((e.method = "next"), (e.arg = n)),
                (e.delegate = null),
                m)
              : i
            : ((e.method = "throw"),
              (e.arg = new TypeError("iterator result is not an object")),
              (e.delegate = null),
              m);
        }
        function R(t) {
          var e = { tryLoc: t[0] };
          1 in t && (e.catchLoc = t[1]),
            2 in t && ((e.finallyLoc = t[2]), (e.afterLoc = t[3])),
            this.tryEntries.push(e);
        }
        function I(t) {
          var e = t.completion || {};
          (e.type = "normal"), delete e.arg, (t.completion = e);
        }
        function O(t) {
          (this.tryEntries = [{ tryLoc: "root" }]),
            t.forEach(R, this),
            this.reset(!0);
        }
        function P(t) {
          if (t) {
            var e = t[a];
            if (e) return e.call(t);
            if ("function" == typeof t.next) return t;
            if (!isNaN(t.length)) {
              var r = -1,
                i = function e() {
                  for (; ++r < t.length; )
                    if (o.call(t, r)) return (e.value = t[r]), (e.done = !1), e;
                  return (e.value = n), (e.done = !0), e;
                };
              return (i.next = i);
            }
          }
          return { next: L };
        }
        function L() {
          return { value: n, done: !0 };
        }
        return (
          (g.prototype = T.constructor = b),
          (b.constructor = g),
          (g.displayName = s(b, c, "GeneratorFunction")),
          (t.isGeneratorFunction = function (t) {
            var e = "function" == typeof t && t.constructor;
            return (
              !!e &&
              (e === g || "GeneratorFunction" === (e.displayName || e.name))
            );
          }),
          (t.mark = function (t) {
            return (
              Object.setPrototypeOf
                ? Object.setPrototypeOf(t, b)
                : ((t.__proto__ = b), s(t, c, "GeneratorFunction")),
              (t.prototype = Object.create(T)),
              t
            );
          }),
          (t.awrap = function (t) {
            return { __await: t };
          }),
          S(x.prototype),
          (x.prototype[u] = function () {
            return this;
          }),
          (t.AsyncIterator = x),
          (t.async = function (e, n, r, o, i) {
            void 0 === i && (i = Promise);
            var a = new x(f(e, n, r, o), i);
            return t.isGeneratorFunction(n)
              ? a
              : a.next().then(function (t) {
                  return t.done ? t.value : a.next();
                });
          }),
          S(T),
          s(T, c, "Generator"),
          (T[a] = function () {
            return this;
          }),
          (T.toString = function () {
            return "[object Generator]";
          }),
          (t.keys = function (t) {
            var e = [];
            for (var n in t) e.push(n);
            return (
              e.reverse(),
              function r() {
                for (; e.length; ) {
                  var n = e.pop();
                  if (n in t) return (r.value = n), (r.done = !1), r;
                }
                return (r.done = !0), r;
              }
            );
          }),
          (t.values = P),
          (O.prototype = {
            constructor: O,
            reset: function (t) {
              if (
                ((this.prev = 0),
                (this.next = 0),
                (this.sent = this._sent = n),
                (this.done = !1),
                (this.delegate = null),
                (this.method = "next"),
                (this.arg = n),
                this.tryEntries.forEach(I),
                !t)
              )
                for (var e in this)
                  "t" === e.charAt(0) &&
                    o.call(this, e) &&
                    !isNaN(+e.slice(1)) &&
                    (this[e] = n);
            },
            stop: function () {
              this.done = !0;
              var t = this.tryEntries[0].completion;
              if ("throw" === t.type) throw t.arg;
              return this.rval;
            },
            dispatchException: function (t) {
              if (this.done) throw t;
              var e = this;
              function r(r, o) {
                return (
                  (u.type = "throw"),
                  (u.arg = t),
                  (e.next = r),
                  o && ((e.method = "next"), (e.arg = n)),
                  !!o
                );
              }
              for (var i = this.tryEntries.length - 1; i >= 0; --i) {
                var a = this.tryEntries[i],
                  u = a.completion;
                if ("root" === a.tryLoc) return r("end");
                if (a.tryLoc <= this.prev) {
                  var c = o.call(a, "catchLoc"),
                    s = o.call(a, "finallyLoc");
                  if (c && s) {
                    if (this.prev < a.catchLoc) return r(a.catchLoc, !0);
                    if (this.prev < a.finallyLoc) return r(a.finallyLoc);
                  } else if (c) {
                    if (this.prev < a.catchLoc) return r(a.catchLoc, !0);
                  } else {
                    if (!s)
                      throw new Error("try statement without catch or finally");
                    if (this.prev < a.finallyLoc) return r(a.finallyLoc);
                  }
                }
              }
            },
            abrupt: function (t, e) {
              for (var n = this.tryEntries.length - 1; n >= 0; --n) {
                var r = this.tryEntries[n];
                if (
                  r.tryLoc <= this.prev &&
                  o.call(r, "finallyLoc") &&
                  this.prev < r.finallyLoc
                ) {
                  var i = r;
                  break;
                }
              }
              i &&
                ("break" === t || "continue" === t) &&
                i.tryLoc <= e &&
                e <= i.finallyLoc &&
                (i = null);
              var a = i ? i.completion : {};
              return (
                (a.type = t),
                (a.arg = e),
                i
                  ? ((this.method = "next"), (this.next = i.finallyLoc), m)
                  : this.complete(a)
              );
            },
            complete: function (t, e) {
              if ("throw" === t.type) throw t.arg;
              return (
                "break" === t.type || "continue" === t.type
                  ? (this.next = t.arg)
                  : "return" === t.type
                  ? ((this.rval = this.arg = t.arg),
                    (this.method = "return"),
                    (this.next = "end"))
                  : "normal" === t.type && e && (this.next = e),
                m
              );
            },
            finish: function (t) {
              for (var e = this.tryEntries.length - 1; e >= 0; --e) {
                var n = this.tryEntries[e];
                if (n.finallyLoc === t)
                  return this.complete(n.completion, n.afterLoc), I(n), m;
              }
            },
            catch: function (t) {
              for (var e = this.tryEntries.length - 1; e >= 0; --e) {
                var n = this.tryEntries[e];
                if (n.tryLoc === t) {
                  var r = n.completion;
                  if ("throw" === r.type) {
                    var o = r.arg;
                    I(n);
                  }
                  return o;
                }
              }
              throw new Error("illegal catch attempt");
            },
            delegateYield: function (t, e, r) {
              return (
                (this.delegate = { iterator: P(t), resultName: e, nextLoc: r }),
                "next" === this.method && (this.arg = n),
                m
              );
            },
          }),
          t
        );
      })("object" === e(t) ? t.exports : {});
      try {
        regeneratorRuntime = n;
      } catch (r) {
        Function("r", "regeneratorRuntime = r")(n);
      }
    }.call(this, n(306)(t)));
  },
  function (t, e) {
    t.exports = function (t) {
      return (
        t.webpackPolyfill ||
          ((t.deprecate = function () {}),
          (t.paths = []),
          t.children || (t.children = []),
          Object.defineProperty(t, "loaded", {
            enumerable: !0,
            get: function () {
              return t.l;
            },
          }),
          Object.defineProperty(t, "id", {
            enumerable: !0,
            get: function () {
              return t.i;
            },
          }),
          (t.webpackPolyfill = 1)),
        t
      );
    };
  },
  function (t, e, n) {
    n(308), (t.exports = n(124).global);
  },
  function (t, e, n) {
    var r = n(309);
    r(r.G, { global: n(88) });
  },
  function (t, e, n) {
    var r = n(88),
      o = n(124),
      i = n(310),
      a = n(312),
      u = n(319),
      c = function s(t, e, n) {
        var c,
          f,
          l,
          p = t & s.F,
          h = t & s.G,
          d = t & s.S,
          y = t & s.P,
          m = t & s.B,
          v = t & s.W,
          g = h ? o : o[e] || (o[e] = {}),
          b = g.prototype,
          w = h ? r : d ? r[e] : (r[e] || {}).prototype;
        for (c in (h && (n = e), n))
          ((f = !p && w && w[c] !== undefined) && u(g, c)) ||
            ((l = f ? w[c] : n[c]),
            (g[c] =
              h && "function" != typeof w[c]
                ? n[c]
                : m && f
                ? i(l, r)
                : v && w[c] == l
                ? (function (t) {
                    var e = function (e, n, r) {
                      if (this instanceof t) {
                        switch (arguments.length) {
                          case 0:
                            return new t();
                          case 1:
                            return new t(e);
                          case 2:
                            return new t(e, n);
                        }
                        return new t(e, n, r);
                      }
                      return t.apply(this, arguments);
                    };
                    return (e.prototype = t.prototype), e;
                  })(l)
                : y && "function" == typeof l
                ? i(Function.call, l)
                : l),
            y &&
              (((g.virtual || (g.virtual = {}))[c] = l),
              t & s.R && b && !b[c] && a(b, c, l)));
      };
    (c.F = 1),
      (c.G = 2),
      (c.S = 4),
      (c.P = 8),
      (c.B = 16),
      (c.W = 32),
      (c.U = 64),
      (c.R = 128),
      (t.exports = c);
  },
  function (t, e, n) {
    var r = n(311);
    t.exports = function (t, e, n) {
      if ((r(t), e === undefined)) return t;
      switch (n) {
        case 1:
          return function (n) {
            return t.call(e, n);
          };
        case 2:
          return function (n, r) {
            return t.call(e, n, r);
          };
        case 3:
          return function (n, r, o) {
            return t.call(e, n, r, o);
          };
      }
      return function () {
        return t.apply(e, arguments);
      };
    };
  },
  function (t, e) {
    t.exports = function (t) {
      if ("function" != typeof t) throw TypeError(t + " is not a function!");
      return t;
    };
  },
  function (t, e, n) {
    var r = n(313),
      o = n(318);
    t.exports = n(90)
      ? function (t, e, n) {
          return r.f(t, e, o(1, n));
        }
      : function (t, e, n) {
          return (t[e] = n), t;
        };
  },
  function (t, e, n) {
    var r = n(314),
      o = n(315),
      i = n(317),
      a = Object.defineProperty;
    e.f = n(90)
      ? Object.defineProperty
      : function (t, e, n) {
          if ((r(t), (e = i(e, !0)), r(n), o))
            try {
              return a(t, e, n);
            } catch (u) {}
          if ("get" in n || "set" in n)
            throw TypeError("Accessors not supported!");
          return "value" in n && (t[e] = n.value), t;
        };
  },
  function (t, e, n) {
    var r = n(89);
    t.exports = function (t) {
      if (!r(t)) throw TypeError(t + " is not an object!");
      return t;
    };
  },
  function (t, e, n) {
    t.exports =
      !n(90) &&
      !n(125)(function () {
        return (
          7 !=
          Object.defineProperty(n(316)("div"), "a", {
            get: function () {
              return 7;
            },
          }).a
        );
      });
  },
  function (t, e, n) {
    var r = n(89),
      o = n(88).document,
      i = r(o) && r(o.createElement);
    t.exports = function (t) {
      return i ? o.createElement(t) : {};
    };
  },
  function (t, e, n) {
    var r = n(89);
    t.exports = function (t, e) {
      if (!r(t)) return t;
      var n, o;
      if (e && "function" == typeof (n = t.toString) && !r((o = n.call(t))))
        return o;
      if ("function" == typeof (n = t.valueOf) && !r((o = n.call(t)))) return o;
      if (!e && "function" == typeof (n = t.toString) && !r((o = n.call(t))))
        return o;
      throw TypeError("Can't convert object to primitive value");
    };
  },
  function (t, e) {
    t.exports = function (t, e) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: e,
      };
    };
  },
  function (t, e) {
    var n = {}.hasOwnProperty;
    t.exports = function (t, e) {
      return n.call(t, e);
    };
  },
  function (t, e, n) {
    (function (t) {
      function e(t) {
        return (e =
          "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
            ? function (t) {
                return typeof t;
              }
            : function (t) {
                return t &&
                  "function" == typeof Symbol &&
                  t.constructor === Symbol &&
                  t !== Symbol.prototype
                  ? "symbol"
                  : typeof t;
              })(t);
      }
      !(function (t) {
        var n = (function () {
            try {
              return !!Symbol.iterator;
            } catch (t) {
              return !1;
            }
          })(),
          r = function (t) {
            var e = {
              next: function () {
                var e = t.shift();
                return { done: void 0 === e, value: e };
              },
            };
            return (
              n &&
                (e[Symbol.iterator] = function () {
                  return e;
                }),
              e
            );
          },
          o = function (t) {
            return encodeURIComponent(t).replace(/%20/g, "+");
          },
          i = function (t) {
            return decodeURIComponent(String(t).replace(/\+/g, " "));
          };
        (function () {
          try {
            var e = t.URLSearchParams;
            return (
              "a=1" === new e("?a=1").toString() &&
              "function" == typeof e.prototype.set &&
              "function" == typeof e.prototype.entries
            );
          } catch (n) {
            return !1;
          }
        })() ||
          (function () {
            var i = function u(t) {
                Object.defineProperty(this, "_entries", {
                  writable: !0,
                  value: {},
                });
                var n = e(t);
                if ("undefined" === n);
                else if ("string" === n) "" !== t && this._fromString(t);
                else if (t instanceof u) {
                  var r = this;
                  t.forEach(function (t, e) {
                    r.append(e, t);
                  });
                } else {
                  if (null === t || "object" !== n)
                    throw new TypeError(
                      "Unsupported input's type for URLSearchParams"
                    );
                  if ("[object Array]" === Object.prototype.toString.call(t))
                    for (var o = 0; o < t.length; o++) {
                      var i = t[o];
                      if (
                        "[object Array]" !==
                          Object.prototype.toString.call(i) &&
                        2 === i.length
                      )
                        throw new TypeError(
                          "Expected [string, any] as entry at index " +
                            o +
                            " of URLSearchParams's input"
                        );
                      this.append(i[0], i[1]);
                    }
                  else
                    for (var a in t)
                      t.hasOwnProperty(a) && this.append(a, t[a]);
                }
              },
              a = i.prototype;
            (a.append = function (t, e) {
              t in this._entries
                ? this._entries[t].push(String(e))
                : (this._entries[t] = [String(e)]);
            }),
              (a["delete"] = function (t) {
                delete this._entries[t];
              }),
              (a.get = function (t) {
                return t in this._entries ? this._entries[t][0] : null;
              }),
              (a.getAll = function (t) {
                return t in this._entries ? this._entries[t].slice(0) : [];
              }),
              (a.has = function (t) {
                return t in this._entries;
              }),
              (a.set = function (t, e) {
                this._entries[t] = [String(e)];
              }),
              (a.forEach = function (t, e) {
                var n;
                for (var r in this._entries)
                  if (this._entries.hasOwnProperty(r)) {
                    n = this._entries[r];
                    for (var o = 0; o < n.length; o++) t.call(e, n[o], r, this);
                  }
              }),
              (a.keys = function () {
                var t = [];
                return (
                  this.forEach(function (e, n) {
                    t.push(n);
                  }),
                  r(t)
                );
              }),
              (a.values = function () {
                var t = [];
                return (
                  this.forEach(function (e) {
                    t.push(e);
                  }),
                  r(t)
                );
              }),
              (a.entries = function () {
                var t = [];
                return (
                  this.forEach(function (e, n) {
                    t.push([n, e]);
                  }),
                  r(t)
                );
              }),
              n && (a[Symbol.iterator] = a.entries),
              (a.toString = function () {
                var t = [];
                return (
                  this.forEach(function (e, n) {
                    t.push(o(n) + "=" + o(e));
                  }),
                  t.join("&")
                );
              }),
              (t.URLSearchParams = i);
          })();
        var a = t.URLSearchParams.prototype;
        "function" != typeof a.sort &&
          (a.sort = function () {
            var t = this,
              e = [];
            this.forEach(function (n, r) {
              e.push([r, n]), t._entries || t["delete"](r);
            }),
              e.sort(function (t, e) {
                return t[0] < e[0] ? -1 : t[0] > e[0] ? 1 : 0;
              }),
              t._entries && (t._entries = {});
            for (var n = 0; n < e.length; n++) this.append(e[n][0], e[n][1]);
          }),
          "function" != typeof a._fromString &&
            Object.defineProperty(a, "_fromString", {
              enumerable: !1,
              configurable: !1,
              writable: !1,
              value: function (t) {
                if (this._entries) this._entries = {};
                else {
                  var e = [];
                  this.forEach(function (t, n) {
                    e.push(n);
                  });
                  for (var n = 0; n < e.length; n++) this["delete"](e[n]);
                }
                var r,
                  o = (t = t.replace(/^\?/, "")).split("&");
                for (n = 0; n < o.length; n++)
                  (r = o[n].split("=")),
                    this.append(i(r[0]), r.length > 1 ? i(r[1]) : "");
              },
            });
      })(
        void 0 !== t
          ? t
          : "undefined" != typeof window
          ? window
          : "undefined" != typeof self
          ? self
          : this
      ),
        (function (t) {
          if (
            ((function () {
              try {
                var e = new t.URL("b", "http://a");
                return (
                  (e.pathname = "c d"),
                  "http://a/c%20d" === e.href && e.searchParams
                );
              } catch (n) {
                return !1;
              }
            })() ||
              (function () {
                var e = t.URL,
                  n = function (e, n) {
                    "string" != typeof e && (e = String(e)),
                      n && "string" != typeof n && (n = String(n));
                    var r,
                      o = document;
                    if (n && (void 0 === t.location || n !== t.location.href)) {
                      (n = n.toLowerCase()),
                        ((r = (o =
                          document.implementation.createHTMLDocument(
                            ""
                          )).createElement("base")).href = n),
                        o.head.appendChild(r);
                      try {
                        if (0 !== r.href.indexOf(n)) throw new Error(r.href);
                      } catch (p) {
                        throw new Error(
                          "URL unable to set base " + n + " due to " + p
                        );
                      }
                    }
                    var i = o.createElement("a");
                    (i.href = e),
                      r && (o.body.appendChild(i), (i.href = i.href));
                    var a = o.createElement("input");
                    if (
                      ((a.type = "url"),
                      (a.value = e),
                      ":" === i.protocol ||
                        !/:/.test(i.href) ||
                        (!a.checkValidity() && !n))
                    )
                      throw new TypeError("Invalid URL");
                    Object.defineProperty(this, "_anchorElement", { value: i });
                    var u = new t.URLSearchParams(this.search),
                      c = !0,
                      s = !0,
                      f = this;
                    ["append", "delete", "set"].forEach(function (t) {
                      var e = u[t];
                      u[t] = function () {
                        e.apply(u, arguments),
                          c && ((s = !1), (f.search = u.toString()), (s = !0));
                      };
                    }),
                      Object.defineProperty(this, "searchParams", {
                        value: u,
                        enumerable: !0,
                      });
                    var l = void 0;
                    Object.defineProperty(this, "_updateSearchParams", {
                      enumerable: !1,
                      configurable: !1,
                      writable: !1,
                      value: function () {
                        this.search !== l &&
                          ((l = this.search),
                          s &&
                            ((c = !1),
                            this.searchParams._fromString(this.search),
                            (c = !0)));
                      },
                    });
                  },
                  r = n.prototype;
                ["hash", "host", "hostname", "port", "protocol"].forEach(
                  function (t) {
                    !(function (t) {
                      Object.defineProperty(r, t, {
                        get: function () {
                          return this._anchorElement[t];
                        },
                        set: function (e) {
                          this._anchorElement[t] = e;
                        },
                        enumerable: !0,
                      });
                    })(t);
                  }
                ),
                  Object.defineProperty(r, "search", {
                    get: function () {
                      return this._anchorElement.search;
                    },
                    set: function (t) {
                      (this._anchorElement.search = t),
                        this._updateSearchParams();
                    },
                    enumerable: !0,
                  }),
                  Object.defineProperties(r, {
                    toString: {
                      get: function () {
                        var t = this;
                        return function () {
                          return t.href;
                        };
                      },
                    },
                    href: {
                      get: function () {
                        return this._anchorElement.href.replace(/\?$/, "");
                      },
                      set: function (t) {
                        (this._anchorElement.href = t),
                          this._updateSearchParams();
                      },
                      enumerable: !0,
                    },
                    pathname: {
                      get: function () {
                        return this._anchorElement.pathname.replace(
                          /(^\/?)/,
                          "/"
                        );
                      },
                      set: function (t) {
                        this._anchorElement.pathname = t;
                      },
                      enumerable: !0,
                    },
                    origin: {
                      get: function () {
                        var t = { "http:": 80, "https:": 443, "ftp:": 21 }[
                            this._anchorElement.protocol
                          ],
                          e =
                            this._anchorElement.port != t &&
                            "" !== this._anchorElement.port;
                        return (
                          this._anchorElement.protocol +
                          "//" +
                          this._anchorElement.hostname +
                          (e ? ":" + this._anchorElement.port : "")
                        );
                      },
                      enumerable: !0,
                    },
                    password: {
                      get: function () {
                        return "";
                      },
                      set: function (t) {},
                      enumerable: !0,
                    },
                    username: {
                      get: function () {
                        return "";
                      },
                      set: function (t) {},
                      enumerable: !0,
                    },
                  }),
                  (n.createObjectURL = function (t) {
                    return e.createObjectURL.apply(e, arguments);
                  }),
                  (n.revokeObjectURL = function (t) {
                    return e.revokeObjectURL.apply(e, arguments);
                  }),
                  (t.URL = n);
              })(),
            void 0 !== t.location && !("origin" in t.location))
          ) {
            var e = function () {
              return (
                t.location.protocol +
                "//" +
                t.location.hostname +
                (t.location.port ? ":" + t.location.port : "")
              );
            };
            try {
              Object.defineProperty(t.location, "origin", {
                get: e,
                enumerable: !0,
              });
            } catch (n) {
              setInterval(function () {
                t.location.origin = e();
              }, 100);
            }
          }
        })(
          void 0 !== t
            ? t
            : "undefined" != typeof window
            ? window
            : "undefined" != typeof self
            ? self
            : this
        );
    }.call(this, n(48)));
  },
  function (t, e, n) {
    (function (t) {
      var r =
          (void 0 !== t && t) || ("undefined" != typeof self && self) || window,
        o = Function.prototype.apply;
      function i(t, e) {
        (this._id = t), (this._clearFn = e);
      }
      (e.setTimeout = function () {
        return new i(o.call(setTimeout, r, arguments), clearTimeout);
      }),
        (e.setInterval = function () {
          return new i(o.call(setInterval, r, arguments), clearInterval);
        }),
        (e.clearTimeout = e.clearInterval =
          function (t) {
            t && t.close();
          }),
        (i.prototype.unref = i.prototype.ref = function () {}),
        (i.prototype.close = function () {
          this._clearFn.call(r, this._id);
        }),
        (e.enroll = function (t, e) {
          clearTimeout(t._idleTimeoutId), (t._idleTimeout = e);
        }),
        (e.unenroll = function (t) {
          clearTimeout(t._idleTimeoutId), (t._idleTimeout = -1);
        }),
        (e._unrefActive = e.active =
          function (t) {
            clearTimeout(t._idleTimeoutId);
            var e = t._idleTimeout;
            e >= 0 &&
              (t._idleTimeoutId = setTimeout(function () {
                t._onTimeout && t._onTimeout();
              }, e));
          }),
        n(322),
        (e.setImmediate =
          ("undefined" != typeof self && self.setImmediate) ||
          (void 0 !== t && t.setImmediate) ||
          (this && this.setImmediate)),
        (e.clearImmediate =
          ("undefined" != typeof self && self.clearImmediate) ||
          (void 0 !== t && t.clearImmediate) ||
          (this && this.clearImmediate));
    }.call(this, n(48)));
  },
  function (t, e, n) {
    (function (t, e) {
      !(function (t, n) {
        "use strict";
        if (!t.setImmediate) {
          var r,
            o,
            i,
            a,
            u,
            c = 1,
            s = {},
            f = !1,
            l = t.document,
            p = Object.getPrototypeOf && Object.getPrototypeOf(t);
          (p = p && p.setTimeout ? p : t),
            "[object process]" === {}.toString.call(t.process)
              ? (r = function (t) {
                  e.nextTick(function () {
                    d(t);
                  });
                })
              : !(function () {
                  if (t.postMessage && !t.importScripts) {
                    var e = !0,
                      n = t.onmessage;
                    return (
                      (t.onmessage = function () {
                        e = !1;
                      }),
                      t.postMessage("", "*"),
                      (t.onmessage = n),
                      e
                    );
                  }
                })()
              ? t.MessageChannel
                ? (((i = new MessageChannel()).port1.onmessage = function (t) {
                    d(t.data);
                  }),
                  (r = function (t) {
                    i.port2.postMessage(t);
                  }))
                : l && "onreadystatechange" in l.createElement("script")
                ? ((o = l.documentElement),
                  (r = function (t) {
                    var e = l.createElement("script");
                    (e.onreadystatechange = function () {
                      d(t),
                        (e.onreadystatechange = null),
                        o.removeChild(e),
                        (e = null);
                    }),
                      o.appendChild(e);
                  }))
                : (r = function (t) {
                    setTimeout(d, 0, t);
                  })
              : ((a = "setImmediate$" + Math.random() + "$"),
                (u = function (e) {
                  e.source === t &&
                    "string" == typeof e.data &&
                    0 === e.data.indexOf(a) &&
                    d(+e.data.slice(a.length));
                }),
                t.addEventListener
                  ? t.addEventListener("message", u, !1)
                  : t.attachEvent("onmessage", u),
                (r = function (e) {
                  t.postMessage(a + e, "*");
                })),
            (p.setImmediate = function (t) {
              "function" != typeof t && (t = new Function("" + t));
              for (
                var e = new Array(arguments.length - 1), n = 0;
                n < e.length;
                n++
              )
                e[n] = arguments[n + 1];
              var o = { callback: t, args: e };
              return (s[c] = o), r(c), c++;
            }),
            (p.clearImmediate = h);
        }
        function h(t) {
          delete s[t];
        }
        function d(t) {
          if (f) setTimeout(d, 0, t);
          else {
            var e = s[t];
            if (e) {
              f = !0;
              try {
                !(function (t) {
                  var e = t.callback,
                    n = t.args;
                  switch (n.length) {
                    case 0:
                      e();
                      break;
                    case 1:
                      e(n[0]);
                      break;
                    case 2:
                      e(n[0], n[1]);
                      break;
                    case 3:
                      e(n[0], n[1], n[2]);
                      break;
                    default:
                      e.apply(void 0, n);
                  }
                })(e);
              } finally {
                h(t), (f = !1);
              }
            }
          }
        }
      })("undefined" == typeof self ? (void 0 === t ? this : t) : self);
    }.call(this, n(48), n(323)));
  },
  function (t, e) {
    var n,
      r,
      o = (t.exports = {});
    function i() {
      throw new Error("setTimeout has not been defined");
    }
    function a() {
      throw new Error("clearTimeout has not been defined");
    }
    function u(t) {
      if (n === setTimeout) return setTimeout(t, 0);
      if ((n === i || !n) && setTimeout)
        return (n = setTimeout), setTimeout(t, 0);
      try {
        return n(t, 0);
      } catch (e) {
        try {
          return n.call(null, t, 0);
        } catch (e) {
          return n.call(this, t, 0);
        }
      }
    }
    !(function () {
      try {
        n = "function" == typeof setTimeout ? setTimeout : i;
      } catch (t) {
        n = i;
      }
      try {
        r = "function" == typeof clearTimeout ? clearTimeout : a;
      } catch (t) {
        r = a;
      }
    })();
    var c,
      s = [],
      f = !1,
      l = -1;
    function p() {
      f &&
        c &&
        ((f = !1), c.length ? (s = c.concat(s)) : (l = -1), s.length && h());
    }
    function h() {
      if (!f) {
        var t = u(p);
        f = !0;
        for (var e = s.length; e; ) {
          for (c = s, s = []; ++l < e; ) c && c[l].run();
          (l = -1), (e = s.length);
        }
        (c = null),
          (f = !1),
          (function (t) {
            if (r === clearTimeout) return clearTimeout(t);
            if ((r === a || !r) && clearTimeout)
              return (r = clearTimeout), clearTimeout(t);
            try {
              r(t);
            } catch (e) {
              try {
                return r.call(null, t);
              } catch (e) {
                return r.call(this, t);
              }
            }
          })(t);
      }
    }
    function d(t, e) {
      (this.fun = t), (this.array = e);
    }
    function y() {}
    (o.nextTick = function (t) {
      var e = new Array(arguments.length - 1);
      if (arguments.length > 1)
        for (var n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
      s.push(new d(t, e)), 1 !== s.length || f || u(h);
    }),
      (d.prototype.run = function () {
        this.fun.apply(null, this.array);
      }),
      (o.title = "browser"),
      (o.browser = !0),
      (o.env = {}),
      (o.argv = []),
      (o.version = ""),
      (o.versions = {}),
      (o.on = y),
      (o.addListener = y),
      (o.once = y),
      (o.off = y),
      (o.removeListener = y),
      (o.removeAllListeners = y),
      (o.emit = y),
      (o.prependListener = y),
      (o.prependOnceListener = y),
      (o.listeners = function (t) {
        return [];
      }),
      (o.binding = function (t) {
        throw new Error("process.binding is not supported");
      }),
      (o.cwd = function () {
        return "/";
      }),
      (o.chdir = function (t) {
        throw new Error("process.chdir is not supported");
      }),
      (o.umask = function () {
        return 0;
      });
  },
  function (t, e, n) {
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    function o(t) {
      return (
        (function (t) {
          if (Array.isArray(t)) return c(t);
        })(t) ||
        (function (t) {
          if (
            ("undefined" != typeof Symbol && null != t[Symbol.iterator]) ||
            null != t["@@iterator"]
          )
            return Array.from(t);
        })(t) ||
        u(t) ||
        (function () {
          throw new TypeError(
            "Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        })()
      );
    }
    function i(t, e) {
      return (
        (function (t) {
          if (Array.isArray(t)) return t;
        })(t) ||
        (function (t, e) {
          var n =
            t &&
            (("undefined" != typeof Symbol && t[Symbol.iterator]) ||
              t["@@iterator"]);
          if (null == n) return;
          var r,
            o,
            i = [],
            a = !0,
            u = !1;
          try {
            for (
              n = n.call(t);
              !(a = (r = n.next()).done) &&
              (i.push(r.value), !e || i.length !== e);
              a = !0
            );
          } catch (c) {
            (u = !0), (o = c);
          } finally {
            try {
              a || null == n["return"] || n["return"]();
            } finally {
              if (u) throw o;
            }
          }
          return i;
        })(t, e) ||
        u(t, e) ||
        (function () {
          throw new TypeError(
            "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        })()
      );
    }
    function a(t, e) {
      var n =
        ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
      if (!n) {
        if (
          Array.isArray(t) ||
          (n = u(t)) ||
          (e && t && "number" == typeof t.length)
        ) {
          n && (t = n);
          var r = 0,
            o = function () {};
          return {
            s: o,
            n: function () {
              return r >= t.length ? { done: !0 } : { done: !1, value: t[r++] };
            },
            e: function (t) {
              throw t;
            },
            f: o,
          };
        }
        throw new TypeError(
          "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      }
      var i,
        a = !0,
        c = !1;
      return {
        s: function () {
          n = n.call(t);
        },
        n: function () {
          var t = n.next();
          return (a = t.done), t;
        },
        e: function (t) {
          (c = !0), (i = t);
        },
        f: function () {
          try {
            a || null == n["return"] || n["return"]();
          } finally {
            if (c) throw i;
          }
        },
      };
    }
    function u(t, e) {
      if (t) {
        if ("string" == typeof t) return c(t, e);
        var n = Object.prototype.toString.call(t).slice(8, -1);
        return (
          "Object" === n && t.constructor && (n = t.constructor.name),
          "Map" === n || "Set" === n
            ? Array.from(t)
            : "Arguments" === n ||
              /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
            ? c(t, e)
            : void 0
        );
      }
    }
    function c(t, e) {
      (null == e || e > t.length) && (e = t.length);
      for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
      return r;
    }
    function s(t, e, n, r, o, i, a) {
      try {
        var u = t[i](a),
          c = u.value;
      } catch (s) {
        return void n(s);
      }
      u.done ? e(c) : Promise.resolve(c).then(r, o);
    }
    function f(t) {
      return function () {
        var e = this,
          n = arguments;
        return new Promise(function (r, o) {
          var i = t.apply(e, n);
          function a(t) {
            s(i, r, o, a, u, "next", t);
          }
          function u(t) {
            s(i, r, o, a, u, "throw", t);
          }
          a(undefined);
        });
      };
    }
    function l(t, e) {
      if (!(t instanceof e))
        throw new TypeError("Cannot call a class as a function");
    }
    function p(t, e) {
      return !e || ("object" !== r(e) && "function" != typeof e)
        ? (function (t) {
            if (void 0 === t)
              throw new ReferenceError(
                "this hasn't been initialised - super() hasn't been called"
              );
            return t;
          })(t)
        : e;
    }
    function h(t) {
      var e = "function" == typeof Map ? new Map() : undefined;
      return (h = function (t) {
        if (
          null === t ||
          ((n = t), -1 === Function.toString.call(n).indexOf("[native code]"))
        )
          return t;
        var n;
        if ("function" != typeof t)
          throw new TypeError(
            "Super expression must either be null or a function"
          );
        if (void 0 !== e) {
          if (e.has(t)) return e.get(t);
          e.set(t, r);
        }
        function r() {
          return d(t, arguments, v(this).constructor);
        }
        return (
          (r.prototype = Object.create(t.prototype, {
            constructor: {
              value: r,
              enumerable: !1,
              writable: !0,
              configurable: !0,
            },
          })),
          m(r, t)
        );
      })(t);
    }
    function d(t, e, n) {
      return (d = y()
        ? Reflect.construct
        : function (t, e, n) {
            var r = [null];
            r.push.apply(r, e);
            var o = new (Function.bind.apply(t, r))();
            return n && m(o, n.prototype), o;
          }).apply(null, arguments);
    }
    function y() {
      if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
      if (Reflect.construct.sham) return !1;
      if ("function" == typeof Proxy) return !0;
      try {
        return (
          Boolean.prototype.valueOf.call(
            Reflect.construct(Boolean, [], function () {})
          ),
          !0
        );
      } catch (t) {
        return !1;
      }
    }
    function m(t, e) {
      return (m =
        Object.setPrototypeOf ||
        function (t, e) {
          return (t.__proto__ = e), t;
        })(t, e);
    }
    function v(t) {
      return (v = Object.setPrototypeOf
        ? Object.getPrototypeOf
        : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t);
          })(t);
    }
    var g = n(91),
      b = n(327),
      w = n(61),
      _ = n(62),
      E = n(126).setTimeout,
      T = n(127),
      S = n(128),
      x = n(334),
      A = n(129),
      R = n(130),
      I = "https://www.youtube.com/watch?v=";
    (e.cache = new R()),
      (e.cookieCache = new R(864e5)),
      (e.watchPageCache = new R());
    var O = (function (t) {
        !(function (t, e) {
          if ("function" != typeof e && null !== e)
            throw new TypeError(
              "Super expression must either be null or a function"
            );
          (t.prototype = Object.create(e && e.prototype, {
            constructor: { value: t, writable: !0, configurable: !0 },
          })),
            e && m(t, e);
        })(o, t);
        var e,
          n,
          r =
            ((e = o),
            (n = y()),
            function () {
              var t,
                r = v(e);
              if (n) {
                var o = v(this).constructor;
                t = Reflect.construct(r, arguments, o);
              } else t = r.apply(this, arguments);
              return p(this, t);
            });
        function o() {
          return l(this, o), r.apply(this, arguments);
        }
        return o;
      })(h(Error)),
      P = [
        "support.google.com/youtube/?p=age_restrictions",
        "youtube.com/t/community_guidelines",
      ];
    e.getBasicInfo = (function () {
      var t = f(
        regeneratorRuntime.mark(function e(t, n) {
          var r, o, i, a, u;
          return regeneratorRuntime.wrap(function (e) {
            for (;;)
              switch ((e.prev = e.next)) {
                case 0:
                  return (
                    (r = Object.assign({}, w.defaultOptions, n.requestOptions)),
                    (n.requestOptions = Object.assign(
                      {},
                      n.requestOptions,
                      {}
                    )),
                    (n.requestOptions.headers = Object.assign(
                      {},
                      {
                        "User-Agent":
                          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.101 Safari/537.36",
                      },
                      n.requestOptions.headers
                    )),
                    (o = function (t) {
                      var e = _.playError(t.player_response, ["ERROR"], O),
                        n = L(t.player_response);
                      if (e || n) throw e || n;
                      return (
                        t &&
                        t.player_response &&
                        (t.player_response.streamingData ||
                          N(t.player_response) ||
                          D(t.player_response))
                      );
                    }),
                    (e.next = 6),
                    U([t, n], o, r, [z, $, X])
                  );
                case 6:
                  return (
                    (i = e.sent),
                    Object.assign(i, {
                      formats: K(i.player_response),
                      related_videos: x.getRelatedVideos(i),
                    }),
                    (a = x.getMedia(i)),
                    (u = {
                      author: x.getAuthor(i),
                      media: a,
                      likes: x.getLikes(i),
                      dislikes: x.getDislikes(i),
                      age_restricted: !!(
                        a &&
                        a.notice_url &&
                        P.some(function (t) {
                          return a.notice_url.includes(t);
                        })
                      ),
                      video_url: I + t,
                      storyboards: x.getStoryboards(i),
                      chapters: x.getChapters(i),
                    }),
                    (i.videoDetails = x.cleanVideoDetails(
                      Object.assign(
                        {},
                        i.player_response &&
                          i.player_response.microformat &&
                          i.player_response.microformat
                            .playerMicroformatRenderer,
                        i.player_response && i.player_response.videoDetails,
                        u
                      ),
                      i
                    )),
                    e.abrupt("return", i)
                  );
                case 12:
                case "end":
                  return e.stop();
              }
          }, e);
        })
      );
      return function (e, n) {
        return t.apply(this, arguments);
      };
    })();
    var L = function (t) {
        var e = t && t.playabilityStatus;
        return e &&
          "LOGIN_REQUIRED" === e.status &&
          e.messages &&
          e.messages.filter(function (t) {
            return /This is a private video/.test(t);
          }).length
          ? new O(e.reason || (e.messages && e.messages[0]))
          : null;
      },
      N = function (t) {
        var e = t.playabilityStatus;
        return (
          e &&
          "UNPLAYABLE" === e.status &&
          e.errorScreen &&
          e.errorScreen.playerLegacyDesktopYpcOfferRenderer
        );
      },
      D = function (t) {
        var e = t.playabilityStatus;
        return e && "LIVE_STREAM_OFFLINE" === e.status;
      },
      F = function (t, e) {
        return "".concat(I + t, "&hl=").concat(e.lang || "en");
      },
      M = function (t, n) {
        var r = F(t, n);
        return e.watchPageCache.getOrSet(r, function () {
          return w(r, n.requestOptions).text();
        });
      },
      C = function (t, e) {
        var n = ""
          .concat("https://www.youtube.com/embed/" + t, "?hl=")
          .concat(e.lang || "en");
        return w(n, e.requestOptions).text();
      },
      B = function (t) {
        var e =
          /<script\s+src="([^"]+)"(?:\s+type="text\/javascript")?\s+name="player_ias\/base"\s*>|"jsUrl":"([^"]+)"/.exec(
            t
          );
        return e ? e[1] || e[2] : null;
      },
      j = function (t, n, r, o) {
        return e.cookieCache.getOrSet(
          r,
          f(
            regeneratorRuntime.mark(function i() {
              var e, r;
              return regeneratorRuntime.wrap(function (i) {
                for (;;)
                  switch ((i.prev = i.next)) {
                    case 0:
                      return (i.next = 2), M(t, n);
                    case 2:
                      if (
                        ((e = i.sent),
                        (r = e.match(/(["'])ID_TOKEN\1[:,]\s?"([^"]+)"/)) || !o)
                      ) {
                        i.next = 6;
                        break;
                      }
                      throw new O(
                        "Cookie header used in request, but unable to find YouTube identity token"
                      );
                    case 6:
                      return i.abrupt("return", r && r[2]);
                    case 7:
                    case "end":
                      return i.stop();
                  }
              }, i);
            })
          )
        );
      },
      U = (function () {
        var t = f(
          regeneratorRuntime.mark(function e(t, n, r, o) {
            var i, u, c, s, f;
            return regeneratorRuntime.wrap(
              function (e) {
                for (;;)
                  switch ((e.prev = e.next)) {
                    case 0:
                      (u = a(o)), (e.prev = 1), u.s();
                    case 3:
                      if ((c = u.n()).done) {
                        e.next = 21;
                        break;
                      }
                      return (
                        (s = c.value),
                        (e.prev = 5),
                        (e.next = 8),
                        V(s, t.concat([i]), r)
                      );
                    case 8:
                      if (
                        ((f = e.sent).player_response &&
                          ((f.player_response.videoDetails = k(
                            i &&
                              i.player_response &&
                              i.player_response.videoDetails,
                            f.player_response.videoDetails
                          )),
                          (f.player_response = k(
                            i && i.player_response,
                            f.player_response
                          ))),
                        (i = k(i, f)),
                        !n(i, !1))
                      ) {
                        e.next = 13;
                        break;
                      }
                      return e.abrupt("break", 21);
                    case 13:
                      e.next = 19;
                      break;
                    case 15:
                      if (
                        ((e.prev = 15),
                        (e.t0 = e["catch"](5)),
                        !(e.t0 instanceof O || s === o[o.length - 1]))
                      ) {
                        e.next = 19;
                        break;
                      }
                      throw e.t0;
                    case 19:
                      e.next = 3;
                      break;
                    case 21:
                      e.next = 26;
                      break;
                    case 23:
                      (e.prev = 23), (e.t1 = e["catch"](1)), u.e(e.t1);
                    case 26:
                      return (e.prev = 26), u.f(), e.finish(26);
                    case 29:
                      return e.abrupt("return", i);
                    case 30:
                    case "end":
                      return e.stop();
                  }
              },
              e,
              null,
              [
                [1, 23, 26, 29],
                [5, 15],
              ]
            );
          })
        );
        return function (e, n, r, o) {
          return t.apply(this, arguments);
        };
      })(),
      k = function (t, e) {
        if (!t || !e) return t || e;
        for (var n = 0, r = Object.entries(e); n < r.length; n++) {
          var o = i(r[n], 2),
            a = o[0],
            u = o[1];
          null !== u && u !== undefined && (t[a] = u);
        }
        return t;
      },
      V = (function () {
        var t = f(
          regeneratorRuntime.mark(function e(t, n, r) {
            var i, a;
            return regeneratorRuntime.wrap(
              function (e) {
                for (;;)
                  switch ((e.prev = e.next)) {
                    case 0:
                      i = 0;
                    case 1:
                      if (!(i <= r.maxRetries)) {
                        e.next = 14;
                        break;
                      }
                      return (e.prev = 2), (e.next = 5), t.apply(void 0, o(n));
                    case 5:
                      return (a = e.sent), e.abrupt("break", 14);
                    case 9:
                      return (
                        (e.prev = 9),
                        (e.t0 = e["catch"](2)),
                        e.delegateYield(
                          regeneratorRuntime.mark(function u() {
                            var t;
                            return regeneratorRuntime.wrap(function (n) {
                              for (;;)
                                switch ((n.prev = n.next)) {
                                  case 0:
                                    if (
                                      !(
                                        e.t0 instanceof O ||
                                        (e.t0 instanceof w.MinigetError &&
                                          e.t0.statusCode < 500) ||
                                        i >= r.maxRetries
                                      )
                                    ) {
                                      n.next = 2;
                                      break;
                                    }
                                    throw e.t0;
                                  case 2:
                                    return (
                                      (t = Math.min(
                                        ++i * r.backoff.inc,
                                        r.backoff.max
                                      )),
                                      (n.next = 5),
                                      new Promise(function (e) {
                                        return E(e, t);
                                      })
                                    );
                                  case 5:
                                  case "end":
                                    return n.stop();
                                }
                            }, u);
                          })(),
                          "t1",
                          12
                        )
                      );
                    case 12:
                      e.next = 1;
                      break;
                    case 14:
                      return e.abrupt("return", a);
                    case 15:
                    case "end":
                      return e.stop();
                  }
              },
              e,
              null,
              [[2, 9]]
            );
          })
        );
        return function (e, n, r) {
          return t.apply(this, arguments);
        };
      })(),
      q = /^[)\]}'\s]+/,
      Y = function (t, e, n) {
        if (!n || "object" === r(n)) return n;
        try {
          return (n = n.replace(q, "")), JSON.parse(n);
        } catch (o) {
          throw Error(
            "Error parsing ".concat(e, " in ").concat(t, ": ").concat(o.message)
          );
        }
      },
      G = function (t, e, n, r, o, i) {
        var a = _.between(n, r, o);
        if (!a) throw Error("Could not find ".concat(e, " in ").concat(t));
        return Y(t, e, _.cutAfterJSON("".concat(i).concat(a)));
      },
      H = function (t, e) {
        var n =
          e &&
          ((e.args && e.args.player_response) ||
            e.player_response ||
            e.playerResponse ||
            e.embedded_player_response);
        return Y(t, "player_response", n);
      },
      W = function (t, e) {
        return "".concat(F(t, e), "&pbj=1");
      },
      $ = (function () {
        var t = f(
          regeneratorRuntime.mark(function n(t, r) {
            var o, i, a, u, c, s, l;
            return regeneratorRuntime.wrap(function (n) {
              for (;;)
                switch ((n.prev = n.next)) {
                  case 0:
                    if (
                      ((o = Object.assign({ headers: {} }, r.requestOptions)),
                      (i = o.headers.Cookie || o.headers.cookie),
                      (o.headers = Object.assign(
                        {
                          "x-youtube-client-name": "1",
                          "x-youtube-client-version": "2.20201203.06.00",
                          "x-youtube-identity-token":
                            e.cookieCache.get(i || "browser") || "",
                        },
                        o.headers
                      )),
                      (a = (function () {
                        var e = f(
                          regeneratorRuntime.mark(function n(e, i) {
                            return regeneratorRuntime.wrap(function (n) {
                              for (;;)
                                switch ((n.prev = n.next)) {
                                  case 0:
                                    if (
                                      !o.headers["x-youtube-identity-token"]
                                    ) {
                                      n.next = 2;
                                      break;
                                    }
                                    return n.abrupt("return");
                                  case 2:
                                    return (n.next = 4), j(t, r, e, i);
                                  case 4:
                                    o.headers["x-youtube-identity-token"] =
                                      n.sent;
                                  case 5:
                                  case "end":
                                    return n.stop();
                                }
                            }, n);
                          })
                        );
                        return function (t, n) {
                          return e.apply(this, arguments);
                        };
                      })()),
                      !i)
                    ) {
                      n.next = 7;
                      break;
                    }
                    return (n.next = 7), a(i, !0);
                  case 7:
                    return (u = W(t, r)), (n.next = 10), w(u, o).text();
                  case 10:
                    if (
                      ((c = n.sent),
                      "now" !== (s = Y("watch.json", "body", c)).reload)
                    ) {
                      n.next = 15;
                      break;
                    }
                    return (n.next = 15), a("browser", !1);
                  case 15:
                    if ("now" !== s.reload && Array.isArray(s)) {
                      n.next = 17;
                      break;
                    }
                    throw Error(
                      "Unable to retrieve video metadata in watch.json"
                    );
                  case 17:
                    return (
                      ((l = s.reduce(function (t, e) {
                        return Object.assign(e, t);
                      }, {})).player_response = H("watch.json", l)),
                      (l.html5player =
                        l.player && l.player.assets && l.player.assets.js),
                      n.abrupt("return", l)
                    );
                  case 21:
                  case "end":
                    return n.stop();
                }
            }, n);
          })
        );
        return function (e, n) {
          return t.apply(this, arguments);
        };
      })(),
      z = (function () {
        var t = f(
          regeneratorRuntime.mark(function e(t, n) {
            var r, o, i;
            return regeneratorRuntime.wrap(function (e) {
              for (;;)
                switch ((e.prev = e.next)) {
                  case 0:
                    return (e.next = 2), M(t, n);
                  case 2:
                    (r = e.sent), (o = { page: "watch" });
                    try {
                      o.player_response = G(
                        "watch.html",
                        "player_response",
                        r,
                        /\bytInitialPlayerResponse\s*=\s*\{/i,
                        "\n",
                        "{"
                      );
                    } catch (a) {
                      (i = G(
                        "watch.html",
                        "player_response",
                        r,
                        /\bytplayer\.config\s*=\s*{/,
                        "</script>",
                        "{"
                      )),
                        (o.player_response = H("watch.html", i));
                    }
                    return (
                      (o.response = G(
                        "watch.html",
                        "response",
                        r,
                        /\bytInitialData("\])?\s*=\s*\{/i,
                        "\n",
                        "{"
                      )),
                      (o.html5player = B(r)),
                      e.abrupt("return", o)
                    );
                  case 8:
                  case "end":
                    return e.stop();
                }
            }, e);
          })
        );
        return function (e, n) {
          return t.apply(this, arguments);
        };
      })(),
      X = (function () {
        var t = f(
          regeneratorRuntime.mark(function e(t, n) {
            var r, o, i;
            return regeneratorRuntime.wrap(function (e) {
              for (;;)
                switch ((e.prev = e.next)) {
                  case 0:
                    return (
                      (r = new URL(
                        "https://"
                          .concat("www.youtube.com")
                          .concat("/get_video_info")
                      )).searchParams.set("video_id", t),
                      r.searchParams.set(
                        "eurl",
                        "https://youtube.googleapis.com/v/" + t
                      ),
                      r.searchParams.set("ps", "default"),
                      r.searchParams.set("gl", "US"),
                      r.searchParams.set("hl", n.lang || "en"),
                      (e.next = 8),
                      w(r.toString(), n.requestOptions).text()
                    );
                  case 8:
                    return (
                      (o = e.sent),
                      ((i = g.parse(o)).player_response = H(
                        "get_video_info",
                        i
                      )),
                      e.abrupt("return", i)
                    );
                  case 12:
                  case "end":
                    return e.stop();
                }
            }, e);
          })
        );
        return function (e, n) {
          return t.apply(this, arguments);
        };
      })(),
      K = function (t) {
        var e = [];
        return (
          t &&
            t.streamingData &&
            (e = e
              .concat(t.streamingData.formats || [])
              .concat(t.streamingData.adaptiveFormats || [])),
          e
        );
      };
    e.getInfo = (function () {
      var t = f(
        regeneratorRuntime.mark(function n(t, r) {
          var i, a, u, c, s, f, l;
          return regeneratorRuntime.wrap(function (n) {
            for (;;)
              switch ((n.prev = n.next)) {
                case 0:
                  return (n.next = 2), e.getBasicInfo(t, r);
                case 2:
                  if (
                    ((i = n.sent),
                    (a =
                      i.player_response &&
                      i.player_response.streamingData &&
                      (i.player_response.streamingData.dashManifestUrl ||
                        i.player_response.streamingData.hlsManifestUrl)),
                    (u = []),
                    !i.formats.length)
                  ) {
                    n.next = 25;
                    break;
                  }
                  if (((n.t1 = i.html5player), n.t1)) {
                    n.next = 13;
                    break;
                  }
                  return (n.t2 = B), (n.next = 11), M(t, r);
                case 11:
                  (n.t3 = n.sent), (n.t1 = (0, n.t2)(n.t3));
                case 13:
                  if (((n.t0 = n.t1), n.t0)) {
                    n.next = 20;
                    break;
                  }
                  return (n.t4 = B), (n.next = 18), C(t, r);
                case 18:
                  (n.t5 = n.sent), (n.t0 = (0, n.t4)(n.t5));
                case 20:
                  if (((i.html5player = n.t0), i.html5player)) {
                    n.next = 23;
                    break;
                  }
                  throw Error("Unable to find html5player file");
                case 23:
                  (c = new URL(i.html5player, I).toString()),
                    u.push(A.decipherFormats(i.formats, c, r));
                case 25:
                  return (
                    a &&
                      i.player_response.streamingData.dashManifestUrl &&
                      ((s = i.player_response.streamingData.dashManifestUrl),
                      u.push(Q(s, r))),
                    a &&
                      i.player_response.streamingData.hlsManifestUrl &&
                      ((f = i.player_response.streamingData.hlsManifestUrl),
                      u.push(J(f, r))),
                    (n.next = 29),
                    Promise.all(u)
                  );
                case 29:
                  return (
                    (l = n.sent),
                    (i.formats = Object.values(
                      Object.assign.apply(Object, [{}].concat(o(l)))
                    )),
                    (i.formats = i.formats.map(T.addFormatMeta)),
                    i.formats.sort(T.sortFormats),
                    (i.full = !0),
                    n.abrupt("return", i)
                  );
                case 35:
                case "end":
                  return n.stop();
              }
          }, n);
        })
      );
      return function (e, n) {
        return t.apply(this, arguments);
      };
    })();
    for (
      var Q = function (t, e) {
          return new Promise(function (n, r) {
            var o,
              i = {},
              a = b.parser(!1);
            (a.onerror = r),
              (a.onopentag = function (e) {
                if ("ADAPTATIONSET" === e.name) o = e.attributes;
                else if ("REPRESENTATION" === e.name) {
                  var n = parseInt(e.attributes.ID);
                  isNaN(n) ||
                    (i[t] = Object.assign(
                      {
                        itag: n,
                        url: t,
                        bitrate: parseInt(e.attributes.BANDWIDTH),
                        mimeType: ""
                          .concat(o.MIMETYPE, '; codecs="')
                          .concat(e.attributes.CODECS, '"'),
                      },
                      e.attributes.HEIGHT
                        ? {
                            width: parseInt(e.attributes.WIDTH),
                            height: parseInt(e.attributes.HEIGHT),
                            fps: parseInt(e.attributes.FRAMERATE),
                          }
                        : { audioSampleRate: e.attributes.AUDIOSAMPLINGRATE }
                    ));
                }
              }),
              (a.onend = function () {
                n(i);
              });
            var u = w(new URL(t, I).toString(), e.requestOptions);
            u.setEncoding("utf8"),
              u.on("error", r),
              u.on("data", function (t) {
                a.write(t);
              }),
              u.on("end", a.close.bind(a));
          });
        },
        J = (function () {
          var t = f(
            regeneratorRuntime.mark(function e(t, n) {
              var r, o;
              return regeneratorRuntime.wrap(function (e) {
                for (;;)
                  switch ((e.prev = e.next)) {
                    case 0:
                      return (
                        (t = new URL(t, I)),
                        (e.next = 3),
                        w(t.toString(), n.requestOptions).text()
                      );
                    case 3:
                      return (
                        (r = e.sent),
                        (o = {}),
                        r
                          .split("\n")
                          .filter(function (t) {
                            return /^https?:\/\//.test(t);
                          })
                          .forEach(function (t) {
                            var e = parseInt(t.match(/\/itag\/(\d+)\//)[1]);
                            o[t] = { itag: e, url: t };
                          }),
                        e.abrupt("return", o)
                      );
                    case 7:
                    case "end":
                      return e.stop();
                  }
              }, e);
            })
          );
          return function (e, n) {
            return t.apply(this, arguments);
          };
        })(),
        Z = function () {
          var t = et[tt],
            n = e[t];
          e[t] = (function () {
            var r = f(
              regeneratorRuntime.mark(function o(r) {
                var i,
                  a,
                  u,
                  c = arguments;
                return regeneratorRuntime.wrap(function (o) {
                  for (;;)
                    switch ((o.prev = o.next)) {
                      case 0:
                        return (
                          (i = c.length > 1 && c[1] !== undefined ? c[1] : {}),
                          _.checkForUpdates(),
                          (o.next = 4),
                          S.getVideoID(r)
                        );
                      case 4:
                        return (
                          (a = o.sent),
                          (u = [t, a, i.lang].join("-")),
                          o.abrupt(
                            "return",
                            e.cache.getOrSet(u, function () {
                              return n(a, i);
                            })
                          )
                        );
                      case 7:
                      case "end":
                        return o.stop();
                    }
                }, o);
              })
            );
            return function (t) {
              return r.apply(this, arguments);
            };
          })();
        },
        tt = 0,
        et = ["getBasicInfo", "getInfo"];
      tt < et.length;
      tt++
    )
      Z();
    (e.validateID = S.validateID),
      (e.validateURL = S.validateURL),
      (e.getURLVideoID = S.getURLVideoID),
      (e.getVideoID = S.getVideoID);
  },
  function (t, e, n) {
    "use strict";
    function r(t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }
    t.exports = function (t, e, n, i) {
      (e = e || "&"), (n = n || "=");
      var a = {};
      if ("string" != typeof t || 0 === t.length) return a;
      var u = /\+/g;
      t = t.split(e);
      var c = 1e3;
      i && "number" == typeof i.maxKeys && (c = i.maxKeys);
      var s = t.length;
      c > 0 && s > c && (s = c);
      for (var f = 0; f < s; ++f) {
        var l,
          p,
          h,
          d,
          y = t[f].replace(u, "%20"),
          m = y.indexOf(n);
        m >= 0
          ? ((l = y.substr(0, m)), (p = y.substr(m + 1)))
          : ((l = y), (p = "")),
          (h = decodeURIComponent(l)),
          (d = decodeURIComponent(p)),
          r(a, h) ? (o(a[h]) ? a[h].push(d) : (a[h] = [a[h], d])) : (a[h] = d);
      }
      return a;
    };
    var o =
      Array.isArray ||
      function (t) {
        return "[object Array]" === Object.prototype.toString.call(t);
      };
  },
  function (t, e, n) {
    "use strict";
    function r(t) {
      return (r =
        "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
          ? function (t) {
              return typeof t;
            }
          : function (t) {
              return t &&
                "function" == typeof Symbol &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
                ? "symbol"
                : typeof t;
            })(t);
    }
    var o = function (t) {
      switch (r(t)) {
        case "string":
          return t;
        case "boolean":
          return t ? "true" : "false";
        case "number":
          return isFinite(t) ? t : "";
        default:
          return "";
      }
    };
    t.exports = function (t, e, n, c) {
      return (
        (e = e || "&"),
        (n = n || "="),
        null === t && (t = undefined),
        "object" === r(t)
          ? a(u(t), function (r) {
              var u = encodeURIComponent(o(r)) + n;
              return i(t[r])
                ? a(t[r], function (t) {
                    return u + encodeURIComponent(o(t));
                  }).join(e)
                : u + encodeURIComponent(o(t[r]));
            }).join(e)
          : c
          ? encodeURIComponent(o(c)) + n + encodeURIComponent(o(t))
          : ""
      );
    };
    var i =
      Array.isArray ||
      function (t) {
        return "[object Array]" === Object.prototype.toString.call(t);
      };
    function a(t, e) {
      if (t.map) return t.map(e);
      for (var n = [], r = 0; r < t.length; r++) n.push(e(t[r], r));
      return n;
    }
    var u =
      Object.keys ||
      function (t) {
        var e = [];
        for (var n in t)
          Object.prototype.hasOwnProperty.call(t, n) && e.push(n);
        return e;
      };
  },
  function (t, e, n) {
    t.exports = n(328);
  },
  function (t, e, n) {
    (function (t) {
      function n(t) {
        return (n =
          "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
            ? function (t) {
                return typeof t;
              }
            : function (t) {
                return t &&
                  "function" == typeof Symbol &&
                  t.constructor === Symbol &&
                  t !== Symbol.prototype
                  ? "symbol"
                  : typeof t;
              })(t);
      }
      !(function (e) {
        (e.parser = function (t, e) {
          return new o(t, e);
        }),
          (e.SAXParser = o),
          (e.SAXStream = u),
          (e.createStream = function (t, e) {
            return new u(t, e);
          }),
          (e.MAX_BUFFER_LENGTH = 65536);
        var r = [
          "comment",
          "sgmlDecl",
          "textNode",
          "tagName",
          "doctype",
          "procInstName",
          "procInstBody",
          "entity",
          "attribName",
          "attribValue",
          "cdata",
          "script",
        ];
        function o(t, n) {
          if (!(this instanceof o)) return new o(t, n);
          var i = this;
          !(function (t) {
            for (var e = 0, n = r.length; e < n; e++) t[r[e]] = "";
          })(i),
            (i.q = i.c = ""),
            (i.bufferCheckPosition = e.MAX_BUFFER_LENGTH),
            (i.opt = n || {}),
            (i.opt.lowercase = i.opt.lowercase || i.opt.lowercasetags),
            (i.looseCase = i.opt.lowercase ? "toLowerCase" : "toUpperCase"),
            (i.tags = []),
            (i.closed = i.closedRoot = i.sawRoot = !1),
            (i.tag = i.error = null),
            (i.strict = !!t),
            (i.noscript = !(!t && !i.opt.noscript)),
            (i.state = T.BEGIN),
            (i.strictEntities = i.opt.strictEntities),
            (i.ENTITIES = i.strictEntities
              ? Object.create(e.XML_ENTITIES)
              : Object.create(e.ENTITIES)),
            (i.attribList = []),
            i.opt.xmlns && (i.ns = Object.create(f)),
            (i.trackPosition = !1 !== i.opt.position),
            i.trackPosition && (i.position = i.line = i.column = 0),
            x(i, "onready");
        }
        (e.EVENTS = [
          "text",
          "processinginstruction",
          "sgmldeclaration",
          "doctype",
          "comment",
          "opentagstart",
          "attribute",
          "opentag",
          "closetag",
          "opencdata",
          "cdata",
          "closecdata",
          "error",
          "end",
          "ready",
          "script",
          "opennamespace",
          "closenamespace",
        ]),
          Object.create ||
            (Object.create = function (t) {
              function e() {}
              return (e.prototype = t), new e();
            }),
          Object.keys ||
            (Object.keys = function (t) {
              var e = [];
              for (var n in t) t.hasOwnProperty(n) && e.push(n);
              return e;
            }),
          (o.prototype = {
            end: function () {
              P(this);
            },
            write: function (t) {
              var o = this;
              if (this.error) throw this.error;
              if (o.closed)
                return O(
                  o,
                  "Cannot write after close. Assign an onready handler."
                );
              if (null === t) return P(o);
              "object" === n(t) && (t = t.toString());
              var i = 0,
                a = "";
              for (; (a = U(t, i++)), (o.c = a), a; )
                switch (
                  (o.trackPosition &&
                    (o.position++,
                    "\n" === a ? (o.line++, (o.column = 0)) : o.column++),
                  o.state)
                ) {
                  case T.BEGIN:
                    if (((o.state = T.BEGIN_WHITESPACE), "\ufeff" === a))
                      continue;
                    j(o, a);
                    continue;
                  case T.BEGIN_WHITESPACE:
                    j(o, a);
                    continue;
                  case T.TEXT:
                    if (o.sawRoot && !o.closedRoot) {
                      for (var u = i - 1; a && "<" !== a && "&" !== a; )
                        (a = U(t, i++)) &&
                          o.trackPosition &&
                          (o.position++,
                          "\n" === a ? (o.line++, (o.column = 0)) : o.column++);
                      o.textNode += t.substring(u, i - 1);
                    }
                    "<" !== a || (o.sawRoot && o.closedRoot && !o.strict)
                      ? (y(a) ||
                          (o.sawRoot && !o.closedRoot) ||
                          L(o, "Text data outside of root node."),
                        "&" === a
                          ? (o.state = T.TEXT_ENTITY)
                          : (o.textNode += a))
                      : ((o.state = T.OPEN_WAKA),
                        (o.startTagPosition = o.position));
                    continue;
                  case T.SCRIPT:
                    "<" === a ? (o.state = T.SCRIPT_ENDING) : (o.script += a);
                    continue;
                  case T.SCRIPT_ENDING:
                    "/" === a
                      ? (o.state = T.CLOSE_TAG)
                      : ((o.script += "<" + a), (o.state = T.SCRIPT));
                    continue;
                  case T.OPEN_WAKA:
                    if ("!" === a) (o.state = T.SGML_DECL), (o.sgmlDecl = "");
                    else if (y(a));
                    else if (g(l, a)) (o.state = T.OPEN_TAG), (o.tagName = a);
                    else if ("/" === a)
                      (o.state = T.CLOSE_TAG), (o.tagName = "");
                    else if ("?" === a)
                      (o.state = T.PROC_INST),
                        (o.procInstName = o.procInstBody = "");
                    else {
                      if (
                        (L(o, "Unencoded <"),
                        o.startTagPosition + 1 < o.position)
                      ) {
                        var c = o.position - o.startTagPosition;
                        a = new Array(c).join(" ") + a;
                      }
                      (o.textNode += "<" + a), (o.state = T.TEXT);
                    }
                    continue;
                  case T.SGML_DECL:
                    "[CDATA[" === (o.sgmlDecl + a).toUpperCase()
                      ? (A(o, "onopencdata"),
                        (o.state = T.CDATA),
                        (o.sgmlDecl = ""),
                        (o.cdata = ""))
                      : o.sgmlDecl + a === "--"
                      ? ((o.state = T.COMMENT),
                        (o.comment = ""),
                        (o.sgmlDecl = ""))
                      : "DOCTYPE" === (o.sgmlDecl + a).toUpperCase()
                      ? ((o.state = T.DOCTYPE),
                        (o.doctype || o.sawRoot) &&
                          L(o, "Inappropriately located doctype declaration"),
                        (o.doctype = ""),
                        (o.sgmlDecl = ""))
                      : ">" === a
                      ? (A(o, "onsgmldeclaration", o.sgmlDecl),
                        (o.sgmlDecl = ""),
                        (o.state = T.TEXT))
                      : m(a)
                      ? ((o.state = T.SGML_DECL_QUOTED), (o.sgmlDecl += a))
                      : (o.sgmlDecl += a);
                    continue;
                  case T.SGML_DECL_QUOTED:
                    a === o.q && ((o.state = T.SGML_DECL), (o.q = "")),
                      (o.sgmlDecl += a);
                    continue;
                  case T.DOCTYPE:
                    ">" === a
                      ? ((o.state = T.TEXT),
                        A(o, "ondoctype", o.doctype),
                        (o.doctype = !0))
                      : ((o.doctype += a),
                        "[" === a
                          ? (o.state = T.DOCTYPE_DTD)
                          : m(a) && ((o.state = T.DOCTYPE_QUOTED), (o.q = a)));
                    continue;
                  case T.DOCTYPE_QUOTED:
                    (o.doctype += a),
                      a === o.q && ((o.q = ""), (o.state = T.DOCTYPE));
                    continue;
                  case T.DOCTYPE_DTD:
                    (o.doctype += a),
                      "]" === a
                        ? (o.state = T.DOCTYPE)
                        : m(a) && ((o.state = T.DOCTYPE_DTD_QUOTED), (o.q = a));
                    continue;
                  case T.DOCTYPE_DTD_QUOTED:
                    (o.doctype += a),
                      a === o.q && ((o.state = T.DOCTYPE_DTD), (o.q = ""));
                    continue;
                  case T.COMMENT:
                    "-" === a ? (o.state = T.COMMENT_ENDING) : (o.comment += a);
                    continue;
                  case T.COMMENT_ENDING:
                    "-" === a
                      ? ((o.state = T.COMMENT_ENDED),
                        (o.comment = I(o.opt, o.comment)),
                        o.comment && A(o, "oncomment", o.comment),
                        (o.comment = ""))
                      : ((o.comment += "-" + a), (o.state = T.COMMENT));
                    continue;
                  case T.COMMENT_ENDED:
                    ">" !== a
                      ? (L(o, "Malformed comment"),
                        (o.comment += "--" + a),
                        (o.state = T.COMMENT))
                      : (o.state = T.TEXT);
                    continue;
                  case T.CDATA:
                    "]" === a ? (o.state = T.CDATA_ENDING) : (o.cdata += a);
                    continue;
                  case T.CDATA_ENDING:
                    "]" === a
                      ? (o.state = T.CDATA_ENDING_2)
                      : ((o.cdata += "]" + a), (o.state = T.CDATA));
                    continue;
                  case T.CDATA_ENDING_2:
                    ">" === a
                      ? (o.cdata && A(o, "oncdata", o.cdata),
                        A(o, "onclosecdata"),
                        (o.cdata = ""),
                        (o.state = T.TEXT))
                      : "]" === a
                      ? (o.cdata += "]")
                      : ((o.cdata += "]]" + a), (o.state = T.CDATA));
                    continue;
                  case T.PROC_INST:
                    "?" === a
                      ? (o.state = T.PROC_INST_ENDING)
                      : y(a)
                      ? (o.state = T.PROC_INST_BODY)
                      : (o.procInstName += a);
                    continue;
                  case T.PROC_INST_BODY:
                    if (!o.procInstBody && y(a)) continue;
                    "?" === a
                      ? (o.state = T.PROC_INST_ENDING)
                      : (o.procInstBody += a);
                    continue;
                  case T.PROC_INST_ENDING:
                    ">" === a
                      ? (A(o, "onprocessinginstruction", {
                          name: o.procInstName,
                          body: o.procInstBody,
                        }),
                        (o.procInstName = o.procInstBody = ""),
                        (o.state = T.TEXT))
                      : ((o.procInstBody += "?" + a),
                        (o.state = T.PROC_INST_BODY));
                    continue;
                  case T.OPEN_TAG:
                    g(p, a)
                      ? (o.tagName += a)
                      : (N(o),
                        ">" === a
                          ? M(o)
                          : "/" === a
                          ? (o.state = T.OPEN_TAG_SLASH)
                          : (y(a) || L(o, "Invalid character in tag name"),
                            (o.state = T.ATTRIB)));
                    continue;
                  case T.OPEN_TAG_SLASH:
                    ">" === a
                      ? (M(o, !0), C(o))
                      : (L(o, "Forward-slash in opening tag not followed by >"),
                        (o.state = T.ATTRIB));
                    continue;
                  case T.ATTRIB:
                    if (y(a)) continue;
                    ">" === a
                      ? M(o)
                      : "/" === a
                      ? (o.state = T.OPEN_TAG_SLASH)
                      : g(l, a)
                      ? ((o.attribName = a),
                        (o.attribValue = ""),
                        (o.state = T.ATTRIB_NAME))
                      : L(o, "Invalid attribute name");
                    continue;
                  case T.ATTRIB_NAME:
                    "=" === a
                      ? (o.state = T.ATTRIB_VALUE)
                      : ">" === a
                      ? (L(o, "Attribute without value"),
                        (o.attribValue = o.attribName),
                        F(o),
                        M(o))
                      : y(a)
                      ? (o.state = T.ATTRIB_NAME_SAW_WHITE)
                      : g(p, a)
                      ? (o.attribName += a)
                      : L(o, "Invalid attribute name");
                    continue;
                  case T.ATTRIB_NAME_SAW_WHITE:
                    if ("=" === a) o.state = T.ATTRIB_VALUE;
                    else {
                      if (y(a)) continue;
                      L(o, "Attribute without value"),
                        (o.tag.attributes[o.attribName] = ""),
                        (o.attribValue = ""),
                        A(o, "onattribute", { name: o.attribName, value: "" }),
                        (o.attribName = ""),
                        ">" === a
                          ? M(o)
                          : g(l, a)
                          ? ((o.attribName = a), (o.state = T.ATTRIB_NAME))
                          : (L(o, "Invalid attribute name"),
                            (o.state = T.ATTRIB));
                    }
                    continue;
                  case T.ATTRIB_VALUE:
                    if (y(a)) continue;
                    m(a)
                      ? ((o.q = a), (o.state = T.ATTRIB_VALUE_QUOTED))
                      : (L(o, "Unquoted attribute value"),
                        (o.state = T.ATTRIB_VALUE_UNQUOTED),
                        (o.attribValue = a));
                    continue;
                  case T.ATTRIB_VALUE_QUOTED:
                    if (a !== o.q) {
                      "&" === a
                        ? (o.state = T.ATTRIB_VALUE_ENTITY_Q)
                        : (o.attribValue += a);
                      continue;
                    }
                    F(o), (o.q = ""), (o.state = T.ATTRIB_VALUE_CLOSED);
                    continue;
                  case T.ATTRIB_VALUE_CLOSED:
                    y(a)
                      ? (o.state = T.ATTRIB)
                      : ">" === a
                      ? M(o)
                      : "/" === a
                      ? (o.state = T.OPEN_TAG_SLASH)
                      : g(l, a)
                      ? (L(o, "No whitespace between attributes"),
                        (o.attribName = a),
                        (o.attribValue = ""),
                        (o.state = T.ATTRIB_NAME))
                      : L(o, "Invalid attribute name");
                    continue;
                  case T.ATTRIB_VALUE_UNQUOTED:
                    if (!v(a)) {
                      "&" === a
                        ? (o.state = T.ATTRIB_VALUE_ENTITY_U)
                        : (o.attribValue += a);
                      continue;
                    }
                    F(o), ">" === a ? M(o) : (o.state = T.ATTRIB);
                    continue;
                  case T.CLOSE_TAG:
                    if (o.tagName)
                      ">" === a
                        ? C(o)
                        : g(p, a)
                        ? (o.tagName += a)
                        : o.script
                        ? ((o.script += "</" + o.tagName),
                          (o.tagName = ""),
                          (o.state = T.SCRIPT))
                        : (y(a) || L(o, "Invalid tagname in closing tag"),
                          (o.state = T.CLOSE_TAG_SAW_WHITE));
                    else {
                      if (y(a)) continue;
                      b(l, a)
                        ? o.script
                          ? ((o.script += "</" + a), (o.state = T.SCRIPT))
                          : L(o, "Invalid tagname in closing tag.")
                        : (o.tagName = a);
                    }
                    continue;
                  case T.CLOSE_TAG_SAW_WHITE:
                    if (y(a)) continue;
                    ">" === a
                      ? C(o)
                      : L(o, "Invalid characters in closing tag");
                    continue;
                  case T.TEXT_ENTITY:
                  case T.ATTRIB_VALUE_ENTITY_Q:
                  case T.ATTRIB_VALUE_ENTITY_U:
                    var s, f;
                    switch (o.state) {
                      case T.TEXT_ENTITY:
                        (s = T.TEXT), (f = "textNode");
                        break;
                      case T.ATTRIB_VALUE_ENTITY_Q:
                        (s = T.ATTRIB_VALUE_QUOTED), (f = "attribValue");
                        break;
                      case T.ATTRIB_VALUE_ENTITY_U:
                        (s = T.ATTRIB_VALUE_UNQUOTED), (f = "attribValue");
                    }
                    ";" === a
                      ? ((o[f] += B(o)), (o.entity = ""), (o.state = s))
                      : g(o.entity.length ? d : h, a)
                      ? (o.entity += a)
                      : (L(o, "Invalid character in entity name"),
                        (o[f] += "&" + o.entity + a),
                        (o.entity = ""),
                        (o.state = s));
                    continue;
                  default:
                    throw new Error(o, "Unknown state: " + o.state);
                }
              o.position >= o.bufferCheckPosition &&
                (function (t) {
                  for (
                    var n = Math.max(e.MAX_BUFFER_LENGTH, 10),
                      o = 0,
                      i = 0,
                      a = r.length;
                    i < a;
                    i++
                  ) {
                    var u = t[r[i]].length;
                    if (u > n)
                      switch (r[i]) {
                        case "textNode":
                          R(t);
                          break;
                        case "cdata":
                          A(t, "oncdata", t.cdata), (t.cdata = "");
                          break;
                        case "script":
                          A(t, "onscript", t.script), (t.script = "");
                          break;
                        default:
                          O(t, "Max buffer length exceeded: " + r[i]);
                      }
                    o = Math.max(o, u);
                  }
                  var c = e.MAX_BUFFER_LENGTH - o;
                  t.bufferCheckPosition = c + t.position;
                })(o);
              return o;
            },
            resume: function () {
              return (this.error = null), this;
            },
            close: function () {
              return this.write(null);
            },
            flush: function () {
              var t;
              R((t = this)),
                "" !== t.cdata && (A(t, "oncdata", t.cdata), (t.cdata = "")),
                "" !== t.script &&
                  (A(t, "onscript", t.script), (t.script = ""));
            },
          });
        var i = function () {},
          a = e.EVENTS.filter(function (t) {
            return "error" !== t && "end" !== t;
          });
        function u(t, e) {
          if (!(this instanceof u)) return new u(t, e);
          i.apply(this),
            (this._parser = new o(t, e)),
            (this.writable = !0),
            (this.readable = !0);
          var n = this;
          (this._parser.onend = function () {
            n.emit("end");
          }),
            (this._parser.onerror = function (t) {
              n.emit("error", t), (n._parser.error = null);
            }),
            (this._decoder = null),
            a.forEach(function (t) {
              Object.defineProperty(n, "on" + t, {
                get: function () {
                  return n._parser["on" + t];
                },
                set: function (e) {
                  if (!e)
                    return (
                      n.removeAllListeners(t), (n._parser["on" + t] = e), e
                    );
                  n.on(t, e);
                },
                enumerable: !0,
                configurable: !1,
              });
            });
        }
        (u.prototype = Object.create(i.prototype, {
          constructor: { value: u },
        })),
          (u.prototype.write = function (e) {
            if (
              "function" == typeof t &&
              "function" == typeof t.isBuffer &&
              t.isBuffer(e)
            ) {
              if (!this._decoder) {
                this._decoder = new null("utf8");
              }
              e = this._decoder.write(e);
            }
            return this._parser.write(e.toString()), this.emit("data", e), !0;
          }),
          (u.prototype.end = function (t) {
            return t && t.length && this.write(t), this._parser.end(), !0;
          }),
          (u.prototype.on = function (t, e) {
            var n = this;
            return (
              n._parser["on" + t] ||
                -1 === a.indexOf(t) ||
                (n._parser["on" + t] = function () {
                  var e =
                    1 === arguments.length
                      ? [arguments[0]]
                      : Array.apply(null, arguments);
                  e.splice(0, 0, t), n.emit.apply(n, e);
                }),
              i.prototype.on.call(n, t, e)
            );
          });
        var c = "http://www.w3.org/XML/1998/namespace",
          s = "http://www.w3.org/2000/xmlns/",
          f = { xml: c, xmlns: s },
          l =
            /[:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD]/,
          p =
            /[:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u00B7\u0300-\u036F\u203F-\u2040.\d-]/,
          h =
            /[#:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD]/,
          d =
            /[#:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u00B7\u0300-\u036F\u203F-\u2040.\d-]/;
        function y(t) {
          return " " === t || "\n" === t || "\r" === t || "\t" === t;
        }
        function m(t) {
          return '"' === t || "'" === t;
        }
        function v(t) {
          return ">" === t || y(t);
        }
        function g(t, e) {
          return t.test(e);
        }
        function b(t, e) {
          return !g(t, e);
        }
        var w,
          _,
          E,
          T = 0;
        for (var S in ((e.STATE = {
          BEGIN: T++,
          BEGIN_WHITESPACE: T++,
          TEXT: T++,
          TEXT_ENTITY: T++,
          OPEN_WAKA: T++,
          SGML_DECL: T++,
          SGML_DECL_QUOTED: T++,
          DOCTYPE: T++,
          DOCTYPE_QUOTED: T++,
          DOCTYPE_DTD: T++,
          DOCTYPE_DTD_QUOTED: T++,
          COMMENT_STARTING: T++,
          COMMENT: T++,
          COMMENT_ENDING: T++,
          COMMENT_ENDED: T++,
          CDATA: T++,
          CDATA_ENDING: T++,
          CDATA_ENDING_2: T++,
          PROC_INST: T++,
          PROC_INST_BODY: T++,
          PROC_INST_ENDING: T++,
          OPEN_TAG: T++,
          OPEN_TAG_SLASH: T++,
          ATTRIB: T++,
          ATTRIB_NAME: T++,
          ATTRIB_NAME_SAW_WHITE: T++,
          ATTRIB_VALUE: T++,
          ATTRIB_VALUE_QUOTED: T++,
          ATTRIB_VALUE_CLOSED: T++,
          ATTRIB_VALUE_UNQUOTED: T++,
          ATTRIB_VALUE_ENTITY_Q: T++,
          ATTRIB_VALUE_ENTITY_U: T++,
          CLOSE_TAG: T++,
          CLOSE_TAG_SAW_WHITE: T++,
          SCRIPT: T++,
          SCRIPT_ENDING: T++,
        }),
        (e.XML_ENTITIES = { amp: "&", gt: ">", lt: "<", quot: '"', apos: "'" }),
        (e.ENTITIES = {
          amp: "&",
          gt: ">",
          lt: "<",
          quot: '"',
          apos: "'",
          AElig: 198,
          Aacute: 193,
          Acirc: 194,
          Agrave: 192,
          Aring: 197,
          Atilde: 195,
          Auml: 196,
          Ccedil: 199,
          ETH: 208,
          Eacute: 201,
          Ecirc: 202,
          Egrave: 200,
          Euml: 203,
          Iacute: 205,
          Icirc: 206,
          Igrave: 204,
          Iuml: 207,
          Ntilde: 209,
          Oacute: 211,
          Ocirc: 212,
          Ograve: 210,
          Oslash: 216,
          Otilde: 213,
          Ouml: 214,
          THORN: 222,
          Uacute: 218,
          Ucirc: 219,
          Ugrave: 217,
          Uuml: 220,
          Yacute: 221,
          aacute: 225,
          acirc: 226,
          aelig: 230,
          agrave: 224,
          aring: 229,
          atilde: 227,
          auml: 228,
          ccedil: 231,
          eacute: 233,
          ecirc: 234,
          egrave: 232,
          eth: 240,
          euml: 235,
          iacute: 237,
          icirc: 238,
          igrave: 236,
          iuml: 239,
          ntilde: 241,
          oacute: 243,
          ocirc: 244,
          ograve: 242,
          oslash: 248,
          otilde: 245,
          ouml: 246,
          szlig: 223,
          thorn: 254,
          uacute: 250,
          ucirc: 251,
          ugrave: 249,
          uuml: 252,
          yacute: 253,
          yuml: 255,
          copy: 169,
          reg: 174,
          nbsp: 160,
          iexcl: 161,
          cent: 162,
          pound: 163,
          curren: 164,
          yen: 165,
          brvbar: 166,
          sect: 167,
          uml: 168,
          ordf: 170,
          laquo: 171,
          not: 172,
          shy: 173,
          macr: 175,
          deg: 176,
          plusmn: 177,
          sup1: 185,
          sup2: 178,
          sup3: 179,
          acute: 180,
          micro: 181,
          para: 182,
          middot: 183,
          cedil: 184,
          ordm: 186,
          raquo: 187,
          frac14: 188,
          frac12: 189,
          frac34: 190,
          iquest: 191,
          times: 215,
          divide: 247,
          OElig: 338,
          oelig: 339,
          Scaron: 352,
          scaron: 353,
          Yuml: 376,
          fnof: 402,
          circ: 710,
          tilde: 732,
          Alpha: 913,
          Beta: 914,
          Gamma: 915,
          Delta: 916,
          Epsilon: 917,
          Zeta: 918,
          Eta: 919,
          Theta: 920,
          Iota: 921,
          Kappa: 922,
          Lambda: 923,
          Mu: 924,
          Nu: 925,
          Xi: 926,
          Omicron: 927,
          Pi: 928,
          Rho: 929,
          Sigma: 931,
          Tau: 932,
          Upsilon: 933,
          Phi: 934,
          Chi: 935,
          Psi: 936,
          Omega: 937,
          alpha: 945,
          beta: 946,
          gamma: 947,
          delta: 948,
          epsilon: 949,
          zeta: 950,
          eta: 951,
          theta: 952,
          iota: 953,
          kappa: 954,
          lambda: 955,
          mu: 956,
          nu: 957,
          xi: 958,
          omicron: 959,
          pi: 960,
          rho: 961,
          sigmaf: 962,
          sigma: 963,
          tau: 964,
          upsilon: 965,
          phi: 966,
          chi: 967,
          psi: 968,
          omega: 969,
          thetasym: 977,
          upsih: 978,
          piv: 982,
          ensp: 8194,
          emsp: 8195,
          thinsp: 8201,
          zwnj: 8204,
          zwj: 8205,
          lrm: 8206,
          rlm: 8207,
          ndash: 8211,
          mdash: 8212,
          lsquo: 8216,
          rsquo: 8217,
          sbquo: 8218,
          ldquo: 8220,
          rdquo: 8221,
          bdquo: 8222,
          dagger: 8224,
          Dagger: 8225,
          bull: 8226,
          hellip: 8230,
          permil: 8240,
          prime: 8242,
          Prime: 8243,
          lsaquo: 8249,
          rsaquo: 8250,
          oline: 8254,
          frasl: 8260,
          euro: 8364,
          image: 8465,
          weierp: 8472,
          real: 8476,
          trade: 8482,
          alefsym: 8501,
          larr: 8592,
          uarr: 8593,
          rarr: 8594,
          darr: 8595,
          harr: 8596,
          crarr: 8629,
          lArr: 8656,
          uArr: 8657,
          rArr: 8658,
          dArr: 8659,
          hArr: 8660,
          forall: 8704,
          part: 8706,
          exist: 8707,
          empty: 8709,
          nabla: 8711,
          isin: 8712,
          notin: 8713,
          ni: 8715,
          prod: 8719,
          sum: 8721,
          minus: 8722,
          lowast: 8727,
          radic: 8730,
          prop: 8733,
          infin: 8734,
          ang: 8736,
          and: 8743,
          or: 8744,
          cap: 8745,
          cup: 8746,
          int: 8747,
          there4: 8756,
          sim: 8764,
          cong: 8773,
          asymp: 8776,
          ne: 8800,
          equiv: 8801,
          le: 8804,
          ge: 8805,
          sub: 8834,
          sup: 8835,
          nsub: 8836,
          sube: 8838,
          supe: 8839,
          oplus: 8853,
          otimes: 8855,
          perp: 8869,
          sdot: 8901,
          lceil: 8968,
          rceil: 8969,
          lfloor: 8970,
          rfloor: 8971,
          lang: 9001,
          rang: 9002,
          loz: 9674,
          spades: 9824,
          clubs: 9827,
          hearts: 9829,
          diams: 9830,
        }),
        Object.keys(e.ENTITIES).forEach(function (t) {
          var n = e.ENTITIES[t],
            r = "number" == typeof n ? String.fromCharCode(n) : n;
          e.ENTITIES[t] = r;
        }),
        e.STATE))
          e.STATE[e.STATE[S]] = S;
        function x(t, e, n) {
          t[e] && t[e](n);
        }
        function A(t, e, n) {
          t.textNode && R(t), x(t, e, n);
        }
        function R(t) {
          (t.textNode = I(t.opt, t.textNode)),
            t.textNode && x(t, "ontext", t.textNode),
            (t.textNode = "");
        }
        function I(t, e) {
          return (
            t.trim && (e = e.trim()),
            t.normalize && (e = e.replace(/\s+/g, " ")),
            e
          );
        }
        function O(t, e) {
          return (
            R(t),
            t.trackPosition &&
              (e +=
                "\nLine: " +
                t.line +
                "\nColumn: " +
                t.column +
                "\nChar: " +
                t.c),
            (e = new Error(e)),
            (t.error = e),
            x(t, "onerror", e),
            t
          );
        }
        function P(t) {
          return (
            t.sawRoot && !t.closedRoot && L(t, "Unclosed root tag"),
            t.state !== T.BEGIN &&
              t.state !== T.BEGIN_WHITESPACE &&
              t.state !== T.TEXT &&
              O(t, "Unexpected end"),
            R(t),
            (t.c = ""),
            (t.closed = !0),
            x(t, "onend"),
            o.call(t, t.strict, t.opt),
            t
          );
        }
        function L(t, e) {
          if ("object" !== n(t) || !(t instanceof o))
            throw new Error("bad call to strictFail");
          t.strict && O(t, e);
        }
        function N(t) {
          t.strict || (t.tagName = t.tagName[t.looseCase]());
          var e = t.tags[t.tags.length - 1] || t,
            n = (t.tag = { name: t.tagName, attributes: {} });
          t.opt.xmlns && (n.ns = e.ns),
            (t.attribList.length = 0),
            A(t, "onopentagstart", n);
        }
        function D(t, e) {
          var n = t.indexOf(":") < 0 ? ["", t] : t.split(":"),
            r = n[0],
            o = n[1];
          return (
            e && "xmlns" === t && ((r = "xmlns"), (o = "")),
            { prefix: r, local: o }
          );
        }
        function F(t) {
          if (
            (t.strict || (t.attribName = t.attribName[t.looseCase]()),
            -1 !== t.attribList.indexOf(t.attribName) ||
              t.tag.attributes.hasOwnProperty(t.attribName))
          )
            t.attribName = t.attribValue = "";
          else {
            if (t.opt.xmlns) {
              var e = D(t.attribName, !0),
                n = e.prefix,
                r = e.local;
              if ("xmlns" === n)
                if ("xml" === r && t.attribValue !== c)
                  L(
                    t,
                    "xml: prefix must be bound to " +
                      c +
                      "\nActual: " +
                      t.attribValue
                  );
                else if ("xmlns" === r && t.attribValue !== s)
                  L(
                    t,
                    "xmlns: prefix must be bound to " +
                      s +
                      "\nActual: " +
                      t.attribValue
                  );
                else {
                  var o = t.tag,
                    i = t.tags[t.tags.length - 1] || t;
                  o.ns === i.ns && (o.ns = Object.create(i.ns)),
                    (o.ns[r] = t.attribValue);
                }
              t.attribList.push([t.attribName, t.attribValue]);
            } else
              (t.tag.attributes[t.attribName] = t.attribValue),
                A(t, "onattribute", {
                  name: t.attribName,
                  value: t.attribValue,
                });
            t.attribName = t.attribValue = "";
          }
        }
        function M(t, e) {
          if (t.opt.xmlns) {
            var n = t.tag,
              r = D(t.tagName);
            (n.prefix = r.prefix),
              (n.local = r.local),
              (n.uri = n.ns[r.prefix] || ""),
              n.prefix &&
                !n.uri &&
                (L(t, "Unbound namespace prefix: " + JSON.stringify(t.tagName)),
                (n.uri = r.prefix));
            var o = t.tags[t.tags.length - 1] || t;
            n.ns &&
              o.ns !== n.ns &&
              Object.keys(n.ns).forEach(function (e) {
                A(t, "onopennamespace", { prefix: e, uri: n.ns[e] });
              });
            for (var i = 0, a = t.attribList.length; i < a; i++) {
              var u = t.attribList[i],
                c = u[0],
                s = u[1],
                f = D(c, !0),
                l = f.prefix,
                p = f.local,
                h = "" === l ? "" : n.ns[l] || "",
                d = { name: c, value: s, prefix: l, local: p, uri: h };
              l &&
                "xmlns" !== l &&
                !h &&
                (L(t, "Unbound namespace prefix: " + JSON.stringify(l)),
                (d.uri = l)),
                (t.tag.attributes[c] = d),
                A(t, "onattribute", d);
            }
            t.attribList.length = 0;
          }
          (t.tag.isSelfClosing = !!e),
            (t.sawRoot = !0),
            t.tags.push(t.tag),
            A(t, "onopentag", t.tag),
            e ||
              (t.noscript || "script" !== t.tagName.toLowerCase()
                ? (t.state = T.TEXT)
                : (t.state = T.SCRIPT),
              (t.tag = null),
              (t.tagName = "")),
            (t.attribName = t.attribValue = ""),
            (t.attribList.length = 0);
        }
        function C(t) {
          if (!t.tagName)
            return (
              L(t, "Weird empty close tag."),
              (t.textNode += "</>"),
              void (t.state = T.TEXT)
            );
          if (t.script) {
            if ("script" !== t.tagName)
              return (
                (t.script += "</" + t.tagName + ">"),
                (t.tagName = ""),
                void (t.state = T.SCRIPT)
              );
            A(t, "onscript", t.script), (t.script = "");
          }
          var e = t.tags.length,
            n = t.tagName;
          t.strict || (n = n[t.looseCase]());
          for (var r = n; e--; ) {
            if (t.tags[e].name === r) break;
            L(t, "Unexpected close tag");
          }
          if (e < 0)
            return (
              L(t, "Unmatched closing tag: " + t.tagName),
              (t.textNode += "</" + t.tagName + ">"),
              void (t.state = T.TEXT)
            );
          t.tagName = n;
          for (var o = t.tags.length; o-- > e; ) {
            var i = (t.tag = t.tags.pop());
            (t.tagName = t.tag.name), A(t, "onclosetag", t.tagName);
            var a = {};
            for (var u in i.ns) a[u] = i.ns[u];
            var c = t.tags[t.tags.length - 1] || t;
            t.opt.xmlns &&
              i.ns !== c.ns &&
              Object.keys(i.ns).forEach(function (e) {
                var n = i.ns[e];
                A(t, "onclosenamespace", { prefix: e, uri: n });
              });
          }
          0 === e && (t.closedRoot = !0),
            (t.tagName = t.attribValue = t.attribName = ""),
            (t.attribList.length = 0),
            (t.state = T.TEXT);
        }
        function B(t) {
          var e,
            n = t.entity,
            r = n.toLowerCase(),
            o = "";
          return t.ENTITIES[n]
            ? t.ENTITIES[n]
            : t.ENTITIES[r]
            ? t.ENTITIES[r]
            : ("#" === (n = r).charAt(0) &&
                ("x" === n.charAt(1)
                  ? ((n = n.slice(2)), (o = (e = parseInt(n, 16)).toString(16)))
                  : ((n = n.slice(1)),
                    (o = (e = parseInt(n, 10)).toString(10)))),
              (n = n.replace(/^0+/, "")),
              isNaN(e) || o.toLowerCase() !== n
                ? (L(t, "Invalid character entity"), "&" + t.entity + ";")
                : String.fromCodePoint(e));
        }
        function j(t, e) {
          "<" === e
            ? ((t.state = T.OPEN_WAKA), (t.startTagPosition = t.position))
            : y(e) ||
              (L(t, "Non-whitespace before first tag."),
              (t.textNode = e),
              (t.state = T.TEXT));
        }
        function U(t, e) {
          var n = "";
          return e < t.length && (n = t.charAt(e)), n;
        }
        (T = e.STATE),
          String.fromCodePoint ||
            ((w = String.fromCharCode),
            (_ = Math.floor),
            (E = function () {
              var t,
                e,
                n = 16384,
                r = [],
                o = -1,
                i = arguments.length;
              if (!i) return "";
              for (var a = ""; ++o < i; ) {
                var u = Number(arguments[o]);
                if (!isFinite(u) || u < 0 || u > 1114111 || _(u) !== u)
                  throw RangeError("Invalid code point: " + u);
                u <= 65535
                  ? r.push(u)
                  : ((t = 55296 + ((u -= 65536) >> 10)),
                    (e = (u % 1024) + 56320),
                    r.push(t, e)),
                  (o + 1 === i || r.length > n) &&
                    ((a += w.apply(null, r)), (r.length = 0));
              }
              return a;
            }),
            Object.defineProperty
              ? Object.defineProperty(String, "fromCodePoint", {
                  value: E,
                  configurable: !0,
                  writable: !0,
                })
              : (String.fromCodePoint = E));
      })(e);
    }.call(this, n(329).Buffer));
  },
  function (t, e, n) {
    "use strict";
    (function (t) {
      var r = n(330),
        o = n(331),
        i = n(332);
      function a() {
        return c.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
      }
      function u(t, e) {
        if (a() < e) throw new RangeError("Invalid typed array length");
        return (
          c.TYPED_ARRAY_SUPPORT
            ? ((t = new Uint8Array(e)).__proto__ = c.prototype)
            : (null === t && (t = new c(e)), (t.length = e)),
          t
        );
      }
      function c(t, e, n) {
        if (!(c.TYPED_ARRAY_SUPPORT || this instanceof c))
          return new c(t, e, n);
        if ("number" == typeof t) {
          if ("string" == typeof e)
            throw new Error(
              "If encoding is specified then the first argument must be a string"
            );
          return l(this, t);
        }
        return s(this, t, e, n);
      }
      function s(t, e, n, r) {
        if ("number" == typeof e)
          throw new TypeError('"value" argument must not be a number');
        return "undefined" != typeof ArrayBuffer && e instanceof ArrayBuffer
          ? (function (t, e, n, r) {
              if ((e.byteLength, n < 0 || e.byteLength < n))
                throw new RangeError("'offset' is out of bounds");
              if (e.byteLength < n + (r || 0))
                throw new RangeError("'length' is out of bounds");
              e =
                n === undefined && r === undefined
                  ? new Uint8Array(e)
                  : r === undefined
                  ? new Uint8Array(e, n)
                  : new Uint8Array(e, n, r);
              c.TYPED_ARRAY_SUPPORT
                ? ((t = e).__proto__ = c.prototype)
                : (t = p(t, e));
              return t;
            })(t, e, n, r)
          : "string" == typeof e
          ? (function (t, e, n) {
              ("string" == typeof n && "" !== n) || (n = "utf8");
              if (!c.isEncoding(n))
                throw new TypeError(
                  '"encoding" must be a valid string encoding'
                );
              var r = 0 | d(e, n),
                o = (t = u(t, r)).write(e, n);
              o !== r && (t = t.slice(0, o));
              return t;
            })(t, e, n)
          : (function (t, e) {
              if (c.isBuffer(e)) {
                var n = 0 | h(e.length);
                return 0 === (t = u(t, n)).length || e.copy(t, 0, 0, n), t;
              }
              if (e) {
                if (
                  ("undefined" != typeof ArrayBuffer &&
                    e.buffer instanceof ArrayBuffer) ||
                  "length" in e
                )
                  return "number" != typeof e.length || (r = e.length) != r
                    ? u(t, 0)
                    : p(t, e);
                if ("Buffer" === e.type && i(e.data)) return p(t, e.data);
              }
              var r;
              throw new TypeError(
                "First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object."
              );
            })(t, e);
      }
      function f(t) {
        if ("number" != typeof t)
          throw new TypeError('"size" argument must be a number');
        if (t < 0) throw new RangeError('"size" argument must not be negative');
      }
      function l(t, e) {
        if ((f(e), (t = u(t, e < 0 ? 0 : 0 | h(e))), !c.TYPED_ARRAY_SUPPORT))
          for (var n = 0; n < e; ++n) t[n] = 0;
        return t;
      }
      function p(t, e) {
        var n = e.length < 0 ? 0 : 0 | h(e.length);
        t = u(t, n);
        for (var r = 0; r < n; r += 1) t[r] = 255 & e[r];
        return t;
      }
      function h(t) {
        if (t >= a())
          throw new RangeError(
            "Attempt to allocate Buffer larger than maximum size: 0x" +
              a().toString(16) +
              " bytes"
          );
        return 0 | t;
      }
      function d(t, e) {
        if (c.isBuffer(t)) return t.length;
        if (
          "undefined" != typeof ArrayBuffer &&
          "function" == typeof ArrayBuffer.isView &&
          (ArrayBuffer.isView(t) || t instanceof ArrayBuffer)
        )
          return t.byteLength;
        "string" != typeof t && (t = "" + t);
        var n = t.length;
        if (0 === n) return 0;
        for (var r = !1; ; )
          switch (e) {
            case "ascii":
            case "latin1":
            case "binary":
              return n;
            case "utf8":
            case "utf-8":
            case undefined:
              return V(t).length;
            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return 2 * n;
            case "hex":
              return n >>> 1;
            case "base64":
              return q(t).length;
            default:
              if (r) return V(t).length;
              (e = ("" + e).toLowerCase()), (r = !0);
          }
      }
      function y(t, e, n) {
        var r = !1;
        if (((e === undefined || e < 0) && (e = 0), e > this.length)) return "";
        if (((n === undefined || n > this.length) && (n = this.length), n <= 0))
          return "";
        if ((n >>>= 0) <= (e >>>= 0)) return "";
        for (t || (t = "utf8"); ; )
          switch (t) {
            case "hex":
              return P(this, e, n);
            case "utf8":
            case "utf-8":
              return A(this, e, n);
            case "ascii":
              return I(this, e, n);
            case "latin1":
            case "binary":
              return O(this, e, n);
            case "base64":
              return x(this, e, n);
            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return L(this, e, n);
            default:
              if (r) throw new TypeError("Unknown encoding: " + t);
              (t = (t + "").toLowerCase()), (r = !0);
          }
      }
      function m(t, e, n) {
        var r = t[e];
        (t[e] = t[n]), (t[n] = r);
      }
      function v(t, e, n, r, o) {
        if (0 === t.length) return -1;
        if (
          ("string" == typeof n
            ? ((r = n), (n = 0))
            : n > 2147483647
            ? (n = 2147483647)
            : n < -2147483648 && (n = -2147483648),
          (n = +n),
          isNaN(n) && (n = o ? 0 : t.length - 1),
          n < 0 && (n = t.length + n),
          n >= t.length)
        ) {
          if (o) return -1;
          n = t.length - 1;
        } else if (n < 0) {
          if (!o) return -1;
          n = 0;
        }
        if (("string" == typeof e && (e = c.from(e, r)), c.isBuffer(e)))
          return 0 === e.length ? -1 : g(t, e, n, r, o);
        if ("number" == typeof e)
          return (
            (e &= 255),
            c.TYPED_ARRAY_SUPPORT &&
            "function" == typeof Uint8Array.prototype.indexOf
              ? o
                ? Uint8Array.prototype.indexOf.call(t, e, n)
                : Uint8Array.prototype.lastIndexOf.call(t, e, n)
              : g(t, [e], n, r, o)
          );
        throw new TypeError("val must be string, number or Buffer");
      }
      function g(t, e, n, r, o) {
        var i,
          a = 1,
          u = t.length,
          c = e.length;
        if (
          r !== undefined &&
          ("ucs2" === (r = String(r).toLowerCase()) ||
            "ucs-2" === r ||
            "utf16le" === r ||
            "utf-16le" === r)
        ) {
          if (t.length < 2 || e.length < 2) return -1;
          (a = 2), (u /= 2), (c /= 2), (n /= 2);
        }
        function s(t, e) {
          return 1 === a ? t[e] : t.readUInt16BE(e * a);
        }
        if (o) {
          var f = -1;
          for (i = n; i < u; i++)
            if (s(t, i) === s(e, -1 === f ? 0 : i - f)) {
              if ((-1 === f && (f = i), i - f + 1 === c)) return f * a;
            } else -1 !== f && (i -= i - f), (f = -1);
        } else
          for (n + c > u && (n = u - c), i = n; i >= 0; i--) {
            for (var l = !0, p = 0; p < c; p++)
              if (s(t, i + p) !== s(e, p)) {
                l = !1;
                break;
              }
            if (l) return i;
          }
        return -1;
      }
      function b(t, e, n, r) {
        n = Number(n) || 0;
        var o = t.length - n;
        r ? (r = Number(r)) > o && (r = o) : (r = o);
        var i = e.length;
        if (i % 2 != 0) throw new TypeError("Invalid hex string");
        r > i / 2 && (r = i / 2);
        for (var a = 0; a < r; ++a) {
          var u = parseInt(e.substr(2 * a, 2), 16);
          if (isNaN(u)) return a;
          t[n + a] = u;
        }
        return a;
      }
      function w(t, e, n, r) {
        return Y(V(e, t.length - n), t, n, r);
      }
      function _(t, e, n, r) {
        return Y(
          (function (t) {
            for (var e = [], n = 0; n < t.length; ++n)
              e.push(255 & t.charCodeAt(n));
            return e;
          })(e),
          t,
          n,
          r
        );
      }
      function E(t, e, n, r) {
        return _(t, e, n, r);
      }
      function T(t, e, n, r) {
        return Y(q(e), t, n, r);
      }
      function S(t, e, n, r) {
        return Y(
          (function (t, e) {
            for (
              var n, r, o, i = [], a = 0;
              a < t.length && !((e -= 2) < 0);
              ++a
            )
              (r = (n = t.charCodeAt(a)) >> 8),
                (o = n % 256),
                i.push(o),
                i.push(r);
            return i;
          })(e, t.length - n),
          t,
          n,
          r
        );
      }
      function x(t, e, n) {
        return 0 === e && n === t.length
          ? r.fromByteArray(t)
          : r.fromByteArray(t.slice(e, n));
      }
      function A(t, e, n) {
        n = Math.min(t.length, n);
        for (var r = [], o = e; o < n; ) {
          var i,
            a,
            u,
            c,
            s = t[o],
            f = null,
            l = s > 239 ? 4 : s > 223 ? 3 : s > 191 ? 2 : 1;
          if (o + l <= n)
            switch (l) {
              case 1:
                s < 128 && (f = s);
                break;
              case 2:
                128 == (192 & (i = t[o + 1])) &&
                  (c = ((31 & s) << 6) | (63 & i)) > 127 &&
                  (f = c);
                break;
              case 3:
                (i = t[o + 1]),
                  (a = t[o + 2]),
                  128 == (192 & i) &&
                    128 == (192 & a) &&
                    (c = ((15 & s) << 12) | ((63 & i) << 6) | (63 & a)) >
                      2047 &&
                    (c < 55296 || c > 57343) &&
                    (f = c);
                break;
              case 4:
                (i = t[o + 1]),
                  (a = t[o + 2]),
                  (u = t[o + 3]),
                  128 == (192 & i) &&
                    128 == (192 & a) &&
                    128 == (192 & u) &&
                    (c =
                      ((15 & s) << 18) |
                      ((63 & i) << 12) |
                      ((63 & a) << 6) |
                      (63 & u)) > 65535 &&
                    c < 1114112 &&
                    (f = c);
            }
          null === f
            ? ((f = 65533), (l = 1))
            : f > 65535 &&
              ((f -= 65536),
              r.push(((f >>> 10) & 1023) | 55296),
              (f = 56320 | (1023 & f))),
            r.push(f),
            (o += l);
        }
        return (function (t) {
          var e = t.length;
          if (e <= R) return String.fromCharCode.apply(String, t);
          var n = "",
            r = 0;
          for (; r < e; )
            n += String.fromCharCode.apply(String, t.slice(r, (r += R)));
          return n;
        })(r);
      }
      (e.Buffer = c),
        (e.SlowBuffer = function (t) {
          +t != t && (t = 0);
          return c.alloc(+t);
        }),
        (e.INSPECT_MAX_BYTES = 50),
        (c.TYPED_ARRAY_SUPPORT =
          t.TYPED_ARRAY_SUPPORT !== undefined
            ? t.TYPED_ARRAY_SUPPORT
            : (function () {
                try {
                  var t = new Uint8Array(1);
                  return (
                    (t.__proto__ = {
                      __proto__: Uint8Array.prototype,
                      foo: function () {
                        return 42;
                      },
                    }),
                    42 === t.foo() &&
                      "function" == typeof t.subarray &&
                      0 === t.subarray(1, 1).byteLength
                  );
                } catch (e) {
                  return !1;
                }
              })()),
        (e.kMaxLength = a()),
        (c.poolSize = 8192),
        (c._augment = function (t) {
          return (t.__proto__ = c.prototype), t;
        }),
        (c.from = function (t, e, n) {
          return s(null, t, e, n);
        }),
        c.TYPED_ARRAY_SUPPORT &&
          ((c.prototype.__proto__ = Uint8Array.prototype),
          (c.__proto__ = Uint8Array),
          "undefined" != typeof Symbol &&
            Symbol.species &&
            c[Symbol.species] === c &&
            Object.defineProperty(c, Symbol.species, {
              value: null,
              configurable: !0,
            })),
        (c.alloc = function (t, e, n) {
          return (function (t, e, n, r) {
            return (
              f(e),
              e <= 0
                ? u(t, e)
                : n !== undefined
                ? "string" == typeof r
                  ? u(t, e).fill(n, r)
                  : u(t, e).fill(n)
                : u(t, e)
            );
          })(null, t, e, n);
        }),
        (c.allocUnsafe = function (t) {
          return l(null, t);
        }),
        (c.allocUnsafeSlow = function (t) {
          return l(null, t);
        }),
        (c.isBuffer = function (t) {
          return !(null == t || !t._isBuffer);
        }),
        (c.compare = function (t, e) {
          if (!c.isBuffer(t) || !c.isBuffer(e))
            throw new TypeError("Arguments must be Buffers");
          if (t === e) return 0;
          for (
            var n = t.length, r = e.length, o = 0, i = Math.min(n, r);
            o < i;
            ++o
          )
            if (t[o] !== e[o]) {
              (n = t[o]), (r = e[o]);
              break;
            }
          return n < r ? -1 : r < n ? 1 : 0;
        }),
        (c.isEncoding = function (t) {
          switch (String(t).toLowerCase()) {
            case "hex":
            case "utf8":
            case "utf-8":
            case "ascii":
            case "latin1":
            case "binary":
            case "base64":
            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return !0;
            default:
              return !1;
          }
        }),
        (c.concat = function (t, e) {
          if (!i(t))
            throw new TypeError('"list" argument must be an Array of Buffers');
          if (0 === t.length) return c.alloc(0);
          var n;
          if (e === undefined)
            for (e = 0, n = 0; n < t.length; ++n) e += t[n].length;
          var r = c.allocUnsafe(e),
            o = 0;
          for (n = 0; n < t.length; ++n) {
            var a = t[n];
            if (!c.isBuffer(a))
              throw new TypeError(
                '"list" argument must be an Array of Buffers'
              );
            a.copy(r, o), (o += a.length);
          }
          return r;
        }),
        (c.byteLength = d),
        (c.prototype._isBuffer = !0),
        (c.prototype.swap16 = function () {
          var t = this.length;
          if (t % 2 != 0)
            throw new RangeError("Buffer size must be a multiple of 16-bits");
          for (var e = 0; e < t; e += 2) m(this, e, e + 1);
          return this;
        }),
        (c.prototype.swap32 = function () {
          var t = this.length;
          if (t % 4 != 0)
            throw new RangeError("Buffer size must be a multiple of 32-bits");
          for (var e = 0; e < t; e += 4)
            m(this, e, e + 3), m(this, e + 1, e + 2);
          return this;
        }),
        (c.prototype.swap64 = function () {
          var t = this.length;
          if (t % 8 != 0)
            throw new RangeError("Buffer size must be a multiple of 64-bits");
          for (var e = 0; e < t; e += 8)
            m(this, e, e + 7),
              m(this, e + 1, e + 6),
              m(this, e + 2, e + 5),
              m(this, e + 3, e + 4);
          return this;
        }),
        (c.prototype.toString = function () {
          var t = 0 | this.length;
          return 0 === t
            ? ""
            : 0 === arguments.length
            ? A(this, 0, t)
            : y.apply(this, arguments);
        }),
        (c.prototype.equals = function (t) {
          if (!c.isBuffer(t)) throw new TypeError("Argument must be a Buffer");
          return this === t || 0 === c.compare(this, t);
        }),
        (c.prototype.inspect = function () {
          var t = "",
            n = e.INSPECT_MAX_BYTES;
          return (
            this.length > 0 &&
              ((t = this.toString("hex", 0, n).match(/.{2}/g).join(" ")),
              this.length > n && (t += " ... ")),
            "<Buffer " + t + ">"
          );
        }),
        (c.prototype.compare = function (t, e, n, r, o) {
          if (!c.isBuffer(t)) throw new TypeError("Argument must be a Buffer");
          if (
            (e === undefined && (e = 0),
            n === undefined && (n = t ? t.length : 0),
            r === undefined && (r = 0),
            o === undefined && (o = this.length),
            e < 0 || n > t.length || r < 0 || o > this.length)
          )
            throw new RangeError("out of range index");
          if (r >= o && e >= n) return 0;
          if (r >= o) return -1;
          if (e >= n) return 1;
          if (this === t) return 0;
          for (
            var i = (o >>>= 0) - (r >>>= 0),
              a = (n >>>= 0) - (e >>>= 0),
              u = Math.min(i, a),
              s = this.slice(r, o),
              f = t.slice(e, n),
              l = 0;
            l < u;
            ++l
          )
            if (s[l] !== f[l]) {
              (i = s[l]), (a = f[l]);
              break;
            }
          return i < a ? -1 : a < i ? 1 : 0;
        }),
        (c.prototype.includes = function (t, e, n) {
          return -1 !== this.indexOf(t, e, n);
        }),
        (c.prototype.indexOf = function (t, e, n) {
          return v(this, t, e, n, !0);
        }),
        (c.prototype.lastIndexOf = function (t, e, n) {
          return v(this, t, e, n, !1);
        }),
        (c.prototype.write = function (t, e, n, r) {
          if (e === undefined) (r = "utf8"), (n = this.length), (e = 0);
          else if (n === undefined && "string" == typeof e)
            (r = e), (n = this.length), (e = 0);
          else {
            if (!isFinite(e))
              throw new Error(
                "Buffer.write(string, encoding, offset[, length]) is no longer supported"
              );
            (e |= 0),
              isFinite(n)
                ? ((n |= 0), r === undefined && (r = "utf8"))
                : ((r = n), (n = undefined));
          }
          var o = this.length - e;
          if (
            ((n === undefined || n > o) && (n = o),
            (t.length > 0 && (n < 0 || e < 0)) || e > this.length)
          )
            throw new RangeError("Attempt to write outside buffer bounds");
          r || (r = "utf8");
          for (var i = !1; ; )
            switch (r) {
              case "hex":
                return b(this, t, e, n);
              case "utf8":
              case "utf-8":
                return w(this, t, e, n);
              case "ascii":
                return _(this, t, e, n);
              case "latin1":
              case "binary":
                return E(this, t, e, n);
              case "base64":
                return T(this, t, e, n);
              case "ucs2":
              case "ucs-2":
              case "utf16le":
              case "utf-16le":
                return S(this, t, e, n);
              default:
                if (i) throw new TypeError("Unknown encoding: " + r);
                (r = ("" + r).toLowerCase()), (i = !0);
            }
        }),
        (c.prototype.toJSON = function () {
          return {
            type: "Buffer",
            data: Array.prototype.slice.call(this._arr || this, 0),
          };
        });
      var R = 4096;
      function I(t, e, n) {
        var r = "";
        n = Math.min(t.length, n);
        for (var o = e; o < n; ++o) r += String.fromCharCode(127 & t[o]);
        return r;
      }
      function O(t, e, n) {
        var r = "";
        n = Math.min(t.length, n);
        for (var o = e; o < n; ++o) r += String.fromCharCode(t[o]);
        return r;
      }
      function P(t, e, n) {
        var r = t.length;
        (!e || e < 0) && (e = 0), (!n || n < 0 || n > r) && (n = r);
        for (var o = "", i = e; i < n; ++i) o += k(t[i]);
        return o;
      }
      function L(t, e, n) {
        for (var r = t.slice(e, n), o = "", i = 0; i < r.length; i += 2)
          o += String.fromCharCode(r[i] + 256 * r[i + 1]);
        return o;
      }
      function N(t, e, n) {
        if (t % 1 != 0 || t < 0) throw new RangeError("offset is not uint");
        if (t + e > n)
          throw new RangeError("Trying to access beyond buffer length");
      }
      function D(t, e, n, r, o, i) {
        if (!c.isBuffer(t))
          throw new TypeError('"buffer" argument must be a Buffer instance');
        if (e > o || e < i)
          throw new RangeError('"value" argument is out of bounds');
        if (n + r > t.length) throw new RangeError("Index out of range");
      }
      function F(t, e, n, r) {
        e < 0 && (e = 65535 + e + 1);
        for (var o = 0, i = Math.min(t.length - n, 2); o < i; ++o)
          t[n + o] =
            (e & (255 << (8 * (r ? o : 1 - o)))) >>> (8 * (r ? o : 1 - o));
      }
      function M(t, e, n, r) {
        e < 0 && (e = 4294967295 + e + 1);
        for (var o = 0, i = Math.min(t.length - n, 4); o < i; ++o)
          t[n + o] = (e >>> (8 * (r ? o : 3 - o))) & 255;
      }
      function C(t, e, n, r, o, i) {
        if (n + r > t.length) throw new RangeError("Index out of range");
        if (n < 0) throw new RangeError("Index out of range");
      }
      function B(t, e, n, r, i) {
        return i || C(t, 0, n, 4), o.write(t, e, n, r, 23, 4), n + 4;
      }
      function j(t, e, n, r, i) {
        return i || C(t, 0, n, 8), o.write(t, e, n, r, 52, 8), n + 8;
      }
      (c.prototype.slice = function (t, e) {
        var n,
          r = this.length;
        if (
          ((t = ~~t) < 0 ? (t += r) < 0 && (t = 0) : t > r && (t = r),
          (e = e === undefined ? r : ~~e) < 0
            ? (e += r) < 0 && (e = 0)
            : e > r && (e = r),
          e < t && (e = t),
          c.TYPED_ARRAY_SUPPORT)
        )
          (n = this.subarray(t, e)).__proto__ = c.prototype;
        else {
          var o = e - t;
          n = new c(o, undefined);
          for (var i = 0; i < o; ++i) n[i] = this[i + t];
        }
        return n;
      }),
        (c.prototype.readUIntLE = function (t, e, n) {
          (t |= 0), (e |= 0), n || N(t, e, this.length);
          for (var r = this[t], o = 1, i = 0; ++i < e && (o *= 256); )
            r += this[t + i] * o;
          return r;
        }),
        (c.prototype.readUIntBE = function (t, e, n) {
          (t |= 0), (e |= 0), n || N(t, e, this.length);
          for (var r = this[t + --e], o = 1; e > 0 && (o *= 256); )
            r += this[t + --e] * o;
          return r;
        }),
        (c.prototype.readUInt8 = function (t, e) {
          return e || N(t, 1, this.length), this[t];
        }),
        (c.prototype.readUInt16LE = function (t, e) {
          return e || N(t, 2, this.length), this[t] | (this[t + 1] << 8);
        }),
        (c.prototype.readUInt16BE = function (t, e) {
          return e || N(t, 2, this.length), (this[t] << 8) | this[t + 1];
        }),
        (c.prototype.readUInt32LE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            (this[t] | (this[t + 1] << 8) | (this[t + 2] << 16)) +
              16777216 * this[t + 3]
          );
        }),
        (c.prototype.readUInt32BE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            16777216 * this[t] +
              ((this[t + 1] << 16) | (this[t + 2] << 8) | this[t + 3])
          );
        }),
        (c.prototype.readIntLE = function (t, e, n) {
          (t |= 0), (e |= 0), n || N(t, e, this.length);
          for (var r = this[t], o = 1, i = 0; ++i < e && (o *= 256); )
            r += this[t + i] * o;
          return r >= (o *= 128) && (r -= Math.pow(2, 8 * e)), r;
        }),
        (c.prototype.readIntBE = function (t, e, n) {
          (t |= 0), (e |= 0), n || N(t, e, this.length);
          for (var r = e, o = 1, i = this[t + --r]; r > 0 && (o *= 256); )
            i += this[t + --r] * o;
          return i >= (o *= 128) && (i -= Math.pow(2, 8 * e)), i;
        }),
        (c.prototype.readInt8 = function (t, e) {
          return (
            e || N(t, 1, this.length),
            128 & this[t] ? -1 * (255 - this[t] + 1) : this[t]
          );
        }),
        (c.prototype.readInt16LE = function (t, e) {
          e || N(t, 2, this.length);
          var n = this[t] | (this[t + 1] << 8);
          return 32768 & n ? 4294901760 | n : n;
        }),
        (c.prototype.readInt16BE = function (t, e) {
          e || N(t, 2, this.length);
          var n = this[t + 1] | (this[t] << 8);
          return 32768 & n ? 4294901760 | n : n;
        }),
        (c.prototype.readInt32LE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            this[t] |
              (this[t + 1] << 8) |
              (this[t + 2] << 16) |
              (this[t + 3] << 24)
          );
        }),
        (c.prototype.readInt32BE = function (t, e) {
          return (
            e || N(t, 4, this.length),
            (this[t] << 24) |
              (this[t + 1] << 16) |
              (this[t + 2] << 8) |
              this[t + 3]
          );
        }),
        (c.prototype.readFloatLE = function (t, e) {
          return e || N(t, 4, this.length), o.read(this, t, !0, 23, 4);
        }),
        (c.prototype.readFloatBE = function (t, e) {
          return e || N(t, 4, this.length), o.read(this, t, !1, 23, 4);
        }),
        (c.prototype.readDoubleLE = function (t, e) {
          return e || N(t, 8, this.length), o.read(this, t, !0, 52, 8);
        }),
        (c.prototype.readDoubleBE = function (t, e) {
          return e || N(t, 8, this.length), o.read(this, t, !1, 52, 8);
        }),
        (c.prototype.writeUIntLE = function (t, e, n, r) {
          ((t = +t), (e |= 0), (n |= 0), r) ||
            D(this, t, e, n, Math.pow(2, 8 * n) - 1, 0);
          var o = 1,
            i = 0;
          for (this[e] = 255 & t; ++i < n && (o *= 256); )
            this[e + i] = (t / o) & 255;
          return e + n;
        }),
        (c.prototype.writeUIntBE = function (t, e, n, r) {
          ((t = +t), (e |= 0), (n |= 0), r) ||
            D(this, t, e, n, Math.pow(2, 8 * n) - 1, 0);
          var o = n - 1,
            i = 1;
          for (this[e + o] = 255 & t; --o >= 0 && (i *= 256); )
            this[e + o] = (t / i) & 255;
          return e + n;
        }),
        (c.prototype.writeUInt8 = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 1, 255, 0),
            c.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)),
            (this[e] = 255 & t),
            e + 1
          );
        }),
        (c.prototype.writeUInt16LE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 2, 65535, 0),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = 255 & t), (this[e + 1] = t >>> 8))
              : F(this, t, e, !0),
            e + 2
          );
        }),
        (c.prototype.writeUInt16BE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 2, 65535, 0),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 8), (this[e + 1] = 255 & t))
              : F(this, t, e, !1),
            e + 2
          );
        }),
        (c.prototype.writeUInt32LE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 4, 4294967295, 0),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e + 3] = t >>> 24),
                (this[e + 2] = t >>> 16),
                (this[e + 1] = t >>> 8),
                (this[e] = 255 & t))
              : M(this, t, e, !0),
            e + 4
          );
        }),
        (c.prototype.writeUInt32BE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 4, 4294967295, 0),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 24),
                (this[e + 1] = t >>> 16),
                (this[e + 2] = t >>> 8),
                (this[e + 3] = 255 & t))
              : M(this, t, e, !1),
            e + 4
          );
        }),
        (c.prototype.writeIntLE = function (t, e, n, r) {
          if (((t = +t), (e |= 0), !r)) {
            var o = Math.pow(2, 8 * n - 1);
            D(this, t, e, n, o - 1, -o);
          }
          var i = 0,
            a = 1,
            u = 0;
          for (this[e] = 255 & t; ++i < n && (a *= 256); )
            t < 0 && 0 === u && 0 !== this[e + i - 1] && (u = 1),
              (this[e + i] = (((t / a) >> 0) - u) & 255);
          return e + n;
        }),
        (c.prototype.writeIntBE = function (t, e, n, r) {
          if (((t = +t), (e |= 0), !r)) {
            var o = Math.pow(2, 8 * n - 1);
            D(this, t, e, n, o - 1, -o);
          }
          var i = n - 1,
            a = 1,
            u = 0;
          for (this[e + i] = 255 & t; --i >= 0 && (a *= 256); )
            t < 0 && 0 === u && 0 !== this[e + i + 1] && (u = 1),
              (this[e + i] = (((t / a) >> 0) - u) & 255);
          return e + n;
        }),
        (c.prototype.writeInt8 = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 1, 127, -128),
            c.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)),
            t < 0 && (t = 255 + t + 1),
            (this[e] = 255 & t),
            e + 1
          );
        }),
        (c.prototype.writeInt16LE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 2, 32767, -32768),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = 255 & t), (this[e + 1] = t >>> 8))
              : F(this, t, e, !0),
            e + 2
          );
        }),
        (c.prototype.writeInt16BE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 2, 32767, -32768),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 8), (this[e + 1] = 255 & t))
              : F(this, t, e, !1),
            e + 2
          );
        }),
        (c.prototype.writeInt32LE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 4, 2147483647, -2147483648),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = 255 & t),
                (this[e + 1] = t >>> 8),
                (this[e + 2] = t >>> 16),
                (this[e + 3] = t >>> 24))
              : M(this, t, e, !0),
            e + 4
          );
        }),
        (c.prototype.writeInt32BE = function (t, e, n) {
          return (
            (t = +t),
            (e |= 0),
            n || D(this, t, e, 4, 2147483647, -2147483648),
            t < 0 && (t = 4294967295 + t + 1),
            c.TYPED_ARRAY_SUPPORT
              ? ((this[e] = t >>> 24),
                (this[e + 1] = t >>> 16),
                (this[e + 2] = t >>> 8),
                (this[e + 3] = 255 & t))
              : M(this, t, e, !1),
            e + 4
          );
        }),
        (c.prototype.writeFloatLE = function (t, e, n) {
          return B(this, t, e, !0, n);
        }),
        (c.prototype.writeFloatBE = function (t, e, n) {
          return B(this, t, e, !1, n);
        }),
        (c.prototype.writeDoubleLE = function (t, e, n) {
          return j(this, t, e, !0, n);
        }),
        (c.prototype.writeDoubleBE = function (t, e, n) {
          return j(this, t, e, !1, n);
        }),
        (c.prototype.copy = function (t, e, n, r) {
          if (
            (n || (n = 0),
            r || 0 === r || (r = this.length),
            e >= t.length && (e = t.length),
            e || (e = 0),
            r > 0 && r < n && (r = n),
            r === n)
          )
            return 0;
          if (0 === t.length || 0 === this.length) return 0;
          if (e < 0) throw new RangeError("targetStart out of bounds");
          if (n < 0 || n >= this.length)
            throw new RangeError("sourceStart out of bounds");
          if (r < 0) throw new RangeError("sourceEnd out of bounds");
          r > this.length && (r = this.length),
            t.length - e < r - n && (r = t.length - e + n);
          var o,
            i = r - n;
          if (this === t && n < e && e < r)
            for (o = i - 1; o >= 0; --o) t[o + e] = this[o + n];
          else if (i < 1e3 || !c.TYPED_ARRAY_SUPPORT)
            for (o = 0; o < i; ++o) t[o + e] = this[o + n];
          else Uint8Array.prototype.set.call(t, this.subarray(n, n + i), e);
          return i;
        }),
        (c.prototype.fill = function (t, e, n, r) {
          if ("string" == typeof t) {
            if (
              ("string" == typeof e
                ? ((r = e), (e = 0), (n = this.length))
                : "string" == typeof n && ((r = n), (n = this.length)),
              1 === t.length)
            ) {
              var o = t.charCodeAt(0);
              o < 256 && (t = o);
            }
            if (r !== undefined && "string" != typeof r)
              throw new TypeError("encoding must be a string");
            if ("string" == typeof r && !c.isEncoding(r))
              throw new TypeError("Unknown encoding: " + r);
          } else "number" == typeof t && (t &= 255);
          if (e < 0 || this.length < e || this.length < n)
            throw new RangeError("Out of range index");
          if (n <= e) return this;
          var i;
          if (
            ((e >>>= 0),
            (n = n === undefined ? this.length : n >>> 0),
            t || (t = 0),
            "number" == typeof t)
          )
            for (i = e; i < n; ++i) this[i] = t;
          else {
            var a = c.isBuffer(t) ? t : V(new c(t, r).toString()),
              u = a.length;
            for (i = 0; i < n - e; ++i) this[i + e] = a[i % u];
          }
          return this;
        });
      var U = /[^+\/0-9A-Za-z-_]/g;
      function k(t) {
        return t < 16 ? "0" + t.toString(16) : t.toString(16);
      }
      function V(t, e) {
        var n;
        e = e || Infinity;
        for (var r = t.length, o = null, i = [], a = 0; a < r; ++a) {
          if ((n = t.charCodeAt(a)) > 55295 && n < 57344) {
            if (!o) {
              if (n > 56319) {
                (e -= 3) > -1 && i.push(239, 191, 189);
                continue;
              }
              if (a + 1 === r) {
                (e -= 3) > -1 && i.push(239, 191, 189);
                continue;
              }
              o = n;
              continue;
            }
            if (n < 56320) {
              (e -= 3) > -1 && i.push(239, 191, 189), (o = n);
              continue;
            }
            n = 65536 + (((o - 55296) << 10) | (n - 56320));
          } else o && (e -= 3) > -1 && i.push(239, 191, 189);
          if (((o = null), n < 128)) {
            if ((e -= 1) < 0) break;
            i.push(n);
          } else if (n < 2048) {
            if ((e -= 2) < 0) break;
            i.push((n >> 6) | 192, (63 & n) | 128);
          } else if (n < 65536) {
            if ((e -= 3) < 0) break;
            i.push((n >> 12) | 224, ((n >> 6) & 63) | 128, (63 & n) | 128);
          } else {
            if (!(n < 1114112)) throw new Error("Invalid code point");
            if ((e -= 4) < 0) break;
            i.push(
              (n >> 18) | 240,
              ((n >> 12) & 63) | 128,
              ((n >> 6) & 63) | 128,
              (63 & n) | 128
            );
          }
        }
        return i;
      }
      function q(t) {
        return r.toByteArray(
          (function (t) {
            if (
              (t = (function (t) {
                return t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, "");
              })(t).replace(U, "")).length < 2
            )
              return "";
            for (; t.length % 4 != 0; ) t += "=";
            return t;
          })(t)
        );
      }
      function Y(t, e, n, r) {
        for (var o = 0; o < r && !(o + n >= e.length || o >= t.length); ++o)
          e[o + n] = t[o];
        return o;
      }
    }.call(this, n(48)));
  },
  function (t, e, n) {
    "use strict";
    (e.byteLength = function (t) {
      var e = s(t),
        n = e[0],
        r = e[1];
      return (3 * (n + r)) / 4 - r;
    }),
      (e.toByteArray = function (t) {
        var e,
          n,
          r = s(t),
          a = r[0],
          u = r[1],
          c = new i(
            (function (t, e, n) {
              return (3 * (e + n)) / 4 - n;
            })(0, a, u)
          ),
          f = 0,
          l = u > 0 ? a - 4 : a;
        for (n = 0; n < l; n += 4)
          (e =
            (o[t.charCodeAt(n)] << 18) |
            (o[t.charCodeAt(n + 1)] << 12) |
            (o[t.charCodeAt(n + 2)] << 6) |
            o[t.charCodeAt(n + 3)]),
            (c[f++] = (e >> 16) & 255),
            (c[f++] = (e >> 8) & 255),
            (c[f++] = 255 & e);
        2 === u &&
          ((e = (o[t.charCodeAt(n)] << 2) | (o[t.charCodeAt(n + 1)] >> 4)),
          (c[f++] = 255 & e));
        1 === u &&
          ((e =
            (o[t.charCodeAt(n)] << 10) |
            (o[t.charCodeAt(n + 1)] << 4) |
            (o[t.charCodeAt(n + 2)] >> 2)),
          (c[f++] = (e >> 8) & 255),
          (c[f++] = 255 & e));
        return c;
      }),
      (e.fromByteArray = function (t) {
        for (
          var e, n = t.length, o = n % 3, i = [], a = 16383, u = 0, c = n - o;
          u < c;
          u += a
        )
          i.push(f(t, u, u + a > c ? c : u + a));
        1 === o
          ? ((e = t[n - 1]), i.push(r[e >> 2] + r[(e << 4) & 63] + "=="))
          : 2 === o &&
            ((e = (t[n - 2] << 8) + t[n - 1]),
            i.push(r[e >> 10] + r[(e >> 4) & 63] + r[(e << 2) & 63] + "="));
        return i.join("");
      });
    for (
      var r = [],
        o = [],
        i = "undefined" != typeof Uint8Array ? Uint8Array : Array,
        a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        u = 0,
        c = a.length;
      u < c;
      ++u
    )
      (r[u] = a[u]), (o[a.charCodeAt(u)] = u);
    function s(t) {
      var e = t.length;
      if (e % 4 > 0)
        throw new Error("Invalid string. Length must be a multiple of 4");
      var n = t.indexOf("=");
      return -1 === n && (n = e), [n, n === e ? 0 : 4 - (n % 4)];
    }
    function f(t, e, n) {
      for (var o, i, a = [], u = e; u < n; u += 3)
        (o =
          ((t[u] << 16) & 16711680) +
          ((t[u + 1] << 8) & 65280) +
          (255 & t[u + 2])),
          a.push(
            r[((i = o) >> 18) & 63] +
              r[(i >> 12) & 63] +
              r[(i >> 6) & 63] +
              r[63 & i]
          );
      return a.join("");
    }
    (o["-".charCodeAt(0)] = 62), (o["_".charCodeAt(0)] = 63);
  },
  function (t, e) {
    (e.read = function (t, e, n, r, o) {
      var i,
        a,
        u = 8 * o - r - 1,
        c = (1 << u) - 1,
        s = c >> 1,
        f = -7,
        l = n ? o - 1 : 0,
        p = n ? -1 : 1,
        h = t[e + l];
      for (
        l += p, i = h & ((1 << -f) - 1), h >>= -f, f += u;
        f > 0;
        i = 256 * i + t[e + l], l += p, f -= 8
      );
      for (
        a = i & ((1 << -f) - 1), i >>= -f, f += r;
        f > 0;
        a = 256 * a + t[e + l], l += p, f -= 8
      );
      if (0 === i) i = 1 - s;
      else {
        if (i === c) return a ? NaN : (h ? -1 : 1) * Infinity;
        (a += Math.pow(2, r)), (i -= s);
      }
      return (h ? -1 : 1) * a * Math.pow(2, i - r);
    }),
      (e.write = function (t, e, n, r, o, i) {
        var a,
          u,
          c,
          s = 8 * i - o - 1,
          f = (1 << s) - 1,
          l = f >> 1,
          p = 23 === o ? Math.pow(2, -24) - Math.pow(2, -77) : 0,
          h = r ? 0 : i - 1,
          d = r ? 1 : -1,
          y = e < 0 || (0 === e && 1 / e < 0) ? 1 : 0;
        for (
          e = Math.abs(e),
            isNaN(e) || e === Infinity
              ? ((u = isNaN(e) ? 1 : 0), (a = f))
              : ((a = Math.floor(Math.log(e) / Math.LN2)),
                e * (c = Math.pow(2, -a)) < 1 && (a--, (c *= 2)),
                (e += a + l >= 1 ? p / c : p * Math.pow(2, 1 - l)) * c >= 2 &&
                  (a++, (c /= 2)),
                a + l >= f
                  ? ((u = 0), (a = f))
                  : a + l >= 1
                  ? ((u = (e * c - 1) * Math.pow(2, o)), (a += l))
                  : ((u = e * Math.pow(2, l - 1) * Math.pow(2, o)), (a = 0)));
          o >= 8;
          t[n + h] = 255 & u, h += d, u /= 256, o -= 8
        );
        for (
          a = (a << o) | u, s += o;
          s > 0;
          t[n + h] = 255 & a, h += d, a /= 256, s -= 8
        );
        t[n + h - d] |= 128 * y;
      });
  },
  function (t, e) {
    var n = {}.toString;
    t.exports =
      Array.isArray ||
      function (t) {
        return "[object Array]" == n.call(t);
      };
  },
  function (t, e) {
    t.exports = {
      5: {
        mimeType: 'video/flv; codecs="Sorenson H.283, mp3"',
        qualityLabel: "240p",
        bitrate: 25e4,
        audioBitrate: 64,
      },
      6: {
        mimeType: 'video/flv; codecs="Sorenson H.263, mp3"',
        qualityLabel: "270p",
        bitrate: 8e5,
        audioBitrate: 64,
      },
      13: {
        mimeType: 'video/3gp; codecs="MPEG-4 Visual, aac"',
        qualityLabel: null,
        bitrate: 5e5,
        audioBitrate: null,
      },
      17: {
        mimeType: 'video/3gp; codecs="MPEG-4 Visual, aac"',
        qualityLabel: "144p",
        bitrate: 5e4,
        audioBitrate: 24,
      },
      18: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 96,
      },
      22: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 192,
      },
      34: {
        mimeType: 'video/flv; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 128,
      },
      35: {
        mimeType: 'video/flv; codecs="H.264, aac"',
        qualityLabel: "480p",
        bitrate: 8e5,
        audioBitrate: 128,
      },
      36: {
        mimeType: 'video/3gp; codecs="MPEG-4 Visual, aac"',
        qualityLabel: "240p",
        bitrate: 175e3,
        audioBitrate: 32,
      },
      37: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "1080p",
        bitrate: 3e6,
        audioBitrate: 192,
      },
      38: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "3072p",
        bitrate: 35e5,
        audioBitrate: 192,
      },
      43: {
        mimeType: 'video/webm; codecs="VP8, vorbis"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 128,
      },
      44: {
        mimeType: 'video/webm; codecs="VP8, vorbis"',
        qualityLabel: "480p",
        bitrate: 1e6,
        audioBitrate: 128,
      },
      45: {
        mimeType: 'video/webm; codecs="VP8, vorbis"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 192,
      },
      46: {
        mimeType: 'audio/webm; codecs="vp8, vorbis"',
        qualityLabel: "1080p",
        bitrate: null,
        audioBitrate: 192,
      },
      82: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 96,
      },
      83: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "240p",
        bitrate: 5e5,
        audioBitrate: 96,
      },
      84: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 192,
      },
      85: {
        mimeType: 'video/mp4; codecs="H.264, aac"',
        qualityLabel: "1080p",
        bitrate: 3e6,
        audioBitrate: 192,
      },
      91: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "144p",
        bitrate: 1e5,
        audioBitrate: 48,
      },
      92: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "240p",
        bitrate: 15e4,
        audioBitrate: 48,
      },
      93: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "360p",
        bitrate: 5e5,
        audioBitrate: 128,
      },
      94: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "480p",
        bitrate: 8e5,
        audioBitrate: 128,
      },
      95: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 15e5,
        audioBitrate: 256,
      },
      96: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "1080p",
        bitrate: 25e5,
        audioBitrate: 256,
      },
      100: {
        mimeType: 'audio/webm; codecs="VP8, vorbis"',
        qualityLabel: "360p",
        bitrate: null,
        audioBitrate: 128,
      },
      101: {
        mimeType: 'audio/webm; codecs="VP8, vorbis"',
        qualityLabel: "360p",
        bitrate: null,
        audioBitrate: 192,
      },
      102: {
        mimeType: 'audio/webm; codecs="VP8, vorbis"',
        qualityLabel: "720p",
        bitrate: null,
        audioBitrate: 192,
      },
      120: {
        mimeType: 'video/flv; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 2e6,
        audioBitrate: 128,
      },
      127: {
        mimeType: 'audio/ts; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 96,
      },
      128: {
        mimeType: 'audio/ts; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 96,
      },
      132: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "240p",
        bitrate: 15e4,
        audioBitrate: 48,
      },
      133: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "240p",
        bitrate: 2e5,
        audioBitrate: null,
      },
      134: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "360p",
        bitrate: 3e5,
        audioBitrate: null,
      },
      135: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "480p",
        bitrate: 5e5,
        audioBitrate: null,
      },
      136: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "720p",
        bitrate: 1e6,
        audioBitrate: null,
      },
      137: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "1080p",
        bitrate: 25e5,
        audioBitrate: null,
      },
      138: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "4320p",
        bitrate: 135e5,
        audioBitrate: null,
      },
      139: {
        mimeType: 'audio/mp4; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 48,
      },
      140: {
        mimeType: 'audio/m4a; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 128,
      },
      141: {
        mimeType: 'audio/mp4; codecs="aac"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 256,
      },
      151: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 5e4,
        audioBitrate: 24,
      },
      160: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "144p",
        bitrate: 1e5,
        audioBitrate: null,
      },
      171: {
        mimeType: 'audio/webm; codecs="vorbis"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 128,
      },
      172: {
        mimeType: 'audio/webm; codecs="vorbis"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 192,
      },
      242: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "240p",
        bitrate: 1e5,
        audioBitrate: null,
      },
      243: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "360p",
        bitrate: 25e4,
        audioBitrate: null,
      },
      244: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "480p",
        bitrate: 5e5,
        audioBitrate: null,
      },
      247: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "720p",
        bitrate: 7e5,
        audioBitrate: null,
      },
      248: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1080p",
        bitrate: 15e5,
        audioBitrate: null,
      },
      249: {
        mimeType: 'audio/webm; codecs="opus"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 48,
      },
      250: {
        mimeType: 'audio/webm; codecs="opus"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 64,
      },
      251: {
        mimeType: 'audio/webm; codecs="opus"',
        qualityLabel: null,
        bitrate: null,
        audioBitrate: 160,
      },
      264: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "1440p",
        bitrate: 4e6,
        audioBitrate: null,
      },
      266: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "2160p",
        bitrate: 125e5,
        audioBitrate: null,
      },
      271: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1440p",
        bitrate: 9e6,
        audioBitrate: null,
      },
      272: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "4320p",
        bitrate: 2e7,
        audioBitrate: null,
      },
      278: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "144p 30fps",
        bitrate: 8e4,
        audioBitrate: null,
      },
      298: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "720p",
        bitrate: 3e6,
        audioBitrate: null,
      },
      299: {
        mimeType: 'video/mp4; codecs="H.264"',
        qualityLabel: "1080p",
        bitrate: 55e5,
        audioBitrate: null,
      },
      300: {
        mimeType: 'video/ts; codecs="H.264, aac"',
        qualityLabel: "720p",
        bitrate: 1318e3,
        audioBitrate: 48,
      },
      302: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "720p HFR",
        bitrate: 25e5,
        audioBitrate: null,
      },
      303: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1080p HFR",
        bitrate: 5e6,
        audioBitrate: null,
      },
      308: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1440p HFR",
        bitrate: 1e7,
        audioBitrate: null,
      },
      313: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "2160p",
        bitrate: 13e6,
        audioBitrate: null,
      },
      315: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "2160p HFR",
        bitrate: 2e7,
        audioBitrate: null,
      },
      330: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "144p HDR, HFR",
        bitrate: 8e4,
        audioBitrate: null,
      },
      331: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "240p HDR, HFR",
        bitrate: 1e5,
        audioBitrate: null,
      },
      332: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "360p HDR, HFR",
        bitrate: 25e4,
        audioBitrate: null,
      },
      333: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "240p HDR, HFR",
        bitrate: 5e5,
        audioBitrate: null,
      },
      334: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "720p HDR, HFR",
        bitrate: 1e6,
        audioBitrate: null,
      },
      335: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1080p HDR, HFR",
        bitrate: 15e5,
        audioBitrate: null,
      },
      336: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "1440p HDR, HFR",
        bitrate: 5e6,
        audioBitrate: null,
      },
      337: {
        mimeType: 'video/webm; codecs="VP9"',
        qualityLabel: "2160p HDR, HFR",
        bitrate: 12e6,
        audioBitrate: null,
      },
    };
  },
  function (t, e, n) {
    function r(t, e) {
      return (
        (function (t) {
          if (Array.isArray(t)) return t;
        })(t) ||
        (function (t, e) {
          var n =
            t &&
            (("undefined" != typeof Symbol && t[Symbol.iterator]) ||
              t["@@iterator"]);
          if (null == n) return;
          var r,
            o,
            i = [],
            a = !0,
            u = !1;
          try {
            for (
              n = n.call(t);
              !(a = (r = n.next()).done) &&
              (i.push(r.value), !e || i.length !== e);
              a = !0
            );
          } catch (c) {
            (u = !0), (o = c);
          } finally {
            try {
              a || null == n["return"] || n["return"]();
            } finally {
              if (u) throw o;
            }
          }
          return i;
        })(t, e) ||
        i(t, e) ||
        (function () {
          throw new TypeError(
            "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        })()
      );
    }
    function o(t, e) {
      var n =
        ("undefined" != typeof Symbol && t[Symbol.iterator]) || t["@@iterator"];
      if (!n) {
        if (
          Array.isArray(t) ||
          (n = i(t)) ||
          (e && t && "number" == typeof t.length)
        ) {
          n && (t = n);
          var r = 0,
            o = function () {};
          return {
            s: o,
            n: function () {
              return r >= t.length ? { done: !0 } : { done: !1, value: t[r++] };
            },
            e: function (t) {
              throw t;
            },
            f: o,
          };
        }
        throw new TypeError(
          "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      }
      var a,
        u = !0,
        c = !1;
      return {
        s: function () {
          n = n.call(t);
        },
        n: function () {
          var t = n.next();
          return (u = t.done), t;
        },
        e: function (t) {
          (c = !0), (a = t);
        },
        f: function () {
          try {
            u || null == n["return"] || n["return"]();
          } finally {
            if (c) throw a;
          }
        },
      };
    }
    function i(t, e) {
      if (t) {
        if ("string" == typeof t) return a(t, e);
        var n = Object.prototype.toString.call(t).slice(8, -1);
        return (
          "Object" === n && t.constructor && (n = t.constructor.name),
          "Map" === n || "Set" === n
            ? Array.from(t)
            : "Arguments" === n ||
              /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
            ? a(t, e)
            : void 0
        );
      }
    }
    function a(t, e) {
      (null == e || e > t.length) && (e = t.length);
      for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
      return r;
    }
    var u = n(62),
      c = n(91),
      s = n(92).parseTimestamp,
      f = "https://www.youtube.com/watch?v=",
      l = { song: { name: "Music", url: "https://music.youtube.com/" } },
      p = function (t) {
        return t ? (t.runs ? t.runs[0].text : t.simpleText) : null;
      };
    e.getMedia = function (t) {
      var e = {},
        n = [];
      try {
        n =
          t.response.contents.twoColumnWatchNextResults.results.results
            .contents;
      } catch (E) {}
      var r = n.find(function (t) {
        return t.videoSecondaryInfoRenderer;
      });
      if (!r) return {};
      try {
        var i,
          a = o(
            (
              r.metadataRowContainer ||
              r.videoSecondaryInfoRenderer.metadataRowContainer
            ).metadataRowContainerRenderer.rows
          );
        try {
          for (a.s(); !(i = a.n()).done; ) {
            var u = i.value;
            if (u.metadataRowRenderer) {
              var c = p(u.metadataRowRenderer.title).toLowerCase(),
                s = u.metadataRowRenderer.contents[0];
              e[c] = p(s);
              var h = s.runs;
              h &&
                h[0].navigationEndpoint &&
                (e["".concat(c, "_url")] = new URL(
                  h[0].navigationEndpoint.commandMetadata.webCommandMetadata.url,
                  f
                ).toString()),
                c in l &&
                  ((e.category = l[c].name), (e.category_url = l[c].url));
            } else if (u.richMetadataRowRenderer) {
              var d,
                y = u.richMetadataRowRenderer.contents,
                m = o(
                  y.filter(function (t) {
                    return (
                      "RICH_METADATA_RENDERER_STYLE_BOX_ART" ===
                      t.richMetadataRenderer.style
                    );
                  })
                );
              try {
                for (m.s(); !(d = m.n()).done; ) {
                  var v = d.value.richMetadataRenderer;
                  e.year = p(v.subtitle);
                  var g = p(v.callToAction).split(" ")[1];
                  (e[g] = p(v.title)),
                    (e["".concat(g, "_url")] = new URL(
                      v.endpoint.commandMetadata.webCommandMetadata.url,
                      f
                    ).toString()),
                    (e.thumbnails = v.thumbnail.thumbnails);
                }
              } catch (E) {
                m.e(E);
              } finally {
                m.f();
              }
              var b,
                w = o(
                  y.filter(function (t) {
                    return (
                      "RICH_METADATA_RENDERER_STYLE_TOPIC" ===
                      t.richMetadataRenderer.style
                    );
                  })
                );
              try {
                for (w.s(); !(b = w.n()).done; ) {
                  var _ = b.value.richMetadataRenderer;
                  (e.category = p(_.title)),
                    (e.category_url = new URL(
                      _.endpoint.commandMetadata.webCommandMetadata.url,
                      f
                    ).toString());
                }
              } catch (E) {
                w.e(E);
              } finally {
                w.f();
              }
            }
          }
        } catch (E) {
          a.e(E);
        } finally {
          a.f();
        }
      } catch (E) {}
      return e;
    };
    var h = function (t) {
      return !(
        !t ||
        !t.find(function (t) {
          return "Verified" === t.metadataBadgeRenderer.tooltip;
        })
      );
    };
    e.getAuthor = function (t) {
      var e,
        n,
        r = [],
        o = !1;
      try {
        var i =
          t.response.contents.twoColumnWatchNextResults.results.results.contents.find(
            function (t) {
              return (
                t.videoSecondaryInfoRenderer &&
                t.videoSecondaryInfoRenderer.owner &&
                t.videoSecondaryInfoRenderer.owner.videoOwnerRenderer
              );
            }
          ).videoSecondaryInfoRenderer.owner.videoOwnerRenderer;
        (e = i.navigationEndpoint.browseEndpoint.browseId),
          (r = i.thumbnail.thumbnails.map(function (t) {
            return (t.url = new URL(t.url, f).toString()), t;
          })),
          (n = u.parseAbbreviatedNumber(p(i.subscriberCountText))),
          (o = h(i.badges));
      } catch (l) {}
      try {
        var a =
            t.player_response.microformat &&
            t.player_response.microformat.playerMicroformatRenderer,
          c =
            (a && a.channelId) || e || t.player_response.videoDetails.channelId,
          s = {
            id: c,
            name: a
              ? a.ownerChannelName
              : t.player_response.videoDetails.author,
            user: a ? a.ownerProfileUrl.split("/").slice(-1)[0] : null,
            channel_url: "https://www.youtube.com/channel/".concat(c),
            external_channel_url: a
              ? "https://www.youtube.com/channel/".concat(a.externalChannelId)
              : "",
            user_url: a ? new URL(a.ownerProfileUrl, f).toString() : "",
            thumbnails: r,
            verified: o,
            subscriber_count: n,
          };
        return (
          r.length &&
            u.deprecate(
              s,
              "avatar",
              s.thumbnails[0].url,
              "author.avatar",
              "author.thumbnails[0].url"
            ),
          s
        );
      } catch (l) {
        return {};
      }
    };
    var d = function (t, e) {
      var n, r, o;
      if (t)
        try {
          var i = p(t.viewCountText),
            a = p(t.shortViewCountText),
            c = e.find(function (e) {
              return e.id === t.videoId;
            });
          /^\d/.test(a) || (a = (c && c.short_view_count_text) || ""),
            (i = (/^\d/.test(i) ? i : a).split(" ")[0]);
          var l = t.shortBylineText.runs[0].navigationEndpoint.browseEndpoint,
            d = l.browseId,
            y = p(t.shortBylineText),
            m = (l.canonicalBaseUrl || "").split("/").slice(-1)[0],
            v = {
              id: t.videoId,
              title: p(t.title),
              published: p(t.publishedTimeText),
              author:
                ((n = {
                  id: d,
                  name: y,
                  user: m,
                  channel_url: "https://www.youtube.com/channel/".concat(d),
                  user_url: "https://www.youtube.com/user/".concat(m),
                  thumbnails: t.channelThumbnail.thumbnails.map(function (t) {
                    return (t.url = new URL(t.url, f).toString()), t;
                  }),
                  verified: h(t.ownerBadges),
                }),
                (r = Symbol.toPrimitive),
                (o = function () {
                  return (
                    console.warn(
                      "`relatedVideo.author` will be removed in a near future release, use `relatedVideo.author.name` instead."
                    ),
                    v.author.name
                  );
                }),
                r in n
                  ? Object.defineProperty(n, r, {
                      value: o,
                      enumerable: !0,
                      configurable: !0,
                      writable: !0,
                    })
                  : (n[r] = o),
                n),
              short_view_count_text: a.split(" ")[0],
              view_count: i.replace(/,/g, ""),
              length_seconds: t.lengthText
                ? Math.floor(s(p(t.lengthText)) / 1e3)
                : e && "".concat(e.length_seconds),
              thumbnails: t.thumbnail.thumbnails,
              richThumbnails: t.richThumbnail
                ? t.richThumbnail.movingThumbnailRenderer.movingThumbnailDetails
                    .thumbnails
                : [],
              isLive: !(
                !t.badges ||
                !t.badges.find(function (t) {
                  return "LIVE NOW" === t.metadataBadgeRenderer.label;
                })
              ),
            };
          return (
            u.deprecate(
              v,
              "author_thumbnail",
              v.author.thumbnails[0].url,
              "relatedVideo.author_thumbnail",
              "relatedVideo.author.thumbnails[0].url"
            ),
            u.deprecate(
              v,
              "ucid",
              v.author.id,
              "relatedVideo.ucid",
              "relatedVideo.author.id"
            ),
            u.deprecate(
              v,
              "video_thumbnail",
              v.thumbnails[0].url,
              "relatedVideo.video_thumbnail",
              "relatedVideo.thumbnails[0].url"
            ),
            v
          );
        } catch (g) {}
    };
    (e.getRelatedVideos = function (t) {
      var e = [],
        n = [];
      try {
        e = t.response.webWatchNextResponseExtensionData.relatedVideoArgs
          .split(",")
          .map(function (t) {
            return c.parse(t);
          });
      } catch (v) {}
      try {
        n =
          t.response.contents.twoColumnWatchNextResults.secondaryResults
            .secondaryResults.results;
      } catch (v) {
        return [];
      }
      var r,
        i = [],
        a = o(n || []);
      try {
        for (a.s(); !(r = a.n()).done; ) {
          var u = r.value,
            s = u.compactVideoRenderer;
          if (s) {
            var f = d(s, e);
            f && i.push(f);
          } else {
            var l = u.compactAutoplayRenderer || u.itemSectionRenderer;
            if (!l || !Array.isArray(l.contents)) continue;
            var p,
              h = o(l.contents);
            try {
              for (h.s(); !(p = h.n()).done; ) {
                var y = p.value,
                  m = d(y.compactVideoRenderer, e);
                m && i.push(m);
              }
            } catch (v) {
              h.e(v);
            } finally {
              h.f();
            }
          }
        }
      } catch (v) {
        a.e(v);
      } finally {
        a.f();
      }
      return i;
    }),
      (e.getLikes = function (t) {
        try {
          var e =
            t.response.contents.twoColumnWatchNextResults.results.results.contents
              .find(function (t) {
                return t.videoPrimaryInfoRenderer;
              })
              .videoPrimaryInfoRenderer.videoActions.menuRenderer.topLevelButtons.find(
                function (t) {
                  return (
                    t.toggleButtonRenderer &&
                    "LIKE" === t.toggleButtonRenderer.defaultIcon.iconType
                  );
                }
              );
          return parseInt(
            e.toggleButtonRenderer.defaultText.accessibility.accessibilityData.label.replace(
              /\D+/g,
              ""
            )
          );
        } catch (n) {
          return null;
        }
      }),
      (e.getDislikes = function (t) {
        try {
          var e =
            t.response.contents.twoColumnWatchNextResults.results.results.contents
              .find(function (t) {
                return t.videoPrimaryInfoRenderer;
              })
              .videoPrimaryInfoRenderer.videoActions.menuRenderer.topLevelButtons.find(
                function (t) {
                  return (
                    t.toggleButtonRenderer &&
                    "DISLIKE" === t.toggleButtonRenderer.defaultIcon.iconType
                  );
                }
              );
          return parseInt(
            e.toggleButtonRenderer.defaultText.accessibility.accessibilityData.label.replace(
              /\D+/g,
              ""
            )
          );
        } catch (n) {
          return null;
        }
      }),
      (e.cleanVideoDetails = function (t, e) {
        return (
          (t.thumbnails = t.thumbnail.thumbnails),
          delete t.thumbnail,
          u.deprecate(
            t,
            "thumbnail",
            { thumbnails: t.thumbnails },
            "videoDetails.thumbnail.thumbnails",
            "videoDetails.thumbnails"
          ),
          (t.description = t.shortDescription || p(t.description)),
          delete t.shortDescription,
          u.deprecate(
            t,
            "shortDescription",
            t.description,
            "videoDetails.shortDescription",
            "videoDetails.description"
          ),
          (t.lengthSeconds =
            e.player_response.microformat &&
            e.player_response.microformat.playerMicroformatRenderer
              .lengthSeconds),
          t
        );
      }),
      (e.getStoryboards = function (t) {
        var e =
          t.player_response.storyboards &&
          t.player_response.storyboards.playerStoryboardSpecRenderer &&
          t.player_response.storyboards.playerStoryboardSpecRenderer.spec &&
          t.player_response.storyboards.playerStoryboardSpecRenderer.spec.split(
            "|"
          );
        if (!e) return [];
        var n = new URL(e.shift());
        return e.map(function (t, e) {
          var o = r(t.split("#"), 8),
            i = o[0],
            a = o[1],
            u = o[2],
            c = o[3],
            s = o[4],
            f = o[5],
            l = o[6],
            p = o[7];
          n.searchParams.set("sigh", p),
            (u = parseInt(u, 10)),
            (c = parseInt(c, 10)),
            (s = parseInt(s, 10));
          var h = Math.ceil(u / (c * s));
          return {
            templateUrl: n.toString().replace("$L", e).replace("$N", l),
            thumbnailWidth: parseInt(i, 10),
            thumbnailHeight: parseInt(a, 10),
            thumbnailCount: u,
            interval: parseInt(f, 10),
            columns: c,
            rows: s,
            storyboardCount: h,
          };
        });
      }),
      (e.getChapters = function (t) {
        var e =
            t.response &&
            t.response.playerOverlays &&
            t.response.playerOverlays.playerOverlayRenderer,
          n =
            e &&
            e.decoratedPlayerBarRenderer &&
            e.decoratedPlayerBarRenderer.decoratedPlayerBarRenderer &&
            e.decoratedPlayerBarRenderer.decoratedPlayerBarRenderer.playerBar,
          r =
            n &&
            n.multiMarkersPlayerBarRenderer &&
            n.multiMarkersPlayerBarRenderer.markersMap,
          o =
            Array.isArray(r) &&
            r.find(function (t) {
              return t.value && Array.isArray(t.value.chapters);
            });
        return o
          ? o.value.chapters.map(function (t) {
              return {
                title: p(t.chapterRenderer.title),
                start_time: t.chapterRenderer.timeRangeStartMillis / 1e3,
              };
            })
          : [];
      });
  },
  function (t) {
    t.exports = JSON.parse(
      '{"name":"ytdl-core","description":"YouTube video downloader in pure javascript.","keywords":["youtube","video","download"],"version":"4.7.0","repository":{"type":"git","url":"git://github.com/fent/node-ytdl-core.git"},"author":"fent <fentbox@gmail.com> (https://github.com/fent)","contributors":["Tobias Kutscha (https://github.com/TimeForANinja)","Andrew Kelley (https://github.com/andrewrk)","Mauricio Allende (https://github.com/mallendeo)","Rodrigo Altamirano (https://github.com/raltamirano)","Jim Buck (https://github.com/JimmyBoh)"],"main":"./lib/index.js","files":["lib"],"scripts":{"test":"nyc --reporter=lcov --reporter=text-summary npm run test:unit","test:unit":"mocha --ignore test/irl-test.js test/*-test.js --timeout 4000","test:irl":"mocha --timeout 16000 test/irl-test.js","lint":"eslint ./","lint:fix":"eslint --fix ./","lint:typings":"tslint typings/index.d.ts","lint:typings:fix":"tslint --fix typings/index.d.ts"},"dependencies":{},"devDependencies":{"@types/node":"^13.1.0","assert-diff":"^3.0.1","dtslint":"^3.6.14","eslint":"^6.8.0","mocha":"^7.0.0","muk-require":"^1.2.0","nock":"^13.0.4","nyc":"^15.0.0"},"engines":{"node":">=10"},"license":"MIT"}'
    );
  },
]);

let indexPlay = -1;
let listYoutube = [];
function setPlaylist(arr) {
  listYoutube = arr;
}
window.nextPlay = function (indexCommand = 1) {
  //   const listYoutube = [
  //     "https://www.youtube.com/watch?v=me7KyKQKj6k",
  //     "https://www.youtube.com/watch?v=cZR2XRbkOr0",
  //     "https://www.youtube.com/watch?v=-GsAwVgUsQw",
  //     "https://www.youtube.com/watch?v=JnfRWwdIZLg",
  //   ];

  if (indexCommand == -1) {
    if (indexPlay < 1) {
      indexPlay = listYoutube.length;
    }
    indexPlay--;
  } else if (indexCommand == 1) {
    indexPlay++;
  }

  const youtubeURL = listYoutube[indexPlay];

  if (typeof youtubeURL !== "string") {
    // Generate a random index between 0 and the length of the array
    const randomIndex = Math.floor(Math.random() * listYoutube.length);

    // Get a random element from the array using the random index
    youtubeURL = listYoutube[randomIndex];
    indexPlay = -1;
    listYoutube = [];
    // window.nextPlay();
    // return;
  }
  getVideoInfo(youtubeURL);
};

// window.nextPlay(1);

function getVideoInfo(youtubeURL) {
  ytdl.getInfo(youtubeURL).then((info) => {
    console.log("------info", info);

    if (typeof info !== "object" || info === null) {
      console.log("fail obj");
      return;
    }

    const videoDetails = info.videoDetails;

    const title = videoDetails.title;
    const uploadDate = videoDetails.uploadDate;
    const thumbnail =
      videoDetails.thumbnails[videoDetails.thumbnails.length - 2].url;

    const ownerChannelName = videoDetails.ownerChannelName;
    const lengthSeconds = videoDetails.lengthSeconds;

    const isLive = videoDetails.isLive || false;

    let audioFormats = ytdl.filterFormats(info.formats, "audioonly");

    console.log("Formats with only audio: " + audioFormats.length);

    if (!Array.isArray(audioFormats)) {
      console.log("fail");
      return;
    }
    const urls = [];
    // let durationLive = 0;
    audioFormats.forEach((url) => {
      // if (typeof url.approxDurationMs === 'number' || typeof url.approxDurationMs === 'string') {
      //   durationLive = url.approxDurationMs * 1;

      //   if (isNaN(durationLive) || durationLive < 1) {
      //     durationLive = 0;
      //   }

      //   durationLive = Math.floor(durationLive / 1000);

      // }

      // chi lay link co am thanh
      if (url.hasAudio === 0 || url.hasVideo === 1) {
        return;
      }
      if (["mp4", "mp3"].indexOf(url.container) < 0) {
        return;
      }
      urls.push(url);
      console.log(url);
    });
    listYoutube =
      listYoutube.length === 0
        ? info.related_videos.map(
            (v) => "https://www.youtube.com/watch?v=" + v.id
          )
        : listYoutube;

    console.log("listYoutube", listYoutube);

    window.webkit.messageHandlers.callbackHandler.postMessage(
      JSON.stringify({
        command: "play-audio",
        videoDetails,
        // urls,
        url: urls[0].url,
        title,
        isLive,
        uploadDate: uploadDate,
        thumbnail: thumbnail,
        owner: ownerChannelName,
        duration: lengthSeconds,
      })
    );
  });
}

var currentLocation = document.location.href;

var lastUrl = location.href;
new MutationObserver(() => {
  const url = location.href;
  if (url !== lastUrl) {
    lastUrl = url;
    onUrlChange();
  }
}).observe(document, { subtree: true, childList: true });

window.callPauseVideo = function () {
  document.querySelectorAll("video").forEach((vid) => {
    vid.muted = true;
    vid.autoplay = false;
    if (vid.paused === true) {
      // clearInterval(window._toCallPause);
      return;
    }
    vid.pause();
    vid.currentTime = vid.duration - 2;
  });
  var player = document.getElementById("player-container-id");
  if (player != null) {
    var node = document.createElement("div");
    node.style =
      "position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;  background-color: transparent";
    player.appendChild(node);
  }
};
window.handlePause = function () {
  window.callPauseVideo();
  window._toCallPause = setInterval(function () {
    window.callPauseVideo();
  }, 300);
};

function onUrlChange() {
  window.handlePause();
  window.webkit.messageHandlers.callbackHandler.postMessage(
    JSON.stringify({
      command: "change-url",
      value: {
        currentLocation,
        url: document.location.href,
      },
    })
  );
}
console.log("ok-loaded-ytdl");
